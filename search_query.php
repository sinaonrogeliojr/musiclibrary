<?php 
include('dbconnection.php');
$search = $_POST["search_index2"];
$sql = mysqli_query($con,"SELECT t1.*,t2.*, concat(t3.`ln`, ' ', t3.`fn`) as name,t1.`id` as s_id FROM tbl_audios t1 
							LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id` 
							LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
							where t1.`active` = 1 and concat(t1.`song_name`,t2.`album_name`,t1.`description`,t1.`keyword`,t1.`composer`,t1.`genre`, t1.`application_id`) like '%$search%'");
$count = mysqli_num_rows($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link href="css/w3.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="style.css">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/function.js"></script>
  <script src="js/audio-index.js"></script>
  <script src="js/sweetalert.min.js"></script>
  <script src="datatables/datatables.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
	function view_search(){
		$("#default").addClass('none');
		$("#view_search").removeClass('none');
		$("#default2").removeClass('col-lg-8');
		$("#default2").addClass('col-lg-5');
		$("#search_index").focus();
	}

	function remove_search(){
		$("#default").removeClass('none');
		$("#view_search").addClass('none');
		$("#default2").addClass('col-lg-8');
	}
</script>
</head>
<body>
<div class="container-fluid hero hero-db">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="index.php"><img class="img-fluid" src="img/logo.png"/></a>
		</div>
		<div class="col-lg-8 col-6 head w3-animate-right" id="default2"><p><img class="img-fluid fence" src="img/profile.png"><span><a href="login.php">LOGIN</a> <span class="fence">|</span> <a href="register.php">REGISTER</a></span></p></div>
		<div class="col-lg-1 col-4" id="default"><a href="#" class="search-icon" onclick="view_search();"><i class="fa fa-search fa-2x"></i></a></div>

		<div class="col-lg-4 col-4 none w3-animate-right" id="view_search">
			<div class="form-group w3-card-2">
	            <div class="input-group">
	            	<form action="search_query.php" class="form-control" method="post">
		                <input type="text" id="search_index" name="search_index2"  autocomplete="off" class="form-control" placeholder="Search...">
		                <span class="input-group-addon input" style="background-color: #6c7989;" onclick="remove_search();"><i class="fa fa-times fa-2x"></i></span>
	            	</form>
	            </div>
        	</div>
		</div>
	</div>   	  	
</div>

<div class="container-fluid results">
	<div class="row">
		<div class="col-lg-12">
			<input type="hidden" id="txt_search" value="<?php echo $_POST["search_index2"]; ?>">
			<p class="title">Search Results: “<?php echo $_POST["search_index2"]; ?>”</p>
			<p>Total: <?php echo $count; ?></p>
		</div>
	</div>
</div>

<div class="container-fluid tracks-table">
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover" id="load_data_table">
				<thead>
					<tr id="labels">
						<th></th>
						<th></th>
						<th>Description</th>
						<th>Genre</th>
						<th>Composer</th>
						<th>Application</th>
						<th>Supporting Files</th>
						<th>Contributor</th>
						<th>Action</th>
						<th></th>
					</tr>
				</thead>
				<tbody >
						<?php 
						$song_id = '';
						
						$sql = mysqli_query($con, "SELECT t1.*,t2.*, concat(t3.`ln`, ' ', t3.`fn`) as name,t1.`id` as s_id FROM tbl_audios t1 
							LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id` 
							LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
							where t1.`active` = 1 and concat(t1.`song_name`,t2.`album_name`,t1.`description`,t1.`keyword`,t1.`composer`,t1.`genre`, t1.`application_id`) like '%$search%' LIMIT 5");

						if (mysqli_num_rows($sql)>0) {

							while($row = mysqli_fetch_array($sql)){ 
							if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
								$img = 'img/slogo3.png';
							}
							else
							{
								$img = $row['album_artwork'];
							}

						?>
						<tr class="tracks">
							<td></td>
							<td class="play">
							    <img class="imgs img-responsive" src="<?php echo $img; ?>" alt="">
							</td>
							<td class="left" style="width: 20%;"> 
								<a href="#" ><h6 style=" margin-left: 1%; margin-top: 1%;"><?php echo $row['song_name']; ?></h6></a>
								<a href="#"><p style=" margin-left: 1%; margin-top: 1%;"><?php echo $row['album_name']; ?></p></a>
							</td>
							<td class="text-left">
								<a href="#"><p class="db-content"><?php echo $row['genre']; ?></p></a>
							</td>
							<td class="text-left"><a href="#"><p class="db-content"><?php echo $row['composer']; ?></p></a></h6></a></td>
							<td class="text-left"><a href="#"><p class="db-content"><?php echo $row['application_id']; ?></p></a></h6></a></td>
							<td class="text-left"><a href="#"><a href="#"><p class="db-content">NONE</p></a></td>
							<td class="text-left"><a href="#"><a href="#"><p class="db-content"><?php echo $row['name']; ?></p></a></td>
							<td>
								<div class="btn-group btn-justified">
									<button class="btn btn-info"><span class="fa fa-download"></span></button>
									<button class="btn btn-info"><span class="fa fa-plus"></span></button>
									<button class="btn btn-info"><span class="fa fa-play-circle"></span></button>
									<button class="btn btn-info"><span class="fa fa-eye"></span></button>
								</div>
							</td>
							<td></td>
							</tr>
						<?php
						 $song_id = $row["s_id"];  
						}
						}else{ ?>

							<tr>  
				                  <td colspan="10">No Data Found...</td>  
				             </tr>

						<?php
						}
						?>
				</tbody>
				<tr id="remove_row">  
	                  <td colspan="10"><button type="button" name="btn_more" data-vid="<?php echo $song_id; ?>" id="btn_more" class="btn btn-success form-control">SEE MORE</button></td>  
	             </tr>  
			</table>
		</div>
	</div>
</div>

<div class="container-fluid cta cta-db">
	<div class="row">
		<div class="col-lg-6">
			
		</div>

		<div class="col-lg-6 content">
			<h1>Share with <br>a Friend</h1>
			<p>Send a link to a friend who is also part of RFI.</p>
			<a href="#"><button type="button" class="btn btn-dark">SUBMIT</button></a>
		</div>
	</div>
</div>

<footer class="container-fluid">
	<div class="row">
		<div class="col-lg-12 content">
			<h1><span><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCIgdmlld0JveD0iMCAwIDQ4NS4yMTEgNDg1LjIxMSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDg1LjIxMSA0ODUuMjExOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTQ4NS4yMTEsMzYzLjkwNmMwLDEwLjYzNy0yLjk5MiwyMC40OTgtNy43ODUsMjkuMTc0TDMyNC4yMjUsMjIxLjY3bDE1MS41NC0xMzIuNTg0ICAgYzUuODk1LDkuMzU1LDkuNDQ2LDIwLjM0NCw5LjQ0NiwzMi4yMTlWMzYzLjkwNnogTTI0Mi42MDYsMjUyLjc5M2wyMTAuODYzLTE4NC41Yy04LjY1My00LjczNy0xOC4zOTctNy42NDItMjguOTA4LTcuNjQySDYwLjY1MSAgIGMtMTAuNTI0LDAtMjAuMjcxLDIuOTA1LTI4Ljg4OSw3LjY0MkwyNDIuNjA2LDI1Mi43OTN6IE0zMDEuMzkzLDI0MS42MzFsLTQ4LjgwOSw0Mi43MzRjLTIuODU1LDIuNDg3LTYuNDEsMy43MjktOS45NzgsMy43MjkgICBjLTMuNTcsMC03LjEyNS0xLjI0Mi05Ljk4LTMuNzI5bC00OC44Mi00Mi43MzZMMjguNjY3LDQxNS4yM2M5LjI5OSw1LjgzNCwyMC4xOTcsOS4zMjksMzEuOTgzLDkuMzI5aDM2My45MTEgICBjMTEuNzg0LDAsMjIuNjg3LTMuNDk1LDMxLjk4My05LjMyOUwzMDEuMzkzLDI0MS42MzF6IE05LjQ0OCw4OS4wODVDMy41NTQsOTguNDQsMCwxMDkuNDI5LDAsMTIxLjMwNXYyNDIuNjAyICAgYzAsMTAuNjM3LDIuOTc4LDIwLjQ5OCw3Ljc4OSwyOS4xNzRsMTUzLjE4My0xNzEuNDRMOS40NDgsODkuMDg1eiIgZmlsbD0iIzk3YTBhMiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /></span>GET UPDATES</h1>

			<p>Click here to enable monthly updates of new music that has been added</p>

			<div class="container subscribe">
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 input">
						<div class="form-group">
							<input type="text" class="form-control" id="name" placeholder="Name" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="email_address" placeholder="Email Address" autocomplete="off">
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="g-recaptcha" data-sitekey="6Lc-L3IUAAAAALYHDZRRFXVzf7mPwzzatwayc7IV" data-callback="callback"></div>	
							</div>
						<div class="col-lg-6">
							<button type="button" style="margin-top: 0px;" class="btn" id="stay_updated" disabled="" type="submit" onclick="subscribe_now();">STAY UPDATED</button>	
						</div>
						</div>					
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
		</div>
	</div>

	<?php include('footer.php'); ?>
</footer>
</body>
</html>

<script>  
$(document).ready(function(){  

      $(document).on('click', '#btn_more', function(){  
      		
           var last_video_id = $(this).data("vid");  
           var search = document.getElementById('txt_search');
           var txt_search = search.value;
           $('#btn_more').html("Loading...");  
           $.ajax({  
                url:"load_more_search.php",  
                method:"POST",  
                data:{last_video_id:last_video_id, txt_search:txt_search},  
                dataType:"text",  
                success:function(data)  
                {  
                     if(data != '')  
                     {    
                          $('#remove_row').remove();  
                          $('#load_data_table').append(data); 
                          counter();
                       	  
                     }  
                     else  
                     {  

                          $('#btn_more').html("No Data");  
                     }  
                }  
           }); 
           
      });  
 });  

 function callback(){
    console.log("The user has already solved the captcha, now you can submit your form.");
    document.getElementById('stay_updated').disabled= false;
  }
 </script>
