var url = 'function.php';

function save_plist(){
  var mydata,playlist_name,play_id;
  play_id = document.getElementById('play_id');
  playlist_name = document.getElementById('p_name');

  //alert(playlist_name.value);
  if(playlist_name.value == ''){
    $('#p_name').focus();
  }else{
    mydata = 'action=save_plist' + '&play_id=' + play_id.value + '&p_name=' + playlist_name.value;
    //alert(mydata);
    $.ajax({
      type:'POST',
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $('#btn-playlist').html("<span class='fa fa-save fa-spin'></span> Saving...");
      },   
      success:function(data)
      {
        if(data == 1){
          $('#btn-playlist').html("<span class='fa fa-save'></span> Save");
          $('#p_name').val('');
          $('#play_id').val('');
          $('#new_playlist').modal('hide');
          music_list();
        }else if(data == 2){
          $('#btn-playlist').html("<span class='fa fa-save'></span> Save");
          $('#p_name').val('');
          $('#play_id').val('');
          $('#new_playlist').modal('hide');
          music_list();
          $('#p_title').html('<span class="fa fa-plus"></span> New Playlist');
        }
        else{
          alert(data);
        }
      }
    })


  }


}

function click_form() {
    document.getElementById("album_click").submit();
}

function view_downloads(){
  $(".show").removeClass('hide');
}
function view_downloads2(){
  $(".show2").removeClass('hide');
}

function count_downloads(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=count_downloads',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#total_downloads").html('<span class="fa fa-download fa-lg"></span> Download List' + ' <span class="badge badge-success badge-pill">'+data+'</span>');
        $("#total_downloads2").html('<span class="fa fa-download fa-lg"></span> Download List' + ' <span class="badge badge-success badge-pill">'+data+'</span>');
        },500);

    }
  });
}

function click_all(){
var buttons = document.getElementsByName('download_');
for(var i = 0; i <= buttons.length; i++)  
   buttons[i].click();
    show_download_list();
    songs_list();
    songs_list();
    count_downloads();
}

function scroll2(){
  $(document).ready(function(){
  $("body").on('scroll',function(){
  if (this.scrollTop > 390 ) 
  {
  $("#menu-nav").addClass('navbar-dark bg-dark w3-animate-opacity w3-card-4');
  }
  else
  {
  $("#menu-nav").removeClass('navbar-dark bg-dark w3-animate-opacity w3-card-4');
  $("#logo_nav").show();
  }
  });
  });

  $(document).ready(function(){
  // Add smooth scrolling to all links
  $(".nav-a").on('click', function(event) {

  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {
  // Prevent default anchor click  behavior
  event.preventDefault();

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 800, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
  });
  } // End if
  });
  });
  }

function notify_me(){
var mydata ='action=notify_me';
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
refresh_account();
new_notif();
show_notif();
//swal("MUSIC LIBRARY!", "Notification is turn on!", "info");
}
else
{
alert(data);
}
}
});

}

function upload_picture(){
  $(document).ready(function(){
  $(document).on('change', '#playlist_image', function(){
    var name = document.getElementById("playlist_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
    {
    alert("Invalid File");
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("playlist_image").files[0]);
    var f = document.getElementById("playlist_image").files[0];
    var fsize = f.size||f.fileSize;
    if(fsize > 2000000)
    {
    alert("File Size is very big");
    }
    else
    {
    form_data.append("playlist_image", document.getElementById('playlist_image').files[0]);
    $.ajax({
    url:"upload.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
    $('#image_preview').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
    $('#btn-chooser').removeClass('btn btn-primary');
    $('#image_preview').html(data);
    document.getElementById('playlist_image').disabled= true;
    $('#btn-chooser').addClass('btn btn-secondary');
    }
    });
    }
    });
    });
    } 

function not_notify_me(){
var mydata ='action=not_notify_me';
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
refresh_account();
new_notif();
show_notif();
//swal("MUSIC LIBRARY!", "Notification is turn off!", "info");
}
else
{
alert(data);
}
}
});

} 

function enable_tooltips(){
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
    $('[data-toggle="tooltip"]').tooltip(); 
});
}

function fullscreen(){
  $("#view_playlist_music").addClass("modal-fullscreen  footer-to-bottom");
  $("#playlist_modal").addClass("scroll_full");
  $("#full").hide('fast');
  $("#actual").show('fast');
}

function actual_size(){
  $("#view_playlist_music").removeClass("modal-fullscreen  footer-to-bottom ");
  $("#playlist_modal").removeClass("scroll_full");
  //$("#playlist_modal").addClass("overflow-bottom");
  $("#actual").hide('fast');
  $("#full").show('fast');
}

function playAudio(aud,name) { 
    $("#myAudio").html('<audio controls controlsList="nodownload noremoteplayback" style="background-color:#5a6a7a;" id="my_Audio" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
    $("#s_name").val(name);
    $("#now_play").show('fast');
    $("#player").show('fast');
    //document.getElementById(id).style.backgroundColor = "#A9A9A9";
} 
function pauseAudio(aud) { 
    $("#myAudio").html('<audio controls controlsList="nodownload noremoteplayback" style="background-color:#5a6a7a;" id="my_Audio" class="col-sm-12" autopause="" src="'+aud+' "></audio> ');
} 

function volup(aud){
	//alert('up')
	var volup = document.getElementById('my_Audio');
	volup.volume += 0.1;
}
function voldown(aud){
	//alert('down')
	var voldown = document.getElementById('my_Audio');
	voldown.volume -= 0.1;
}

function play_(aud){
	//alert('down')
	var voldown = document.getElementById('my_Audio');
	var now_play = document.getElementById('now_play');
	voldown.play();
	now_play.start();
}

function pause_(aud){
	//alert('down')
	var voldown = document.getElementById('my_Audio');
	var now_play = document.getElementById('now_play');
	voldown.pause();
	now_play.stop();
}

function clear_music(){
	$("#myAudio").html('');
	$("#now_play").hide('fast');
  $("#player").hide('fast');
}

function select_all_music() {

  $("#sampleTbl tbody td:nth-child(7)").each(function (index) {
    //alert(id);
     var id = $(this).html().trim('');
     playlist = document.getElementById('play_id2');
     //alert(id);
     var mydata ='action=save_all_music' + '&id=' + id + '&play_id2=' + playlist.value; 
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    load_audio2();
    load_audio2_mobile();
    p_music_list();
    p_music_list_2();
    show_playlist();
    total_duration();
    total_songs();
    }
    else if(data == 404){
      swal("Error", "Song(s) is/are already exist in List!", "error");
    }
    else
    {
    alert(data);
    }
    }
    });

  });
  // body...
}


/*$("#download_all").click(function () {
  $("#table_download_all tbody td:nth-child(3)").each(function (index) {
     var id = $(this).html().trim('');
     alert(id);
  });
}); */

//diplay list of music
function load_audio(){

var genres = document.getElementById('genres');
var playlist2 = document.getElementById('playlist2');

var mydata = 'genres=' + genres.value + '&playlist2=' + playlist2.value;

$.ajax({
type: "POST",
url:"display_audio.php",
data:mydata,
cache: false,
beforeSend:function(){
$("#display_audio").html('<br><center> <img src="../img/giphy.gif" width="100px"> </i></center>');
},
success: function (data){
// alert(data);
$("#display_audio").html(data);

}
});
}


function submit_album()
{
  var album_image = $('#album_image').val(); 
  var album_name = $('#album_name').val();
  var extension = $('#album_image').val().split('.').pop().toLowerCase();
  var link = $("#link").val();   
  
  if(album_name == '')  
  {  
    swal("Oops","Please Input Album Name","warning"); 
    return false;  
  } 
  else if(album_image == '')  
  {  
    swal("Oops","Please Select Image","warning"); 
    return false;  
  }  
  else if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)  
  {  
    swal("Error","Invalid Image File","error"); 
    $('#album_image').val('');  
    return false;  
  }
  else 
  {
    var mydata = 'link=' + link + '&album_name=' + album_name;
    $.ajax({
      type:"POST",
      url:"save_album.php",
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data);
        if (data == 1) 
        {
          swal("Success","Album Created","success"); 
          show_album();
          $('#newAlbumModal').modal('hide');
          $('#album_name').val('');
          save_able();
          mymusic();
        }
      }
    });
  }  
}

/*function insert_audio(){

var id = [];
var playlist = document.getElementById('playlist');
$(':checkbox:checked').each(function(i){
id[i] = $(this).val();
});

if(id.length === 0) //tell you if the array is empty
{
alert("Please Select atleast one checkbox");
}
else
{
var mydata = 'id=' + id.value + '&playlist=' + playlist.value;

$.ajax({
url:'save_song.php',
method:'POST',
data:mydata,
success:function()
{
swal("Success","Successfully Added to your playlist.","success");
$('#addmusic').modal('hide');
$('#display_audio').html('');
$('#genres').val('Genres');
pager('show_playlist');
}

});
}

} */

//pick song
function pick_song(){
var playlist = document.getElementById('playlist');
var id = document.getElementById('p_id');

var mydata = 'p_id=' + id.value + '&playlist=' + playlist.value; 

$.ajax({
url:'save_song.php',
method:'POST',
data:mydata,
success:function(data)
{
//pager('show_playlist');
}

});
}

$(document).ready(function(){  
$("#now_play").hide('fast');
$("#player").hide('fast');
$("#actual").hide('fast');

$('#chk_all').click(function(event) {   
if(this.checked) {
// Iterate each checkbox
$(':checkbox').each(function() {
this.checked = true;                        
});
} else {
$(':checkbox').each(function() {
this.checked = false;                       
});
}
});

$('#addmusic').on('hidden.bs.modal', function () {

$("#genres").val('Genres');
$("#playlist2").val('Playlist');
$("#display_audio").html('');

})

$('#view_playlist_music').on('hidden.bs.modal', function () {
$("#myAudio").html('');
$("#player").html('');
$("#display_musics").html('');

}) 

$('#p_addmusic').on('hidden.bs.modal', function () {
$('#display_audio2').html('');
$('#genres2').val(0);
$('#search_song2').val('');

});


$('.stick_').on('click', function(event){
    // The event won't be propagated up to the document NODE and 
    // therefore delegated events won't be fired
    event.stopPropagation();
});


});

function delete_my_music(index){
var mydata ='action=del_music'+'&id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to delete this Music?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
total_duration();
total_songs();
swal("Deleted!", "Music has been deleted !", "success");
show_playlist();

}
else
{
alert(data);
}
}
});
});
}  

function delete_all_my_music(index){
var mydata ='action=del_music'+'&id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to delete this Music?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
total_duration();
total_songs();
swal("Deleted!", "Music has been deleted !", "success");
show_playlist();

}
else
{
alert(data);
}
}
});
});
}  


//show playlist
function show_playlist(){
$.ajax({
type:"POST",
url:"show_playlist.php",
data:"",
cache:false,
success:function(data){
$("#display_playlist").html(data);
}
});
}

var start,limit;
start = 0;
limit = 6;

function music_list(){
var search1 = $("#search1").val();
var mydata = 'action=show_plist' + '&search1=' + search1;
// alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#display_playlist").html('<tr class="text-center"><td colspan="5"><img src="../img/load.gif" class="img-fluid" width="30"></td></tr>'); },
success:function(data){
setTimeout(function(){$("#display_playlist").html(data);},500); 
}
}); 
}

function next_record(){
  start = limit;
  limit = limit + 6;
  music_list();
  console.log(start +' '+ limit);
}

function prev_record(){
  if (start <= 0) 
  {
  start = 0;
  limit = 6;
  music_list();
  console.log(start +' '+ limit);
  }
  else
  {
  start = start - 6;
  limit = limit - 6;
  music_list();
  console.log(start +' '+ limit);
  }
  
}


function delete_plist(index){
var mydata ='action=del_plist'+'&id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to delete this Playlist?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Deleted!", "Playlist has been deleted !", "success");
music_list();
}
else
{
alert(data);
}
}
});
});
}


function delete_all_plist(index){
var mydata ='action=delete_all_plist' + '&user_id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to delete all your playlist?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Deleted!", "All Playlist is deleted!", "success");
show_playlist();
}
else
{
alert(data);
}
}
});
});
}


function delete_all_download(index){
var mydata ='action=delete_all_download' + '&user_id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to clear your download list?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Deleted!", "Download list is empty", "success");
show_download_list();
count_downloads();
songs_list();
}
else
{
alert(data);
}
}
});
});
}

function delete_download(index){
var mydata ='action=delete_download' + '&audio_id=' + index;
swal({
title: "Are you sure?",
text: "Do you want to remove this song to your download list?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Deleted!", "Song Successfully Removed", "success");
show_download_list();
songs_list();
count_downloads();
}
else
{
alert(data);
}
}
});
});
}

function edit_playlist(id,name){
$('#p_title').html('<span class="fa fa-edit"></span> Update Playlist');
$("#new_playlist").modal({'backdrop':'static'});
$("#p_name").val(name);
$("#play_id").val(id);
$("#p_name").focus();   
}

function update_playlist(){
var u_playlist, u_id;
u_playlist = document.getElementById('u_playlist');
u_id = document.getElementById('u_id');

if (u_playlist.value == "") 
{
$('#u_playlist').focus();
}
else
{
var mydata = 'action=update_plist' + '&u_id=' + u_id.value + '&u_playlist=' + u_playlist.value;
// alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
// alert(data);
if (data == 1) {
music_list();
$('#update_playlist').modal('hide');
//swal("Success","Playlist Successfully Updated!","success");
}
}
});
}
}

//insert song to playlist
function save_song(index){  
playlist = document.getElementById('playlist2');

if(playlist.value === "Playlist"){
swal("Music Library","Please Select Playlist!","info");
}else{
var mydata ='action=save_song'+'&id=' + index + '&playlist2=' + playlist.value; 
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
show_playlist();
total_duration();
total_songs();
}
else if(data == 404){
  swal("Error", "This Song is already exist in List!", "error");
}
else
{
alert(data);
}
}
});
}

}

function total_duration(){
  var playlist_id = document.getElementById('v_pid');
  var mydata = 'action=total_duration' + '&v_pid=' + v_pid.value;

  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#total_time_duration").html('<span class="fa fa-refresh fa-spin"></span>');},
  success:function(data){
  setTimeout(function(){$("#total_time_duration").html(data);},800); 
  }
  }); 

}

function total_songs(){
  var playlist_id = document.getElementById('v_pid');
  var mydata = 'action=total_songs' + '&v_pid=' + v_pid.value;

  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#total_songs").html('<span class="fa fa-refresh fa-spin"></span>');},
  success:function(data){
  setTimeout(function(){$("#total_songs").html(data);},800); 
  }
  }); 

}

function total_album_songs(){
  var album_id = document.getElementById('album_id');
  var mydata = 'action=total_album_songs' + '&album_id=' + album_id.value;

  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#total_album_songs").html('<span class="fa fa-refresh fa-spin"></span>');},
  success:function(data){
  setTimeout(function(){$("#total_album_songs").html(data);},800); 
  }
  }); 

}

function show_profile(){
  document.getElementById("setting_overlay").style.width = "100%";
  refresh_account();
  load_pictures();
}



function get_p_id(id,name,img){
  //alert(img);
//$("#view_playlist_music").modal({'backdrop':true});
//$("#view_playlist_music").modal("show");

$("#v_pid").val(id);
$("#v_name").html(name);
$("#playlist_image").html('<img src="../img/img.png" class="img-album img-fluid w3-round animated jello" style="background-image: url(\''+img+'\');" width="230">');
p_music_list();
}

//WEBSITE PLAYLIST
function p_music_list(){
      var search3 = $("#search_song").val();
      var v_pid = document.getElementById('v_pid');
      var mydata = 'action=show_music_playlist' + '&search_song=' + search3 + '&v_pid=' + v_pid.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#display_musics").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#display_musics").html(data);},800); 
      }
      }); 
      }

//MOBILE PLAYLIST
function p_music_list_2(){
      var search3 = $("#search_song_mobile").val();
      var mydata = 'action=show_music_playlist_mobile' + '&search_song_mobile=' + search3 + '&v_pid=' + v_pid.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#display_musics").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#display_musics_mobile").html(data);},500); 
      }
      }); 
      }

function add_new_songs(){
//$("#p_addmusic").modal({'backdrop':'static'});
document.getElementById("music_overlay").style.width = "100%";
var playlist_ = document.getElementById('v_pid');
$("#play_id2").val(playlist_.value);
load_audio2();

}

//WEBSITE
function load_audio2(){

var genres = document.getElementById('genres2');
var search_audio = document.getElementById('search_audio');
var playlist = document.getElementById('v_pid');
var mydata = 'action=load_audio2' + '&genres2=' + genres.value + '&search_audio=' + search_audio.value + '&v_pid=' + playlist.value;
// alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
//beforeSend:function(){$("#display_audio2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#display_audio2").html(data);},500); 
}
}); 
}

//MOBILE
function load_audio2_mobile(){
var genres = document.getElementById('genres2_mobile');
var search_audio = document.getElementById('search_audio_mobile');
var playlist = document.getElementById('play_id2');
var mydata = 'action=load_audio2_mobile' + '&genres2_mobile=' + genres.value + '&search_audio_mobile=' + search_audio.value + '&play_id2=' + playlist.value;
// alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
//beforeSend:function(){$("#display_audio2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#display_audio2_mobile").html(data);},500); 
}
}); 
}




function save_song1(id){ 

playlist = document.getElementById('v_pid');

var mydata ='action=save_song1' + '&id=' + id + '&v_pid=' + playlist.value; 
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
p_music_list();
show_playlist();
total_duration();
total_songs();
load_audio2();
}
else if(data == 404){
  swal("Error", "This Song is already exist in List!", "error");
}
else
{
alert(data);
}
}
});

}

function save_song2(index){ 

playlist = document.getElementById('playlist2');

if(playlist.value =='Playlist'){
swal("Music Library", "Please select your playlist", "info");
}else{
  var mydata ='action=save_song2' + '&id=' + index + '&playlist2=' + playlist.value; 
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  success:function(data){
  if (data == 1) 
  {
  p_music_list();
  show_playlist();
  }
  else if(data == 404){
    swal("Error", "This Song is already exist in List!", "error");
  }
  else
  {
  alert(data);
  }
  }
  });

}

}

function clear_songs(index){
var playlist_ = document.getElementById('v_pid');
var mydata ='action=clear_songs' + '&v_pid=' + playlist_.value;
swal({
title: "Are you sure?",
text: "Do you want to remove all music in your Playlist?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},

function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Deleted!", "All Songs is deleted!", "success");
p_music_list();
p_music_list_2();
//remove_player();
clear_music();
show_playlist();
total_duration();
total_songs();
}
else
{
alert(data);
}
}
});
});
}

function del_music(id){
var mydata ='action=delete1_music' + '&t_id=' + id;
swal({
title: "Are you sure?",
text: "Do you want to remove this song to your playlist?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Music Library!", "Music Removed!", "success");
p_music_list();
p_music_list_2();
show_playlist();
total_duration();
total_songs();
}
else
{
alert(data);
}
}
});
});
}

function refresh_account(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=refresh_account',
    cache:false,
    beforeSend:function(){
      $("#my_data").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#my_data").html(data);},900);
    }
  });
} 

 function load_pictures(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=update_picture',
      cache:false,
      success:function(data){
        //alert(data);
        // alert(data);
        $("#img_pf").html('<img src='+data.trim()+' class="imgs img-responsive w3-circle animated bounceIn" alt="">');
        $("#img_pf_pic").html('<img src="../img/img.png" style="background-image: url('+data.trim()+');" id="logo_img" class="ad img-album w3-circle animated bounceIn">');
        $("#profile_pic").html('<img src='+data.trim()+' class="profile_pic img-responsive animated bounceIn" alt="">');

      }
    });
  }

function update_name(fn,ln,id){
  $("#choose").hide('fast');
  $('#fn').val(fn);
  $('#ln').val(ln);
  $('#id').val(id);
  $("#form-names").show('fast');
}

function name(fn,ln,id){
  if (fn == "") 
  {
    $('#fn').focus();
  }
  else if (ln == "") 
  {
    $('#ln').focus();
  }
  else
  {
  var mydata = 'action=update_account' + '&fn=' + fn + '&ln=' + ln + '&id=' + id;
  // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
      $('#edit_name_modal').modal('hide');
      swal("Success","Your Name has been changed!","success");
      
    }
    }
  });
  }
}

function email_add(email){
  if (email == "") 
  {
    $('#email').focus();
  }
  else
  {
  var mydata = 'action=update_email' + '&email=' + email;
   // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
      setTimeout(function(){cancel_updates();},1200);
    }
    }
  });
  }
}

function update_email(emails){
  $('#email').val(emails);
  $("#form-email").show('fast');
  $("#choose").hide('fast');
}

function cancel_updates(){
  $('#fn').val('');
  $('#mn').val('');
  $('#ln').val('');
  $('#id').val('');
  $('#bdate').val('');
  $('#email').val('');
  $('#username').val('');
  $("#o_pwd").val('');
  $("#n_pwd").val('');
  $("#c_pwd").val('');
  $("#old").val('');
  $("#form-names").hide('fast');
  $("#form-pwd").hide('fast');
  $("#form-bd").hide('fast');
  $("#form-email").hide('fast');
  $("#form-username").hide('fast');
  $("#choose").show('fast');
}


function update_pwd(index){
  $("#old").val(index);
  $("#form-pwd").show('fast');
  $("#choose").hide('fast');
  refresh_account();
}

function change_pass(o_pwd,c_pwd,n_pwd,old){
  if (o_pwd == "") 
  {
    $("#o_pwd").focus();
  }
  else if (o_pwd != old) 
  {
    swal("Sorry","Password Doesn\'t Match !","error");
    $("#o_pwd").focus();
  }
  else if (n_pwd == "") 
  {
    $("#n_pwd").focus();
  }
  else if (c_pwd == "") 
  {
    $("#c_pwd").focus();
  }
  else if (c_pwd != n_pwd) 
  {
    swal("Sorry","Please confirm your password!","error");
    $("#c_pwd").focus();
  }
  else
  {
    var mydata = 'action=update_pwd' + '&pwd=' + n_pwd;
 // alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      // alert(data);
      if (data == 1) {
        // alert(data);
        $("#o_pwd").val('');
        $("#c_pwd").val('');
        $("#n_pwd").val('');
        refresh_account();
        swal("Success","Password has been changed!","success");
      }
      }
    });
  }
  
}

function update_user(index){
  var fn,mn,ln,id,bdate,email;
  if (index == 'name') 
  {
  fn = $('#fn').val();
  ln = $('#ln').val();
  id =$('#id').val();
  name(fn,mn,ln,id);
  }
  else if(index == 'bdate')
  {
  bdate = $("#bdate").val();
  bdates(bdate);
  }
  else if (index == 'email') 
  {
  email = $("#email").val();
  email_add(email);
  }
  else if (index == 'username') 
  {
  var username = $("#username").val();
  usernames(username);
  }
  else if (index == 'pwd') 
  {
  var o_pwd,n_pwd,c_pwd,old;
  o_pwd = $("#o_pwd").val();
  n_pwd = $("#n_pwd").val();
  c_pwd = $("#c_pwd").val();
  old = $("#old").val();
  change_pass(o_pwd,c_pwd,n_pwd,old);
  }
}

function show_musics(){
  $.ajax({
    type:"POST",
    url:"show_musics.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_songs").html(data);
    $("#search3").show();
    }
    });
    }

/*$(document).ready(function(){  
      $(document).on('click', '.column_sort', function(){  
           var column_name = $(this).attr("id");  
           var order = $(this).data("order");  
           var arrow = '';  
           var search3 = $("#search3").val();
          var mydata = 'action=showsongs' + '&search3=' + search3 ;
           //glyphicon glyphicon-arrow-up  
           //glyphicon glyphicon-arrow-down  
           if(order == 'desc')  
           {  
                arrow = '&nbsp;<span class="glyphicon glyphicon-arrow-down"></span>';  
           }  
           else  
           {  
                arrow = '&nbsp;<span class="glyphicon glyphicon-arrow-up"></span>';  
           }  
           $.ajax({  
                url:url,  
                method:"POST",  
                cache:false,
                data:{mydata:mydata,column_name:column_name, order:order},  
                success:function(data)  
                {  
                     $('#music_data').html(data);  
                     $('#'+column_name+'').append(arrow);  
                }  
           })  
      });  
 });*/

function songs_list(){
      var search3 = $("#search3").val();
      var mydata = 'action=showsongs' + '&search3=' + search3;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_songs").html('<div class="col-sm-12 text-center"><img src="../img/giphy.gif" class="img-fluid" width="30"></div>'); },
      success:function(data){
      setTimeout(function(){$("#tbl_songs").html(data);},500); 
      }
      }); 
      }

function songs_list_mobile(){
      var search3 = $("#search3_mobile").val();
      var order = document.getElementById('order_music_mobile');  
      var title_name = document.getElementById('title_name_music_mobile');  
      var mydata = 'action=showsongs_mobile' + '&search3_mobile=' + search3 + '&order_music_mobile=' + order.value + '&title_name_music_mobile=' + title_name.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_songs").html('<div class="col-sm-12 text-center"><img src="../img/giphy.gif" class="img-fluid" width="30"></div>'); },
      success:function(data){
      setTimeout(function(){$("#tbl_songs_mobile").html(data);},500); 
      }
      }); 
      }


function download_music(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc2');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc2=' + current_loc.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
    show_download_list();
    songs_list();
    count_downloads();
      swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function add_download(audio_id){

  var mydata = 'action=add_download' + '&id=' + audio_id;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      count_downloads();
      songs_list();
      p_music_list();
      show_download_list();
      //swal("Music Library","Music Added to your Download List","success");
    }
    else if(data == 404){
      swal("Error", "This Song is already exist in List!", "error");
    }
    else
    {
      swal("Opps","Error On Adding Music","error");
    }
    }
  });
}

function add_download_album(audio_id){

  var mydata = 'action=add_download_album' + '&id=' + audio_id;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      album_musics();
    }
    else if(data == 404){
      swal("Error", "This Song is already exist in List!", "error");
    }
    else
    {
      swal("Opps","Error On Adding Music","error");
    }
    }
  });
}

function show_download_list(){
      var search_dl =  document.getElementById('search_dl');
      var mydata = 'action=show_download_list' + '&search_dl=' + search_dl.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_download_list").html('<tr class="text-center"><td colspan="10"><img src="../img/load.gif" class="img-fluid" width="30"></td></tr>'); },
      success:function(data){
      setTimeout(function(){$("#tbl_download_list").html(data);},800); 
      }
      }); 
      }


function show_contributors(){
      var search_c =  document.getElementById('search_c');
      var mydata = 'action=show_contributor' + '&search_c=' + search_c.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_contributors").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_contributors").html(data);},800); 
      }
      }); 
      }

function subscribe(user_id){

  var mydata = 'action=subscribe' + '&user_id=' + user_id;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      //swal("Music Library","Contributor added to your subscriptions","success");
      $("#contri_info_modal").modal('hide');
      show_contributors();
    }
    else
    {
      swal("Opps","Error on subscribing","error");
    }
    }
  });

}

function contri_info(id){
$("#c_id").val(id);
load_contri_info();
$("#contri_info_modal").modal({'backdrop':'static'});
}

function load_contri_info(){
  var c_id = document.getElementById('c_id');
  var mydata = 'action=contri_details' + '&c_id=' + c_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#contri_info").html('<tr colspan="3"><td><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center></td></tr>');},
  success:function(data){
  setTimeout(function(){$("#contri_info").html(data);},800); 
  }
  }); 
}

function subs_info(id){
$("#subs_id").val(id);
load_subs_info();
$("#subs_info_modal").modal({'backdrop':'static'});
}


function load_subs_info(){
  var subs_id = document.getElementById('subs_id');
  var mydata = 'action=subscription_details' + '&subs_id=' + subs_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  //beforeSend:function(){$("#subs_info").html('<tr colspan="3"><td><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center></td></tr>');},
  success:function(data){
  setTimeout(function(){$("#subs_info").html(data);},800); 
  }
  }); 
}

function get_notified(id){

  var mydata = 'action=get_notify' + '&id=' + id;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      load_subs_info();
      subscriptions();
      //swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error 404","error");
    }
    }
  });

}

function not_notify(id){

  var mydata = 'action=not_notify' + '&id=' + id;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      load_subs_info();
      subscriptions();
      //swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error 404","error");
    }
    }
  });
}

function new_notif(){
var mydata = 'action=new_notif';
// alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
setTimeout(function(){$(".notif").html(data);},1000); 
}
}); 
}


function reload(){
    //setInterval(function(){ new_notif(); }, 3000);
}

function show_notif(){
  var mydata = 'action=show_notif';
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#show_notifs").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');},
  success:function(data){

  setTimeout(function(){$("#show_notifs").html(data);},800); 

  }
  }); 
}

function hide_notif(){
  //$('.notif').hide('fast');
}
 
function contributors_list(){
      var search5 = $("#search5").val();
      var mydata = 'action=show_music_contributor' + '&search5=' + search5;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_contributors").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_contributors").html(data);},800); 
      }
      }); 
      }

function topic_list(){
      var search4 = $("#search4").val();
      var mydata = 'action=showtopics' + '&search4=' + search4;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_genres").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_genres").html(data);},800); 
      }
      }); 
      }

function composer_list(){
      var search = $("#search_composer").val();
      var mydata = 'action=show_composers' + '&search_composer=' + search;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_composers").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_composers").html(data);},800); 
      }
      }); 
      }

function description_list(){
  var search = $("#search_desc").val();
  var mydata = 'action=show_descriptions' + '&search_desc=' + search;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#tbl_descriptions").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
  success:function(data){
  setTimeout(function(){$("#tbl_descriptions").html(data);},800); 
  }
  }); 
  }
function music_info(id){
$("#music_id").val(id);
load_music_info();
$("#music_info_modal").modal({'backdrop':'static'});
}
function load_music_info(){
  var music_id = document.getElementById('music_id');
  var mydata = 'action=music_details' + '&music_id=' + music_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#music_info").html('<img src="../img/giphy.gif" class="img-fluid" width="30">');},
  success:function(data){
  setTimeout(function(){$("#music_info").html(data);},800); 
  }
  }); 
}

function add_downloadlist(){
  var search = $("#search_download").val();
  var mydata = 'action=add_downloadlist' + '&search_download=' + search;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  //beforeSend:function(){$("#addtodownload").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
  success:function(data){
  setTimeout(function(){$("#addtodownload").html(data);},800); 
  }
  }); 
  }

function subscriptions(){
      var search =  document.getElementById('search_subs');
      var mydata = 'action=subscriptions' + '&search_subs=' + search.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_subscriptions").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_subscriptions").html(data);},800); 
      }
      }); 
      }

function unsubscribe(index){
var mydata ='action=unsubscribe'+'&id=' + index;
swal({
title: "Are you sure?",
text: "You want to unsubscribe this contributor?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "UNSUBCRIBED",
closeOnConfirm: true
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
$("#subs_info_modal").modal('hide');
subscriptions();
}
else
{
alert(data);
}
}
});
});
}  

function uploading(){
  $(document).ready(function(){
  $(document).on('change', '#user_image', function(){
    var name = document.getElementById("user_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("user_image").files[0]);
  var f = document.getElementById("user_image").files[0];
  var fsize = f.size||f.fileSize;

      if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
      swal("Sorry","Invalid file format !","error");
        }
      else if(fsize > 2000000){
       swal("Sorry","File is too large !","error");
        }
      else
        {
        form_data.append("user_image", document.getElementById('user_image').files[0]);
        $.ajax({
          url:"upload.php",
          method:"POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend:function(){
            $('#image_preview').html("<label class='text-success'>Image Uploading <span class='fa fa-spinner fa-spin'></span> ...</label>");
            },   
          success:function(data)
            {
            $("#btn-up").hide('fast');
            $('#image_preview').html(data);
            $("#btn_save_del").show('fast');
            }
            });
            }
            });
  });
}


function save_image(path){
    //alert(path);
    var mydata = 'action=save_image' + '&path=' + path;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#profile_pic").html('<br><br><center><img src="../img/giphy.gif" width="30" class="img-fluid w3-margin-bottom"></center>');
      },
      success:function(data){
      if (data == 1) 
      {
      setTimeout(function(){
        load_pictures();
      },1000);
      $("#change_profile_pic").modal("hide"); 
      $("#user_img").val('');
      $("#btn_save_del").hide('fast');
      $("#btn-up").show('fast');
      }
    }
  });
}

function remove_image(path,img){
    var mydata = 'action=del_image' + '&path=' + path;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        // alert(path); 
        if (data == 1) 
        {
          $("#btn-up").show('fast');
          $("#user_image").val('');
          $("#btn_save_del").hide('fast');
          $('#image_preview').html('<img src='+img+' class="profile_pic img-responsive animated bounceIn" alt="">');
        }
      }
    });
  }

function save_recent_play(id){
    //alert(path);
    var mydata = 'action=save_recent_play' + '&id=' + id;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      if (data == 1) 
      {
     
      }
    }
  });
  }

function save_play(id){
    //alert(path);
    var mydata = 'action=save_play' + '&id=' + id;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      if (data == 1) 
      {
     
      }
    }
  });
}

function show_recently_played(){
      var mydata = 'action=show_recently_played';
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_subscriptions").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#show_recent_played").html(data);},800); 
      }
      }); 
}

function recent_playlists(){
      var search = $("#search_rp").val();
      var mydata = 'action=recent_playlists' + '&search_rp=' + search;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      //beforeSend:function(){$("#tbl_songs").html('<div class="col-sm-12 text-center"><img src="../img/giphy.gif" class="img-fluid" width="30"></div>'); },
      success:function(data){
      setTimeout(function(){$("#recent_playlists").html(data);},500); 
      }
      }); 
      }


//album musics
  function album_musics(){
  var album_search = $("#album_search").val();
  var album_id = $("#album_id").val();
  var mydata = 'action=album_musics' + '&album_search=' + album_search + '&album_id=' + album_id;
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  //beforeSend:function(){$("#tbl_album_music").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
  success:function(data){
  setTimeout(function(){$("#tbl_album_music").html(data);},800); 
  }
  }); 
  }