//Album Script
var alb_url = 'function.php';

function alb_song_able()
{
  $('#btn-select').removeClass('btn-secondary');
  $('#btn-select').addClass('btn-primary');
  $('#uploaded_song').val('');
}

function alb_song_disable()
{
  $('#btn-select').removeClass('btn-primary');
  document.getElementById('uploaded_song').disabled= true;
  $('#btn-select').addClass('btn-default');
}

function alb_song_multi_able()
{
  $('#btn-multi-select').removeClass('btn btn-secondary');
  document.getElementById('multi_uploaded_song').disabled= false;
  $('#btn-multi-select').addClass('btn btn-success');
  $('#uploaded_song').val('');
}

function alb_song_multi_disable()
{
  $('#btn-multi-select').removeClass('btn btn-success');
  document.getElementById('multi_uploaded_song').disabled= true;
  $('#btn-multi-select').addClass('btn btn-secondary');
}

function show_single_upload()
{
  $('#single_song').show('fast');
  $('#input_choice').hide('fast');
}

function show_multi_upload()
{
  $('#multi_song').show('fast');
  $('#input_choice').hide('fast');
}

function show_input_choice()
{
  $('#input_choice').show('fast');
  $('#single_song').hide('fast');
  $('#multi_song').hide('fast');
}



function albums()
{
  var search = $("#search").val();
  var mydata = 'action=showalbums' + '&search=' + search;

  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_album").html('<tr><td colspan="7"><img src="../img/load.gif" class="img-fluid" width="20"> </td></tr>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_album").html(data);},500);  
    }
  }); 
}

function albums2()
{
  var searchalb = $("#searchalb").val();
  var mydata = 'action=showalbums2' + '&searchalb=' + searchalb;

  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_album2").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_album2").html(data);},800);  
    }
  }); 
}

function display_album_audio(album_id)
{
  $("#alb_id_modal").val(album_id);
  $("#modal_playlist").modal('show');
  load_my_album_songs();
  load_my_album_info();
}

function load_my_album_songs(){
  var alb_id_modal = document.getElementById('alb_id_modal');
  var album_songs_ = document.getElementById('album_songs_');
  var mydata = 'action=showAlbumAudio' + '&alb_id_modal=' + alb_id_modal.value + '&album_songs_=' + album_songs_.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#display_album_audio").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#display_album_audio").html(data);},800);  
    }
  }); 
}

function load_my_album_info(){
  var alb_id_modal = document.getElementById('alb_id_modal');
  var mydata = 'action=showAlbumInfo' + '&alb_id_modal=' + alb_id_modal.value;
  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#myAlbumInfo").html(data);},800);  
    }
  });
}

function submit_album()
{
  var album_image = $('#album_image').val(); 
  var alb_id = document.getElementById('alb_id');
  var album_name = document.getElementById('album_name2');
  var album_descr = document.getElementById('album_descr');
  var extension = $('#album_image').val().split('.').pop().toLowerCase();
  var link = $("#link").val();   
  var link2 = document.getElementById('album_path');
  //alert(album_name.value);
  
  if(album_name == '')  
  {  
    swal("Oops","Please Input Album Name","warning"); 
    return false;  
  }else if(album_descr == ''){
     swal("Oops","Please Input Album Description","warning"); 
    return false;  
  } 
  /*else if(album_image == '')  
  {  
    swal("Oops","Please Select Image","warning"); 
    return false;  
  }  
  else if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)  
  {  
    swal("Error","Invalid Image File","error"); 
    $('#album_image').val('');  
    return false;  
  }*/
  else 
  {
    var mydata = 'link=' + link + '&album_name2=' + album_name.value + '&album_descr=' + album_descr.value + '&alb_id=' + alb_id.value + '&album_path=' + link2.value;
    $.ajax({
      type:"POST",
      url:"save_album.php",
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data);
        if (data == 1) 
        {
           $('#album_name2').val('');
          swal("Success","Album Created","success"); 
          $('#newAlbumModal').modal('hide');
          albums();
          //save_able();
        }
        else if (data == 2){
          swal("Success","Album Updated","success");
          $('#newAlbumModal').modal('hide');
          albums();
        }
      }
    });
  }  
}

function submit_album2()
{
  var album_name = document.getElementById('name_album');
  var album_desc = document.getElementById('album_desc');
  //alert(album_name.value);
  
  if(album_name.value == '')  
  {  
    swal("Oops","Please Input Album Name","warning"); 
    return false;  
    album_name.focus();
  }else if(album_desc.value == ''){
    swal("Oops","Please Input Album Description","warning"); 
    return false; 
    album_desc.focus(); 
  }
  else 
  {
    var mydata = 'action=add_album2' + '&name_album=' + name_album.value + '&album_desc=' + album_desc.value;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data);
        if (data == 1) 
        {
           $('#name_album').val('');
           $('#album_desc').val('');
          swal("Success","Album Created","success"); 
          $('#add_album2').modal('hide');
          display_albums();
          //save_able();
        }else if (data == 404){
          swal("Music Library","Album name already exists","info"); 
          $('#name_album').focus();
        }
      }
    });
  }  
}

function upload_music()
{
  $(document).ready(function(){
    $(document).on('change', '#uploaded_song', function(){

      var name = document.getElementById("uploaded_song").files[0].name;
      var form_data = new FormData();
      var ext1 = name.split('.').pop().toLowerCase();
      
      if(jQuery.inArray(ext1, ['mp3']) == -1) 
      {
        alert("Invalid Image File");
      }
      
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("uploaded_song").files[0]);
      var f = document.getElementById("uploaded_song").files[0];
      var fsize = f.size||f.fileSize;
      
      if(fsize > 50000000)
      {
        alert("Image File Size is very big");
      }
      else
      {
        form_data.append("uploaded_song", document.getElementById('uploaded_song').files[0]);
        //alert(form_data);
        
        $.ajax({
          url:"uploadsong.php",
          method:"POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend:function(){
            $('#up_loader').html("<label class='text-success text-center'>Music Uploading...</label>");
          },   
          success:function(data)
          {
            $('#up_loader').hide('fast');
            $('#song_preview').html(data);
            alb_song_disable();
            $("#alb_upload_song").show('fast');
          }
        });
      }
    });
  });
}

function multi_upload_music()
{
  $(document).ready(function(){
    $(document).on('change', '#multi_uploaded_song', function(){

      var filedata = document.getElementById("multi_uploaded_song")
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var name = document.getElementById("multi_uploaded_song").files[i].name;
        var form_data = new FormData();
        var ext1 = name.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['mp3']) == -1) 
        {
          alert("Invalid Image File");
        }
        
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multi_uploaded_song").files[i]);
        var f = document.getElementById("multi_uploaded_song").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          alert("Image File Size is very big");
        }
        else
        {
          form_data.append("multi_uploaded_song", document.getElementById('multi_uploaded_song').files[i]);

          $.ajax({
            url:"multi_uploadsong.php",
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#song_preview').html("<label class='text-success'>Music Uploading...</label>");
            },   
            success:function(data)
            { 
              $('#song_preview').text("");
              setTimeout(function(){ $('#song_preview').append(data);}, 1000);
              alb_song_multi_disable();
            }
          });
        }
      }
    });
  });
}

function get_duration2(total){
var seconds = "0" + (Math.floor(total) - minutes * 60);
var seconds = "0" + Math.floor(total % 60);
var minutes = "0" + Math.floor(total / 60);
var seconds = "0" +  Math.floor(total - minutes * 60);
//concat minutes and seconds
var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
$("#duration2").val(dur);
}
/*
function submitsong()
{
  var uploaded_song = $('#uploaded_song').val(); 
  var songname = $('#songname').val();
  var compose = $('#compose').val();
  var artis = $('#artis').val();
  var extension = $('#uploaded_song').val().split('.').pop().toLowerCase();
  var link1 = $("#link1").val();
  var genres = $("#genres").val();
  var audience = $("#audience2").val();
  var application = $("#application2").val();   
  var album = $("#id_album").val(); 
  var duration = $("#duration2").val(); 
      
  // alert(al);
  if(uploaded_song == '')  
  {  
    alert("Please Select Song");  
    return false;  
  } 
  else if(songname == '')  
  {  
    swal("Oops", "Please Input Song Name", "warning");  
    return false;  
  } 
  else if(compose == '')  
  {  
    swal("Oops", "Please Input Composer", "warning");  
    return false;  
  }
  else if(artis == '')  
  {  
    swal("Oops", "Please Input Artist", "warning");  
    return false;  
  } 
  else if(jQuery.inArray(extension, ['mp3']) == -1)  
  {  
    alert('Invalid Song File');  
    $('#uploaded_song').val('');  
    return false; 
  }
  else if (genres == null || genres == "" || genres == "genres") 
  {
     swal("Oops", "Please Select Style", "warning");    
  }
  else if (audience == null || audience == "" || audience == "audience2") 
  {
     swal("Oops", "Please Select Audience", "warning");    
  }
  else if (application == null || application == "" || application == "application2") 
  {
     swal("Oops", "Please Select Application", "warning");    
  }
  else 
  {
    var mydata ='action=upload_song' + '&link2=' + link1 + '&song_name=' + songname + '&composer=' + compose + '&artist=' + artis + '&genres=' + genres + '&album=' + album + '&audience=' + audience + '&application=' + application + '&duration=' + duration;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:alb_url,
      data:mydata,
      cache:false,
      success:function(data){
      console.log(data);
        if (data == 1) 
        {
          swal("Success", "Song has been Added!", "success");
          $('#songname').val('');
          $('#compose').val('');
          $('#artis').val('');
          display_album_audio(album);
        }
        else
        {
          swal("Error", "Error occured while Saving", "error");
        }
      }
    });
  }  
}*/

function alb_remove_song(alb_path, path_id){

  var path = 'action=remove_song'+ '&path1=' + alb_path;
    
  swal({
    title: "Are you sure?",
    text: "Do you want to Remove this Song?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
  },
    function(){  
    $.ajax({  
      url:alb_url,  
      type:"POST",  
      data:path,  
      success:function(data)
      {  
        if(data != '')  
        {  
          swal("Removed!", "Song has been Removed!", "success");
          $('#'+path_id+'').hide('slow');
          $("#alb_upload_song").hide('fast');
          alb_song_able();
          //alb_song_multi_able();
        }
        else  
        {  
          return false;  
        }  
      }  
    });  
  });
}

function play_Audio(aud) {
  $("#myAudio").html('<audio controls controlsList="nodownloads noremoteplayback" style="background-color:white;" id="my_Audio" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#alb_Play").hide('fast');
  $("#alb_Pause").show('fast');
  $("#alb_stop").show('fast');
}

function play_(){
  //alert('down')
  $("#alb_Play").hide('fast');
  $("#alb_Pause").show('fast');
  $("#alb_stop").show('fast');

  var voldown = document.getElementById('my_Audio');
  voldown.play();
}

function pause_(){
  //alert('down')
  $("#alb_Play").show('fast');
  $("#alb_Pause").hide('fast');

  var voldown = document.getElementById('my_Audio');
  var now_play = document.getElementById('now_play');
  voldown.pause();
}

function clear_music(){
  $("#alb_stop").hide('fast');
  $("#alb_Play").show('fast');
  $("#alb_Pause").hide('fast');

  $("#myAudio").html('');
}

function volup(){
  //alert('up')
  var volup = document.getElementById('my_Audio');
  volup.volume += 0.1;
}

function voldown(){
  //alert('down')
  var voldown = document.getElementById('my_Audio');
  voldown.volume -= 0.1;
}




function delete_alb_music(index,music){
  var album = $("#id_album").val(); 
  var mydata ='action=del_music'+'&id=' + index +'&music=' + music;
  alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    load_my_album_songs(album);
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    }   


function empty_album(){
  var album = $("#id_album").val(); 
  var alb_id_modal = document.getElementById('alb_id_modal');
  var mydata ='action=clear_album_songs' + '&alb_id_modal=' + alb_id_modal.value;
  //alert (mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to Empty this Album?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Songs has been deleted !", "success");
    display_album_audio(album);
    }
    else
    {
      swal("Error", "No Items Deleted", "info");
    }
    }
    });
    });
    } 
