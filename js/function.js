var url = 'function/function.php';

function save_play(id){
    //alert(path);
    var mydata = 'action=save_play' + '&id=' + id;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      if (data == 1) 
      {
     
      }
    }
  });
}

function subscribe_now(){
  var name,email,mydata;
  email = document.getElementById('email_address');
  name = document.getElementById('name');
  if (email.value == "") 
    {
     email.focus();
    }
   else if (name.value == "") 
    {
     name.focus();
    }
  else{
      var mydata = 'action=subscribe' + '&email_address=' + email.value + '&name=' + name.value;
      //alert(mydata);
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loading").addClass('w3-spin');
        },
        success:function(data){
          //alert(data);
          //console.log(data);
          //setTimeout(function(){  $("#load_img").removeClass('w3-spin');},2000);
         if (data == 1) 
         {
          swal("Success","Successfully subscribed..","success");
          $("#email_address").val('');
          $("#name").val('');
         }
         else if(data == 2){
          email.focus();
          swal("Email already taken","Please enter another email address.","info");
         }
         else if(data == 404){
          email.focus();
          swal("Invalid email address.","Please enter a valid email address.","info");
         }
         else
         {
          swal("Sorry","Error on subscribing..","error");
         }
        }
      });
    }

}

//FORGOT PASSWORD
function send_email(){
  var email,mydata;
  email = document.getElementById('email_verify');
  if (email.value == "") 
    {
     email.focus();
    }
  else{
      var mydata = 'action=forgot_password' + '&email_verify=' + email.value;
      //alert(mydata);
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loading").addClass('w3-spin');
        },
        success:function(data){
          //alert(data);
          //console.log(data);
          //setTimeout(function(){  $("#load_img").removeClass('w3-spin');},2000);
         if (data == 1) 
         {
          swal("Success","Email Successfully Sent","success");
          $("#email_verify").val('');
         }
         else if(data == 2){
          email.focus();
          swal("Email Not Found","Please enter another email address.","info");
         }
         else if(data == 404){
          email.focus();
          swal("Invalid email address.","Please enter a valid email address.","info");
         }
         else
         {
          swal("Sorry","Error on Sending Email..","error");
         }
        }
      });
    }

}

function login(){
    var username,password;
    username = $("#username").val();
    password = $("#password").val();
    
    if (username == "") 
    {
      $("#username").focus();
    }
    else if (password == "") 
    {
       $("#password").focus();
    }
    else
    {
      var mydata = 'action=login' + '&username=' + username + '&password=' + password;
      //alert(mydata);
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#load_img").addClass('w3-spin');
        },
        success:function(data){
          // alert(data);
          console.log(data);
          setTimeout(function(){  $("#load_img").removeClass('w3-spin');},2000);
         if (data == 1) 
         {
            window.location="./admin_panel/";
         }
         else if (data == 2) 
         {
          window.location="./user_profile/";
         }
         else if (data == 3) 
         {
           window.location="./guest_profile/";
         }
         else if (data == 303) 
         {
          swal("Sorry","Please enter valid e-mail address!","error");
         }
         else
         {
          swal("Sorry","Invalid Account !","error");
         }
        }
      });
    }
  }

  function lenght_input(){
    var input = $("#pwd").val();
    // alert(id);
    if (input.length <= 5) 
    {
    $("#pwd").removeClass('w3-green');
    $("#pwd").removeClass('bg-warning');
    $("#pwd").addClass('w3-red');
    $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-exclamation pull-left text-danger w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-danger">Password must be 6 characters!</span>');
     setTimeout(function(){
         $("#pwd").removeClass('bg-red');
         $("#loading_reg").html('');
       },5000);
    }
    else if (input.length == 6 && input.length <=9) 
    {
    $("#pwd").removeClass('w3-green');
    $("#pwd").removeClass('w3-red');
    $("#pwd").addClass('bg-warning');
    $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-thumbs-down pull-left text-warning w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-warning"> Weak!</span>');
     setTimeout(function(){
         $("#pwd").removeClass('bg-warning');
         $("#loading_reg").html('');
       },5000);
    }
    else if (input.length > 10) 
    {
    $("#pwd").removeClass('bg-warning');
    $("#pwd").removeClass('w3-red');
    $("#pwd").addClass('w3-green');
    $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-thumbs-up pull-left text-success w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-success"> Strong!</span>');
     setTimeout(function(){
         $("#pwd").removeClass('w3-green');
         $("#loading_reg").html('');
       },5000);
    }
  }

   function verify_pwd(){
   pwd = $("#pwd").val();
   c_pwd = $("#c_pwd").val();
    if (c_pwd == "") 
    {

    }
    else
    {
      if (pwd == c_pwd) 
    {
      $("#c_pwd").removeClass('w3-red');
      $("#c_pwd").addClass('w3-green');
      $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-thumbs-up pull-left text-success w3-large animated bounceIn infinite"></span>');
      setTimeout(function(){
         $("#c_pwd").removeClass('w3-green');
         $("#loading_reg").html('');
       },5000);
    }
    else
    {
      $("#c_pwd").removeClass('w3-green');
      $("#c_pwd").addClass('w3-red');
    }
    }
    
   }

   function clear_register(){
    $("#fn").val('');
    $("#mn").val('');
    $("#ln").val('');
    $("#bdate").val('');
    $("#gender").val('');
    $("#email").val('');
    $("#uname").val('');
    $("#pwd").val('');
    $("#c_pwd").val('');
   }

  var code_capcha = '';


  function get_code_caps(num){
    if (num == 0) 
    {
      code_capcha = 'j1KSu2Qo';
    }
    else if (num == 1) 
    {
      code_capcha = 'Lk9e3JK2';
    }
    else if (num == 2) 
    {
      code_capcha = 'g5h0m3pq';
    }
    else if (num == 3) 
    {
      code_capcha = '4k5rRLi5';
    }
    else if (num == 4) 
    {
      code_capcha = 'lS3Mul1b';
    }
    else if (num == 5) 
    {
      code_capcha = 'J149B6Ls';
    }
    else if (num == 6) 
    {
      code_capcha = 'pDyU73Nm';
    }
    else if (num == 7) 
    {
      code_capcha = 'bsN322Fk';
    }
    else if (num == 8) 
    {
      code_capcha = 'T4GPmo4s';
    }
  }

 /* function show_captcha(){
    var cap_num = Math.floor(Math.random() * 9);
    get_code_caps(cap_num);
    $("#show_quest").html(' <div class="text-center"><img src="img/capcha/c'+cap_num+'.png" class="img-fluid w3-round w3-animate-opacity" style="width: 100%; height: 80px;"></div>');
  }*/


   function register(){
    var fn, mn, ln, gender, bdate, email, uname, pwd, c_pwd, ver_cap, rfi;
    fn = $("#fn").val();
    ln = $("#ln").val();
    email = $("#email").val();
    pwd = $("#pwd").val();
    c_pwd = $("#c_pwd").val();
    rfi = $("#rfi").val();
    //ver_cap = $("#ver_cap").val();

    if (fn == "") 
    {
      $("#fn").focus();
    }
    else if (ln == "") 
    {
      $("#ln").focus();
    }
    else if (email == "") 
    {
      $("#email").focus();
    }
    else if (pwd == "") 
    {
      $("#pwd").focus();
    }
    else if (c_pwd != pwd) 
    {
      $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-danger w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-danger">Password doesn\'t match !</span>');
      $("#c_pwd").addClass('w3-red');
      $("#c_pwd").focus();
    }
    /*else if (ver_cap == "") 
    {
      $("#ver_cap").focus();
      $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-danger w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-danger">Please verify code!</span>');
    }
    else if (ver_cap != code_capcha) 
    {
      show_captcha();
      $("#ver_cap").focus();
      $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-danger w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-danger">Code doesn\'t match !</span>');
    }*/
    else
    {
      $("#c_pwd").removeClass('w3-red');
      
      var mydata = 'action=register' + '&fn=' + fn + '&ln=' + ln + '&email=' + email + '&password=' + pwd + '&rfi=' + rfi;
      //alert(mydata);
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loading_reg").html('<span style="margin-top:5px;" class=" w3-animate-opacity fa fa-music pull-left text-white w3-large animated jackInTheBox infinite"></span> <span class="pull-left ml3 text-white w3-animate-opacity">Please Wait...</span>');
        },
        success:function(data){
          // alert(data);
          console.log(data);
          setTimeout(function(){$("#loading_reg").html('');},5000);
        if (data == 2) 
        {
           verify_account(email); 
           swal("Success !","Your registration is being reviewed by Admin. Further advice will be sent to your email address with 72 hrs.","success");
          // swal("Thank You !","You will receive verification code to your email.","success");
          $("#uname").removeClass('w3-red');
          clear_register();
          $("#c_pwd").removeClass('w3-red');
          $("#pwd").removeClass('w3-red');          
        }
        else if (data == 1) 
        {
          $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-warning w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-warning">This email address is already registered!</span>');
          $("#email").addClass('w3-red');
          $("#email").focus();

        }
        else if (data == 3) 
        {
          $("#loading_reg").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-warning w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-warning">Invalid e-mail address !</span>');
          $("#email").focus();

        }
        }
      });
    }
  }


  

function pager(index){
    $('#pages').load(index+'.php');
}


function verify_account(id){
  // alert(id);
  $("#v_email").val(id);
  $("#verify").modal({'backdrop':false});
  $("#register_form").modal('hide');

}


function verify_check(email,code){
  if (code == "") 
  {
   $('#verify_code').focus();
  }
  else
  {
  var mydata = 'action=verify_user' + '&email=' + email + '&code=' + code;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      // alert(data);
      if (data == 1) 
      {
        $("#verify").modal('hide');
        $("#v_email").val('');
         $('#verify_code').val('');
        swal("Success !","Your account has been activated !","success");
      }
      else
      {
         swal("Oops !","Verification code doesn\'t match !","error");
      }
    }
  });
  }
}


  function scroll(){
  $(document).ready(function(){
  $("body").on('scroll',function(){
  if (this.scrollTop > 390 ) 
  {
  $("#menu-nav").addClass('navbar-dark bg-dark w3-animate-opacity w3-card-4');
  $("#logo_nav").addClass('img_resize');
  //$("#logo_nav").hide();
  }
  else
  {
  $("#menu-nav").removeClass('navbar-dark bg-dark w3-animate-opacity w3-card-4');
  $("#logo_nav").removeClass('img_resize');
  $("#logo_nav").show();
  }
  });
  });

  $(document).ready(function(){
  // Add smooth scrolling to all links
  $(".nav-a").on('click', function(event) {

  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {
  // Prevent default anchor click  behavior
  event.preventDefault();

  // Store hash
  var hash = this.hash;

  // Using jQuery's animate() method to add smooth page scroll
  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 800, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = hash;
  });
  } // End if
  });
  });
  }

//NEW UPDATES
//load genres
function load_genres(){
$.ajax({
    type:"POST",
    url:url,
    data:'action=load_genres',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#load_genres").html(data);
        },500);
    }
  });
}

function load_genres2(){
$.ajax({
    type:"POST",
    url:url,
    data:'action=load_genres',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#load_genres2").html(data);
        },500);
    }
  });
}

function get_genres(name){
  document.getElementById("genre_search").style.width = "100%";
  $("#search_music2").val(name);
  load_genre_music();
  load_genres2();
}

function search_music(){
  document.getElementById("genre_search").style.width = "100%";
  var search_music = $("#search_music").val();
  $("#search_music2").val(search_music);
  load_genre_music();
  load_genres2();
}

function load_genre_music(){
  var search,mydata;
  search = document.getElementById("search_music2");
  mydata = 'action=load_genre_musics' + '&search_music2=' + search.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#tbl_searched").html(data);
        },500);
    }
  });
}


function remove_genre(){
  $("#genre_name").val('');
  load_genre_music();
}


function view_search(){
    $("#default").addClass('none');
    $("#view_search").removeClass('none');
    $("#default2").removeClass('col-lg-8');
    $("#default2").addClass('col-lg-5');
    $("#search_index").focus();
  }

  function remove_search(){
    $("#default").removeClass('none');
    $("#view_search").addClass('none');
    $("#default2").addClass('col-lg-8');
  }

//NEW UPDATES

function send_message(){
    var name, email, message, mydata;
    name = $("#name").val();
    email = $("#email").val();
    message = $("#message").val();
    //ver_cap = $("#ver_cap").val();

    if (name == "") 
    {
      $("#name").focus();
    }
    else if (email == "") 
    {
      $("#email").focus();
    }
    else if (message == "") 
    {
      $("#message").focus();
    }
    else
    {      
      var mydata = 'action=send_message' + '&name=' + name + '&email=' + email + '&message=' + message;
      //alert(mydata);
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loading").html('<span style="margin-top:5px;" class=" w3-animate-opacity fa fa-music pull-left text-white w3-large animated jackInTheBox infinite"></span> <span class="pull-left ml3 text-white w3-animate-opacity">Please Wait...</span>');
        },
        success:function(data){
          // alert(data);
          console.log(data);
          setTimeout(function(){$("#loading").html('');},5000);
        if (data == 2) 
        {
           swal("Success !","Your Message Successfully Sent.","success");       
        }
        else if (data == 3) 
        {
          $("#loading").html('<span style="margin-top:5px;" class=" fa fa-warning pull-left text-warning w3-large animated bounceIn infinite"></span> <span class="pull-left ml3 text-warning">Invalid e-mail address !</span>');
          $("#email").focus();
        }
        }
      });
    }
  }

function load_about_content(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=about_content',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#about_content").html(data);
        },100);
    }
  });
}

function load_faqs_content(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=faqs_content',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#faqs_content").html(data);
        },100);
    }
  });
}
