/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.6.12-log : Database - music_library
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`music_library` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `music_library`;

/*Table structure for table `tbl_downloads` */

DROP TABLE IF EXISTS `tbl_downloads`;

CREATE TABLE `tbl_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date_` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_downloads` */

insert  into `tbl_downloads`(`id`,`audio_id`,`user_id`,`date_`) values 
(1,'song-22352','user-001','2018-06-20'),
(2,'song-1466','user-001','2018-06-20'),
(3,'song-22352','user-001','2018-06-20'),
(4,'song-1466','user-001','2018-06-20'),
(5,'song-63266','user-001','2018-06-20'),
(6,'song-22352','user-001','2018-06-20'),
(7,'song-381485','user-001','2018-06-20'),
(8,'song-2889','user-001','2018-06-20'),
(9,'song-161569','user-001','2018-06-20'),
(10,'song-2889','user-001','2018-06-20'),
(11,'song-691376','user-001','2018-06-20');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
