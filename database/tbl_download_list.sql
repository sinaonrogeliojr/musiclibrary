/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.6.12-log : Database - music_library
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`music_library` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `music_library`;

/*Table structure for table `tbl_download_list` */

DROP TABLE IF EXISTS `tbl_download_list`;

CREATE TABLE `tbl_download_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `datetrans` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_download_list` */

insert  into `tbl_download_list`(`id`,`audio_id`,`user_id`,`datetrans`,`status`) values 
(1,'song-2615','user-003','2018-06-21 15:31:21',1),
(2,'song-171844','user-003','2018-06-21 16:00:35',1),
(3,'song-562052','user-003','2018-06-21 16:01:37',1),
(4,'song-171844','user-003','2018-06-21 16:05:21',1),
(5,'song-612225','user-003','2018-06-21 16:05:23',1),
(6,'song-2615','user-003','2018-06-21 16:40:30',1),
(7,'song-482165','user-003','2018-06-21 16:40:37',1),
(8,'song-2615','user-003','2018-06-21 16:42:46',1),
(9,'song-851951','user-003','2018-06-21 16:52:00',1),
(10,'song-171844','user-003','2018-06-21 16:52:03',1),
(11,'song-2615','user-003','2018-06-21 16:54:42',1),
(12,'song-171844','user-003','2018-06-21 16:54:44',1),
(13,'song-2615','user-004','2018-06-21 17:02:15',0),
(14,'song-851951','user-004','2018-06-21 17:03:02',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
