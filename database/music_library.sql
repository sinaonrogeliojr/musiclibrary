/*
MySQL Data Transfer
Source Host: localhost
Source Database: music_library
Target Host: localhost
Target Database: music_library
Date: 6/14/2018 6:01:13 PM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for tbl_album
-- ----------------------------
CREATE TABLE `tbl_album` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `album_id` varchar(255) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_artwork` text,
  `user_id` varchar(255) DEFAULT NULL,
  `date_released` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`),
  KEY `album_id` (`album_id`),
  CONSTRAINT `userid` FOREIGN KEY (`user_id`) REFERENCES `tbl_user_profile` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_audios
-- ----------------------------
CREATE TABLE `tbl_audios` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `music` text,
  `song_name` varchar(255) DEFAULT NULL,
  `composer` varchar(255) DEFAULT NULL,
  `artist` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `album_id` varchar(255) DEFAULT NULL,
  `upload_date` varchar(255) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `albumid` (`album_id`),
  KEY `genid` (`genre`),
  CONSTRAINT `albumid` FOREIGN KEY (`album_id`) REFERENCES `tbl_album` (`album_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_genre
-- ----------------------------
CREATE TABLE `tbl_genre` (
  `gen_id` int(255) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_playlist
-- ----------------------------
CREATE TABLE `tbl_playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `user_id` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_playlist_info
-- ----------------------------
CREATE TABLE `tbl_playlist_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `playlist_id` int(11) DEFAULT NULL,
  `date_` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_user_profile
-- ----------------------------
CREATE TABLE `tbl_user_profile` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `b_date` varchar(20) DEFAULT NULL,
  `email_add` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` int(1) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `picture` text,
  `pwd_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `tbl_album` VALUES ('5', 'album-3534', 'qwertyuioasaasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'img/album_img/581.png', 'user-25281', '2018-06-01');
INSERT INTO `tbl_album` VALUES ('6', 'album-78538', 'Alone', 'img/album_img/609.jpg', 'user-25281', '2018-06-01');
INSERT INTO `tbl_album` VALUES ('10', 'album-2115', 'kjhkjlh', 'img/album_img/144.jpg', 'user-25281', '2018-06-06');
INSERT INTO `tbl_album` VALUES ('11', 'album-171054', 'sdfgdsfsdf', 'img/album_img/609.jpg', 'user-25281', '2018-06-07');
INSERT INTO `tbl_album` VALUES ('12', 'album-671151', 'Marvin\'s Album', 'img/album_img/788.png', 'user-25281', '2018-06-07');
INSERT INTO `tbl_album` VALUES ('14', 'album-331335', 'My Album', 'img/album_img/369.jpg', 'user-25281', '2018-06-07');
INSERT INTO `tbl_album` VALUES ('16', 'album-821532', 'Marvin Louel', 'img/album_img/266.jpg', 'user-932784', '2018-06-10');
INSERT INTO `tbl_album` VALUES ('17', 'album-531663', 'Favs', 'img/album_img/607.jpg', 'user-932784', '2018-06-11');
INSERT INTO `tbl_album` VALUES ('25', 'album-131749', 'marvins album', 'img/album_img/433.jpg', 'user-932784', '2018-06-12');
INSERT INTO `tbl_album` VALUES ('26', 'album-992567', 'MY PLAYLIST', 'img/album_img/855.png', 'user-942868', '2018-06-14');
INSERT INTO `tbl_audios` VALUES ('11', 'song-721022', 'uploads/uploadedsongs/228.mp3', 'Marvin', 'Louel', 'Besana', 'RnB', 'album-531663', '2018-06-12', '0');
INSERT INTO `tbl_audios` VALUES ('12', 'song-591192', 'uploads/uploadedsongs/375.mp3', 'vin', 'louel', 'besana', 'Rock', 'album-531663', '2018-06-13', '0');
INSERT INTO `tbl_audios` VALUES ('13', 'song-491272', 'uploads/uploadedsongs/291.mp3', 'heyhey', 'hoho ', 'yuyu', 'Others', 'album-821532', '2018-06-14', '0');
INSERT INTO `tbl_audios` VALUES ('15', 'song-591431', 'uploads/uploadedsongs/535.mp3', 'sfgsdfg', 'dsfg', 'as', 'Classical Ensemble', 'album-531663', '2018-06-16', '0');
INSERT INTO `tbl_audios` VALUES ('17', 'song-231698', 'uploads/uploadedsongs/744.mp3', 'vin', 'ert', 'ghjfghjfgh', 'RnB', 'album-131749', '2018-06-18', '0');
INSERT INTO `tbl_audios` VALUES ('19', 'song-911841', 'uploads/uploadedsongs/779.mp3', 'fhfhj', 'fghfgh', 'fghjfghj', 'Rock', 'album-131749', '2018-06-20', '0');
INSERT INTO `tbl_genre` VALUES ('1', 'Acoustic');
INSERT INTO `tbl_genre` VALUES ('2', 'RnB');
INSERT INTO `tbl_genre` VALUES ('3', 'Rock');
INSERT INTO `tbl_genre` VALUES ('4', 'Jazz');
INSERT INTO `tbl_genre` VALUES ('5', 'Others');
INSERT INTO `tbl_genre` VALUES ('6', 'Pop');
INSERT INTO `tbl_genre` VALUES ('7', 'Modern Ensemble');
INSERT INTO `tbl_genre` VALUES ('8', 'Classical Ensemble');
INSERT INTO `tbl_genre` VALUES ('9', 'All');
INSERT INTO `tbl_genre` VALUES ('11', 'Item');
INSERT INTO `tbl_genre` VALUES ('12', 'Chorus');
INSERT INTO `tbl_genre` VALUES ('13', 'Busking');
INSERT INTO `tbl_genre` VALUES ('14', 'Background');
INSERT INTO `tbl_genre` VALUES ('15', 'Devotional');
INSERT INTO `tbl_genre` VALUES ('16', 'Sunday School');
INSERT INTO `tbl_playlist` VALUES ('1', 'Playlist1', '2018-06-14 15:37:50', 'user-372987');
INSERT INTO `tbl_playlist` VALUES ('2', 'Playlist2', '2018-06-14 15:54:54', 'user-372987');
INSERT INTO `tbl_playlist` VALUES ('3', 'sample', '2018-06-14 15:56:20', 'user-372987');
INSERT INTO `tbl_playlist_info` VALUES ('1', 'song-911841', 'user-372987', '3', '0', '2018-06-14 17:18:12', '1');
INSERT INTO `tbl_playlist_info` VALUES ('2', 'song-491272', 'user-372987', '3', '0', '2018-06-14 17:18:12', '1');
INSERT INTO `tbl_playlist_info` VALUES ('3', 'song-721022', 'user-372987', '3', '0', '2018-06-14 17:18:12', '1');
INSERT INTO `tbl_playlist_info` VALUES ('4', 'song-591431', 'user-372987', '3', '0', '2018-06-14 17:18:12', '1');
INSERT INTO `tbl_playlist_info` VALUES ('5', 'song-911841', 'user-372987', '3', '0', '2018-06-14 17:29:35', '1');
INSERT INTO `tbl_playlist_info` VALUES ('6', 'song-491272', 'user-372987', '3', '0', '2018-06-14 17:29:35', '1');
INSERT INTO `tbl_user_profile` VALUES ('1', 'user-001', 'John Luis', 'Soriano', 'Barcelona', 'Male', '1996-11-14', 'johnluis@gmail.com', 'johnluis14', 'barcelona', '1', '1', '/avatar/pict-995.png', null);
INSERT INTO `tbl_user_profile` VALUES ('9', 'user-25281', 'marvin', 'louel', 'openio', 'Male', '2018-12-31', 'dasdasdasdasddasd@yahoo.com', 'marvin', '123456789', '3', '1', null, null);
INSERT INTO `tbl_user_profile` VALUES ('21', 'user-821038', 'fghjkl;', 'ghjkl;', 'ghjkl;', 'Male', '2018-12-31', 'johnluisb14@gmail.com', '123456789099', '1234567890', '2', '1', null, null);
INSERT INTO `tbl_user_profile` VALUES ('24', 'user-882236', 'johnluis', 'soriano', 'barcelona', 'Male', '2017-12-31', 'johnluisb14@gmail.com', 'johnluis', 'johnluis', '2', '0', null, '0M69s0l4');
INSERT INTO `tbl_user_profile` VALUES ('25', 'user-752586', 'Marvin Louel', 'b.', 'Openio', 'Male', '2018-12-31', 'marvinlouelopenio@gmail.com', 'marvin123', 'marvin123', '2', '0', null, '0M59s2l2');
INSERT INTO `tbl_user_profile` VALUES ('26', 'user-252656', 'marvin louel', 'besana', 'openio', 'Male', '1997-09-19', 'marvinlouelopenio@gmail.com', 'marvinlouel', 'marvinlouel', '2', '0', null, '0M94s5l5');
INSERT INTO `tbl_user_profile` VALUES ('27', 'user-932784', 'marvin', 'louel', 'openio', 'Male', '1997-09-19', 'marvinlouelopenio@gmail.com', 'marvinpogi', 'marvinlouel', '2', '1', null, null);
INSERT INTO `tbl_user_profile` VALUES ('28', 'user-942868', 'Juan', 'Melchor', 'Dela Cruz', 'Male', '1996-12-31', 'juan@gmail.com', 'juan', 'juan123456', '2', '1', null, '6M55s3l4');
INSERT INTO `tbl_user_profile` VALUES ('29', 'user-372987', 'Rogelio Jr.', 'Dalloran', 'Sinaon', 'Male', '1996-12-31', 'sinaonrogeliojr@gmail.com', 'rogelio_jr', '12345', '3', '1', null, '6M41s5l3');
