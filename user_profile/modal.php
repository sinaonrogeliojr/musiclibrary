<!-- Create New Album --> 
  <div class="modal fade" id="addSong">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <p class="title">ADD NEW ALBUM</p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body" >        
            <div id="image_preview" class="text-center"> </div> <br/>
            <input type="file" name="album_image" id="album_image" style="display: none;"/> 
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="show" name="album_name" id="album_name" class="form-control text-center" placeholder="Album Name" required="true" maxlength="30">
                  </div>
                </div>
                <div class="col-sm-3" >
                  <div class="" style="display: none;">
                    <button class="btn btn-primary btn-oji" id="btn-chooser" onclick="$('#album_image').click();"> 
                      <i class="fa fa-file fa-lg"></i> Choose Fissle</button>
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-sm-5"></div>
            <div class="col-sm-3" style="padding-right: 0px;">
              <button class="btn btn-small btn-dark" data-dismiss="modal" onclick="N_cancel();">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-small btn-dark" name="submit" id="submit" onclick="submit_album();" >Create Album</button>
            </div>
        </div>
      </div>  
    </div>  
  </div>

<!-- Update Album -->
  <div id="update_alb" class="modal fade">  
    <div class="modal-dialog">  
      <div class="modal-content" style=" background-color: #6C7A89; "> 
        <div class="modal-header">
            <h5 class="calbum modal-title text-white">Update Album</h5>  
            <a href="#"  class="pull-right N_album_close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
          </div>  
          <div class="modal-body">         
            <input type="hidden" name="id" id="id"> <br/>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <input type="text" name="albname" id="albname" class="form-control text-center" placeholder="Album Name" required="true" maxlength="30">
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-sm-5"></div>
            <div class="col-sm-3" style="padding-right: 0px;">
              <button class="btn btn-block btn-oji btn-primary" style="color: #fff;" data-dismiss="modal">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-block btn-oji btn-primary" style="color: #fff;" onclick="save_alb();">Update Album</button>
            </div>
        </div>
      </div>  
    </div>  
  </div>

<!-- Create Playlist --> 
  <div id="newPlaylistModal" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog">  
      <div class="plbg modal-content pl_radius N_content">  
        <div class="modal-header">
          <h5 class="w3-wide w3-xlarge text-white pl_title">Add New Playlist</h5>  
          <a href="#"  class="pull-right AM-close" data-dismiss="modal" onclick="cancelplist();">
            <i id="closehover" class="fa fa-times"></i></a>
        </div>  
        <div class="modal-body"> 
            <div class="row">        
              <div class="col-sm-12"><br>
                <div class="form-group">
                  <input type="text" name="playlist_name" id="playlist_name" class="form-control text-center" placeholder="Playlist Name" required="true" maxlength="30">
                </div><br/>
              </div>
            </div>
        </div> 
        <div class="modal-footer">
          <div class="col-sm-5"></div>
          <div class="col-sm-3" style="padding-right: 0px;">
            <button class="btn rounded N_cancel btn-block" style="color: #fff;" onclick="cancelplist();">Cancel</button>
          </div>
          <div class="col-sm-4">
            <button class="btn rounded btn-block Pl_save" style="color: #fff;" onclick="submit_plist();">Save</button>
          </div>
        </div>
      </div>  
    </div>  
  </div>

<!-- Update Playlist -->
  <div id="update_plist" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog">  
      <div class="plbg modal-content pl_radius N_content">  
        <div class="modal-header">
          <h5 class="w3-wide w3-xlarge text-white pl_title">Update Playlist</h5>  
          <a href="#"  class="pull-right AM-close" data-dismiss="modal" onclick="cancelpupdate();">
            <i id="closehover" class="fa fa-times"></i></a>
        </div>  
        <div class="modal-body"> 
          <input type="hidden" name="pid" id="pid"> 
            <div class="row">        
              <div class="col-sm-12"><br>
                <div class="form-group">
                  <input type="text"  name="plist_name" id="plist_name" class="form-control text-center" placeholder="Playlist Name" required="true" maxlength="30">
                </div><br/>
              </div>
            </div>
        </div> 
        <div class="modal-footer">
          <div class="col-sm-5"></div>
          <div class="col-sm-3" style="padding-right: 0px;">
            <button class="btn rounded N_cancel btn-block" style="color: #fff;" onclick="cancelpupdate();">Cancel</button>
          </div>
          <div class="col-sm-4">
            <button class="btn rounded btn-block Pl_save" style="color: #fff;" onclick="submit_plist2();">Save</button>
          </div>
        </div>
      </div>  
    </div>  
  </div>

<!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="plbg modal-content AM-content N_content">
        <div class="modal-header text-center AM-header">
          <h4 class="font-m1 w3-wide w3-xlarge text-white AM-title">READY TO LEAVE?</h4>  
          <a href="#"  class="pull-right AM-close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
          <hr class="AM-hr">
        </div> 
        <div class="modal-body logout_body">
          <div class="row">
            <div class="text-center text-white AM-title logout_text">
              Are you sure you want to Logout?..
            </div>

            <div class="col-sm-12">
              <hr class="AM-hr">
              <div class="col-sm-5"></div>
              <div class="col-sm-3 logout_cancel">
                <button class="btn btn-default rounded btn-block" type="button" data-dismiss="modal">Cancel</button>
              </div>
              <div class="col-sm-4 pull-right" style="padding-top: 10px;">    
                <a class="btn rounded Pl_save btn-block" style="color: #fff;" href="../logout.php" >Logout</a>
              </div>
            </div>
         </div>
        </div>
      </div>
    </div>
  </div>

<!-- Update Profile Picture -->
  <div class="modal fade" role="dialog" id="change_profile_pic" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content pl_radius N_content">
        <div class="modal-header bg-dark text-white Up-header">Change Profile Picture
           <a href="#"  class="pull-right Up_close" data-dismiss="modal">
              <i id="closehover" class="fa fa-times"></i></a>
        </div>
        <div class="bg_img modal-body bg-dark Up_body">
          <input type="file" name="user_img" id="user_img" accept="image/*" style="display: none;">
          <div class="text-center">
            <div id="user_img_preview">
              <img src="<?php echo $_SESSION['user_pic'] ?>" class="img_pp r r-2x img-full">
            </div>
            <div id="btn-up"><br>
              <button class="btn btn-primary" onclick="$('#user_img').click();"><i class="fa fa-image"></i> Change Picture</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Update Name -->
  <div class="modal fade" role="dialog" id="edit_name_modal" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content upn_content N_content">
        <div class="modal-header text-center upn_header">Update Name
         <a href="#" class="pull-right upn_close" data-dismiss="modal">
              <i id="closehover" class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="upn_form" id="form-names">
              <div class="form-group">
                <input type="text" name="fn" id="fn" class="form-control text-center" autocomplete="off" placeholder="First Name">
              </div>
              <div class="form-group">
                <input type="text" name="ln" id="ln" class="form-control text-center" autocomplete="off" placeholder="Last Name">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger rounded" data-dismiss="modal">Cancel</button>
          <button class="btn rounded Pl_save" style="color: #fff;" onclick="update_user('name');">Save Changes</button>
        </div>
      </div>
    </div>
  </div>

<!-- Update Birthday -->
  <div class="modal fade" role="dialog" id="edit_bdate_modal" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content upn_content N_content">
        <div class="modal-header text-center upn_header">Update Birthday
          <a href="#" class="pull-right upn_close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="upn_form" id="form-names">
              <div class="form-group">
                <input type="date" name="bdate" id="bdate" class="form-control" autocomplete="off" >
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn rounded btn-danger" data-dismiss="modal">Cancel</button>
          <button class="btn rounded Pl_save" style="color: #fff;" onclick="update_user('bdate');">Save Changes</button>
        </div>
      </div>
    </div>
  </div>

<!-- Update Email -->
  <div class="modal fade" role="dialog" id="edit_email_modal" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content upn_content N_content ">
        <div class="modal-header text-center upn_header">Update Email
          <a href="#" class="pull-right upn_close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
          <div class="upn_form" id="form-names">
            <div class="form-group">
              <input type="text" name="email" id="email" class="form-control text-center" autocomplete="off" placeholder="Email Address">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn rounded btn-danger" data-dismiss="modal">Cancel</button>
          <button class="btn rounded btn-primary Pl_save" style="color: #fff;" onclick="update_user('email');">Save Changes</button>
        </div>
      </div>
    </div>
  </div>

<!-- Update Password -->
  <div class="modal fade" role="dialog" id="edit_pass_modal" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content upn_content N_content">
        <div class="modal-header text-center upn_header">Update Password
          <a href="#" class="pull-right upn_close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <div class="upn_form" id="form-names">
              <div class="form-group">
                <input type="text" name="o_pwd" id="o_pwd" class="form-control text-center" placeholder="Old Password">
              </div>
              <div class="form-group">
                <input type="Password" name="n_pwd" id="n_pwd" class="form-control text-center" placeholder="New Password">
              </div>
              <div class="form-group">
                <input type="Password" name="c_pwd" id="c_pwd" class="form-control text-center" placeholder="Confirm-Password">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn rounded btn-danger" onclick="cancel_updates();">Cancel</button>
          <button class="btn rounded btn-primary Pl_save" style="color: #fff;" onclick="update_user('pwd');">Save Changes</button>
        </div>
      </div>
    </div>
  </div>

<!-- Album Songs -->
<div class="modal fade animated zoom" role="dialog" id="modal_playlist">
  <div class="modal-dialog modal-lg">
    <div class="modal-content AM-content plbg N_content">
      <div class="modal-header  bg-modal-hd-success  w3-card-4" style="border: none; padding-right: 0px;">
        <div id="myAlbumInfo"></div>
      </div>

      <div class="modal-body">
          <input type="hidden" id="alb_id_modal">
          <div class="row">
            <div class="col-sm-6 pull-right" style="margin-bottom: 15px;">
              <div class="input-group">
                <span class="input-group-btn text-black">
                  <button class="btn bg-white btn-icon rounded" onclick="load_my_album_songs();">
                   <i class="fa fa-search"></i>
                  </button>
                </span>
                <input type="text" oninput="load_my_album_songs();" name="album_songs_" id="album_songs_" class="form-control no-border rounded" placeholder="Search..." autocomplete="off">
              </div>
            </div>

          <div class="col-sm-12">
            <div class="table-responsive">
              <table class="table table-hover">
                 <thead class="text-white thbgcolor">
                    <tr>
                      <input type="hidden" name="sort_id" id="sort_id" value="0">
                      <input type="hidden" name="artist_id" id="artist_id" value="0">
                      <input type="hidden" name="composer_id" id="composer_id" value="0">

                        <th class="thheader" >SONG NAME</th>
                        <th class="thheader">COMPOSER</th>
                        <th class="thheader">STYLE</th>
                        <th class="thheader">APPLICATION</th>
                        <th class="thheader" style="text-align: center;">ACTION</th>
                    </tr>
                 </thead>
                 <tbody id="display_album_audio"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="modal_audio_player" id="modal_audio_player"></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function mdl_play(aud) {
  $("#modal_audio_player").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio2" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player").show('fast');
  }

  function mdl_pause(){
  var voldown = document.getElementById('my_Audio2');
  var now_play = document.getElementById('now_play');
  voldown.pause();
}

function mdl_play2(){
  var voldown = document.getElementById('my_Audio2');
  voldown.play();
}
</script>









<style>
.sa-bg{
  background-image: url('../img/USbg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}
.ml-form-bg{
  background-image: url('../img/main-bg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
  position: fixed;
}
.ml-button{

}

.ml-button2{
  margin-left: -5px;
  padding-left: 15px; 
  font-size: 15px;
  height: 35px;
  margin-bottom: 10px;
}


.ml-padding-left{
  padding-left: 0px;
}

.s-title{
  padding-left: 0px;
  padding-top: 15px; 
  padding-right: 0px;
  padding-bottom: 0px;
}
.s-f-title{
 
  
  padding-bottom: 0px;
}
.s-f-composer{
  
  
  padding-bottom: 0px;
}
.s-desc{
  margin-top: -5px;
  padding-left: 0px;
  padding-right: 0px;
  padding-bottom: 0px;
}
.s-f-descrip{
  
 
  padding-bottom: 0px;
}
.s-f-ksw{
    
 
  padding-bottom: 0px;
}
.ml-table{
  display: table-cell;
  overflow-x: auto;
  width: 100%;
}
@font-face{
    font-family:'MavenPro-Regular';
    src:url('../fonts/MavenPro-Regular.ttf');
  }

.font-m1{
   font-family:'MavenPro-Regular';
   color: white;
   font-size: 1.2em;
   border-width:1.2px;
   border-color: white;
   border-radius:50px;
  }
.panel{
  font-family:'MavenPro-Regular';
  font-size: 1.2em;
}

#pdf_btn{
  font-family:'MavenPro-Regular';
  font-size: 1.2em;
}

#closehover:hover{
  color: #fff;
}
#submitsong2{
  color: #fff; 
  border: solid 1px #fff;
}
#submitsong2:hover{
  border: solid 1px #000;
}

/*#btn-select2:hover{
  background-color: #6610f2;
}

#alb_select:hover{
  background-color: #6610f2;
}*/

.AM-content{
  border-top-left-radius: 20px; 
  border-top-right-radius: 20px; 
  border-bottom-left-radius: 20px; 
  border-bottom-right-radius: 20px;
}
.AM-header{
  border: solid 1px rgba(0,0,0,0.0); 
  border-top-left-radius: 20px; 
  border-top-right-radius: 20px; 
  padding-bottom: 0px;
}
.AM-title{
  text-shadow:2px 2px 4px #555; 
  font-size: 20px; 
  letter-spacing: 2px;
}
.AM-close{
  color: #dad7d7; 
  font-size: 20px; 
  margin-top: -40px;
}
.AM-body{
  
  border-bottom-left-radius: 20px; 
  border-bottom-right-radius: 20px;
}
.AM-hr{
  padding-bottom: 0px; 
  margin-bottom: 0px;
}
.AM-button{
  border-radius: 5px;
  margin-left: -5px;
  padding-left: 15px; 
  font-size: 15px;
  height: 40px;
  margin-bottom: 10px;
  background-color: #292f35;
  font-family: MavenPro-Regular;
}
.AM-button:hover{
  background-color:  #000000;
}
.AM-button1{
   border-top-left-radius: 5px; 
   border-top-right-radius: 5px; 
   border-bottom-left-radius: 5px; 
   border-bottom-right-radius: 5px;
}
.AM-padding{
  padding-right: 0px; 
  padding-left: 2px;
}
.AM-style{
  color: #fff; 
  text-shadow:1px 1px 3px #555; 
  background-color: #585858;
  border-top-left-radius: 5px; 
  border-top-right-radius: 5px; 
  font-weight: bold; 
  letter-spacing: 1px;
  margin-bottom: 0px;
}
.AM-form{
  padding-right: 2px; 
  padding-left: 0px;
}
.AM-input1{
  height: 40px; 
}
.AM-input2{
  height: 80px; 
}
.AM-style2{
  border: solid 1px rgba(0,0,0,0.0);
  background-color: #fff; 
  border-bottom-left-radius: 5px; 
  border-bottom-right-radius: 5px;
}
.AM-mbottom{
  margin-bottom: 15px;
}
.AM-sf{
  margin-top: 13px; 
  padding-left: 0px; 
  padding-right: 0px;
}
.AM-date1{
  border-top-left-radius: 5px; 
  border-bottom-left-radius: 5px; 
  font-family: MavenPro-Regular;
}
.AM-date2{
  height: 35px;  
  border-top-right-radius: 5px; 
  border-bottom-right-radius: 5px; 
  font-family: MavenPro-Regular;
}

.PL-head{
  border: solid 1px rgba(0,0,0,0.0); 
  border-top-left-radius: 20px; 
  border-top-right-radius: 20px; 
  background:rgba(0,0,0,0.0); 
  padding-bottom: 0px;
}
.PL-title{
  text-shadow:2px 2px 4px #555; 
  font-size: 25px; 
  letter-spacing: 2px;
}
.Style-close{
  color: #dad7d7; 
  font-size: 20px; 
  margin-top: -5px;
}
.ALB-close{
  color: #dad7d7; 
  font-size: 20px; 
  margin-top: 5px;
  padding-right: 10px;
}
.ALB2-close{
  color: #dad7d7; 
  font-size: 20px; 
  margin-top: -50px;
}
</style>

<?php
include('../dbconnection.php');
$today = date('Y-m-d');
$my_id = $_SESSION['users'];
?>
<!-- Add Musics -->

<div class="modal fade" data-backdrop="static" id="addmusic">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="background-color: #6C7A89;">
      <div class="modal-header">
        <h5 class="modal-title">ADD NEW MUSIC</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="songid">
        <input type="hidden" id="music_path">
        <input type="hidden" id="duration">

        <div class="col-sm-12 row">
          <div class="col-sm-6">
            <button class="btn btn-primary btn-block" style="color: white;" id="btn-select2" onclick="$('#uploaded_song2').click();"><i class="fa fa-music fa-lg" ></i> &nbsp;Select Song
            </button>
          </div>
          <div class="col-sm-6">
             <button class="btn btn-primary btn-block" id="alb_select" onclick="addto_album();" style="color: white;">
                <i class="fa fa-folder-o"></i> &nbsp;Album 
              </button>
          </div>
          <input type="hidden" name="selectedalb_id" id="selectedalb_id">
        </div>
        <br>
        <div class="col-sm-12 row" >
            <div class="form-group col-sm-6">
              <input type="text" name="song_name2" id="song_name2" class="form-control AM-input1" placeholder="Select Title" required maxlength="30">
            </div>
            <div class="form-group col-sm-6">
              <input type="text" name="composer2" id="composer2" class="form-control AM-input1" placeholder="Composer" required maxlength="30">
            </div>
        </div>
        <div class="col-sm-12 row">
            <div class="form-group col-sm-6">
              <textarea class="form-control parsley-validated img-responsive AM-input2" name="description" id="description" rows="4" data-minwords="6" data-required="true" placeholder="Description"></textarea>
            </div>
            <div class="form-group col-sm-6">
              <textarea class="form-control parsley-validated img-responsive AM-input2" name="keyword" id="keyword" rows="4" data-minwords="6" data-required="true" placeholder="Key search words"></textarea>
            </div>
        </div>
        <div class="col-sm-12" style="margin: 0 auto; margin-bottom: 10px;">
         <input type="hidden" id="style_value">
         <header class="panel panel-heading AM-style">STYLE</header>
            <div class="form-group" style="margin: 0 auto; ">
                <div class="col-sm-12 AM-style2" style=" margin: 0 auto; " id="style_select"></div>

            </div>
        </div>
        <div class="col-sm-12">
          <input type="hidden" id="app_value">
         <header class="panel panel-heading AM-style">APPLICATION</header>
            <div class="form-group">
                <div class="col-sm-12 AM-style2" id="app_select"></div>
            </div>
        </div>
        <div class="col-sm-12 AM-mbottom">
                <header class="panel panel-heading AM-style">SUPPORTING FILES</header>
                  <div class="col-sm-12 row">
                    <div class="col-sm-5 AM-sf">
                      <button class="btn btn-primary btn-block" style="color: #fff;" id="btn_select" onclick="$('#multi_upload_file').click();">
                        &nbsp;Select Files</button>
                        <input type="file" multiple name="multi_upload_file[]" id="multi_upload_file" style="display: none;">
                        <input type="hidden" name="group_id" id="group_id">
                    </div>  
                    <div class="col-sm-2"></div> 
                    <div class="col-sm-5 AM-sf"> 
                      <div class="form-group">
                        <div class="input-group">
                          <span class="btn input-group-addon bg-info text-white AM-date1" style="background-color: #292f35; color: #fff;">Available from:</span>
                          <input type="date" class="input-md input-s datepicker-input form-control AM-date2" name="date_upload" id="date_upload" size="16">
                        </div>
                     </div>
                    </div>
                  </div>

                  <div class="col-sm-12 AM-style2" style="overflow-y: auto; height:280px; color: #000; margin-top: 10px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <table class="table table-responsive table-hover table-sm" style="">
                      <thead style="background-color: #212529; border-color: #32383e; color: #fff;">
                        <tr>
                          <th style="" class="col-sm-2">File Name</th>
                          <th style="" class="col-sm-2">File Description</th>
                          <th style="" class="col-sm-2">Link Name</th>
                          <th style="" class="col-sm-2">Link</th>
                          <th style=" text-align: center; " class="col-sm-2">Task</th>
                        </tr>
                      </thead>
                      <tbody id="display_supporting_files" >
                        
                      </tbody>
                    </table>
                  </div>
              </div>

              <input type="file" name="uploaded_song2" id="uploaded_song2" style="display: none;" /> 
              <div class="col-sm-12 text-center" id="song_preview2" style="padding-left: 0px; padding-right: 0px;"></div>



      <div class="col-sm-12" style="padding-top: 15px;">
        <hr>
        <button class="btn btn-lg btn-primary btn-block" disabled="" name="submitsong2" id="submitsong2" onclick="submitsong2();" >Save</button>
      </div>

        </div> 
      </div>
    </div>
  </div>
</div>


<style>
  .SF-con{
    border-top-left-radius: 20px; 
    border-top-right-radius: 20px; 
    border-bottom-left-radius: 20px; 
    border-bottom-right-radius: 20px;
  }
  .SF-head{
    border: solid 1px rgba(0,0,0,0.0); 
    border-top-left-radius: 20px; 
    border-top-right-radius: 20px; 
    background:rgba(255,255,255,0.15); 
    padding-bottom: 0px;
  }
  .SF-title{
    text-shadow:2px 2px 4px #555; 
    font-size: 20px; 
    letter-spacing: 2px;
  }
  .SF-close{
    color: #dad7d7; 
    font-size: 20px; 
    margin-top: -40px;
  }
  .SF-hr{
    padding-bottom: 0px; 
    margin-bottom: 0px;
  }
  .SF-body{
    background:rgba(255,255,255,0.15);  
    border-bottom-left-radius: 20px; 
    border-bottom-right-radius: 20px;
  }
  .SF-action{
    padding-right: 0px; 
    margin-top: 20px;
  }
  .SF-button{
    margin-top: 15px;
  }
  #M-files{
    font-family:'MavenPro-Regular';
    font-size: 1.2em;
  }
</style>

<div id="select_file_modal" class="modal fade" data-backdrop="static">  
  <div class="modal-dialog modal-lg">  
    <div class="modal-content sa-bg SF-con N_content">  
      <div class="modal-header text-center SF-head">
        <h4 class="font-m1 w3-wide w3-xlarge text-white SF-title">SUPPORTING FILES</h4>  
        <a href="#"  class="pull-right SF-close" data-dismiss="modal" onclick="cancel_select_files();">
          <i id="closehover" class="fa fa-times"></i></a>
        <hr class="SF-hr">
      </div> 
          <div class="modal-body SF-body">   
            <div class="row">
              <div class="col-sm-12" style="margin-bottom: 5px;">
                 <div class="col-sm-4" style="padding-left: 0px;">
                    <button class="btn btn-info rounded btn-block" id="pdf_btn" onclick="$('#multi_upload_file').click();" style="height: 36px;">Select File</button>
                    <!-- <input type="file" name="upload_file" id="upload_file" style="display: none;"> -->
                    <input type="file" multiple name="multi_upload_file[]" id="multi_upload_file" style="display: none;">
                  </div>
                </div> 
                <!-- <div class="col-sm-12" style="margin-bottom: 5px;">
                 <div class="col-sm-4" style="padding-left: 0px;">
                    <button class="btn btn-info rounded btn-block" id="pdf_btn" onclick="$('#upload_file').click();" style="height: 36px;">Select File</button>
                    <input type="file" multiple name="upload_file" id="upload_file" style="display: none;">
                  </div>
                </div>  -->
                 <!--  <div class="col-sm-12"  style="">
                    <div class="input-group" id="" style="padding-left: 0px;"></div>
                  </div> -->
        <div class="col-sm-12">
          <div id="content" >
            <div class="vbox" >
               <div class="scrollable w-f-md" style="">
                  <div class="hbox stretch dker">
                    <aside class="col-sm-5 no-padder " id="sidebar">
                      <div class=" vbox animated fadeInUp"><br>
                        <div class="center bg-dark dker scroll" id="file_preview" style="background-color: rgba(0,0,0,0.10); height: 400px;"></div>
                      </div>
                    </aside>
                  </div>
                </div>
            </div>
          </div>
          </div>
             <!--  <div class="col-sm-12" style="margin-bottom: 5px;">
                <div class="col-sm-4 pull-right" style="padding-left: 0px;">
                  <button class="btn btn-info rounded bg-info btn-block" id="M-files" onclick="$('#multiple_files').click();" style="height: 36px;">Select File</button>
                  <input type="file" style="display: none;" name="multiple_files" id="multiple_files" multiple />
                </div>
              </div>

              <div class="table-responsive" id="file_table">
                
              </div> -->

              <!-- <div class="col-sm-12" style="margin-bottom: 20px;">
                 <div class="col-sm-2 pull-left" style="padding-left: 0px;">
                  <button class="btn btn-primary btn-block" id="word_btn" onclick="$('#upload_word').click();" style="height: 36px;">Word</button>
                  <input type="file" name="upload_word" id="upload_word" style="display: none;">
                  </div>
                  <div class="col-sm-10" style=" padding-left: 0px; padding-right: 0px;">
                    <div class="input-group" id="word_prev" style="padding-left: 0px;"></div>
                  </div>
              </div>
 -->
              

              <!-- <div class="col-sm-12" style="margin-bottom: 5px;">
                  <div class="col-sm-3" style=" padding-top: 5px; padding-right: 0px;">
                    <label class="text-black" style="font-size: 16px; padding-right: 0px;">Link Name:</label>
                  </div>
                  <div class="col-sm-9" style=" padding-left: 0px;">
                    <input type="text" name="link_name" id="link_name" class="form-control" placeholder="Enter Link Name">
                  </div>
              </div>    
              <div class="col-sm-12" style="margin-bottom: 5px;">
                  <div class="col-sm-3" style=" padding-top: 5px; padding-right: 0px;">
                    <label class="text-black" style="font-size: 16px; padding-right: 0px;">Video Link:</label>
                  </div>
                  <div class="col-sm-9" style=" padding-left: 0px;">
                    <input type="text" name="s_link" id="s_link" class="form-control" placeholder="Enter Link Here">
                  </div>
              </div> -->
                 
            <div class="col-sm-12 SF-action">
              <hr class="SF-hr">
              <div class="col-sm-6"></div>
              <div class="col-sm-3 SF-button">
                <button class="btn btn-danger btn-block rounded" data-dismiss="modal" onclick="cancel_select_files();"> Cancel</button>     
              </div>
              <div class="col-sm-3 pull-right SF-button">
                <button class="btn btn-success btn-block rounded" onclick="save_sfiles();"> Save</button>      
              </div>
            </div>

          </div>
          
          


          </div>  
      </div>  
    </div>  
  </div>

<div id="EditFileModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content AM-content" style="background-color: #6C7A89;">
      <div class="modal-header text-center AM-header">
        <h4 class="font-m1 w3-wide w3-xlarge text-white AM-title">Edit Supporting File</h4>  
        <a href="#"  class="pull-right AM-close" data-dismiss="modal" onclick="close_edit();">
          <i id="closehover" class="fa fa-times"></i></a>
        <hr class="AM-hr">
      </div>
        <div class="modal-body">
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">File Name:</label>
            <input type="text" name="file_name" id="file_name" placeholder="File Name" class="form-control" />
          </div>
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">File Description:</label>
            <input type="text" name="file_description" id="file_description" placeholder="File Description" class="form-control" />
          </div>
           <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">Link Name:</label>
            <input type="text" name="link_name" id="link_name" placeholder="Link Name" class="form-control" />
          </div>
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">Link:</label>
            <input type="text" name="link" id="link" placeholder="Link" class="form-control" />
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="old_name" id="old_name"/>
          <input type="hidden" name="edit_action" id="edit_action"/>
          <input type="hidden" name="file_id" id="file_id"/>
          <input type="hidden" name="file_ext" id="file_ext"/>
          <button type="button" class="btn rounded N_button" style="font-family: MavenPro-Regular; color: #fff;" onclick="edit_file();">Update</button>
        </div>
     
    </div>
  </div>
</div>


<div class="modal fade" id="select_album_modal" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">SELECT ALBUM</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-4" style="padding-right: 0px; margin-bottom: 5px;">
                    <button class="btn btn-success rounded" onclick="create_album();">
                      <i class="fa fa-plus"></i> &nbsp;Create New Album
                    </button>
                  </div>
                  <div class="col-sm-6 pull-right" style="padding-right: 0px; margin-bottom: 5px;">
                    <div class="form-group">
                      <div class="input-group">
                        <span class="btn btn-default btn-sm input-group-addon bg-default text-white rounded" onclick="load_albums();">
                          <i class="fa fa-search"></i></span>
                        <input type="text" oninput="load_albums();" name="searchalbums" id="searchalbums" class="rounded form-control" placeholder="Search..." autocomplete="off" style="font-family: serif;">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div id="tbl_album_select">
                    
                  </div>
                </div>

          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" onclick="cancel_select();"> Cancel</button>
      </div>
    </div>
  </div>
</div>

  <div id="createAlbumModal" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog">  
      <div class="modal-content sa-bg AM-content N_content">  
        <div class="modal-header text-center SF-head">
          <h4 class="font-m1 w3-wide w3-xlarge text-white SF-title">CREATE NEW ALBUM</h4>  
          <a href="#"  class="pull-right SF-close" data-dismiss="modal" onclick="cancelcreate();">
            <i id="closehover" class="fa fa-times"></i></a>
          <hr class="SF-hr">
        </div>    
          <div class="modal-body AM-body">         

            <div id="image_preview2" class="text-center"> </div> <br/>
            <input type="file" name="album_image2" id="album_image2" style="display: none;" /> 
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <input type="text" name="album_name2" id="album_name2" class="form-control text-capitalize" placeholder="Album Name" required="true" maxlength="30" style="text-transform: capitalize;">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="">
                  <button class="btn btn-primary" id="btn-chooser2" onclick="$('#album_image2').click();"> 
                    <i class="fa fa-file fa-lg"></i> Select Image</button>
                </div>
              </div>
              <div class="col-sm-1"></div>

            <div class="col-sm-12">
              <hr class="AM-hr">
              <div class="pull-right" style="padding-top: 10px;">
                <button class="btn bg-black pull-right rounded font-m1" onclick="submit_album2();">Save Album</button>
                <button class="btn btn-danger  rounded font-m1" data-dismiss="modal" onclick="cancelcreate();"> Cancel</button>  
              </div>
            </div>

            </div>
          </div>  
      </div>  
    </div>  
  </div>


<style>
  .infoinput{
    padding-left: 10px; 
    width: 70%; 
    border: none; 
    text-transform: capitalize;
    color: black;
    background-color: white;
    font-size: 17px;
    width: 50%;
  }
  .infohead{
    font-size: 18px;
    padding-left: 15px;
  }
  .infoItem{
    padding-left: 45px;
    font-size: 17px;
    width: 40%;
  }
</style>



<div class="modal fade" role="dialog" id="info_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="text-info text-left infohead"><b>Details</b></div>
          <input type="hidden" id="info_id"><br>

          <div class="col-sm-12">
            <label class="infoItem">Title:</label>
            <span>
              <input type="text" disabled  id="song_name_info" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12">
            <label class="infoItem">Composer:</label>
            <span>
              <input type="text" disabled id="composer_info" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12">
            <label class="infoItem">Style:</label>
            <span>
              <input type="text" disabled id="genre_info" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12">
            <label class="infoItem">Application:</label>
            <span>
              <input type="text" disabled id="info_app" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12">
            <label class="infoItem">Album:</label>
            <span>
              <input type="text" disabled id="album_info" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12">
            <label class="infoItem">Duration:</label>
            <span>
              <input type="text" disabled id="info_duration" class="text-left infoinput">
            </span>
          </div><br/>

          <div class="col-sm-12" >
            <label class="infoItem">Date Uploaded:</label>
            <span >
              <input type="text" disabled id="ud_info" class="text-left infoinput">
            </span>
          </div><br><br><br>
        </div>
        
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" role="dialog" id="info_modal2">
  <div class="modal-dialog">
    <div class="modal-content N_content">
      <div class="modal-body">
        <div class="text-info text-left" style="font-size: 18px;"><b>Details</b></div>
        <input type="hidden" name="" id="info_id2">
      <br>
          <div class="col-sm-12 text-black">
            <label>Title:</label><span><input type="text" disabled  name="" id="song_name_info2" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/>

          <div class="col-sm-12 text-black">
            <label>Artist:</label><span><input type="text" disabled name="" id="artist_info2" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/>

          <div class="col-sm-12 text-black">
            <label>Composer:</label><span><input type="text" disabled name="" id="composer_info2" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/>

          <div class="col-sm-12 text-black">
            <label>Album:</label><span><input type="text" disabled name="" id="album_info2" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/>

          <div class="col-sm-12 text-black">
            <label>Genre:</label><span><input type="text" disabled name="" id="genre_info2" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/>

          <!-- <div class="col-sm-12 text-black">
            <label>Time:</label><span><input type="text" disabled name="" id="time_info" class="text-left pull-right bg-white text-black" style="padding-left: 10px; width: 70%; border: none;"></span>
          </div><br/> -->

          <div class="col-sm-12 text-black">
            <label>Date Uploaded:</label><span><input type="text" name="" id="ud_info2" class="text-left pull-right text-black" style="padding-left: 10px; border: none; width: 50%;"></span>
          </div><br><br><br>

        </div>
      </div>
    </div>
  </div>


  <style>
    .plcontent{
      border-bottom-left-radius: 20px; 
      border-bottom-right-radius: 20px; 
      border-top-right-radius: 20px; 
      border-top-left-radius: 20px;
    }
    #playlist_header{
      border-top-right-radius: 20px; 
      border-top-left-radius: 20px;
    }
    .plfooter{
      padding-top: 5px; 
      height: 65px; 
      margin-top: -3px; 
      border-bottom-left-radius: 20px; 
      border-bottom-right-radius: 20px; 
      background-color: rgba(0,0,0,0.0);
    }
    #tbl_plist_songs{
      background-color: #13284eb8; 
      height: 340px;
    }
    #plist_song_search{
      font-family: serif;
    }
    .plgroup{
      margin-bottom: 3px;
    }
  </style>


  <div class="modal fade" role="dialog" id="playlistsongsmodal" data-backdrop="static">
  <div class="modal-dialog modal-lg"> 
    <div class="plbg modal-content plcontent N_content">
      <div id="playlist_header"></div>
       <!--  <input disabled type="hidden" name="" id="modal_pl_title" > -->
        <input type="hidden" id="plistid">
        <div class="modal-body">
          <div class="col-sm-12">       
            <div class="btn-group col-sm-4 plgroup">
              <button class="btn btn-info rounded"  onclick="addplistsongs(); remove_player(); $('#newplistid').val(document.getElementById('plistid').value);"><i class="fa fa-plus-circle"></i> &nbsp; Add Songs</button>
              <button class="btn btn-danger rounded" onclick="empty_playlist();"><i class="fa fa-trash-o"></i> &nbsp; Empty Playlist</button>
            </div>
            <div class="col-sm-8">
              <div class="input-group">
                <span class="input-group-btn text-black">
                  <button class="btn bg-white btn-icon rounded" onclick="load_playlist_songs();">
                   <i class="fa fa-search"></i>
                  </button>
                </span>
                <input type="text" oninput="load_playlist_songs();" name="plist_song_search" id="plist_song_search" class="form-control no-border rounded" placeholder="Search..." autocomplete="off">
                <span class="input-group-btn">
                  <button class="btn bg-info btn-icon rounded" onclick="click_all();" id="download-btn" title="Download All Songs">
                    <i class="fa fa-download"></i>
                  </button>
                </span>
              </div>
            </div>
          </div>
          <div id="content">
            <div class="vbox">
               <div class="scrollable w-f-md">
                  <div class="hbox stretch dker">
                    <aside class="col-sm-5 no-padder " id="sidebar">
                      <div class=" vbox animated fadeInUp"><br>
                        <div class="center bg-dark dker scroll" id="tbl_plist_songs"></div>
                      </div>
                    </aside>
                  </div>
                </div>
            </div>
          </div>


              <div class="music-card" style="margin-top: 10px; display: none;">

                <div id="jp_container_N">
                                 <div class="jp-type-playlist">
                                    <div id="jplayer_N" class="jp-jplayer hide"></div>
                                    <div class="jp-gui">
                                       <div class="jp-video-play hide"> 
                                          <a class="jp-video-play-icon">play</a> 
                                       </div>
                                       <div class="jp-interface">
                                          <div class="jp-controls">
                                             <div>
                                                <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg"></i></a>
                                             </div>
                                             <div> 
                                              <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="icon-control-play i-lg"></i></a>
                                             </div>
                                             <div>
                                                <a href="#prev" onclick="next_aud();" class="btn-musics">
                                                  <i class="fa fa-forward fa-lg "></i>
                                                </a>
                                             </div>
                                             <div>
                                                <a href="#prev" onclick="remove_player();" class="btn-musics"><i class="fa fa-stop i-lg "></i>
                                                </a>
                                             </div>
                                              <div>
                                                <span id="album_artwork" class="img-artwork"></span>
                                              </div>
                                             <div class="jp-progress hidden-xs">
                                                <div class="jp-seek-bar dk">
                                                  <div class="jp-play-bar" style="background-color: #13284eb8; padding-top: 8px;">
                                                    <span id="music_title" class="animated pulse" style="text-transform: uppercase; font-family: MavenPro-Regular; color: white;"></span>
                                                    <label id="song_id" style="display: none;"></label>
                                                    
                                                   </div>
                                                   <div class="jp-title text-lt">
                                                    <!-- Seekbar -->
                                                    <div class="seek-bar">
                                                      <input type="range" min="0"  max="100" class="player_slider" onmousemove="move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'))" onmouseup=" move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">
                                                    </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="hidden-xs hidden-sm jp-current-time text-xs text-white">
                                               <b><span id="counter_strike">00:00</span></b>/
                                             </div>
                                             <div class="hidden-xs hidden-sm jp-duration text-xs text-white">
                                               <b><span id="timer_audio">00:00</span></b>
                                             </div>
                                             <div > 
                                                <div class="volume-control">
                                                    <a href="#vol" class="text-white"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg"></i></a>
                                                  </div>
                                             </div>
                                             <div class="hidden-xs hidden-sm jp-volume text-white">
                                                <div class="jp-volume-bar ">
                                                   <div class="jp-volume-bar-value lter"><input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);"></div>
                                                </div>
                                             </div>                                            
                                          </div>
                                       </div>
                                    </div>
                                    <div class="jp-playlist dropup" id="playlist">
                                       <ul class="dropdown-menu aside-xl dker">
                                          <!-- The method Playlist.displayPlaylist() uses this unordered list --> 
                                          <li class="list-group-item"></li>
                                       </ul>
                                    </div>
                                    <div class="jp-no-solution hide"> 
                                       <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your 
                                       <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>. 
                                    </div>
                                 </div>
                              </div>
                              <div id="music_audio" style="width: 100%;">
                            <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
                              Sorry, your browser does not support audio
                            </audio>
                            <div id="show_progress"></div>
                          </div>
                
              </div>

        </div>

        <!-- <div class="modal-footer plfooter">
           <div  id="modal_audio_player6"></div>
        </div> -->






    </div>
  </div>
</div>

<script type="text/javascript">
  function click_all(){
var buttons = document.getElementsByName('dl_playlist_');
for(var i = 0; i <= buttons.length; i++)  
   buttons[i].click();
}

function stopplay7(){
  $("#modal_audio_player6").html('');
  }
  function mdl_play7(aud) {
  $("#modal_audio_player6").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio7" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player6").show('fast');
  }
</script>

  <div id="addplistsongs" class="modal fade" data-backdrop="static" style="margin-top: -15px;">  
    <div class="modal-dialog modal-lg">  
      <div class="plbg modal-content N_content" style="border-top-right-radius: 20px; border-top-left-radius: 20px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;">  
        <div id="plist_title"></div>
        <input type="hidden" name="" id="plistmodaltitle">
        
          <div class="modal-body">    
            <div class="col-sm-12">
              <div class="col-sm-4">
                <select class="btn btn-info rounded col-sm-12" style="font-family: MavenPro-Regular;" id="select_style" onchange="load_tbl_audios();" oninput="load_tbl_audios();"></select> 
              </div>
              <div class="col-sm-8">
                <div class="form-group">
                <div class="input-group">
                  <span class="input-group-btn text-black">
                  <button class="btn bg-white btn-icon rounded" onclick="load_tbl_audios();">
                  <i class="fa fa-search"></i></button></span>
                  <input type="text"  oninput="load_tbl_audios();" name="splist" id="splist" class="form-control no-border rounded" placeholder="Search..." autocomplete="off" style="font-family: serif;">
                </div>
                </div>
              </div>
            </div>

              <div class="row">
                <div class="col-sm-12"> 
                  <input type="hidden" name="playlist_id" id="newplistid">
                  <div id="plistcbox" class="scroll bg-dark dker" style="height: 390px;"></div><br>
                </div>

                <div class="col-sm-12">
                  <div class="col-sm-6"></div>
                  <button class="btn btn-danger col-sm-3 rounded font-m1" data-dismiss="modal" onclick="stopplay8();"> Cancel</button>
                  <!-- <button type="submit" class="btn btn-success col-sm-3 rounded" onclick="savemulti();"> Save</button> -->
                  <button class="btn bg-black col-sm-3 rounded font-m1" onclick="savemulti();">Save</button>
                </div>
              </div>


            </div>  

            <div class="modal-footer" style="padding-top: 5px; height: 65px; margin-top: -3px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; background-color: rgba(0,0,0,0.0);">
           <div  id="modal_audio_player7"></div>
        </div>

        <!-- <div class="modal-footer bg-black" style="height: 65px; margin-top: -3px;">
            <div  id="modal_audio_player7" style="margin-top: -14px;"></div>
           <button class="btn btn-success" name="submit" onclick="addmultiplesongs();">Select All</button>
        </div> --> 

      </div>  
    </div>  
  </div>






<script type="text/javascript">
  function stopplay8(){
    $("#playlistsongsmodal").modal('show');
    $("#modal_audio_player7").html('');
    }
    function mdl_play8(aud) {
    $("#modal_audio_player7").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio8" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
    $("#modal_audio_player7").show('fast');
    }
</script>


  


<style>
  .Contri-header{
  border: solid 1px rgba(0,0,0,0.0); 
  border-top-left-radius: 20px; 
  border-top-right-radius: 20px; 
  padding-bottom: 0px;
}
</style>

  

<div class="modal fade" role="dialog" id="viewSongsModal" data-backdrop="static">
  <div class="modal-dialog modal-lg"><input type="hidden" id="c_id">
    <div class="contribg modal-content AM-content N_content">
      <div class="pull-right m-l"> 
       <!--  <button type="button" class="close text-white" data-dismiss="modal" onclick="stopplay5();" style="margin-top: 1px; margin-right: 5px;">&times;</button> -->
       
      </div>
        <div id="contri_info"></div>

        <div class="doc-buttons col-sm-5" style="margin-top: 7px; margin-left: 10px;"> 
          <button type="button" class="btn btn-s-md btn-success col-sm-5 rounded" onclick="con_songs();" id="btnconsong">
            <i class="fa fa-music"></i> Songs</button> 
          <button type="button" class="btn btn-s-md btn-default col-sm-5 rounded" onclick="con_albums(); stopplayer();" id="btnconalbum" style="margin-left: 5px;"><i class="icon-grid"></i> Albums</button> 
        </div> 

        <div class="col-sm-6 pull-right" style="margin-top: 7px; margin-right: 3px;" id="songsearch">
          <div class="form-group">
            <div class="input-group">
              <span class="btn btn-primary input-group-addon bg-primary text-white rounded" onclick="load_contri_songs();"><i class="fa fa-search"></i></span>
                <input type="text" oninput="load_contri_songs();" name="consongs" id="consongs" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: serif;">
            </div>
          </div>
        </div>

        <div class="col-sm-6 pull-right" style="margin-top: 7px; margin-right: 3px; display: none;" id="albumsearch">
          <div class="form-group">
            <div class="input-group">
              <span class="btn btn-primary input-group-addon bg-primary text-white rounded" onclick="load_contri_albums();"><i class="fa fa-search"></i></span>
                <input type="text" oninput="load_contri_albums();" name="conalbums" id="conalbums" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: serif;">
            </div>
          </div>
        </div>

        <br/>
        <div class="modal-body"> 

          <div id="content" >
            <div class="vbox">
               <div class="scrollable w-f-md"style="border: solid 1px rgba(0,0,0,0.0); ">
                  <div class="hbox stretch dker">
                    <aside class="col-sm-5 no-padder " id="sidebar">
                      <div class=" vbox animated fadeInUp"><br>
                        <div class="center bg-dark dker scroll" id="contributor_songs" style="background-color: #13284eb8; height: 400px;"></div>
                        <div id="contributor_albums" class=" center bg-dark dker animated fadeInRight" style="display: none; height: 400px;"></div>
                                 <div id="conalbum_songs" class="center bg-dark dker animated fadeInRight"></div>

                      </div>
                    </aside>
                  </div>
                </div>
            </div>
          </div>

          <!-- 
              <div id="content">
                  <div class="vbox">
                     <div class="w-f-md">
                        <div class="hbox stretch dker">
                           <aside class="col-sm-5 no-padder" id="sidebar">
                              <div class="vbox animated fadeInUp"><input type="hidden" name="con_aid" id="con_aid">
                                <input type="hidden" name="con_alb_name" id="con_alb_name"><br>

                                 <div id="contributor_songs" class="center bg-dark dker animated fadeInLeft" style=" height: 400px; background-color: #13284eb8;"></div>
                                 <div id="contributor_albums" class=" center bg-dark dker animated fadeInRight" style="display: none; height: 400px;"></div>
                                 <div id="conalbum_songs" class="center bg-dark dker animated fadeInRight"></div>
                              </div>
                            </aside>
                        </div>
                      </div>
                  </div>
              </div> -->
        </div>
        
        <div class="modal-footer" style="padding-top: 5px; height: 65px; margin-top: -3px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; background-color: rgba(0,0,0,0.0);">
           <div  id="modal_audio_player4"></div>
        </div>
        <!-- <div class="modal-footer bg-black" style=" height: 85px;">
          <div  id="modal_audio_player4"></div>
        </div> -->

    </div>
  </div>
</div>
<script type="text/javascript">
  function stopplayer(){
  $("#modal_audio_player4").html('');
  }

  function mdl_play5(aud) {
  $("#modal_audio_player4").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio5" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player4").show('fast');
  }

  function mdl_pause(){
  var voldown = document.getElementById('my_Audio5');
  var now_play = document.getElementById('now_play');
  voldown.pause();
}

function mdl_play2(){
  var voldown = document.getElementById('my_Audio5');
  voldown.play();
}
</script>








  <!--<div class="modal fade animated zoom" role="dialog" id="modal_playlist2" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class=" contribg modal-content AM-content N_content">
        
        <div id="display_album_audio2" ></div>

      <div class="modal-body">
        <input type="hidden" id="ab-id">
          <div class="col-sm-6 pull-right" style="margin-top: 7px; margin-right: 3px;">
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-btn text-black" >
                  <button class="btn  bg-primary btn-icon rounded" onclick="p_music_list();">
                  <i class="fa fa-search"></i></button></span>
                <input type="text" oninput="p_music_list();"  name="search_song" id="search_song" class="form-control no-border rounded" placeholder="Search..." autocomplete="off" style="font-family: serif;">
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-sm-12">
              <div class="table-responsive scroll" style="height: 400px;" >
                <table class=" table dker" style="background-color: rgba(0,0,0,0.15);">
                  <thead class="bg-primary" style="font-size: 18px;">
                    <tr>
                      <th>Song Name</th>
                      <th>Composer</th>
                      <th>Style</th>
                      <th>Application</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="display_musics"  class="text-white">
                    
                  </tbody>
                  
                </table>
              </div>
            </div>
        </div>
      </div>

        <div class="modal-footer" style="padding-top: 5px; height: 65px; margin-top: -3px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; background-color: rgba(0,0,0,0.0);">
           <div  id="modal_audio_player5"></div>
        </div>


        </div>
      </div>
    </div> -->

<script type="text/javascript">
  function stopplay5(){
  $("#modal_audio_player5").html('');
  }

  function mdl_play6(aud) {
  $("#modal_audio_player5").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio6" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player5").show('fast');
  /*$("#pauseconsong").show('fast');
  $("#playconsong").hide('fast');*/
  }

  function mdl_pause6(){
  var voldown = document.getElementById('my_Audio6');
  var now_play = document.getElementById('now_play');
  voldown.pause();
   /*$("#playconsongs").show('fast');
   $("#pauseconsong").hide('fast');*/

}

function mdl_play6a(){
  var voldown = document.getElementById('my_Audio6');
  voldown.play();
  /*$("#playconsongs").hide('fast');
   $("#pauseconsong").show('fast');
*/}
</script>




  <div class="modal fade animated zoom" role="dialog" id="albumsong_modal" data-backdrop="static">
    <div class="modal-dialog modal-lg">
      <div class="albumbg modal-content AM-content N_content">

        <input type="hidden" name="" id="album_song_id">
        <a href="#"  class="pull-right ALB-close" data-dismiss="modal" onclick="stopplay4();">
                  <i id="closehover" class="fa fa-times"></i></a>
          <div id="conalbum_info"></div>
             
          <div class="col-sm-6 pull-right" style="margin-top: 7px; margin-right: 3px;">
            <div class="form-group">
              <div class="input-group">
                <span class="btn btn-primary input-group-addon bg-primary text-white rounded" onclick="showalbumsongs();"><i class="fa fa-search"></i></span>
                  <input type="text" oninput="showalbumsongs();" name="search_album_songs" id="search_album_songs" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: serif;">
              </div>
            </div>
          </div>

            <div class="modal-body"> 

          <div id="content">
            <div class="vbox">
               <div class="scrollable w-f-md" style="border: solid 1px rgba(0,0,0,0.0);">
                  <div class="hbox stretch dker">
                    <aside class="col-sm-5 no-padder " id="sidebar">
                      <div class=" vbox animated fadeInUp"><br>
                        <div class="center bg-dark dker scroll" id="album_song" style="background-color: #13284eb8; height: 400px;"></div>
                      </div>
                    </aside>
                  </div>
                </div>
            </div>
          </div>
        </div>

        <div class="modal-footer" style="padding-top: 5px; height: 65px; margin-top: -3px; border-bottom-left-radius: 20px; border-bottom-right-radius: 20px; background-color: rgba(0,0,0,0.0);">
           <div  id="modal_audio_player3"></div>
        </div>
          <!-- <div class="modal-footer bg-black" style=" height: 85px;">
               <div id="modal_audio_player3"></div>
        </div> -->

      </div>
    </div>
  </div>

<script type="text/javascript">
  function stopplay4(){
  $("#modal_audio_player3").html('');
  }

  function mdl_play4(aud) {
  $("#modal_audio_player3").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio4" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player3").show('fast');
  }

  function mdl_pause(){
  var voldown = document.getElementById('my_Audio4');
  var now_play = document.getElementById('now_play');
  voldown.pause();
}

function mdl_play2(){
  var voldown = document.getElementById('my_Audio4');
  voldown.play();
}
</script>







  <div class="modal fade" role="dialog" id="album_audio_modal" data-backdrop="static" style="margin-top: -5px;">
  <div class="modal-dialog" style="width: 70%;">
    <div class="stylebg modal-content N_content" style="height: 660px;">
      <div class="modal-header text-left" style="background-color:rgba(0,0,0,0.4);">
        <!-- <input type="disabled" disabled class="stylename" name="" id="stylename">  -->
        <button class="cp_close close" data-dismiss="modal">&times;</button>
      </div>
        <!-- <input type="hidden" name="" id="current_loc"> -->
            <div class="modal-body" id="load_album_audio"></div>



        <div class="modal-footer" style="background-color: rgba(0,0,0,0.4); height: 78px;">
               <!-- <div class="modal_audio_player2" id="modal_audio_player2" style="margin-top: -8px;"></div> -->
        </div>
    </div>
  </div>
</div>





<script type="text/javascript">
function stopplay3(){
  $("#modal_audio_player2").html('');
}

  function mdl_play3(aud) {
  $("#modal_audio_player2").html('<audio controls controlsList="nodownload noremoteplayback" loop="true" autobuffer style="background-color:white;" id="my_Audio3" class="col-sm-12" autoplay="" src="'+aud+'"></audio>');
  $("#modal_audio_player2").show('fast');
  }

  function mdl_pause(){
  var voldown = document.getElementById('my_Audio3');
  var now_play = document.getElementById('now_play');
  voldown.pause();
}

function mdl_play2(){
  var voldown = document.getElementById('my_Audio3');
  voldown.play();
}
</script>


 




   <div id="addtoModal" class="modal fade">  
    <div class="modal-dialog modal-sm">  
      <div class="sa-bg modal-content AM-content N_content">  
        <div class="modal-header text-center AM-header">
          <h4 class="font-m1 w3-wide w3-xlarge text-white AM-title">ADD TO PLAYLIST</h4>  
          <a href="#"  class="pull-right AM-close" data-dismiss="modal">
            <i id="closehover" class="fa fa-times"></i></a>
          <hr class="AM-hr">
        </div>  
          <div class="modal-body AM-body">
          <input type="text" id="plistname" style="display: none;">         
              <div class="row">
                <input type="hidden" name="audio_id" id="audio_id">

                <div class="col-sm-12">        
                  <div id="tbl_addplist" class="scroll" style="height: 280px;">
                    
                  </div>
                </div>
                
                <div class="col-sm-12">
                  <hr class="AM-hr">
                  <div class="pull-right" style="padding-top: 10px;">
                    <button class="btn btn-danger rounded font-m1" data-dismiss="modal"> Cancel</button>
                  </div>
                </div>
              </div>
          </div>  
      </div>  
    </div>  
  </div>







  <div id="newPlaylistModal2" class="modal fade">  
    <div class="modal-dialog modal-sm">  
      <div class="modal-content N_content">  
        <div class="modal-header bg-info text-white">
          <h5 class="calbum modal-title">Create New Playlist</h5>  
            <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
        </div>  
        <div class="modal-body">        
        
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <input type="text" name="playlist_name2" id="playlist_name2" class="form-control text-capitalize" placeholder="Playlist Name" required="true" maxlength="30" style="text-transform: capitalize;">
                </div>
              </div>
              <div class="col-sm-1"></div>
            </div>
        </div>  
        <div class="modal-footer">
          <button class="btn btn-danger" onclick="cancelplist2();"> Cancel</button>
          <button class="btn btn-success"  onclick="submit_playlist2();" >Ok</button>
        </div>
      </div>  
    </div>  
  </div>

<!-- <div class="modal-fade" role="dialog" id="addtoModal" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">Add to..</div>
        <button class="cp_close close" data-dismiss="modal">&times;</button>
      </div>
      
    </div>
  </div>
</div> -->








 


<script type="text/javascript">
  
function con_albums(){
    $('#contributor_songs').hide('fast');
    $('#contributor_albums').show('fast');
    $('#btnconsong').removeClass('btn-success');
    $('#btnconsong').addClass('btn-default');
    $('#btnconalbum').addClass('btn-success');
    $('#albumsearch').show('fast');
    $('#songsearch').hide('fast');
    $('#conalbum_songs').hide('fast');
    $('#btnback').hide('fast');
    $('#btnconsong').show('fast');
    $('#btnconalbum').show('fast');
    $('#albumsongsearch').hide('fast');
    }

function con_songs(){
  $('#contributor_songs').show('fast');
  $('#contributor_albums').hide('fast');
  $('#btnconsong').addClass('btn-success');
  $('#btnconalbum').removeClass('btn-success');
  $('#btnconalbum').addClass('btn-default');
  $('#albumsearch').hide('fast');
  $('#songsearch').show('fast');
  }



function viewconalbum(album_id){
  $("#ab-id").val(album_id);
  $('#viewSongsModal').modal('hide');
  var mydata = 'action=show_contri_album_audio' + '&Id=' + album_id;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    success:function(data){
      $("#modal_playlist2").modal('show');
      setTimeout(function(){$("#display_album_audio2").html(data);},800); 
      p_music_list(); 
    }
  }); 
}

function closecontri(){
  $('#viewSongsModal').modal('show');
}

</script>





<style>
  .closemdl{
    color:white; 
    font-size:35px;
    
  }

  #modal_pl_title{
    font-family: MavenPro-Medium;
  
  }


    @font-face{
    font-family: Agreloy;
    src: url('../fonts/Agreloy.ttf');
    }

.scroll
    {
      height: 260px;
      overflow: scroll;
      overflow-x: hidden; 
    
    }

.stylebg{
  background-image: url('../img/USbg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}

.plbg{
  background-image: url('../img/USbg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}

.albumbg{
  background-image: url('../img/USbg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}

.pliststyle{
    background-image: url('../img/bg5.jpg');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}

.stylename{
    color: #fff; 
    font-size: 30px;
    letter-spacing: 2px;
    font-family: TradeMark;
    background-color: rgba(0,0,0,0.0); 
    border: solid 1px rgba(0,0,0,0.0);
}

.contribg{
  background-image: url('../img/USbg.png');
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}



.modalbg{
  background-image: url();
  background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}

.albsongsbg{
    background-image: url('../img/modalbg.jpg');
    background-repeat: no-repeat;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  background-size: cover;
  -o-background-size: cover;
}
  .bg_img{
    background-image: url('../img/mdbg.png');
  }

  .cp_close:hover{
    color: red;
  }

  .tbldes:hover{
    background-color: rgba(0,0,0,0.4);
  }

  
</style>

<div class="modal fade" role="dialog" id="song_details">
  <div class="modal-dialog ">
    <div class="modal-content" style="background-color: #6C7A89; color: #fff;">
      <div class="modal-body">
        <div class="text-default text-center" style="font-size: 18px;"><b>Details</b></div>
        <input type="hidden" name="" id="aud_id">
        <input type="hidden" name="" id="alb_id">
        <input type="hidden" name="" id="us_id">
      <br>

      <div class="list-group" id="tbl_det">
        
      </div>
        </div>
      </div>
    </div>
  </div>