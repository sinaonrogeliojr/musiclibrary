var url = 'function.php';

$(document).ready(function(){  

$('#addSong2').on('hidden.bs.modal', function () {
$("#songid2").val('');
//$("#music_path2").val('');
$("#duration2").val('');
$("#uploaded_songs2").val('');
$("#song_name2").val('');
$("#composer2").val('');
$("#description2").html('');
$("#keyword2").html('');
$("#song_preview2").html('');
$('#s_files_view2').html('');

});

});


function get_path2(index){
    $("#music_path2").val(index);
  }

function submitsong2(){

    //var song_path = document.getElementById('uploaded_songs'); 
    var songid = $("#songid2").val();
    var style = $("#style_value2").val();
    var app = $("#app_value2").val();
    var song_name = $("#song_name2").val();
    var composer = $("#composer2").val();
    //var album = $("#album_lists2").val();
    var album = document.getElementById('album_lists2');
    var description = $("#description2").val();
    var keyword = $("#keyword2").val();
    var extension = $('#uploaded_songs2').val().split('.').pop().toLowerCase();
 //alert(album.value);
    var song_path = $("#link2").val();
    //var duration = $("#myduration2").val();
    var song_path = $("#music_path2").val();
    var duration = $("#duration2").val();
    var date_upload = $("#date_upload2").val();
    var file_id = $("#group_id2").val();
    var song_preview2 = document.getElementById('song_preview2');
    var group_id = $("#group_id2").val();
    
      if(song_name == '')  
      {  
       swal("Oops", "Please Input Song Name!", "warning"); 
       $("#song_name2").focus();
        return false;
       
      } 
      else if(composer == '')  
      {  
        swal("Oops", "Please Input Composer!", "warning"); 
        $("#composer2").focus();
         return false;
      } 
      else if(album == '')  
      {  
        swal("Oops", "Please Select Album!", "warning"); 
        $("#album_lists2").focus();
         return false;
      } 
      else if(date_upload == '')  
      {  
        swal("Oops", "Please Input Date!", "warning"); 
        $("#date_upload2").focus();
         return false;
      } 
      /*else if(jQuery.inArray(extension, ['mp3']) == -1)  
      {  
        swal("Oops", "Please Select Song", "warning");  
        return false;
      }*/
      else 
      {
     /* var mydata ='action=upload_song' + '&song_name=' + song_name + '&composer=' + composer + '&description=' + description + 
                   '&album=' + album +'&keyword=' + keyword + '&song_path=' + song_path + '&duration=' + duration + '&date_upload=' + date_upload;
      alert(mydata);*/
      $.ajax({
      url:"addsong.php",
      method:"POST",
      data:{style:style,app:app,song_name:song_name,composer:composer,album:album.value,description:description,keyword:keyword,song_path:song_path,duration:duration,date_upload:date_upload,file_id:file_id,songid:songid,group_id:group_id},
      cache:false,
      success:function(data){
      // alert(data);
      console.log(data);
      if (data == 1) 
      {
      swal("Success", "Song has been Added!", "success");
      $('#addSong2').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id2').val('');
      $('#music_path2').val('');
      $('#s_files_view2').html('');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      album_musics();
      //song_able2();
      }
      else if(data == 2){
      swal("Success", "Song has been Updated!", "success");
      $('#addSong2').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id2').val('');
      $('#music_path2').val('');
      $('#s_files_view2').html('');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      album_musics();
      //song_able2();
      }
      }
      });
      }  
      }

function selectalb(album_name,album_id){
  document.getElementById("alb_select").innerHTML = album_name;
  $("#selectedalb_id").val(album_id);
  $("#select_album_modal").modal('hide');
  //swal("Success", "Album Selected!", "success");
}

function create_album(){
  $("#select_album_modal").modal('hide');
  $("#createAlbumModal").modal('show');
}

function cancel_select(){
  $("#select_album_modal").modal('hide');
    $("#addmusic").modal('show');
}

function load_albums(){
  var searchalbums = document.getElementById('searchalbums');
  var mydata = 'action=load_album_selection' + '&searchalbums=' + searchalbums.value;
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#tbl_album_select").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#tbl_album_select").html(data);},800); 
  }
  }); 
}

function addto_album(){
  $("#addmusic").modal('hide');
  load_albums();
  $("#select_album_modal").modal('show');
}


function get_style_value2(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#style_select2 input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;

  $("#style_value2").val(selected);
}

function get_app_value2(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#app_select2 input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(',' + ' ') ;

  $("#app_value2").val(selected);
}

function generate_group_id2(){
  var mydata = 'action=gen_groupid';
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  success:function(data){
    //alert(data);
    $("#group_id2").val(data.trim());
  }
  }); 

}

function display_files2()
{
  var id = document.getElementById('group_id2');
  var mydata = 'action=show_supporting_files2' + '&group_id2=' + id.value;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_supporting_files2").html(data);},800); 
    }
  }); 
}

function clear_form2(){
  $("#song_preview2").html('');
  $("#alb_select2").html('<i class="fa fa-folder-o"></i> &nbsp;Album');
  $("#selectedalb_id2").val('');
  $("#style_value2").val('');
  $("#app_value2").val('');
  document.getElementById('btn-select2').disabled = false;
  $("#composer2").val('');
  $("#description2").val('');
  $("#keyword2").val('');
  $("#duration2").val('');
  //$("#music_path2").val('');
  $("#display_supporting_files2").html('');

}

function load_style_select2(){
var style = document.getElementById('style_value2');
//alert(style.value);
var mydata = 'action=show_style_select2' + '&style_value2=' + style.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#style_select2").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#style_select2").html(data);},800); 
}
});
}

function load_app_select2(){
var app = document.getElementById('app_value2');
var mydata = 'action=show_app_select2'+ '&app_value2=' + app.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#app_select2").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#app_select2").html(data);},800); 
}
});
}

function get_dateupload2(){
    var songid = document.getElementById("songid2");
    var mydata = 'action=get_dateupload2' +'&songid2=' + songid.value;
    //alert(mydata);
     $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        //alert(data);
          $("#date_upload2").val(data.trim());
      }
    }); 
  }

function upload_music22(){
 $(document).ready(function(){
  $(document).on('change', '#uploaded_songs', function(){
    var name = document.getElementById("uploaded_songs").files[0].name;
    var form_data = new FormData();
    var ext1 = name.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext1, ['mp3']) == -1) 
      {
      swal("Error", "Invalid File","error")
      return false;
      }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploaded_songs").files[0]);
    var f = document.getElementById("uploaded_songs").files[0];
    var fsize = f.size||f.fileSize;
      if(fsize > 70000000)
      {
      alert("Invalid, Large File Size!");
      }
      else
      {
      form_data.append("uploaded_songs", document.getElementById('uploaded_songs').files[0]);
      
      // alert(form_data);
      $.ajax({
      url:"uploadsong2.php",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend:function(){
      $('#song_preview').html("<label class='text-success col-sm-12'>Music Uploading...</label>");
      },   
      success:function(data)
      {
      
      $('#btn-select').removeClass('btn btn-dark');
      $('#song_preview').html(data);
      document.getElementById('submitsong').disabled=false;
      document.getElementById('btn-select').disabled=true;
      $('#btn-select').addClass('btn btn-dark');
      //swal("Success", "File Uploaded", "success");
      }
      });
      }
      });
     });
}

function upload_music_album(){
 $(document).ready(function(){
  $(document).on('change', '#uploaded_songs2', function(){
    var name = document.getElementById("uploaded_songs2").files[0].name;
    var form_data = new FormData();
    var ext1 = name.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext1, ['mp3']) == -1) 
      {
      swal("Error", "Invalid File","error")
      return false;
      }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploaded_songs2").files[0]);
    var f = document.getElementById("uploaded_songs2").files[0];
    var fsize = f.size||f.fileSize;
      if(fsize > 70000000)
      {
      alert("Invalid, Large File Size!");
      }
      else
      {
      form_data.append("uploaded_songs2", document.getElementById('uploaded_songs2').files[0]);
      
      // alert(form_data);
      $.ajax({
      url:"uploadsong2_album.php",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend:function(){
      $('#song_preview2').html("<label class='text-success col-sm-12'> Music Uploading... <span class='fa fa-spinner fa-spin'></span></label>");
      },   
      success:function(data)
      {
      /*get_path2();
      $('#btn-select2').removeClass('btn btn-dark');
      $('#song_preview2').html(data);
      document.getElementById('submitsong2').disabled=false;
      document.getElementById('btn-select2').disabled=true;
      $('#btn-select2').addClass('btn btn-dark');
      //swal("Success", "File Uploaded", "success");*/

      if(data == 404){
        swal("Warning", "Song Already Exists.", "warning");
          $('#song_preview2').html("<label class='text-danger col-sm-12'>Music Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");  
          document.getElementById('btn-select2').disabled=false;
       }else{
          $('#btn-select2').removeClass('btn btn-dark');
          $('#song_preview2').html(data);
          document.getElementById('submitsong2').disabled=false;
          document.getElementById('btn-select2').disabled=true;
          $('#btn-select2').addClass('btn btn-dark');
        //swal("Success", "File Uploaded", "success");
       }

      }
      });
      }
      });
     });
}

function multi_upload_file(){
  $(document).ready(function(){
    $(document).on('change', '#multi_upload_file', function(){

      var filedata = document.getElementById("multi_upload_file");
      var group_id = document.getElementById("group_id");
      //alert(group_id.value);
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var filename = document.getElementById("multi_upload_file").files[i].name;
        var formdata = new FormData();
        var ext1 = filename.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['pdf','doc','docx','txt']) == -1) 
        {
          swal("Error","Invalid File","error");
          $("#multi_upload_file").val('');
          return false;
        }

        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multi_upload_file").files[i]);
        var f = document.getElementById("multi_upload_file").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          swal("Warning", "Large File Size","warning");
        }
        else
        {
          formdata.append("multi_upload_files", document.getElementById('multi_upload_file').files[i]);
          formdata.append("group_id", group_id.value);

          $.ajax({
            url:"multi_upload_file.php",
            method:"POST",
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#file_preview').html('<span><img src="../img/loder1.gif" width="20"> </span>');
            },   
            success:function(data)

            { 
              //alert(data);
              $('#file_preview').text("");
              setTimeout(function(){ $('#file_preview').append(data);}, 1000);
              //$('#display_supporting_files').html(data);
              $('#pdf_btn').removeClass('btn btn-info');
              //document.getElementById('multi_upload_file').disabled= true;
              $('#pdf_btn').addClass('btn btn-dark');
              swal("Success", "Files Uploaded", "success");
              var upload_file = $('#upload_file').val();
              file_able();
              display_files();
            }
          });
        }
      }
    });
  });
}

function file_able(){
  $("#select_file_modal").modal('hide');
  $("#addmusic").modal('show');
  $("#link_name").val('');
  $("#s_link").val('');
  //$("#word_prev").html('');
  //$("#file_preview").html('');
  $("#pdf_btn").removeClass('btn btn-dark');
  $("#pdf_btn").addClass('btn btn-info');
  document.getElementById('multi_upload_file').disabled= false;
  //$("#word_btn").removeClass('btn btn-dark');
  //$("#word_btn").addClass('btn btn-primary');
  //document.getElementById('upload_word').disabled= false;
}

function display_files()
{
  var id = document.getElementById('group_id');
  var mydata = 'action=show_supporting_files' + '&group_id=' + id.value;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_supporting_files").html(data);},800); 
    }
  }); 
}

function get_file_data2(new_file_id) 
{
  $('#edit_action').val("update_file");
  var mydata = 'new_file_id=' + new_file_id;
  $.ajax({
   url:"get_files_data.php",
   method:"POST",
   data:mydata,
   dataType:"JSON",
   success:function(data)
   {
    var str = data.file_name;
    var fn = str.split(".");

    $('#file_id2').val(data.file_id);
    $('#file_name2').val(fn[0]);
    $('#file_description2').val(data.file_description);
    $('#link_name2').val(data.link_name);
    $('#link2').val(data.link);

    $('#file_ext2').val(fn[1]);
    $('#EditFileModal2').modal('show');
   }
  });
}

function edit_file(){

  var file_id = $("#file_id").val();
  var file_name = $("#file_name").val();
  var file_desc = $("#file_description").val();
  var link_name = $("#link_name").val();
  var link = $("#link").val();
  var link_ext = $("#file_ext").val();

  var fn = file_name + '.' + link_ext; 

  var mydata = 'action=edit_file' + '&file_id=' + file_id + '&file_name=' + fn + '&file_desc=' + file_desc + 
                '&link_name=' + link_name + '&link=' + link;
  $.ajax({
    url:url,
    method:"POST",
    data:mydata,
    success:function(data)
    {
      if (data == 1) 
      {
        swal("Success", "Successfully Updated Supporting Files", "info");
        $('#EditFileModal').modal('hide');
        $('#addmusic').modal('show');
        display_files();
      }
    }
  });
}

function remove_file2(id,path){ 
  var path ='action=remove_file'+ '&path=' + path + '&id=' + id;
  //alert(path);
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this File?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "File has been Removed!", "success");
      s_files2();
      //$('#file_preview').html('');
      //$('#pdf_btn').removeClass('btn btn-dark');
      //$('#pdf_btn').addClass('btn btn-info');
      //$('#upload_file').val('');
      //document.getElementById('upload_file').disabled= false;
    } 
    else  
    {  
      return false;  
    } 
    /*$('#btn-select').removeClass('btn btn-dark');
    document.getElementById('uploaded_songs').disabled= false;
    $('#btn-select').addClass('btn btn-primary');
    $('#uploaded_songs').val('');*/
    }  
    });  
    });
  }

function get_duration2(total){
var seconds = "0" + (Math.floor(total) - minutes * 60);
var seconds = "0" + Math.floor(total % 60);
var minutes = "0" + Math.floor(total / 60);
var seconds = "0" +  Math.floor(total - minutes * 60);
//concat minutes and seconds
var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
$("#duration2").val(dur);
// alert(dur);
}

function display_albums(){
var id = document.getElementById('songid');
var mydata = 'action=album_lists' + '&songid=' + id.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
setTimeout(function(){$("#album_lists").html(data);},500); 
}
});
}

function display_albums2(){
var id = document.getElementById('album_id');
var mydata = 'action=album_lists2' + '&album_id=' + id.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
setTimeout(function(){$("#album_lists2").html(data);},500); 
}
});
}


function edit_audio2(id,genres,song_name,composer,description,keyword,application_id,music_path,duration,group_id){
    display_albums2();
    $("#title").html('UPDATE MUSIC')
    $("#songid2").val(id);
    $("#style_value2").val(genres);
    $("#song_name2").val(song_name);
    $("#composer2").val(composer);
    $("#description2").val(description);
    $("#keyword2").val(keyword);
    $("#app_value2").val(application_id);
    $("#music_path2").val(music_path);
    $("#duration2").val(duration);
    $("#group_id2").val(group_id);
    $("#addSong2").modal('show');
    document.getElementById('btn-select2').disabled = true;
     document.getElementById('submitsong2').disabled= false;
    load_style_select2(); 
    load_app_select2(); 
    s_files2();
    get_dateupload2();
  
    }


function remove_song2(path1){ 
  var path = 'action=remove_song2'+ '&path1=' + path1;
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this Song?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "Song has been Removed!", "success");
      $('#song_preview2').html('');
      $('#music_path2').val('');
      $('#duration2').val('');
    } 
    else  
    {  
      return false;  
    } 
    $('#btn-select2').removeClass('btn btn-dark');
    document.getElementById('uploaded_songs2').disabled= false;
    document.getElementById('submitsong2').disabled= true;
    document.getElementById('btn-select2').disabled = false;
    $('#btn-select2').addClass('btn btn-dark');
    $('#uploaded_songs2').val('');
    }  
    });  
    });
  }

function s_files2()
{
  var id = document.getElementById('group_id2');
  var mydata = 'action=s_files2' + '&group_id2=' + id.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#s_files_view2").html(data);},800); 
    }
  }); 
}


 function edit_audio(id,genres,song_name,composer,description,keyword,application_id,music_path,duration,album_id,group_id){
    display_albums();
    $("#title").html('UPDATE MUSIC')
    $("#songid").val(id);
    $("#style_value").val(genres);
    $("#song_name").val(song_name);
    $("#composer").val(composer);
    $("#description").val(description);
    $("#keyword").val(keyword);
    $("#app_value").val(application_id);
    $("#music_path").val(music_path);
    $("#myduration").val(duration);
    $("#selectedalb_id").val(album_id);
    $("#group_id").val(group_id);
    $("#addSong").modal('show');
    document.getElementById('btn-select').disabled = true;
     document.getElementById('submitsong').disabled= false;
    load_style_select(); 
    load_app_select(); 
    s_files2();
    get_dateupload();
  
    }

function multi_upload_file(){
  $(document).ready(function(){
    $(document).on('change', '#multi_upload_file', function(){

      var filedata = document.getElementById("multi_upload_file");
      var group_id = document.getElementById("group_id2");
      //alert(group_id.value);
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var filename = document.getElementById("multi_upload_file").files[i].name;
        var formdata = new FormData();
        var ext1 = filename.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['pdf','doc','docx','txt']) == -1) 
        {
          swal("Error","Invalid File","error");
          $("#multi_upload_file").val('');
          return false;
        }

        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multi_upload_file").files[i]);
        var f = document.getElementById("multi_upload_file").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          swal("Warning", "Large File Size","warning");
        }
        else
        {
          formdata.append("multi_upload_files", document.getElementById('multi_upload_file').files[i]);
          formdata.append("group_id2", group_id.value);

          $.ajax({
            url:"multi_upload_file2.php",
            method:"POST",
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#file_preview').html('<span><img src="../img/loder1.gif" width="20"> </span>');
            },   
            success:function(data)

            { 

              if(data == 1){
                s_files2();
                $('#file_preview').text("");
                setTimeout(function(){ $('#file_preview').append(data);}, 1000);
                //$('#display_supporting_files').html(data);
                $('#pdf_btn').removeClass('btn btn-info');
                //document.getElementById('multi_upload_file').disabled= true;
                $('#pdf_btn').addClass('btn btn-dark');
                swal("Success", "Filess Uploaded", "success");
                var upload_file = $('#upload_file').val();
              }else if(data == 404){
                swal("Warning", "File Already Exists on Database.", "warning");
              }else if(data == 405){
                swal("Warning", "File Already Exists on the File Folder.", "warning");
              }
            }
          });
        }
      }
    });
  });
}

function get_file_data(new_file_id) 
{
  $('#edit_action').val("update_file");
  var mydata = 'new_file_id=' + new_file_id;
  $.ajax({
   url:"get_files_data.php",
   method:"POST",
   data:mydata,
   dataType:"JSON",
   success:function(data)
   {
    var str = data.file_name;
    var fn = str.split(".");

    $('#file_id').val(data.file_id);
    $('#file_name').val(fn[0]);
    $('#file_description').val(data.file_description);
    $('#link_name').val(data.link_name);
    $('#link').val(data.link);

    $('#file_ext').val(fn[1]);
    $('#addmusic').modal('hide');
    $('#EditFileModal').modal('show');
   }
  });
}

function edit_file2(){

  var file_id = $("#file_id2").val();
  var file_name = $("#file_name2").val();
  var file_desc = $("#file_description2").val();
  var link_name = $("#link_name2").val();
  var link = $("#link2").val();
  var link_ext = $("#file_ext2").val();

  var fn = file_name + '.' + link_ext; 

  var mydata = 'action=edit_file2' + '&file_id2=' + file_id + '&file_name2=' + fn + '&file_desc2=' + file_desc + 
                '&link_name2=' + link_name + '&link2=' + link;
  $.ajax({
    url:url,
    method:"POST",
    data:mydata,
    success:function(data)
    {
      if (data == 1) 
      {
        swal("Success", "Successfully Updated Supporting Files", "info");
        $('#EditFileModal2').modal('hide');
        //$('#addmusic').modal('show');
        s_files2();
      }
    }
  });
}

function remove_file(id,path){ 
  var path ='action=remove_file'+ '&path=' + path + '&id=' + id;
  //alert(path);
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this File?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "File has been Removed!", "success");
      s_files2();
      //$('#file_preview').html('');
      //$('#pdf_btn').removeClass('btn btn-dark');
      //$('#pdf_btn').addClass('btn btn-info');
      //$('#upload_file').val('');
      //document.getElementById('upload_file').disabled= false;
    } 
    else  
    {  
      return false;  
    } 
    /*$('#btn-select').removeClass('btn btn-dark');
    document.getElementById('uploaded_songs').disabled= false;
    $('#btn-select').addClass('btn btn-primary');
    $('#uploaded_songs').val('');*/
    }  
    });  
    });
  }