<?php 
date_default_timezone_set('Australia/Brisbane');
require("../api/fpdf.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

//WEBSITE PLAYLIST
function show_music_playlist($con,$id,$search_song){
if ($search_song == "" || $search_song == null) {

	$sql = "SELECT t1.*,t2.`id` as t_id,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date, t4.`album_artwork` from tbl_audios t1 
		LEFT JOIN tbl_playlist_info t2 ON t1.`id` = t2.`audio_id`  
		LEFT JOIN tbl_album t4 ON t1.`album_id` = t4.`album_id`  
		where t2.`playlist_id`='$id' ORDER BY song_name ASC";

	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result) > 0)
	{
	     while($row = mysqli_fetch_array($result))
	     {
	      $id = $row["t_id"];
	      $a_id = $row["audio_id"];
	      $detect = detect_download($con,$a_id);	
	      $date_ = date('Y-m-d');
		  $app_date = $row['app_date'];

		  if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        		$imgs = '../img/slogo3.png';
        	}
        	else
        	{
        		$imgs = '../'.$row['album_artwork'];
        	}
	    ?>
	      <tr class="text-black bg-white" id="music_playlist" ondblclick="choose_play_plist('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play_plist(); only_play_plist();">
	        <td data-toggle="tooltip" title="<?php echo $row["song_name"]; ?>" style="white-space: nowrap; max-width: 5px; overflow: hidden; text-overflow: ellipsis; ">      
	        	<input type="hidden" id="d_audio_id" value="<?php echo $id; ?>"/>
				<?php echo $row["song_name"]; ?> &nbsp;
				<?php if($date_ == $app_date){ ?>
					<span class="badge badge-sm bg-success" title="Recently added"><span class="fa fa-check-circle"></span> new</span>
				<?php }else{ ?>

				<?php } ?>    
			</td>
			<td data-toggle="tooltip" title="<?php echo $row["description"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["description"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title="<?php echo $row["genre"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["genre"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title="<?php echo $row["composer"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["composer"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title="<?php echo date('H:i', strtotime($row['duration'])); ?>">
	            <?php echo date('H:i', strtotime($row['duration'])); ?> 
	        </td>
	        <td>
	        	<span class="btn fa fa-play-circle fa-lg" title="Play" onclick="choose_play_plist('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play_plist(); only_play_plist();"></span>
	        	<span class="btn fa fa-trash fa-lg" onclick="del_music('<?php echo $row['t_id'];?>');" title="Delete From Playlist"></span>

	        	<?php if($detect == 1){ ?>
	        		<span class="btn fa fa-check fa-lg"  data-toggle="tooltip" title="This song is already added to your download list."></span>
	        	<?php }else{ ?>
	        		<span class="btn fa fa-plus fa-lg" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download Playlist" onclick="add_download('<?php echo $row['audio_id'] ?>');"></span>
	        	<?php } ?>
	        </td>
	      </tr> 
	    <?php  
	     }
	     ?>
	<?php      
	}
	else
	{
	?>
		<div class="alert alert-default text-center" >
		  <strong class="text-dark"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
		</div>
	<?php  
	}

}else{

	//concat(song_name,composer,artist,genre)
	$sql = "SELECT t1.*,t2.`id` as t_id,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date, t4.`album_artwork` from tbl_audios t1 
		LEFT JOIN tbl_playlist_info t2 ON t1.`id` = t2.`audio_id` 
		LEFT JOIN tbl_album t4 ON t1.`album_id` = t4.`album_id`  
		where t2.`playlist_id`='$id' and concat(song_name,composer,genre,description,keyword) like '%$search_song%' ORDER BY song_name ASC";

	$result = mysqli_query($con, $sql);

	if(mysqli_num_rows($result) > 0)
	{
	    while($row = mysqli_fetch_array($result))
	     {
	      $id = $row["t_id"];
	      $a_id = $row["audio_id"];
	      $detect = detect_download($con,$a_id);	
	      $date_ = date('Y-m-d');
		  $app_date = $row['app_date'];

		  if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        		$imgs = '../img/slogo3.png';
        	}
        	else
        	{
        		$imgs = '../'.$row['album_artwork'];
        	}
	    ?>
	     <tr class="text-black bg-white" id="music_playlist" ondblclick="choose_play_plist('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play_plist(); only_play_plist();">
	        <td data-toggle="tooltip" title="<?php echo $row["song_name"]; ?>" style="white-space: nowrap; max-width: 5px; overflow: hidden; text-overflow: ellipsis; ">      
	        	<input type="hidden" id="d_audio_id" value="<?php echo $id; ?>"/>
				<?php echo $row["song_name"]; ?> &nbsp;
				<?php if($date_ == $app_date){ ?>
					<span class="badge badge-sm bg-success" title="Recently added"><span class="fa fa-check-circle"></span> new</span>
				<?php }else{ ?>

				<?php } ?>    
			</td>
			<td data-toggle="tooltip" title="<?php echo $row["description"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["description"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title="<?php echo $row["genre"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["genre"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title="<?php echo $row["composer"]; ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
	            <?php echo $row["composer"]; ?> 
	        </td>
	        <td data-toggle="tooltip" title=" <?php echo date('H:i', strtotime($row['duration'])); ?>">
	            <?php echo date('H:i', strtotime($row['duration'])); ?> 
	        </td>
	        <td>
	        	<span class="btn fa fa-play-circle fa-lg" title="Play" onclick="choose_play_plist('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play_plist(); only_play_plist();"></span>
	        	<span class="btn fa fa-trash fa-lg" onclick="del_music('<?php echo $row['t_id'];?>');" title="Delete From Playlist"></span>

	        	<?php if($detect == 1){ ?>
	        		<span class="btn fa fa-check fa-lg"  data-toggle="tooltip" title="This song is already added to your download list."></span>
	        	<?php }else{ ?>
	        		<span class="btn fa fa-plus fa-lg" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download Playlist" onclick="add_download('<?php echo $row['audio_id'] ?>');"></span>
	        	<?php } ?>
	        </td>
	      </tr>
	    <?php  
	     }	
	 }
	else
	{
	?>
		<div class="alert alert-default text-center" >
		  <strong class="text-dark"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
		</div>
	<?php  
	}

}

}

//GROUP ID DENERATION
function gen_groupid($con){
$my_id = $_SESSION['users'];
$query = mysqli_query($con, "SELECT MAX(group_id) FROM tbl_supporting_file where user_id='$my_id'");
$row = mysqli_fetch_assoc($query);
if (mysqli_num_rows($query)>0) {
 		echo 'GROUP-'.rand(1,9).rand(1,9).rand(1,9).rand(1,9);
}else{
  		echo 'GROUP-0001';
}

}


function send_emailer($title,$description,$composer,$genre,$posted_by){

		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
		    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
		    $mail->Password = 'testing5!';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587;                                    // TCP port to connect to

		    $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		        )
		    );
		    //Recipients
		    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
		    $mail->addAddress('sinaonrogeliojr@gmail.com');     // Add a recipient
		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Music Library';
		    $mail->Body    = '<h1>Music Library</h1><br><p>TITLE: '.$title.'</p><p>COMPOSER: '.$composer.'</p><p>GENRE: '.$genre.'</p><p>DESCRIPTION: '.$description.'</p><br><p>POSTED BY: '.$posted_by.'</p>';
		    
		    $mail->AltBody = '(Music Library) You have Music Request!';

		    $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
}

function insert_album($con,$album_name,$id){
	if ($id == "") {
	$sql = mysqli_query($con,"INSERT INTO tbl_album values(id,'$album_name')");
	if ($sql) {
		echo 1;
	}
	}
	else
	{
	$sql = mysqli_query($con,"UPDATE tbl_album set album_name='$album_name' where id='$id'");
	if ($sql) {
		echo 2;
	}
	}
}

function new_file_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_supporting_file");
	$row = mysqli_fetch_assoc($sql);
	return 'file-'.rand(1,9).rand(1,9).$row['max(id)'].rand(1,9).rand(1,9);
}

function save_supporting_files($con,$pdfpath,$wordpath,$link_name,$s_link){
	$my_id = $_SESSION['users'];
	$file_id = new_file_id($con);

	$sql = mysqli_query($con, "INSERT INTO tbl_supporting_file (user_id,file_id,pdf_path,word_path,file_name,link) 
		VALUES ('$my_id','$file_id','$pdfpath','$wordpath','$link_name','$s_link')");
	if ($sql) {
		echo 1;
	}
}

//clear album songs
function clear_playlist_songs($con, $plistid){
	$sql = mysqli_query($con,"DELETE from tbl_playlist_info where playlist_id='$plistid'");
	if ($sql) {
		echo 1;
	}
	else{
		echo 2;
	}
}

function clear_album_songs($con, $alb_id_modal){
	$sql = mysqli_query($con,"DELETE from tbl_audios where album_id='$alb_id_modal'");
	if ($sql) {
		echo 1;
	}
	else{
		echo 2;
	}
}

function detect_name($con,$audio_id,$id){
	$sql = mysqli_query($con, "SELECT * FROM tbl_playlist_info WHERE audio_id='$audio_id' AND playlist_id='$id'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}

}

function add_to_plist($con,$audio_id,$id){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$detect = detect_name($con,$audio_id,$id);

	if ($detect == 1) {
		echo 404;
	}
	else{
		$sql = mysqli_query($con,"INSERT INTO tbl_playlist_info values(id,'$audio_id','$my_id','2','$id','$date_',1)");
	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}

function savemultisongs($con,$audio_id,$newplistid){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$user_type = 2;
	$is_active = 1;
	if ($audio_id) {
	foreach ($audio_id as $audio_id) {
	mysqli_query($con, "INSERT INTO tbl_playlist_info(audio_id, user_id, user_type, playlist_id, date_, is_active) 
VALUES( '".mysqli_real_escape_string($con, $audio_id)."', '$myid', '$user_type', '$newplistid', '$date_', '$is_active')");
	}
		}
}

function detect_name2($con,$plistid,$p_ids){
	$sql = mysqli_query($con, "SELECT * FROM tbl_playlist_info WHERE audio_id='$p_ids' AND playlist_id='$plistid'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}
}

function save_song($con,$plistid,$p_ids){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$detect = detect_name2($con,$plistid,$p_ids);
	if ($detect == 1) {
		echo 404;
	}
	else{
	$sql =mysqli_query($con,"INSERT INTO tbl_playlist_info (audio_id,user_id,user_type,playlist_id,date_,is_active) 
		VALUES ('$p_ids','$my_id',2,'$plistid','$date_',1)");
	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}

function detect_name3($con,$id,$plistid){
	$sql = mysqli_query($con, "SELECT * FROM tbl_playlist_info WHERE audio_id='$id' AND playlist_id='$plistid'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}
}

function save_selected_song($con,$id,$plistid){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$detect = detect_name3($con,$id,$plistid);
	if ($detect == 1) {
		echo 404;
	}
	else{
	$sql =mysqli_query($con,"INSERT INTO tbl_playlist_info (audio_id,user_id,user_type,playlist_id,date_,is_active) 
		VALUES ('$id','$my_id',2,'$plistid','$date_',1)");
	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}

function insert_plist($con,$playlist_name){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"INSERT INTO tbl_playlist values(id,'$playlist_name','$date_','$my_id')");
	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
}

function insert_plist2($con,$playlist_name2){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"INSERT INTO tbl_playlist values(id,'$playlist_name2','$date_','$my_id')");
	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
}

function update_plist($con,$pid,$plist_name){
	$sql = mysqli_query($con,"UPDATE tbl_playlist set name='$plist_name' where id='$pid'");
	if ($sql) {
		echo 1;
	}
	
}

function delete_plist($con,$id){
	$sql = mysqli_query($con,"DELETE from tbl_playlist where id='$id' ");
	$sql2 = mysqli_query($con,"DELETE from tbl_playlist_info where playlist_id='$id' ");
	if ($sql) {
		echo 1;
	}
	if ($sql2) {
		echo 1;
	}
}


function insert_music($con,$song_name,$composer,$artist,$genre,$albumname,$audiencename,$applicationname,$id){
	if ($id == "") {
	$sql = mysqli_query($con,"INSERT INTO tbl_audios values(id,'$song_name','$composer','$artist','$genre','$album_id',$audience_id','$application_id')");
	if ($sql) {
		echo 1;
	}
	}
	else
	{
	$sql = mysqli_query($con,"UPDATE tbl_audios set song_name='$song_name',composer='$composer',artist='$artist',genre='$genre',album_id='$albumname',audience_id='$audiencename',application_id='$applicationname'  where id='$id'");
	if ($sql) {
		echo 2;
	}
	}
}

function delete_music($con,$id,$music){
	$sql = mysqli_query($con, "DELETE from tbl_audios where id='$id' and music='$music'");
	if ($sql) {
		echo 1;
	}
}

function remove_file($con,$path,$id){
	$sql = mysqli_query($con, "DELETE from tbl_supporting_file where new_file_id='$id' ");
	if ($sql) {
		echo 1;
	}
}

function removefromlist($con,$audio_id,$pl_id){
	$sql = mysqli_query($con, "DELETE from tbl_playlist_info where audio_id='$audio_id' and playlist_id='$pl_id'");
	if ($sql) {
		echo 1;
	}

}

function delete_music_list($con,$audio_id,$music){
	$sql = mysqli_query($con, "DELETE from tbl_audios where audio_id='$audio_id' and music='$music'");
	if ($sql) {
		echo 1;
	}
}

function download_music($con,$current_loc, $id){
	$my_id = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$sql = "INSERT INTO  tbl_downloads(audio_id,user_id,date_,geo_location)
	  VALUES ('$id','$my_id','$date_','$current_loc');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
}

function downloadplaylist_music($id){
	$sql = "SELECT * FROM tbl_playlist_info where playlist_id = '$id'";
} 


function delete_album($con,$id,$album_artwork){
	$sql = mysqli_query($con,"DELETE from tbl_album where id='$id' and album_artwork='$album_artwork'");
	if ($sql) {
		echo 1;
	}
}

function new_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_audios");
	$row = mysqli_fetch_assoc($sql);
	return 'song-'.rand(1,9).rand(1,9).$row['max(id)'].rand(1,9).rand(1,9);
}

function upload_music($con,$link2,$song_name,$composer,$genres,$artist,$album,$ud,$audience,$application,$duration){

	$random_id = new_id($con);
	$sql = mysqli_query($con,"INSERT INTO tbl_audios values(id,'$random_id','$link2','$song_name','$composer','$artist','$genres','$album', '$ud',0,'','$audience','$application','$duration')");
	if ($sql) {
		echo 1;
	}
	else
	{
		echo 2;
	}
}

function show_songs($con,$search1)
{
	$myid = $_SESSION['users'];
	if ($search1 == "" || $search1 == null) {
	$sql =mysqli_query($con,"SELECT t1.*  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t2.`user_id` = '$myid' 
	ORDER BY t1.`song_name` ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
		<div class="row row-sm" > 
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	?>

		<div class="col-xs-6 col-sm-5 col-md-3 col-lg-2"> 
		<div class="item" > 
		<div class="pos-rlt" >
			<div class="item-overlay opacity r r-2x bg-black">  
                <div class="center text-center m-t-n"> 
                  <a href="#" data-toggle="class"> 
                    <i class="icon-control-play i-2x text" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
                    <i class="icon-control-pause i-2x text-active" onclick="pause_2();"></i> 
                  </a> 
                </div> 
              </div> 
			<div class="navbar bg-dark" style="margin-bottom: -5px;"> 
				<ul class="list-unstyled text-center text-white col-sm-12"> 
					<li class="dropdown" style="width: 100%; padding-top: 10px;">
					<?php
						if (strlen($row['genre'])>10) 
						{
							echo '<a href="#" class="dropdown-toggle text-white text-capitalize text-center" data-toggle="dropdown" style=" padding-top: 10px;"><marquee style="overflow:hidden;" class="col-sm-12"><b>'.strtoupper($row['genre']).'</b></marquee></a>'; 
						}
						else
						{
							echo '<a href="#" class="dropdown-toggle text-white text-capitalize text-center" data-toggle="dropdown" style="width: 100%; padding-top: 10px;"><b>'.strtoupper($row['genre']).'</b></a>'; 
						}
						?>
						<br>
						<?php  
						if($active == 1){ ?>
							<span class="badge bg-success text-white"><span class="fa fa-check-square-o"></span>&nbsp; Approved</span>
						<?php 	
						}elseif($active == 0){ ?>
							<span class="badge bg-info text-white"><span class="fa fa-minus-square-o"></span>&nbsp; Pending</span>
						<?php
						}else { ?>
							<span class="badge bg-danger text-white"><span class="fa fa-thumbs-o-down"></span>&nbsp; Disapproved</span>
						<?php	
						}
						?>  
						<ul class="dropdown-menu animated fadeInDown" style="margin-top: 20px;"> 
							<li><a href="#" onclick="delete_music('<?php echo $row['id'] ?>','<?php echo $row['music'] ?>');">Delete</a></li> 
							<li> 
								<a href="#" onclick="edit_audio('<?php echo $row['id'] ?>'">Edit</a> 
							</li> 
							<li><a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')">Add to</a></li>
         			 	</ul> 
        			</li> 
      			</ul> 
    		</div> 
		<a href="#"><img src="../img/musicicon2.png" alt="" class="r r-2x img-full"></a>
		</div>
		<div class="padder-v bg-dark" style="margin-top: -15px; margin-bottom: 10px; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;"> 
				<?php
				if (strlen($row['song_name'])>20) {
					echo '<a href="#" class="text-ellipsis bg-dark" style="padding-left:5px; padding-right:5px;"><marquee style=" overflow:hidden;" class="col-sm-12">'.strtoupper($row['song_name']).'</marquee></a>'; 
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px;">'.ucfirst($row['composer']).'</a>'; 
					
				}
				else
				{
					echo '<a href="#" class="text-ellipsis col-sm-12 bg-dark" style="padding-left: 5px;">'.strtoupper($row['song_name']).'</a>';
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px;">'.ucfirst($row['composer']).'</a>'; 
				}			
				?>				
        </div>
		</div>
		</div>
	<?php
	}
	?>
	</div>
	<?php		
	}
	else
	{
		echo '<div id="no_songs" class="text-center text-white col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;" data-toggle="modal" data-target="#addmusic" onclick="load_dropdowns();">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> Add Your Songs Now!</label></strong>
		</div>';
	}
	}
	else
	{

	$sql =mysqli_query($con,"SELECT t1.*,t6.`genre_name`  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id`
	LEFT JOIN tbl_genre t6 on t1.`genre` = t6.`gen_id`
	WHERE t2.`user_id` = '$myid' and concat(song_name,composer,upload_date,genre,application_id,description,keyword) like '%$search1%'
	ORDER BY t1.`song_name` ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>
	<div class="row row-sm">
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	?>
		<div class="col-xs-6 col-sm-5 col-md-3 col-lg-2"> 
		<div class="item" > 
		<div class="pos-rlt" >
			<div class="item-overlay opacity r r-2x bg-black">  
                <div class="center text-center m-t-n"> 
                  <a href="#" data-toggle="class"> 
                    <i class="icon-control-play i-2x text" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
                    <i class="icon-control-pause i-2x text-active" onclick="pause_2();"></i> 
                  </a> 
                </div> 
              </div> 
			<div class="navbar bg-dark" style="margin-bottom: -5px;"> 
				<ul class="list-unstyled text-center text-white col-sm-12"> 
					<li class="dropdown" style="width: 100%; padding-top: 10px;">
					<?php
						if (strlen($row['genre'])>10) 
						{
							echo '<a href="#" class="dropdown-toggle text-white text-capitalize text-center" data-toggle="dropdown" style=" padding-top: 10px;"><marquee style="overflow:hidden;" class="col-sm-12"><b>'.strtoupper($row['genre_name']).'</b></marquee></a>'; 
						}
						else
						{
							echo '<a href="#" class="dropdown-toggle text-white text-capitalize text-center" data-toggle="dropdown" style="width: 100%; padding-top: 10px;"><b>'.strtoupper($row['genre_name']).'</b></a>'; 
						}
						?>
						<br>
						<?php  
						if($active == 1){ ?>
							<span class="badge bg-success text-white"><span class="fa fa-check-square-o"></span>&nbsp; Approved</span>
						<?php 	
						}elseif($active == 0){ ?>
							<span class="badge bg-info text-white"><span class="fa fa-minus-square-o"></span>&nbsp; Pending</span>
						<?php
						}else { ?>
							<span class="badge bg-danger text-white"><span class="fa fa-thumbs-o-down"></span>&nbsp; Disapproved</span>
						<?php	
						}
						?>  
						<ul class="dropdown-menu animated fadeInDown" style="margin-top: 20px;"> 
							<li><a href="#" onclick="delete_music('<?php echo $row['id'] ?>','<?php echo $row['music'] ?>');">Delete</a></li> 
							<li> 
								<a href="#" onclick="edit_audio('<?php echo $row['id']?>');">Edit</a> 
							</li> 
							<li><a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')">Add to</a></li>
         			 	</ul> 
        			</li> 
      			</ul> 
    		</div> 
		<a href="#"><img src="../img/musicicon2.png" alt="" class="r r-2x img-full"></a>
		</div>
		<div class="padder-v bg-dark" style="margin-top: -15px; margin-bottom: 10px; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;"> 
				<?php
				if (strlen($row['song_name'])>20) {
					echo '<a href="#" class="text-ellipsis bg-dark" style="padding-left:5px; padding-right:5px;"><marquee style=" overflow:hidden;" class="col-sm-12">'.strtoupper($row['song_name']).'</marquee></a>'; 
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px;">'.ucfirst($row['composer']).'</a>'; 
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px; margin-bottom:5px;  border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;">'.ucfirst($row['artist']).'</a>'; 
				}
				else
				{
					echo '<a href="#" class="text-ellipsis col-sm-12 bg-dark" style="padding-left: 5px;">'.strtoupper($row['song_name']).'</a>';
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px;">'.ucfirst($row['composer']).'</a>'; 
					echo '<a href="#" class="text-ellipsis text-xs text-muted bg-dark" style="padding-left: 5px;  margin-bottom:5px;  border-bottom-left-radius: 4px; border-bottom-right-radius: 4px;">'.ucfirst($row['artist']).'</a>'; 
				}			
				?>				
        </div>
		</div>
		</div>
	<?php
	}
	?>
	</div>
	<?php
	}
	else
	{
		echo '<div id="no_songs" class="text-center text-white col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> No Records Found!</label></strong>
		</div>';
	}	
	}
	}


//WEBSITE
function showmymusic($con,$mymusic_search)
{
	$myid = $_SESSION['users'];
	if ($mymusic_search == "" || $mymusic_search == null) {
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t2.`user_id` = '$myid'
	ORDER BY song_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
		//$id = $row["t_id"];
		$a_id = $row["audio_id"];
		$group_id = $row['file_id'];

		$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
		$rows = mysqli_fetch_assoc($query);
		$total_files = $rows['files'];

	  	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
    	 $date_upload =  $row['upload_date'];
	?>	
			<tr class="track item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 40%;">
						<input type="checkbox" onclick="count_check();" name="id[]" class="select_accounts" value="<?php echo $row["music"]; ?>" /></h6>
				</td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<form action="album_musics_playlist.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php  
  						if($active == 1){ ?>
							<div class="badge bg-success text-white" >Approved</div>
  						<?php 	
  						}elseif($active == 0){ ?>
  							<div class="badge bg-info text-white">Pending
  							</div>
  						<?php
  						}else { ?>
  							<div class="badge bg-danger text-white ">Disapproved</div>
  						<?php	
  						}

  						?> 
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="w3-dropdown-hover">
							<button class="w3-button">Action <span class="fa fa-caret-down"></span></button>
							<div class="w3-dropdown-content w3-bar-block w3-border">
								<a href="#" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');" class="w3-bar-item w3-button">
									<i class="fa fa-info"></i><span> &nbsp;Details</span>
								</a>
								<a href="#" onclick="edit_audio('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['album_id'] ?>','<?php echo $row['file_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-edit"></i><span> &nbsp;Edit</span>
								</a>
								<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
								</a>
							</div>
						</div>               		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="8" class="text-center" style="background-color: #fff; color: #000; font-family: MavenPro-Regular; font-size: 18px;">
			<div class="alert alert-default alert-dismissible text-center" >
				  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
			</div>
		</td>
	<?php
	}
	}
	else
	{
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t2.`album_artwork`,t2.`album_name`  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	WHERE t2.`user_id` = '$myid' 
	AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
	LIKE '%$mymusic_search%'
	ORDER BY song_name ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}

	?>
			<tr class="track item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 40%;">
						<input type="checkbox" onclick="count_check();" name="id[]" class="select_accounts" value="<?php echo $row["music"]; ?>" /></h6>
				</td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<form action="album_musics_playlist.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php  
  						if($active == 1){ ?>
							<div class="badge bg-success text-white" >Approved</div>
  						<?php 	
  						}elseif($active == 0){ ?>
  							<div class="badge bg-info text-white">Pending
  							</div>
  						<?php
  						}else { ?>
  							<div class="badge bg-danger text-white ">Disapproved</div>
  						<?php	
  						}

  						?> 
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="w3-dropdown-hover">
							<button class="w3-button">Action <span class="fa fa-caret-down"></span></button>
							<div class="w3-dropdown-content w3-bar-block w3-border">
								<a href="#" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');" class="w3-bar-item w3-button">
									<i class="fa fa-info"></i><span> &nbsp;Details</span>
								</a>
								<a href="#" onclick="edit_audio('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['album_id'] ?>','<?php echo $row['file_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-edit"></i><span> &nbsp;Edit</span>
								</a>
								<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
								</a>
							</div>
						</div>               		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="8" class="text-center" style="background-color: #fff; color: #000; font-family: MavenPro-Regular; font-size: 18px;">
			<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	}

function show_albums($con,$search)
{
	if ($search == "" || $search == null) 
	{
	$myid = $_SESSION['users'];
	$sql = mysqli_query($con, "SELECT t1.`id`,t1.`album_id`,t1.`album_artwork`,t1.`album_name`,t1.`date_released`,COUNT(t2.`album_id`) AS songs, t1.`album_description` FROM tbl_album t1 LEFT JOIN tbl_audios t2 ON t1.`album_id`=t2.`album_id` WHERE t1.`user_id`='$myid' GROUP BY t1.`album_id` ");
	if (mysqli_num_rows($sql)) 
	{
	?>

	<?php
	while ($row = mysqli_fetch_assoc($sql)) 
	{

	$pict ='';
	if ($row['album_artwork'] == "") {
	$pict = "../img/slogo3.png";
	}
	else
	{
	$pict = '../'.$row['album_artwork'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$pict.");'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
	$toll_title="<center>".'ALBUM NAME'."</center>";	
	?>	
			<tr class="tracks item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 20%;">
						<input type="checkbox" onclick="count_check();" name="id[]" class="select_albums" value="<?php echo $row["id"]; ?>" /></h6>
				</td>
				<td class="text-center" style="width: 10%;">
					<img class="imgs_album img-fluid" src="<?php echo $pict; ?>" alt="" style="padding: 5px;">
				</td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo $row['album_name'] ?></h6></td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo $row['album_description'] ?></h6></td>
				<td class="text-left">
					<h6 class="db-content default-fs default_margin" style="margin-left: 4%;">
						<?php echo $row['songs']; ?>
						
					</h6>
				</td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo date('m-d-Y', strtotime($row['date_released'])); ?></h6></td>
				<td class="text-left">
					<div class="btn-group btn-justified text-center" id="btns"> 
					       <button class="btn btn-dark btn-small" onclick="edit_album('<?php echo $row['id'] ?>','<?php echo $row['album_name'] ?>','<?php echo '../'.$row['album_artwork'] ?>', '<?php echo $row['album_description'] ?>','<?php echo $row['album_artwork'] ?>');"><span class="fa fa-edit"></span>
							</button>
							<form action="album_musics.php" method="post">
								<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
				                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
								<button class="btn btn-dark btn-small" type="submit" id="btn_submit"><span class="fa fa-eye fa-lg"></span>
		                   	 	</button>  
		            		</form>    	
					</div>
				</td>
			</tr>
			<script type="text/javascript">
				enable_tooltips();
			</script>
		<?php
			}
		?>

	<?php
		}
		else
		{ ?>
			
			<tr>
				<td colspan="7">
					<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-image fa-3x"></span> <br> No Album Found!</strong>
					</div>
				</td>
			</tr>
<?php
		}
	}
	else
	{
		$myid = $_SESSION['users'];

		$sql =mysqli_query($con,"SELECT t1.`id`,t1.`album_id`,t1.`album_artwork`,t1.`album_name`,t1.`date_released`,COUNT(t2.`album_id`) AS songs, t1.`album_description` FROM tbl_album t1 LEFT JOIN tbl_audios t2 ON t1.`album_id`=t2.`album_id` WHERE t1.`user_id`='$myid' and concat(t1.`album_name`,t1.`date_released`) like '%$search%' GROUP BY t1.`album_id` order by t1.`album_name`");
			
		if (mysqli_num_rows($sql)) 
		{
	?>

		<?php
			while ($row = mysqli_fetch_assoc($sql)) 
			{
				$pict ='';
				if ($row['album_artwork'] == "") {
				$pict = "../img/slogo3.png";
				}
				else
				{
				$pict = '../'.$row['album_artwork'];
				}

				$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$pict.");'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
				$toll_title="<center>".'ALBUM NAME'."</center>";			
					?>

			<tr class="tracks item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 20%;">
						<input type="checkbox" onclick="count_check();" name="id[]" class="select_albums" value="<?php echo $row["id"]; ?>" /></h6>
				</td>
				<td class="text-center" style="width: 10%;">
					<img class="imgs_album img-fluid" src="<?php echo $pict; ?>" alt="" style="padding: 5px;">
				</td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo $row['album_name'] ?></h6></td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo $row['album_description'] ?></h6></td>
				<td class="text-left">
					<h6 class="db-content default-fs default_margin" style="margin-left: 4%;">
						<?php echo $row['songs']; ?>
						
					</h6>
				</td>
				<td class="text-left"><h6 class="db-content default-fs default_margin" style="margin-left: 4%;"><?php echo date('m-d-Y', strtotime($row['date_released'])); ?></h6></td>
				<td class="text-left">
					<div class="btn-group btn-justified text-center" id="btns"> 
					       <button class="btn btn-dark btn-small" onclick="edit_album('<?php echo $row['id'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['album_artwork'] ?>');"><span class="fa fa-edit"></span>
							</button>
							<button class="btn btn-dark btn-small"" onclick="display_album_audio('<?php echo $row['album_id']?>');"><span class="fa fa-eye"></span>
							</button>     	
					</div>
				</td>
			</tr>

			<script type="text/javascript">
					enable_tooltips();
				</script>

		<?php
			}
		?>


	<?php
		}
		else
		{
			echo '<tr>
				<td colspan="7"><div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-image fa-3x"></span> <br> No Album Found!</strong>
					</div></td>
			</tr>';
		}
	}
}

function show_album_audio($con,$alb_id_modal,$album_songs_) {
if ($album_songs_ == "" || $album_songs_ == null) {
	$sql = mysqli_query($con,"SELECT a.*,c.`genre_name`,d.`application_name` from tbl_audios a 
		LEFT JOIN tbl_album b ON a.`album_id` = b.`album_id` 
		LEFT JOIN tbl_genre c ON a.`genre` = c.`gen_id`
		LEFT JOIN tbl_application d ON a.`application_id` = d.`application_id`  
		where b.`album_id`='$alb_id_modal' ORDER BY song_name ASC");

	if(mysqli_num_rows($sql) > 0)
	{
	     while($row = mysqli_fetch_array($sql))
	     {

	    ?>
 			<tr  class="bg-white text-black">
	        <td >      
	        	<input type="hidden" id="d_audio_id" value="<?php echo $row['audio_id'];?>"/>
	        	<span class="fa fa-music"></span> &nbsp; <?php echo ucfirst($row["song_name"]); ?>     
	        <td>
	            <?php echo ucfirst($row["composer"]); ?> 
	        </td>
	        <td>
	            <?php echo ucfirst($row["genre"]); ?> 
	        </td>
	        <td>
	            <?php echo ucfirst($row["application_id"]); ?> 
	        </td>
	        <td style="text-align: center;">
    			<span onclick="add_to('<?php echo $row['audio_id'] ?>')"><i class="icon-plus i-1x text-info"></i></span>
    			<a href="#" data-toggle="class" style="background-color: rgba(0,0,0,0.0);"> 
                    <i class="icon-control-play text text-success" title="Play" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
                    <i class="icon-control-pause text-active" title="Pause" onclick="pause_2('../<?php echo $row['music'] ?>');"></i> 
	            </a>
	            <span title="Delete" onclick="delete_alb_music('<?php echo $row['id'] ?>','<?php echo $row['music'] ?>');">
	            	<i class="icon-trash text-danger"></i>
	            </span>
	        </td>
	      </tr> 
	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-dark"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
		</div>
		</td>
	<?php  
	}

}else{

	//concat(song_name,composer,artist,genre)
	$sql =mysqli_query($con, "SELECT a.* from tbl_audios a 
		LEFT JOIN tbl_album b ON a.`album_id` = b.`album_id` 
		WHERE b.`album_id`='$alb_id_modal' 
		AND CONCAT(song_name,composer,upload_date,genre,application_id,description,keyword) 
		LIKE '%$album_songs_%' 
		ORDER BY a.`song_name` ASC");


	if(mysqli_num_rows($sql) > 0)
	{
	     while($row = mysqli_fetch_array($sql))
	     {
	      
	    ?>
			<tr  class="bg-white text-black">
	        <td >      
	        	<input type="hidden" id="d_audio_id" value="<?php echo $row['audio_id'];?>"/>
	        	<span class="fa fa-music"></span> &nbsp; <?php echo ucfirst($row["song_name"]); ?>     
	        <td>
	            <?php echo ucfirst($row["composer"]); ?> 
	        </td>
	        <td>
	            <?php echo ucfirst($row["genre"]); ?> 
	        </td>
	        <td>
	            <?php echo ucfirst($row["application_id"]); ?> 
	        </td>
	        <td style="text-align: center;">
    			<span onclick="add_to('<?php echo $row['audio_id'] ?>')"><i class="icon-plus i-1x text-info"></i></span>
    			<a href="#" data-toggle="class" style="background-color: rgba(0,0,0,0.0);"> 
                    <i class="icon-control-play text text-success" title="Play" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
                    <i class="icon-control-pause text-active" title="Pause" onclick="pause_2('../<?php echo $row['music'] ?>');"></i> 
	            </a>
	            <span title="Delete" onclick="delete_alb_music('<?php echo $row['id'] ?>','<?php echo $row['music'] ?>');">
	            	<i class="icon-trash text-danger"></i>
	            </span>
	        </td>
	      </tr> 
	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-dark"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
		</div>
		</td>
	<?php  
	}

}
}

function ShowAlbum_Audio($con, $id) 
{
	$sql = mysqli_query($con, "SELECT t1.`album_id`, t1.`album_name`, t1.`album_artwork`, CONCAT(t2.`fn`,' ',t2.`ln`) AS NAME
								FROM tbl_album t1 LEFT JOIN tbl_user_profile t2 ON t1.`user_id` = t2.`user_id` 
								WHERE album_id = '$id'");

	$sql2 = mysqli_query($con, "SELECT * FROM tbl_audios WHERE album_id = '$id'");
	$nums = mysqli_num_rows($sql2);

	$album_rows = mysqli_fetch_assoc($sql);
?>
	<section class="content">
        <div class="row">
            <div class="col-sm-4">
              <div class="text-center">
                <img src="../<?php echo $album_rows['album_artwork']?>" height="200" width="200">
              </div>
            </div>

            <div class="col-sm-8">
            	<div class="container">
                	<h6>ALBUM</h6>
                	<h3><?php echo $album_rows['album_name']?></h3><br>
                	<h6>Created by: <?php echo $album_rows['NAME']?> | <?php echo $nums?> songs</h6><br>

	                <div class="m-b-sm"> 
	                	<div class="btn-group text-center"> 
	                		<a href="#" class="btn btn-success" id="btn_modal_play" style="display: ;" onclick="">
	                			<span class="fa fa-play"></span> PLAY
	                		</a> 
	                		<a href="#" class="btn btn-success" id="btn_modal_pause" style="display:none ;" onclick="">
	                			<span class="fa fa-pause"></span> PAUSE
	                		</a> 
	                		<!-- <a href="#" class="btn btn-info">FOLLOW</a>  -->
	                		<a href="#" id="btn-select" class="btn btn-primary" onclick="$('#uploaded_song').click();">
	                			<span class="fa fa-plus"></span> UPLOAD SONG
	                		</a> 
	                		<a href="#" class="btn btn-danger" toggle="tooltip" title="Empty Album">
	                			<span class="icon-trash"></span>
	                		</a>
	                	</div>
	              	</div>
            	</div>
            </div> 	
    </section>
     <br>  
   
<?php 
}


//update  account name
function update_name($con,$id,$fn,$mn,$ln){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set fn='$fn',mn='$mn',ln='$ln' where user_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function update_bdate($con,$bdate,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set b_date='$bdate' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_email($con,$email,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set email_add='$email' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_username($con,$username,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set username='$username' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}


function update_pwd($con,$pwd,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set password='$pwd' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}


//Session update
function refresh_account($con,$myid){
	$id = $_SESSION['users'];
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$myid' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['users'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
	$_SESSION['user_pic'] = '../avatar/def.png';
					
	}
	else
	{
		$_SESSION['user_pic'] = '../'.$row['picture'];
	}

	$dt1 = date_create($row['age']);
	$dt2 = date('Y-m-d');
	$dt3 = date_create($dt2);	
	$age = date_diff($dt1,$dt3);
	$is_notify = $row['is_notify'];

	?>
	<div class="list-group glass" style="padding-top: 10px; padding-right: 10px; padding-left: 10px; padding-bottom: 10px;">
			
			<a href="#" data-target="#edit_name_modal" style="color: #6C7A89" onclick="update_name('<?php echo $_SESSION['fn'] ?>','<?php echo $_SESSION['ln'] ?>','<?php echo $_SESSION['users'] ?>');" class="list-group-item text-left w3-hover-shadow" data-toggle="modal" data-placement="left" title="Name"><i class=" fa fa-user fa-lg"></i> &nbsp; <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a><br>
			
			<a href="#" data-target="#edit_email_modal"style="color: #6C7A89"  onclick="update_email('<?php echo $_SESSION['email_add'] ?>');" class="list-group-item w3-hover-shadow text-left" data-toggle="modal" data-placement="left" title="E-mail"><i class=" fa fa-envelope fa-lg"></i> &nbsp; <?php echo $_SESSION['email_add'] ?></a><br>
			
			<a href="#" data-target="#edit_pass_modal" style="color: #6C7A89" onclick="update_pwd('<?php echo $_SESSION['password'] ?>');"  data-toggle="modal" data-placement="left" title="Change your password" class="list-group-item w3-hover-shadow text-left"><i class=" fa fa-lock fa-lg"></i> &nbsp; Change Password</a><br>
		</div>
		<script type="text/javascript">

		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		});
		</script>
	<?php
}

function pict_session($con,$myid){
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$myid' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['users'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
		$pict = '../avatar/def.png';
					
		}
		else
		{
			$_SESSION['user_pic'] = '../'.$row['picture'];
		}

		echo $_SESSION['user_pic'];
}

function update_pf($con,$path){
	$myid = $_SESSION['users'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set picture='$path' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
	else{
		echo 2;
	}
}


function show_genres($con,$search2)
{
	if ($search2 == "" || $search2 == null) {
	$sql =mysqli_query($con,"SELECT a.*,b.*, count(b.`id`) as total_songs FROM tbl_genre a 
		LEFT JOIN tbl_audios b on a.genre_name=b.genre GROUP BY a.genre_name ORDER BY genre_name ASC");

	if (mysqli_num_rows($sql)>0) {
	?>		
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>
        <ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto">
            <li class="list-group-item text-white" >
               <div class="pull-right "> 
                   <span class="btn-group pull-right">
                      <a href="#" class="fa fa-chevron-down pull-right dropdown-toggle" data-toggle="dropdown"></a>
                      <ul class="dropdown-menu text-left col-sm-6">
                      	<li>
                      		<a href="#" onclick="stylemusics('<?php echo $row['genre_name']?>');">
                      		<i class="fa fa-eye"></i><span> &nbsp;Open</span>
                      		</a>
                      	</li>
						  <li>
							<a href="#">
							    <i class="fa fa-play-circle-o"></i><span> &nbsp;Play</span>
							</a>
						  </li>
					  </ul>
					</span><br>          	
                    <div class="clear text-ellipsis text-muted" style="font-size: 13px;">
                     
                    </div>
				</div>        
                  	<a href="#" class=" m-r-sm pull-left" onclick="stylemusics('<?php echo $row['genre_name'] ?>');"> 
                    	<img src="../img/microphone.png" width="40" height="40">
                   	</a> 
					<div class="clear text-ellipsis"> 
						<span><?php echo ucfirst($row['genre_name']); ?></span> 
						<div class="clear text-ellipsis text-muted" style="font-size: 13px;">
							<?php echo ucfirst($row['total_songs']); ?> Total Songs
						</div>
					</div>
			</li>
		</ul>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<div class="col-sm-12 text-center text-white" style="background-color:rgba(0,0,0,0.23); font-size:20px;">No Records Found...</div>';
		}
		}
		else
		{

		$sql =mysqli_query($con,"SELECT a.*,b.*, count(b.`id`) as total_songs FROM tbl_genre a 
		LEFT JOIN tbl_audios b on a.genre_name=b.genre  where concat(genre_name) like '%$search2%' GROUP BY a.genre_name ORDER BY genre_name ASC");

		if (mysqli_num_rows($sql)>0) {
		?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
        <ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto">
            <li class="list-group-item text-white" >
               <div class="pull-right "> 
                   <span class="btn-group pull-right">
                      <a href="#" class="fa fa-chevron-down pull-right dropdown-toggle" data-toggle="dropdown"></a>
                      <ul class="dropdown-menu text-left col-sm-6">
                      	<li>
                      		<a href="#" onclick="stylemusics('<?php echo $row['genre_name']?>');">
                      		<i class="fa fa-eye"></i><span> &nbsp;Open</span>
                      		</a>
                      	</li>
						  <li>
							<a href="#">
							    <i class="fa fa-play-circle-o"></i><span> &nbsp;Play</span>
							</a>
						  </li>
					  </ul>
					</span><br>          	
                    <div class="clear text-ellipsis text-muted" style="font-size: 13px;">
                     
                    </div>
				</div>        
                  	<a href="#" class=" m-r-sm pull-left" onclick="stylemusics('<?php echo $row['genre_name']?>');"> 
                    	<img src="../img/microphone.png" width="40" height="40">
                   	</a> 
					<div class="clear text-ellipsis"> 
						<span><?php echo ucfirst($row['genre_name']); ?></span> 
						<div class="clear text-ellipsis text-muted" style="font-size: 13px;">
							<?php echo ucfirst($row['total_songs']); ?> Total Songs
						</div>
					</div>
			</li>
		</ul>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<div class="col-sm-12 text-center text-white" style="background-color:rgba(0,0,0,0.23); font-size:20px;">No Records Found...</div>';
		}
		}
		}

function show_genres2($con,$search2a)
	{
	if ($search2a == "" || $search2a == null) {
	$sql =mysqli_query($con,"SELECT a.*,b.*, count(b.`id`) as total_songs FROM tbl_genre a 
		LEFT JOIN tbl_audios b on a.genre_name=b.genre GROUP BY a.genre_name ORDER BY genre_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>		
	
	<div class="row row-sm" > 
	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>
	
		<div class="col-xs-6 col-sm-5 col-md-3 col-lg-2"> 
		<div class="item" > 
		<div class="pos-rlt" >

				<div class="item-overlay opacity r r-2x bg-black">  
                    <div class="center text-center m-t-n"> 
                      <a href="#" data-toggle="class" onclick="stylemusics('<?php echo $row['genre_name'] ?>');"> 
                        <i class="icon-eye i-2x"></i> 
                      </a> 
                    </div> 
                  </div>  
                


				<a href="#">
                    <img src="../img/microphone.png" alt="" class="r r-2x img-full">
                  </a>

				</div>

				<div class="padder-v" style="margin-top: -1px; margin-bottom: 10px; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px; background-color: #03344c;"> 
					<div class="pull-right " style="padding-right: 5px;"> 
                   		<span class="btn-group pull-right">
                      		<a href="#" class="fa fa-chevron-down pull-right dropdown-toggle text-white" data-toggle="dropdown"></a>
                      		   <ul class="dropdown-menu text-left col-sm-6">
	                      	<li>
								<a href="#"  onclick="">
								    <i class="fa fa-play-circle-o"></i><span> &nbsp;Play</span>
								</a>
							  </li>
						  </ul>
                      	</span>
                    </div>
                    <div class="clear text-ellipsis" style="padding-left: 5px;">
                    	<span>
							<a href="#" class="text-white"><?php echo $row['genre_name']?></a>
                    	</span>
                       <div class="clear text-ellipsis text-muted" style="font-size: 13px;"><?php echo $row['total_songs']?> songs</div>
                    </div>
                </div> 

				</div>
				</div>
		<?php
		}
		?>
			</div>
		<?php
		}
		else
		{
			echo '<div class="col-sm-12 text-center text-white" style="background-color:rgba(0,0,0,0.23); font-size:20px;">No Records Found...</div>';
		}
		}
		else
		{
		$sql =mysqli_query($con,"SELECT a.*,b.*, count(b.`id`) as total_songs FROM tbl_genre a 
		LEFT JOIN tbl_audios b on a.genre_name=b.genre  where concat(genre_name) like '%$search2a%' GROUP BY a.genre_name ORDER BY genre_name ASC");

		if (mysqli_num_rows($sql)>0) {
		?>
			<div class="row row-sm" >
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
	
		<div class="col-xs-6 col-sm-5 col-md-3 col-lg-2"> 
		<div class="item" > 
		<div class="pos-rlt" >

				<div class="item-overlay opacity r r-2x bg-black">  
                    <div class="center text-center m-t-n"> 
                      <a href="#" data-toggle="class" onclick="stylemusics('<?php echo $row['genre_name'] ?>');"> 
                        <i class="icon-eye i-2x"></i> 
                      </a> 
                    </div> 
                  </div>  
                


				<a href="#">
                    <img src="../img/microphone.png" alt="" class="r r-2x img-full">
                  </a>

				</div>

				<div class="padder-v" style="margin-top: -1px; margin-bottom: 10px; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px; background-color: #03344c;"> 
					<div class="pull-right " style="padding-right: 5px;"> 
                   		<span class="btn-group pull-right">
                      		<a href="#" class="fa fa-chevron-down pull-right dropdown-toggle text-white" data-toggle="dropdown"></a>
                      		   <ul class="dropdown-menu text-left col-sm-6">
	                      	<li>
								<a href="#"  onclick="">
								    <i class="fa fa-play-circle-o"></i><span> &nbsp;Play</span>
								</a>
							  </li>
						  </ul>
                      	</span>
                    </div>
                    <div class="clear text-ellipsis" style="padding-left: 5px;">
                    	<span>
							<a href="#" class="text-white"><?php echo $row['genre_name']?></a>
                    	</span>
                       <div class="clear text-ellipsis text-muted" style="font-size: 13px;"><?php echo $row['total_songs']?> songs</div>
                    </div>
                </div> 

				</div>
				</div>
		<?php
		}
		?>
			</div>
		<?php
		}
		else
		{
			echo '<div class="col-sm-12 text-center text-white" style="background-color:rgba(0,0,0,0.23); font-size:20px;">No Records Found...</div>';
		}
		}
		}
 
//WEBSITE
function show_musics($con,$search3,$show_applications,$order,$title_name)
	{
	if ($search3 == "" || $search3 == null) {
		if ($show_applications == '0') {
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`active` = 1
					ORDER BY ".$title_name." ".$order." ");
		}else{
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id` 
					WHERE a.`application_id` like '%$show_applications%' AND a.`active` = 1
					ORDER BY ".$title_name." ".$order."");
		}
		
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>
			<tr scope="row" class="bg-white text-left">
                <td  data-toggle="tooltip" title="<?php echo ucfirst($row['song_name']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
                   	<?php echo ucfirst($row['song_name'])?>
                </td>
                <td  data-toggle="tooltip" title="<?php echo ucfirst($row['description']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
                   	<?php echo ucfirst($row['description'])?>
               	</td>
               	<td  data-toggle="tooltip" title="<?php echo ucfirst($row['genre']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['genre'])?>
           		</td>
           		<td  data-toggle="tooltip" title="<?php echo ucfirst($row['composer']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['composer'])?>
           		</td>
               	<td  data-toggle="tooltip" title="<?php echo ucfirst($row['application_id']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['application_id'])?>
           		</td>
           		<td  data-toggle="tooltip" title="<?php echo date('m-d-Y', strtotime($row['upload_date']));?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
           			<?php echo date('m-d-Y', strtotime($row['upload_date']));?>
           		</td>
               	<td>
               		<div class="col-sm-12 row text-center">
               		<div class="col-sm-4">
               			<a href="#" class="btn btn-primary">
	                   	<i class="fa fa-play-circle fa-lg"></i>
	                   </a>
               		</div>
               		<div class="col-sm-4">
	               		<a href="../<?php echo $row['music'] ?>" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');" class="btn btn-primary">
	                   	<i class="fa fa-download fa-lg"></i>
	                   </a>
               		</div>
               		<div class="col-sm-4">
 					<ul class="nav nav-pills ">
					  <li class="nav-item dropdown ">
					    <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="true"></a>
					    <div class="dropdown-menu active btn btn-primary">
					      <a href="#" onclick="song_details('<?php echo $row['audio_id'] ?>','<?php echo $row['album_id']?>','<?php echo $row ['user_id']?>');" class="dropdown-item">
					  		<i class="fa fa-info fa-lg"></i><span> &nbsp;Details</span>
					  		</a>
							<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="dropdown-item">
						  		<i class="fa fa-plus-square-o fa-lg"></i><span> &nbsp;Add to...</span>
						  	</a>
					    </div>
					  </li>
					</ul>
               		</div>
               		</div>
           		</td>
            </tr>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<td colspan="7" class="text-center" style="background-color: rgba(0,0,0,0.3);"><div class="text-white" style="font-size:20px; text-align: center;">No Records Found...</div></td>';
		}
		}
		else
		{
		if ($show_applications == '0') {
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`active` = 1 
					AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
					LIKE '%$search3%'  
					ORDER BY ".$title_name." ".$order."");
		}else{
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`application_id` like '%$show_applications%' 
					AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
					LIKE '%$search3%'
					AND a.`active` = 1
					ORDER BY ".$title_name." ".$order."");
		}
		if (mysqli_num_rows($sql)>0) {
		?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
			<tr scope="row" class="bg-white text-left">
                <td  data-toggle="tooltip" title="<?php echo ucfirst($row['song_name']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
                   	<?php echo ucfirst($row['song_name'])?>
                </td>
                <td  data-toggle="tooltip" title="<?php echo ucfirst($row['description']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
                   	<?php echo ucfirst($row['description'])?>
               	</td>
               	<td  data-toggle="tooltip" title="<?php echo ucfirst($row['genre']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['genre'])?>
           		</td>
           		<td  data-toggle="tooltip" title="<?php echo ucfirst($row['composer']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['composer'])?>
           		</td>
               	<td  data-toggle="tooltip" title="<?php echo ucfirst($row['application_id']); ?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
               		<?php echo ucfirst($row['application_id'])?>
           		</td>
           		<td  data-toggle="tooltip" title="<?php echo date('m-d-Y', strtotime($row['upload_date']));?>" style="white-space: nowrap; width: 10px; max-width: 11px; overflow: hidden; text-overflow: ellipsis; ">
           			<?php echo date('m-d-Y', strtotime($row['upload_date']));?>
           		</td>
               	<td>
               		<div class="col-sm-12 row text-center">
               		<div class="col-sm-4">
               			<a href="#" class="btn btn-primary">
	                   	<i class="fa fa-play-circle fa-lg"></i>
	                   </a>
               		</div>
               		<div class="col-sm-4">
	               		<a href="../<?php echo $row['music'] ?>" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');" class="btn btn-primary">
	                   	<i class="fa fa-download fa-lg"></i>
	                   </a>
               		</div>
               		<div class="col-sm-4">
 					<ul class="nav nav-pills ">
					  <li class="nav-item dropdown ">
					    <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="true"></a>
					    <div class="dropdown-menu active btn btn-primary">
					      <a href="#" onclick="song_details('<?php echo $row['audio_id'] ?>','<?php echo $row['album_id']?>','<?php echo $row ['user_id']?>');" class="dropdown-item">
					  		<i class="fa fa-info fa-lg"></i><span> &nbsp;Details</span>
					  		</a>
							<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="dropdown-item">
						  		<i class="fa fa-plus-square-o fa-lg"></i><span> &nbsp;Add to...</span>
						  	</a>
					    </div>
					  </li>
					</ul>
               		</div>
               		</div>
           		</td>
            </tr>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<td colspan="7" class="text-center" style="background-color: rgba(0,0,0,0.3);"><div class="text-white" style="font-size:20px; text-align: center;">No Records Found...</div></td>';
		}
	}
}

function show_musics2($con,$search3a,$show_applications)
{
		if ($search3a == "" || $search3a == null) {
		if ($show_applications == '0') {
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`active` = 1
					ORDER BY a.`song_name` DESC");
		}else{
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`application_id` like '%$show_applications%'
					AND a.`active` = 1
					ORDER BY a.`song_name` DESC");

		}
		if (mysqli_num_rows($sql)>0) {
		?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>

		<tr class="text-black bg-white">
                <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['song_name'])?>
                </td>
                <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['composer'])?>
               	</td>
               <!--  <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['artist'])?>
               	</td> -->
               	<td style="text-transform: capitalize;">
               		<?php echo ucfirst($row['genre'])?>
           		</td>
               	<td style="text-transform: capitalize;">
               		<?php echo ucfirst($row['application_id'])?>
           		</td>
           		<td style="text-transform: capitalize;">
               		<?php echo date('M d, Y', strtotime($row['upload_date'])); ?>
           		</td>
               	<td style="text-transform: capitalize; text-align: center;">
               		<a href="#"  data-toggle="class" style="background-color: rgba(0,0,0,0.0);"> 
	                    <i class="icon-control-play text" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
	                    <i class="icon-control-pause text-active" onclick="pause_2('../<?php echo $row['music'] ?>');"></i> 
	                </a> &nbsp;&nbsp;&nbsp;

	                <a href="../<?php echo $row['music'] ?>" class="m-r-sm" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
                   		<i class="icon-cloud-download "></i>
                   	</a>&nbsp;&nbsp;&nbsp;

                    <a href="#" class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" style="margin-top: -7px;"></a>
                      <ul class="dropdown-menu text-left col-sm-6">
						  <li>
							<a href="#" onclick="song_details('<?php echo $row['audio_id'] ?>','<?php echo $row['album_id']?>','<?php echo $row ['user_id']?>');">
							    <i class="icon-info"></i><span> &nbsp;Info</span>
							</a>
						  </li>
						  <li>
						  	<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')">
						  		<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
						  	</a>
						  </li>
					  </ul>
           		</td>
            </tr>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<td colspan="7" class="text-center" style="background-color: rgba(0,0,0,0.3);"><div class="text-white" style="font-size:20px; text-align: center;">No Records Found...</div></td>';
		}
		}
		else
		{

		if ($show_applications == '0') {
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`active` = 1 
					AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
					LIKE '%$search3a%'
					ORDER BY a.`song_name` DESC");
		}else{
			$sql = mysqli_query($con,"SELECT a.*,b.* FROM tbl_audios a 
					LEFT JOIN tbl_album b on a.`album_id`=b.`album_id`
					WHERE a.`application_id` like '%$show_applications%' 
					AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
					LIKE '%$search3a%'
					AND a.`active` = 1
					ORDER BY a.`song_name` DESC");
		}
		if (mysqli_num_rows($sql)>0) {
		?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
			<tr class="text-black bg-white">
                <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['song_name'])?>
                </td>
                <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['composer'])?>
               	</td>
               <!--  <td style="text-transform: capitalize;">
                   	<?php echo ucfirst($row['artist'])?>
               	</td> -->
               	<td style="text-transform: capitalize;">
               		<?php echo ucfirst($row['genre'])?>
           		</td>
               	<td style="text-transform: capitalize;">
               		<?php echo ucfirst($row['application_id'])?>
           		</td>
           		<td style="text-transform: capitalize;">
               		<?php echo date('M d, Y', strtotime($row['upload_date'])); ?>
           		</td>
               	<td style="text-transform: capitalize; text-align: center;">
               		<a href="#"  data-toggle="class" style="background-color: rgba(0,0,0,0.0);"> 
	                    <i class="icon-control-play text" onclick="playmyAudio('../<?php echo $row['music'] ?>');"></i> 
	                    <i class="icon-control-pause text-active" onclick="pause_2('../<?php echo $row['music'] ?>');"></i> 
	                </a> &nbsp;&nbsp;&nbsp;
	                <a href="../<?php echo $row['music'] ?>" class="m-r-sm" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
                   	<i class="icon-cloud-download "></i></a>&nbsp;&nbsp;&nbsp;
                      <a href="#" class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown" style="margin-top: -7px;"></a>
                      <ul class="dropdown-menu text-left col-sm-6">
						  <li>
							<a href="#"  onclick="song_info('<?php echo $row['id'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['duration'] ?>','<?php echo date('M d, Y', strtotime($row['upload_date'])); ?>');">
							    <i class="icon-info"></i><span> &nbsp;Info</span>
							</a>
						  </li>
						  <li>
						  	<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')">
						  		<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
						  	</a>
						  </li>
					  </ul>
           		</td>
            </tr>
		<?php
		}
		?>
		<?php
		}
		else
		{
			echo '<td colspan="7" class="text-center" style="background-color: rgba(0,0,0,0.3);"><div class="text-white" style="font-size:20px; text-align: center;">No Records Found...</div></td>';
		}
		}
		}




//show upload details
function album_details($con,$album_id){

$sql = mysqli_query($con, "SELECT t1.*,t2.*, count(t3.`id`) as total_songs FROM tbl_user_profile t1
	LEFT JOIN tbl_album t2 on t1.`user_id` = t2.`user_id` 
	LEFT JOIN tbl_audios t3 on t2.`album_id` = t3.`album_id` 
	where t2.`album_id` = '$album_id'");
$row = mysqli_fetch_assoc($sql);

if ($row['album_artwork'] == "") {
		$img =  '../img/slogo3.png';
		}
		else
		{
		$img = '../'.$row['album_artwork'];
		} ?>
<div class="col-sm-3" id="album_img_info">
        <img class="img-responsive contri_img" src="../img/img.png" style="background-image: url(<?php echo $img; ?>); border-radius: 5px;">
        <button class="btn btn-block btn-primary btn-oji"><span class="fa fa-folder"></span><?php echo $row['album_name']; ?></button> 
        <button class="btn btn-block btn-primary btn-oji"><span class="fa fa-user"></span><?php echo $row['fn'].' '. $row['ln']; ?></button>  
</div>
      <div class="col-sm-9">
        <ul class="nav nav-tabs">
          <li class="nav-item">
              <a class="nav-link active show" data-toggle="tab" href="#songs">
              	<?php 
              	if($row['total_songs'] <= 1 ){ ?>
              		<?php echo $row['total_songs']; ?> Song
              	<?php }else{ ?>
					<?php echo $row['total_songs']; ?> Songs
              	<?php } ?> 
              </a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active show" id="songs">
              <table class="table table-hover" id="album_songs">
                <tbody>

                	<?php 
                		$query = mysqli_query($con, "SELECT t1.* FROM tbl_audios t1 
                			LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
                			LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
                			WHERE t2.`album_id` = '$album_id'
                			");
                if (mysqli_num_rows($query)>0) 
                {
					while ($row2 = mysqli_fetch_assoc($query)) 
					{
					?>
                  <tr class="w3-hover text-white">
                    <td class="text-left">
                    	<span class="fa fa-music"></span>&nbsp; <?php echo $row2['song_name'];?><br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row2['composer']; ?>
                    </td>
                    <td><?php echo date('H:i', strtotime($row2['duration'])); ?> </td>
                  </tr>
          	  <?php } ?>
          <?php } else
          		{ ?>
              	No Music Found.
          <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
      </div>

<?php 
}

function show_playlist_songs($con,$plistid,$plist_song_search)
{
	if ($plist_song_search == "" || $plist_song_search == null) {
		$sql = mysqli_query($con,"SELECT a.*,e.`album_artwork`,c.`name`,b.`playlist_id`,b.`id` as t_id from tbl_audios a 
		LEFT JOIN tbl_playlist_info b ON a.`audio_id` = b.`audio_id`  
		LEFT JOIN tbl_playlist c ON b.`playlist_id` = c.`id`
		LEFT JOIN tbl_genre d ON a.`genre` = d.`gen_id`
		LEFT JOIN tbl_album e ON a.`album_id` = e.`album_id`
		where b.`playlist_id`='$plistid' ORDER BY song_name ASC");
		if (mysqli_num_rows($sql)>0) {
		?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {	
			$id = $row["t_id"];
			$a_id = $row["audio_id"];
		  if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        		$imgs = '../img/slogo3.png';
        	}
        	else
        	{
        		$imgs = '../'.$row['album_artwork'];
        	}			
		?>
        <ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto">
          <li class="list-group-item text-white">   
            <div class="pull-right"> 
            	 <a href="#" class="fa fa-chevron-down pull-right dropdown-toggle text-white" data-toggle="dropdown"></a>
              <ul class="dropdown-menu col-sm-6" style="text-align: center;">
              	 <li>
              	 	<input type="hidden" id="d_id" value="<?php echo $a_id; ?>"> 
              		<a href="#" id="play"  title="Play" onclick="choose_play('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>'); on_firs_play(); only_play();"> Play
              		</a>
              	</li> 
              	<li>
              		<a href="#" title="Details">
                   	<!-- <i class="icon-info"></i>  --> Details</a>
              	</li>
              	<li>
              		<a href="../<?php echo $row['music'] ?>" name="dl_playlist_" id="dl_playlist_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
                   	<!-- <i class="fa fa-download"></i> --> Download</a>
              	</li>
				  <li>
					  <a href="#" onclick="rem_on_list('<?php echo $row ['audio_id']?>','<?php echo $row['playlist_id']?>');">
					   <!-- <i class="fa fa-times-circle"></i><span> --> Remove<!-- </span> -->
					  </a>
				  </li>
			  </ul>
            </div> 
            <a href="#" class=" m-r-sm pull-left" onclick="choose_play('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>'); on_firs_play(); only_play();"> 
            	<img src="../img/musicicon2.png" width="40" height="40">
           	</a> 
			<div class="clear text-ellipsis"> 

				<span style="text-transform: capitalize;">Song Name: &nbsp;<?php echo ucfirst($row['song_name']); ?></span> 
				<div class="clear text-ellipsis text-muted" style="font-size: 13px; text-transform: capitalize;">
					Composer: &nbsp; &nbsp; &nbsp;<?php echo ucfirst($row['composer']); ?>
				</div>
			</div>
          </li>
        </ul>
		<?php
		}
		?>
		<?php
		}
		else
		{ 
		$sql2 = mysqli_query($con,"SELECT * FROM tbl_playlist where id='$plistid'");
		$row2 = mysqli_fetch_assoc($sql2);	
		?>
		<div id="no_songs" class="text-center text-white col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;" onclick="addplistsongs('<?php echo ucfirst($row2['name']) ?>','<?php echo $row2['id'] ?>');">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> Add Your Songs Now!</label></strong>
		</div>
	<?php
	}
	}else{

	$sql = mysqli_query($con,"SELECT a.*,e.`album_artwork`,c.`name`,b.`playlist_id`,b.`id` as t_id from tbl_audios a 
		LEFT JOIN tbl_playlist_info b ON a.`audio_id` = b.`audio_id`  
		LEFT JOIN tbl_playlist c ON b.`playlist_id` = c.`id`
		LEFT JOIN tbl_genre d ON a.`genre` = d.`gen_id`
		LEFT JOIN tbl_album e ON a.`album_id` = e.`album_id`
		where b.`playlist_id`='$plistid'
		and concat(song_name) like '%$plist_song_search%'
		 ORDER BY song_name ASC");
		if (mysqli_num_rows($sql)>0) {
		?>	
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		$id = $row["t_id"];
			$a_id = $row["audio_id"];
		  if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        		$imgs = '../img/slogo3.png';
        	}
        	else
        	{
        		$imgs = '../'.$row['album_artwork'];
        	}	
		?>
        <ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto">
          <li class="list-group-item text-white">   
            <div class="pull-right"> 
            	 <a href="#" class="fa fa-chevron-down pull-right dropdown-toggle text-white" data-toggle="dropdown"></a>
              <ul class="dropdown-menu col-sm-6" style="text-align: center;">
              	 <li>
              	 	<input type="hidden" id="d_id" value="<?php echo $a_id; ?>"> 
              		<a href="#" id="play"  title="Play" onclick="choose_play('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>'); on_firs_play(); only_play();"> Play
              		</a>
              	</li> 
              	<li>
              		<a href="#" title="Details">
                   	<!-- <i class="icon-info"></i>  --> Details</a>
              	</li>
              	<li>
              		<a href="../<?php echo $row['music'] ?>" name="dl_playlist_" id="dl_playlist_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
                   	<!-- <i class="fa fa-download"></i> --> Download</a>
              	</li>
				  <li>
					  <a href="#" onclick="rem_on_list('<?php echo $row ['audio_id']?>','<?php echo $row['playlist_id']?>');">
					   <!-- <i class="fa fa-times-circle"></i><span> --> Remove<!-- </span> -->
					  </a>
				  </li>
			  </ul>
            </div> 
            <a href="#" class=" m-r-sm pull-left" onclick="choose_play('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>'); on_firs_play(); only_play();"> 
            	<img src="../img/musicicon2.png" width="40" height="40">
           	</a> 
			<div class="clear text-ellipsis"> 

				<span style="text-transform: capitalize;">Song Name: &nbsp;<?php echo ucfirst($row['song_name']); ?></span> 
				<div class="clear text-ellipsis text-muted" style="font-size: 13px; text-transform: capitalize;">
					Composer: &nbsp; &nbsp; &nbsp;<?php echo ucfirst($row['composer']); ?>
				</div>
			</div>
          </li>
        </ul>
        <?php
		}
		?>
		<?php
		}
		else
		{
		echo '<div class="text-center text-dark col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> No Records Found!</label></strong>
		</div>';
		}
		}
		}


function load_style_select($con, $style){
	?>
	    <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT * FROM tbl_genre ORDER BY genre_name ASC ");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
                <label class="form-check-label oji-3"> 

                	<?php 

                	$myString = $style;

                	if(stristr($myString, $row['genre_name'] )){ ?>

                	<input onclick="get_style_value();" checked="true" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	
                	<span class="text-ellipsis style-name">
					<?php echo $row['genre_name']?> 
                	</span>

                	<?php	}else{ ?>

                	<input onclick="get_style_value();" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	 
                	<span class="text-ellipsis style-name">

                		<?php echo $row['genre_name']?> 
                	</span>

                	<?php
                		}

                	?>

                	 
      			</label> 
      			
	          <?php
	        }
	       ?>
	   </div>
	<?php
	}

function load_style_select2($con, $style){
	?>
	    <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT * FROM tbl_genre ORDER BY genre_name ASC ");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
                <label class="form-check-label oji-3"> 

                	<?php 

                	$myString = $style;

                	if(stristr($myString, $row['genre_name'] )){ ?>

                	<input onclick="get_style_value2();" checked="true" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	
                	<span class="text-ellipsis style-name">
					<?php echo $row['genre_name']?> 
                	</span>

                	<?php	}else{ ?>

                	<input onclick="get_style_value2();" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	 
                	<span class="text-ellipsis style-name">

                		<?php echo $row['genre_name']?> 
                	</span>

                	<?php
                		}

                	?>

                	 
      			</label> 
      			
	          <?php
	        }
	       ?>
	   </div>
	<?php
	}


	function load_app_select($con,$app){
	?>
        <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT * FROM tbl_application order by application_name ASC");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
				<label class="form-check-label oji-3"> 
				<?php 

                	$myString = $app;

                	if(stristr($myString, $row['application_name'] )){ ?>

                	<input onclick="get_app_value();" type="checkbox"  checked="true" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php	}else{ ?>

                		<input onclick="get_app_value();" type="checkbox" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php
                		}

                	?>
      			</label> 
	          <?php
	        }
	       ?>
	    </div>
	<?php
	}

	function load_app_select2($con,$app){
	?>
        <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT * FROM tbl_application order by application_name ASC");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
				<label class="form-check-label oji-3"> 
				<?php 

                	$myString = $app;

                	if(stristr($myString, $row['application_name'] )){ ?>

                	<input onclick="get_app_value2();" type="checkbox"  checked="true" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php	}else{ ?>

                		<input onclick="get_app_value2();" type="checkbox" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php
                		}

                	?>
      			</label> 
	          <?php
	        }
	       ?>
	    </div>
	<?php
	}

function get_dateupload($con, $songid){

	$sql = mysqli_query($con,"SELECT * FROM tbl_audios where id = '$songid'");

		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				echo date('Y-m-d', strtotime($row['upload_date']));
			}	
		}else{
			echo date('Y-m-d');
		}
}

function get_dateupload2($con, $songid){

	$sql = mysqli_query($con,"SELECT * FROM tbl_audios where id = '$songid'");

		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				echo date('Y-m-d', strtotime($row['upload_date']));
			}	
		}else{
			echo date('Y-m-d');
		}
}

function edit_supporting_file($con, $id, $fn, $fd, $ln, $link) 
{
	$sql = mysqli_query($con, "UPDATE tbl_supporting_file SET file_name = '$fn', file_description = '$fd', link_name = '$ln', link = '$link' WHERE file_id = '$id'");

	if ($sql) 
	{
		echo 1;
	}
}

function edit_supporting_file2($con, $id, $fn, $fd, $ln, $link) 
{
	$sql = mysqli_query($con, "UPDATE tbl_supporting_file SET file_name = '$fn', file_description = '$fd', link_name = '$ln', link = '$link' WHERE file_id = '$id'");

	if ($sql) 
	{
		echo 1;
	}
}

function show_supporting_files($con,$id) 
{	
	$user = $_SESSION['users'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_supporting_file WHERE user_id = '$user' and group_id = '$id' ");
	if (mysqli_num_rows($sql)>0) {
			while ($rows = mysqli_fetch_assoc($sql)) 
	{ ?>

		<tr class="tracks">
		<td><?php echo $rows['file_name']?></td>
		<td><?php echo $rows['file_description']?></td>
		<td><?php echo $rows['link_name'] ?></td>
		<td><?php echo $rows['link']?></td>
		<td style="text-align: center;">
			<div class="btn-group btn-justified">
				<a href="#" class="btn btn-normal btn-dark"  data-toggle="modal" data-target="#EditFileModal" 
				id="edit" title="Edit" onclick="get_file_data('<?php echo $rows['new_file_id']?>')"><span class="fa fa-edit fa-lg"></span> </a>

				<a href="#" class="btn btn-normal btn-dark" id="delte" title="Delete" onclick="remove_file('<?php echo $rows['new_file_id']?>','../uploads/uploaded_files/<?php echo $rows['file_name']?>');">
					<span class="fa fa-trash fa-lg"></span> 
		        </a>
			</div>
	       	
		</td>
	</tr>
	
<?php
	}
	}else{
			
	}
	
}

//ADD SONG ON ALBUM
function show_supporting_files2($con,$id) 
{	
	$user = $_SESSION['users'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_supporting_file WHERE user_id = '$user' and group_id = '$id' ");
	if (mysqli_num_rows($sql)>0) {
			while ($rows = mysqli_fetch_assoc($sql)) 
	{ ?>

		<tr class="tracks">
		<td><?php echo $rows['file_name']?></td>
		<td><?php echo $rows['file_description']?></td>
		<td><?php echo $rows['link_name'] ?></td>
		<td><?php echo $rows['link']?></td>
		<td style="text-align: center;">
			<div class="btn-group btn-justified">
				<a href="#" class="btn btn-normal btn-dark"  data-toggle="modal" data-target="#EditFileModal" 
				id="edit" title="Edit" onclick="get_file_data('<?php echo $rows['new_file_id']?>')"><span class="fa fa-edit fa-lg"></span> </a>

				<a href="#" class="btn btn-normal btn-dark" id="delte" title="Delete" onclick="remove_file('<?php echo $rows['new_file_id']?>','../uploads/uploaded_files/<?php echo $rows['file_name']?>');">
					<span class="fa fa-trash fa-lg"></span> 
		        </a>
			</div>
	       	
		</td>
	</tr>
	
<?php
	}
	}else{
			
	}
	
}

function album_lists($con){
$myid = $_SESSION['users'];
?>
<option selected disabled value="0">Select Album</option>
<?php
$sql = mysqli_query($con,"SELECT album_id,album_name from tbl_album where user_id = '$myid'");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option value="<?php echo $row['album_id']; ?>"><?php echo $row['album_name']; ?></option>
<?php
}
}

// display album name on add song on album.
function album_lists2($con,$album_id){
$myid = $_SESSION['users'];
?>
<?php
$sql = mysqli_query($con,"SELECT album_id,album_name from tbl_album where album_id = '$album_id'");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option selected="" disabled="" value="<?php echo $row['album_id']; ?>"><?php echo $row['album_name']; ?></option>
<?php
}
}

function music_details($con,$id){
	$sql = mysqli_query($con,"SELECT t1.*,t3.*, t4.`genre_name`,t2.`album_name`, t2.`album_artwork`,count(t5.`file_name`) as files,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.album_id = t2.album_id 
		LEFT JOIN tbl_user_profile t3 on t2.user_id = t3.user_id
		LEFT JOIN tbl_genre t4 on t1.genre = t4.gen_id 
		LEFT JOIN tbl_supporting_file t5 on t1.file_id = t5.group_id 
		WHERE t1.`audio_id` = '$id'");
	$row = mysqli_fetch_assoc($sql);
	$description = $row['description'];
	$song_name = $row['song_name'];
	$composer = $row['composer'];
	$artist = $row['artist'];
	$genre = $row['genre'];
	$application_id = $row['application_id'];
	$files = $row['files'];
	$group_id = $row['file_id'];
	$album_name = $row['album_name'];
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));

	if($row['picture'] == ''){
			$img = '../img/contributor.png';
			}else{
				$img = '../'.$row['picture'];
			}

			$tooltip ="<p><img src='".$img."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
			$toll_title="<center>".'Contributor'."</center>";

			if($row['album_artwork'] == ''){
			$album_image = '../img/logo2.png';
			}else{
				$album_image = '../'.$row['album_artwork'];
			}

			$tooltip_album ="<p><img src='".$album_image."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
			$album_title="<center>".'ALBUM'."</center>";
	?>	

		<div class="modal-header music-dark mtxt-light">
		Music Information: <?php echo $song_name; ?> 
		<?php if($detect_month != 1){ ?>
			<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
		<?php }else{ ?>

		<?php } ?>
		<button class="close" data-dismiss="modal">&times;</button></div>

		<div class="modal-body"> 
				<div class="row">
					<div class="col-lg-12">
						<div class="coupon">
						  <div class="container" style="line-height: 25px;"><br>
						  	<div class="col-lg-12 table-responsive">
       	 						<table class="table table-sm table-hover">
       	 							<tr>
       	 								<td><span class="text-default">Album: </span></td>
       	 								<td>
       	 									<span class="text-default" data-toggle="popover" data-html="true" data-title="<?php echo $album_title ?>" data-trigger="hover" data-content="<?php echo $tooltip_album ?>">
						    					<?php echo $row['album_name']; ?>
						    				</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Description:</span> </td>
       	 								<td><span class="text-default"><b><?php echo $description; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Contributor:</span> </td>
       	 								<td><span class="text-default" ><b data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>"><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Composer:</span></td>
       	 								<td><span class="text-default" ><b><?php echo $composer; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Genres:</span></td>
       	 								<td><span><strong style="font-weight: bolder;"><?php echo $genre; ?></strong></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Application:</span> </td>
       	 								<td><span class="text-default" ><b><?php echo $application_id; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Date Uploaded:</span></td>
       	 								<td><span class="text-default" ><b><?php echo date('M d, Y', strtotime($row['upload_date'])); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Supporting Files:</span> </td>
       	 								<td>
       	 									<span class="text-default" >
										    	<b>
											    	<?php
											    		if($files < 1){ ?>
											    			None
											    	<?php } elseif($files = 1){ ?>
											    		<?php echo $files ?> file
											    	<?php } else{ ?>
											    		<?php echo $files ?> files
											    	<?php } ?>
										    	</b>
										    </span>		
										</td>
       	 							</tr>
       	 							<tr>
       	 								<td colspan="2">
       	 									<div class="list-group">
												<?php 
													$query = mysqli_query($con, "SELECT file_name,file_path FROM tbl_supporting_file where group_id = '$group_id'");
													while ($rows = mysqli_fetch_assoc($query)) { ?>

														<li class="list-group-item d-flex justify-content-between align-items-center">
													    <span class="text-primary"><b><span class="fa fa-file-text-o"></span>&nbsp;<?php echo $rows['file_name'];  ?></b></span> 
													    <span class="text-primary">
													    	<a href="<?php echo $rows['file_path'] ?>" title="Download <?php echo $rows['file_name'] ?>" download >
															<span class="fa fa-download"></span> 
															</a>
													    </span>
													  </li>										
													<?php }	?>
											</div>
       	 								</td>

       	 							</tr>
       	 						</table>
       	 					</div>
						  </div>
						</div>
					</div>
				</div>
		</div>  
		<script type="text/javascript">
				enable_tooltips();	
				</script>
<?php
}

//music player
function music_player($con,$search)
{
	$myid = $_SESSION['users'];
	if ($search == "" || $search == null) {
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t1.active = 1
	ORDER BY song_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
		//$id = $row["t_id"];
		$a_id = $row["audio_id"];
		$group_id = $row['file_id'];

		$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
		$rows = mysqli_fetch_assoc($query);
		$total_files = $rows['files'];

	  	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
    	 $date_upload =  $row['upload_date'];
	?>	
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<form action="album_musics_playlist.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="btn-group btn-justified text-center" id="btns"> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
					</div>              		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center" >
			<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	else
	{
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	WHERE t1.active = 1
	AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
	LIKE '%$search%'
	ORDER BY song_name ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
	?>
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<form action="album_musics_playlist.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
				</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="btn-group btn-justified text-center" id="btns"> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
					</div>              		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	}

//Album music contributor
function album_musics($con,$search,$album_id)
{

	$myid = $_SESSION['users'];
	if ($search == "" || $search == null) {
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t1.`album_id` = '$album_id' and t2.`user_id` = '$myid'
	ORDER BY song_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
		//$id = $row["t_id"];
		$a_id = $row["audio_id"];
		$group_id = $row['file_id'];

		$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
		$rows = mysqli_fetch_assoc($query);
		$total_files = $rows['files'];

	  	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
    	 $date_upload =  $row['upload_date'];
    	 //am - Album Musics
	?>	

			<tr class="track item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 40%;">
						<input type="checkbox" onclick="count_check_am();" name="id[]" class="select_am" value="<?php echo $row["music"]; ?>" />
					</h6>
				</td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="w3-dropdown-hover">
							<button class="w3-button">Action <span class="fa fa-caret-down"></span></button>
							<div class="w3-dropdown-content w3-bar-block w3-border">
								<a href="#" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');" class="w3-bar-item w3-button">
									<i class="fa fa-info"></i><span> &nbsp;Details</span>
								</a>
								<a href="#" onclick="edit_audio2('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['file_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-edit"></i><span> &nbsp;Edit</span>
								</a>
								<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
								</a>
							</div>
						</div>               		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center" >
			<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	else
	{
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	WHERE t1.active = 1
	AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
	LIKE '%$search%'
	ORDER BY song_name ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}

	?>
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td class="text-center">
               		<h6 class="db-content default-fs default_margin" >
						<div class="w3-dropdown-hover">
							<button class="w3-button">Action <span class="fa fa-caret-down"></span></button>
							<div class="w3-dropdown-content w3-bar-block w3-border">
								<a href="#" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');" class="w3-bar-item w3-button">
									<i class="fa fa-info"></i><span> &nbsp;Details</span>
								</a>
								<a href="#" onclick="edit_audio('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['album_id'] ?>','<?php echo $row['file_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-edit"></i><span> &nbsp;Edit</span>
								</a>
								<a href="#" onclick="add_to('<?php echo $row['audio_id'] ?>')" class="w3-bar-item w3-button">
									<i class="fa fa-plus-square-o"></i><span> &nbsp;Add to...</span>
								</a>
							</div>
						</div>               		
				</h6>
           		</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
  }
}

//Album music all
function album_musics_playlist($con,$search,$album_id)
{
	$myid = $_SESSION['users'];
	if ($search == "" || $search == null) {
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t1.`album_id` = '$album_id' and t1.`active` = 1
	ORDER BY song_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
		//$id = $row["t_id"];
		$a_id = $row["audio_id"];
		$group_id = $row['file_id'];
		$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
		$rows = mysqli_fetch_assoc($query);
		$total_files = $rows['files'];

	  	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
    	 $date_upload =  $row['upload_date'];
    	 //am - Album Musics
	?>	
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
				</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td style=" width: 10%;">
					<div class="btn-group btn-justified text-center" id="btns"> 
						<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
						<i class="fa fa-download fa-lg text-white"></i>
						</a> 

	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
					</div>
				</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center" >
			<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	else
	{
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	WHERE t1.active = 1
	AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
	LIKE '%$search%'
	ORDER BY song_name ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}

	?>
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
				</td>
           		
           		<td class="text-center" style="text-transform: capitalize; text-align: center; padding: auto; ">
           			<h6 class="db-content default-fs default_margin" >
                     	<?php echo ucfirst($row['fn'].' '. $row['ln'])?>
                    </h6>
           		</td>
               	<td style=" width: 10%;">
					<div class="btn-group btn-justified text-center" id="btns"> 
						<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
						<i class="fa fa-download fa-lg text-white"></i>
						</a> 

	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
					</div>
				</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
  }
}

function newalbum_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_album");
	$row = mysqli_fetch_assoc($sql);
	return 'album-'.rand(1,9).rand(1,9).$row['max(id)'].rand(1,9).rand(1,9);
}

function detect_album_name($con,$album_name){
	$sql = mysqli_query($con, "SELECT * FROM tbl_album WHERE album_name='$album_name'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}
}

function add_album2($con,$album_name,$album_desc){

$myid = $_SESSION['users'];
$random_id = newalbum_id($con);
$dt = date('Y-m-d');
$detect = detect_album_name($con,$album_name);

if ($detect == 1) {
	echo 404;
}else{

$sql = mysqli_query($con,"INSERT INTO tbl_album values(id,'$random_id','$album_name','$album_desc',null,'$myid','$dt')");
if ($sql) {
	echo 1;
}

}

}

function s_files($con,$group_id) 
{	
	$user = $_SESSION['users'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_supporting_file WHERE user_id = '$user' and group_id = '$group_id' ");
	if (mysqli_num_rows($sql)>0) {
			while ($rows = mysqli_fetch_assoc($sql)) 
	{ ?>

		<tr class="tracks">
		<td><?php echo $rows['file_name']?></td>
		<td><?php echo $rows['file_description']?></td>
		<td><?php echo $rows['link_name'] ?></td>
		<td><?php echo $rows['link']?></td>
		<td style="text-align: center;">
			<div class="btn-group btn-justified">
				<a href="#" class="btn btn-normal btn-dark"  data-toggle="modal" data-target="#EditFileModal" 
				id="edit" title="Edit" onclick="get_file_data('<?php echo $rows['new_file_id']?>')"><span class="fa fa-edit fa-lg"></span> </a>

				<a href="#" class="btn btn-normal btn-dark" id="delte" title="Delete" onclick="remove_file('<?php echo $rows['new_file_id']?>','../uploads/uploaded_files/<?php echo $rows['file_name']?>');">
					<span class="fa fa-trash fa-lg"></span> 
		        </a>
			</div>
	       	
		</td>
		</tr>
	
<?php
	}
	}else{
			
	}
	
}

function s_files2($con,$group_id) 
{	
	$user = $_SESSION['users'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_supporting_file WHERE user_id = '$user' and group_id = '$group_id' ");
	if (mysqli_num_rows($sql)>0) {
			while ($rows = mysqli_fetch_assoc($sql)) 
	{ ?>

		<tr class="tracks">
		<td><?php echo $rows['file_name']?></td>
		<td><?php echo $rows['file_description']?></td>
		<td><?php echo $rows['link_name'] ?></td>
		<td><?php echo $rows['link']?></td>
		<td style="text-align: center;">
			<div class="btn-group btn-justified">
				<a href="#" class="btn btn-normal btn-dark"  data-toggle="modal" data-target="#EditFileModal" 
				id="edit" title="Edit" onclick="get_file_data2('<?php echo $rows['new_file_id']?>')"><span class="fa fa-edit fa-lg"></span> </a>

				<a href="#" class="btn btn-normal btn-dark" id="delte" title="Delete" onclick="remove_file2('<?php echo $rows['new_file_id']?>','../uploads/uploaded_files/<?php echo $rows['file_name']?>');">
					<span class="fa fa-trash fa-lg"></span> 
		        </a>
			</div>
	       	
		</td>
		</tr>
	
<?php
	}
	}else{
			
	}
	
}

function total_album_songs($con, $album_id){
	$sql = mysqli_query($con,"SELECT COUNT(audio_id) FROM tbl_audios WHERE album_id = '$album_id'");
	$row = mysqli_fetch_assoc($sql);

	if($row['COUNT(audio_id)'] > 1){
		echo $row['COUNT(audio_id)'].' '. 'Songs';
	}else{
		echo $row['COUNT(audio_id)'].' '. 'Song';
	}	
}

function save_play($con, $audio_id){
	$user = $_SESSION['users'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "INSERT INTO tbl_played_songs(audio_id,user_id,date_play,location) VALUES('$audio_id','$user','$date_',null)");
	if($sql){
		echo 1;
	}
}

?>