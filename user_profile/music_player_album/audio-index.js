var max_aud = 0;
get_max_aus();
var play_now = 1;
var p = 0;
var can_play = 0;
var audio_player = document.getElementById('my_player');
////////
var current_duration = 0;
var get_total;
var seeding = 0;
var now_playing = 0;
var is_mute = 0;
///////

var num_counter = 0;

function get_max_aus(){
  $.ajax({
    type:"POST",
    url:url,
    data:"action=get_max_au",
    cache:false,
    success:function(data){
    max_aud = data;
    }
  });
}

function next_aud(){
//alert(max_aud);
play_now++;
$('audio').attr("autoplay"," ");
setTimeout(function(){
  if (play_now > max_aud) 
  {
    play_now = 1;
    play_list(1);
    //alert('asd');
  }
  else
  {
    play_list(play_now);  
  }
},200);
play_musics('plays');
//alert(play_now);
}

function prev_aud(){
play_now--;

setTimeout(function(){
  if (play_now < 1) 
  {
    play_now = 1;
    play_list(max_aud);
    //alert('asd');
  }
  else
  {
    play_list(play_now);  
  }
},200);
}

var index_limit = 5;
var index_start = 0;

function play_list(id){
  // alert(id);
  var album_id = document.getElementById('album_id');
  var mydata = 'action=sound_audio_album' + '&id=' + id + '&album_id=' + album_id.value;
  // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    //alert(data);
    $("#show_progress").html(data);
    $("#player_btn").click();
    // pause_music();
    }
  });
}

function on_firs_play(){
  $('audio').attr("autoplay"," ");
    can_play = 1;
    //alert(1);
}

function pause_media(){
  $('audio').removeAttr('autoplay');
}

function only_play(){
$("#plays").removeClass("fa-play-circle");
$("#plays").addClass("fa-stop-circle animated pulse");
now_playing = 1;
}

  //$('[data-toggle="tooltip"]').tooltip(); 
function choose_play(img,aud,title,id){
// alert(img+' '+aud+' '+title);
// $("#list").hide('slow');
// $("#list_nav").hide('slow');
$('.footer').removeClass('none');
$("#album_artwork").html('<img src="../img/load.gif" class="img_playlist img-responsive animated jello">');
setTimeout(function(){
if (img == "" || img == null) 
{

$("#album_artwork").html('<img src="../img/logo.png" class="img_playlist img-responsive animated jello" alt="">');
$("#my_player").attr('src',aud);
$("#music_title").html(title);
}
else
{
  $("#album_artwork").html('<img src='+img+' class="img_playlist img-responsive animated jello" alt="">');
  $("#my_player").attr('src',aud);
  $("#music_title").html(title);
}

},1000);
  }


// function love(id){
// $("#"+id+"").toggleClass('animated jackInTheBox text-danger');
// }

function love(id){
  swal("Oops!","You need to log in first !","info");
  // $("#login_form").modal('show');
}



// function pause_music(){
//  audio_player.pause();
// }

// new functions-------------------------------------------------------------------------------------Player---------------------------

function get_duration(duration){
current_duration = duration;
}

function start_player(){
  var music_player = document.getElementById('my_player');
  
  setInterval(function(){
  var sub_tot = 100 / current_duration;
  var total = music_player.currentTime * sub_tot; 
  $("#pl").val(Math.round(total));

   move_slider_progress($('#pl').val(),$('#pl').attr('min'),$('#pl').attr('max'));
},1000);
}

function move_slider(vals,min,max){
  var music_player = document.getElementById('my_player');
    var val = (vals - min) / (max - min);
    var percent = val * current_duration;
    var percent_bar = val * 100;
  
    $('#pl').css('background-image',
        '-webkit-gradient(linear, left top, right top, ' +
        'color-stop(' + percent_bar + '%, #00b3b3), ' +
        'color-stop(' + percent_bar + '%, #c1c1c1)' +')');

    $('#pl').css('background-image',
        '-moz-linear-gradient(left center, #DF7164 0%, #DF7164 ' + percent_bar + '%, #c1c1c1 ' + percent_bar + '%, #c1c1c1 100%)');
    music_player.currentTime = percent;
    interval_playing(percent);
    // alert(music_player.currentTime);
    
}


function move_slider_progress(vals,min,max){

    var val = (vals - min) / (max - min);
    var percent = val * current_duration;
    var percent_bar = val * 100;
    // seeding = percent_bar;
    // $('#pl').val(percent);
    // console.log(percent_bar);
    $('#pl').css('background-image',
        '-webkit-gradient(linear, left top, right top, ' +
        'color-stop(' + percent_bar + '%, #00b3b3), ' +
        'color-stop(' + percent_bar + '%, #c1c1c1)' +
        ')');

    $('#pl').css('background-image',
        '-moz-linear-gradient(left center, #DF7164 0%, #DF7164 ' + percent_bar + '%, #c1c1c1 ' + percent_bar + '%, #c1c1c1 100%)');
}

 
  function play_music(x) {
    var player = document.getElementById('my_player');
    if (now_playing == 0) 
    {
      player.play();
      
         now_playing =1;
         
    }
    else
    {
      player.pause();
     
       now_playing = 0;
    }
      $("#"+x+"").toggleClass("fa-play-circle");
      $("#"+x+"").toggleClass("fa-stop-circle animated pulse");
  
}


 function play_musics(x) {
    var player = document.getElementById('my_player');
      player.play();
      
      now_playing =1;
   
      $("#"+x+"").removeClass("fa-play-circle");
      $("#"+x+"").addClass("fa-stop-circle animated pulse");
  
}



function toggle_mute(){
   var player = document.getElementById('my_player');
   if (is_mute == 0) 
   {
     $("#volume_control").toggleClass("fa-volume-up");
       $("#volume_control").toggleClass("fa-volume-down  animated wobble");
       player.volume = 0.0;
       $("#vol_controler").val(0);
       is_mute = 1;
   }
   else
   {
     $("#volume_control").toggleClass("fa-volume-up");
       $("#volume_control").toggleClass("fa-volume-down  animated wobble");
       player.volume = 1.0;
       $("#vol_controler").val(100);
       is_mute = 0;
   }

}


function get_minutes(total){
var seconds = "0" + (Math.floor(total) - minutes * 60);
var seconds = "0" + Math.floor(total % 60);

var minutes = "0" + Math.floor(total / 60);
var seconds = "0" +  Math.floor(total - minutes * 60);
var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
$("#timer_audio").text(dur);
}




function interval_playing(total){
var music_player = document.getElementById('my_player');
 var counters =  music_player.currentTime;
// if (counters == total) 
// {
//  num_counter = 0;
// }
  var minutes = "0" + Math.floor(counters / 60);
  var seconds = "0" +  Math.floor(counters - minutes * 60);
  var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
  $("#counter_strike").text(dur);
}


// /////////////////////////////////////////////////////////////////////////////Volume//////////////////////////////////////////////////////////////
 function show_player(){
    $("#myplayer").animate({bottom:'0px'},400);
    $("#btn_players").slideUp(1000);
    $("#bg-blur").toggleClass('jl-blur');
    $("#myplayer").addClass('animated fadeIn');
  }

  function close_player(){
    $("#myplayer").animate({bottom:'-100%'},400);
    $("#myplayer").removeClass('animated fadeIn');
    $("#bg-blur").toggleClass('jl-blur');
    $("#btn_players").slideDown(1000);
  }

  function volup(vol){
  var a = vol * 0.01;
  var volup = document.getElementById('my_player');
  volup.volume = a;

  if (volup.volume != 0.0) 
  {
     $("#volume_control").removeClass("fa-volume-down  animated bounceIn");
     $("#volume_control").addClass("fa-volume-up");
  }
  else if(volup.volume == 0.0)
  {
    $("#volume_control").addClass("fa-volume-down  animated bounceIn");
     $("#volume_control").removeClass("fa-volume-up");
  }
  // alert(voll);
  }