<style type="text/css">
  .modal-small{
      max-width: 500px;
      width: 100%;
    }
  .modal-medium{
      max-width: 700px;
      width: 100%;
    }
</style>

<!-- ADD ALBUM DEFAULT -->
<div class="modal fade" id="newAlbumModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <p class="title" id="title">ADD NEW ALBUM</p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body"><br>
            <input type="hidden" id="alb_id">
             <input type="hidden" id="album_path">
             <input type="hidden" id="album_path_backup">
            <input type="file" name="album_image" id="album_image" style="display: none;"/> 
              <div class="col-lg-12">
                 <div class="form-group">
                    <input type="show" name="album_name2" id="album_name2" placeholder="album Name" class="form-control" />
                  </div>
              </div>
              <div class="col-lg-12">
                 <textarea placeholder="Description" class="input-small big" name="album_descr" id="album_descr" autocomplete="on"></textarea>
              </div>
              <div class="row text-center">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 text-center">
                  <div id="image_preview" class="text-center"> </div> <br/>
                  <div id="album_image2" class="text-center"></div>
                  <button class="btn btn-dark btn-small" id="btn-chooser" onclick="$('#album_image').click();"> 
                        <i class="fa fa-file fa-lg"></i> Choose File</button>
                </div>
                <div class="col-lg-2"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-sm-5"></div>
            <div class="col-sm-3" style="padding-right: 0px;">
              <button class="btn btn-dark btn-small" data-dismiss="modal" onclick="N_cancel();">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-dark btn-small" name="submit" id="submit" onclick="submit_album();" >Create Album</button>
            </div>
        </div>
      </div>  
    </div>  
  </div>

<!-- ADD ALBUM ON MODAL -->
<div class="modal fade" id="add_album2" tabindex="-1" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <p class="title" id="title">ADD NEW ALBUM</p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body"><br>
              <div class="row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-10">
                    <div class="form-group">
                      <input type="show" name="name_album" id="name_album" placeholder="New Album" class="form-control" autocomplete="on" />
                    </div>
                    <div class="form-group">
                        <textarea placeholder="Album Description." class="input-small big" name="album_desc" id="album_desc" autocomplete="on"></textarea>                    
                      </div>
                  </div>
                  <div class="col-sm-1"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-sm-5"></div>
            <div class="col-sm-3" style="padding-right: 0px;">
              <button class="btn btn-dark btn-small" data-dismiss="modal" onclick="N_cancel();">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-dark btn-small" name="submit" id="submit" onclick="submit_album2();" >Create Album</button>
            </div>
        </div>
      </div>  
    </div>  
  </div>

  <!-- View Music Information -->
<div id="music_info_modal" class="modal fade ">  
  <div class="modal-dialog modal-medium">  
    <input type="hidden" id="music_id">
    <div class="modal-content" id="music_info"> 

    </div>  
  </div>  
</div>

<div id="EditFileModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
      <p class="title">Edit Supporting File</p>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
        <div class="modal-body">
          <div class="container">
            <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
                    <label>File Name:</label>
                    <input type="show" name="file_name" id="file_name" placeholder="File Name" class="form-control" />
                </div>
                <div class="form-group">
                    <label>File Description:</label>
                    <input type="show" name="file_description" id="file_description" placeholder="File Description" class="form-control" />
                 </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
                    <label>Link Name:</label>
                    <input type="show" name="link_name" id="link_name" placeholder="Link Name" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Link:</label>
                    <input type="show" name="link" id="link" placeholder="Link" class="form-control" />
                  </div>
          </div>
        </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="old_name" id="old_name"/>
          <input type="hidden" name="edit_action" id="edit_action"/>
          <input type="hidden" name="file_id" id="file_id"/>
          <input type="hidden" name="file_ext" id="file_ext"/>
          <button type="button" class="btn btn-dark btn-small" style="font-family: MavenPro-Regular;" onclick="edit_file();">Update</button>
        </div>
    </div>
  </div>
</div>



<!-- Update Album -->

  <div class="modal fade" id="update_alb">
  <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
          <div class="modal-header">
            <p class="title" id="title">UPDATE ALBUM</p>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div> 
          <div class="modal-body">         
            <input type="hidden" name="id" id="id"> <br/>
              <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-lg-10">
                  <div class="form-group">
                    <input type="show" name="albname" id="albname" class="form-control text-center" placeholder="Album Name" required="true" maxlength="30">
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-lg-2"></div>
            <div class="col-sm-4" style="padding-right: 0px;">
              <button class="btn btn-dark btn-small" style="color: #fff;" data-dismiss="modal">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-dark btn-small" style="color: #fff;" onclick="save_alb();">Update Album</button>
            </div>
            <div class="col-lg-2"></div>
        </div>
      </div>  
    </div>  
  </div>