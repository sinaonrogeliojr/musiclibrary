<?php 
session_start();
include_once("../dbconnection.php");
include_once("classes.php");
$myid = $_SESSION['users'];

$action = mysqli_real_escape_string($con,$_POST['action']);
switch ($action) {

	case 'save_play':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		save_play($con,$id);
	break;
	
	case 'gen_groupid':
	gen_groupid($con);
	break;

	case 'album_lists2';
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);	
	album_lists2($con, $album_id);
	break;

	case 'add_album2':
	$album_name = mysqli_real_escape_string($con,$_POST['name_album']);
	$album_desc = mysqli_real_escape_string($con,$_POST['album_desc']);
	add_album2($con, $album_name, $album_desc);
	break;

	case 'music_player':
	$search = mysqli_real_escape_string($con,$_POST['search_music_player']);
	music_player($con, $search);
	break;

	case 'album_musics':
	$search = mysqli_real_escape_string($con,$_POST['album_search']);
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	album_musics($con, $search, $album_id);
	break;

	case 'album_musics_playlist':
	$search = mysqli_real_escape_string($con,$_POST['album_search']);
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	album_musics_playlist($con, $search, $album_id);
	break;

	case 'album_lists':
		album_lists($con);
	break;

	case 'music_details':
	$music_id = mysqli_real_escape_string($con,$_POST['music_id']);
	music_details($con, $music_id);
	break;

	case 'gen_groupid2':
	gen_groupid2($con);
	break;

	case 'sound_audio':
		$id = $_POST['id'];
		$num = 1;
		$sql = mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
			LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
			LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
			LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
			LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
			WHERE t1.active = 1
			ORDER BY song_name ASC");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
		break;

		case 'sound_audio2':
		$audio_id = $_POST['audio_id'];
		$num = 1;
		//$my_audio_id2 = mysqli_real_escape_string($con,$_POST['my_audio_id2']);

		$sql = mysqli_query($con,"SELECT t1.*,t2.`user_id`,t6.`genre_name`,t5.`application_name`,t2.`album_artwork`  from tbl_audios t1 
			LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
			LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
			LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
			LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
			LEFT JOIN tbl_genre t6 on t1.`genre`= t6.`gen_id`
			WHERE t2.`user_id` = '$myid' 
			ORDER BY t1.`song_name` ASC ");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($audio_id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn2" style="display: none;" onclick="choose_play2('<?php echo '../'.$row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
	break;

	//Sound Audio Album
	case 'sound_audio_album':
		$id = $_POST['id'];
		$album_id = $_POST['album_id'];
		$num = 1;
		$sql = mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
			LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
			LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
			LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
			LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
			WHERE t1.`album_id` = '$album_id' and t1.`active` = 1
			ORDER BY song_name ASC");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
	}
	break;

	case 'showplaylist':
	$search1 = mysqli_real_escape_string($con,$_POST['search1']);
	show_songs($con,$search1);
	break;
	//website
	case 'showmymusic':
	$mymusic_search = mysqli_real_escape_string($con,$_POST['mymusic_search']);
	showmymusic($con,$mymusic_search);
	break;
	//mobile
	case 'showmymusic_mobile':
	$mymusic_search = mysqli_real_escape_string($con,$_POST['mymusic_search_mobile']);
	$order = mysqli_real_escape_string($con,$_POST['order_mobile']);
	$title_name = mysqli_real_escape_string($con,$_POST['title_name_mobile']);
	showmymusic_mobile($con,$mymusic_search,$order,$title_name);
	break;

	case 'showgenre':
	$search2 = mysqli_real_escape_string($con,$_POST['search2']);
	show_genres($con,$search2);
	break;

	case 'showgenre2':
	$search2a = mysqli_real_escape_string($con,$_POST['search2a']);
	show_genres2($con,$search2a);
	break;


	case 'showsongs':
	//$audience_id = mysqli_real_escape_string($con,$_POST['audience_id']);
	$search3 = mysqli_real_escape_string($con,$_POST['search3']);
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$order = mysqli_real_escape_string($con,$_POST['order']);
	$title_name = mysqli_real_escape_string($con,$_POST['title_name']);
	show_musics($con,$search3,$show_applications,$order,$title_name);
	break;

	case 'showsongs_mobile':
	//$audience_id = mysqli_real_escape_string($con,$_POST['audience_id']);
	$search3 = mysqli_real_escape_string($con,$_POST['search3_mobile']);
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications_mobile']);
	$order = mysqli_real_escape_string($con,$_POST['order_mobile']);
	$title_name = mysqli_real_escape_string($con,$_POST['title_name_mobile']);
	show_musics_mobile($con,$search3,$show_applications,$order,$title_name);
	break;

	case 'showtopics':
	$search4 = mysqli_real_escape_string($con,$_POST['search4']);
	show_topics($con,$search4);
	break;

	case 'showcontributors':
	$search5 = mysqli_real_escape_string($con,$_POST['search5']);
	show_contributors($con,$search5);
	break;

	case 'showalbumslist':
	$search6 = mysqli_real_escape_string($con,$_POST['search6']);
	show_albums_list($con,$search6);
	break;


	case 'showalbums':
	$search = mysqli_real_escape_string($con,$_POST['search']);
	show_albums($con,$search);
	break;

	case 'showalbums2':
	$searchalb = mysqli_real_escape_string($con,$_POST['searchalb']);
	show_albums2($con,$searchalb);
	break;

	case 'showdescription':
	$description = mysqli_real_escape_string($con,$_POST['description']);
	show_description($con,$description);
	break;

	
	case 'show_music_playlist':
	$search_song = mysqli_real_escape_string($con,$_POST['search_song']);
	$id = mysqli_real_escape_string($con,$_POST['v_pid']);
	show_music_playlist($con,$id,$search_song);
	break;

	case 'show_music_playlist_mobile':
	$search_song = mysqli_real_escape_string($con,$_POST['search_song_mobile']);
	$id = mysqli_real_escape_string($con,$_POST['v_pid']);
	show_music_playlist_mobile($con,$id,$search_song);
	break;	

	case 'show_song_plist':
	$plist_search = mysqli_real_escape_string($con,$_POST['plist_search']);
	$order = mysqli_real_escape_string($con,$_POST['order_plist']);
	$title_name = mysqli_real_escape_string($con,$_POST['title_name_plist']);
	show_song_plist($con,$plist_search,$order,$title_name);
	break;

	case 'show_song_plist2':
	$plist_search2 = mysqli_real_escape_string($con,$_POST['plist_search2']);
	show_song_plist2($con,$plist_search2);
	break;

	case 'showAlbumAudio':
	$alb_id_modal = mysqli_real_escape_string($con,$_POST['alb_id_modal']);
	$album_songs_ = mysqli_real_escape_string($con,$_POST['album_songs_']);
	show_album_audio($con,$alb_id_modal,$album_songs_);
	break;

	case 'showAlbumInfo':
	$alb_id_modal = mysqli_real_escape_string($con,$_POST['alb_id_modal']);
	show_album_info($con,$alb_id_modal);
	break;

	case 'show_album_audio':
	$albumId = mysqli_real_escape_string($con,$_POST['Id']);
	ShowAlbum_Audio($con,$albumId);
	break;

	case 'del_music':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$music = mysqli_real_escape_string($con,$_POST['music']);
	unlink('../'.$music);
	delete_music($con,$id,$music);
	break;

	case 'remove_song':
	$path1 = mysqli_real_escape_string($con,$_POST['path1']);
	unlink($path1);
	echo 1;
	break;

	case 'remove_song2':
	$path1 = mysqli_real_escape_string($con,$_POST['path1']);
	unlink($path1);
	echo 1;
	break;

	case 'remove_file':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	unlink($path);
	remove_file($con,$path,$id);
	echo 1;
	break;

	case 'remove_word':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	unlink($path);
	echo 1;
	break;

	case 'del_album':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$album_artwork = mysqli_real_escape_string($con,$_POST['album_artwork']);
	delete_album($con,$id,$album_artwork);
	break;

	case 'insert_album':
	$album_name = mysqli_real_escape_string($con,$_POST['album_name']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	insert_album($con,$album_name,$id);
	break;

	case 'insert_music':
	$song_name = mysqli_real_escape_string($con,$_POST['song_name']);
	$composer = mysqli_real_escape_string($con,$_POST['composer']);
	$artist = mysqli_real_escape_string($con,$_POST['artist']);
	$genre = mysqli_real_escape_string($con,$_POST['genre']);
	$albumname = mysqli_real_escape_string($con,$_POST['albumname']);
	$audiencename = mysqli_real_escape_string($con,$_POST['audiencename']);
	$applicationname = mysqli_real_escape_string($con,$_POST['applicationname']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	insert_music($con,$song_name,$composer,$artist,$genre,$albumname,$audiencename,$applicationname,$id);
	break;

	case 'showdate_asc':
	$asc = mysqli_real_escape_string($con,$_POST['asc']);
	showdate_asc($con,$asc);
	break;


	case 'update_alb':
	pict_session($con,$id);
	break;

	case 'update_account':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
	$mn = mysqli_real_escape_string($con,$_POST['mn']);
	$ln = mysqli_real_escape_string($con,$_POST['ln']);
	update_name($con,$id,$fn,$mn,$ln);
	break;

	case 'update_bdate':
	$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
	update_bdate($con,$bdate,$myid);
	break;

	case 'update_email':
	$email = mysqli_real_escape_string($con,$_POST['email']);
	update_email($con,$email,$myid);
	break;

	case 'update_username':
	$username = mysqli_real_escape_string($con,$_POST['username']);
	update_username($con,$username,$myid);
	break;
	
	case 'update_pwd':
	$pwd = mysqli_real_escape_string($con,$_POST['pwd']);
	update_pwd($con,$pwd,$myid);
	break;

	case 'refresh_account':
	refresh_account($con,$myid);
	break;

	case 'update_picture':
	pict_session($con,$myid);
	break;

	case 'save_image':
	unlink('../'.$_SESSION['user_pic']);
	$path = mysqli_real_escape_string($con,$_POST['path']);
	update_pf($con,$path,$myid);
	break;

	case 'remove_image':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	unlink($path);
	echo 1;
	break;

	case 'del_image':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	unlink($path);
	echo 1;
	break;

	case 'upload_song':
	$link2 = mysqli_real_escape_string($con,$_POST['link2']);
	$song_name = mysqli_real_escape_string($con,$_POST['song_name']);
	$composer = mysqli_real_escape_string($con,$_POST['composer']);
	$genres = mysqli_real_escape_string($con,$_POST['genres']);
	$artist = mysqli_real_escape_string($con,$_POST['artist']);
	$album = mysqli_real_escape_string($con,$_POST['album']);
	$audience = mysqli_real_escape_string($con,$_POST['audience']);
	$application = mysqli_real_escape_string($con,$_POST['application']);
	$duration = mysqli_real_escape_string($con,$_POST['duration']);
	$ud = date('Y-m-d');
	upload_music($con,$link2,$song_name,$composer,$genres,$artist,$album,$ud,$audience,$application,$duration);
	break;

	case 'showcomposer':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$search7 = mysqli_real_escape_string($con,$_POST['search7']);
	show_composer($con,$search7,$show_applications);
	break;
	
	case 'showcomposer2':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$search7a = mysqli_real_escape_string($con,$_POST['search7a']);
	show_composer2($con,$search7a,$show_applications);
	break;

	case 'showartist':
	$audience_id = mysqli_real_escape_string($con,$_POST['audience_id']);
	$artistsearch = mysqli_real_escape_string($con,$_POST['artistsearch']);
	show_artist($con,$artistsearch,$audience_id);
	break;

	case 'showartist2':
	$audience_id = mysqli_real_escape_string($con,$_POST['audience_id']);
	$artistsearch2 = mysqli_real_escape_string($con,$_POST['artistsearch2']);
	show_artist2($con,$artistsearch2,$audience_id);
	break;
	
	case 'showstyle':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$stylesearch = mysqli_real_escape_string($con,$_POST['stylesearch']);
	show_style($con,$stylesearch,$show_applications);
	break;

	case 'showstyle2':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$stylesearch2 = mysqli_real_escape_string($con,$_POST['stylesearch2']);
	show_style2($con,$stylesearch2,$show_applications);
	break;

	case 'showdate_list':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$search8 = mysqli_real_escape_string($con,$_POST['search8']);
	show_date_list($con,$search8,$show_applications);
	break;

	case 'showdate_list2':
	$show_applications = mysqli_real_escape_string($con,$_POST['show_applications']);
	$search8a = mysqli_real_escape_string($con,$_POST['search8a']);
	show_date_list2($con,$search8a,$show_applications);
	break;

	case 'del_music_list':
	$audio_id = mysqli_real_escape_string($con,$_POST['audio_id']);
	$music = mysqli_real_escape_string($con,$_POST['music']);
	unlink('../'.$music);
	delete_music_list($con,$audio_id,$music);
	break;

	case 'download_music':
	$current_loc = mysqli_real_escape_string($con,$_POST['current_loc']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	download_music($con,$current_loc,$id);
	break;

	case 'downloadall_music':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	downloadplaylist_music($con,$id);
	break;

	case 'contri_details':
	$c_id = mysqli_real_escape_string($con,$_POST['contri_id']);
	contri_details($con, $c_id);
	break;

	case 'style_details':
	$gen_id = mysqli_real_escape_string($con,$_POST['gen_id']);
	style_details($con, $gen_id);
	break;

	case 'contri_albums':
	$id = mysqli_real_escape_string($con,$_POST['c_id']);
	$conalbums = mysqli_real_escape_string($con,$_POST['conalbums']);
	contri_albums($con, $id, $conalbums);
	break;

	case 'contri_songs':
	$consongs = mysqli_real_escape_string($con,$_POST['consongs']);
	$id = mysqli_real_escape_string($con,$_POST['c_id']);
	contri_songs($con,$id,$consongs);
	break;

	case 'show_contri_album_audio':
	$albumId = mysqli_real_escape_string($con,$_POST['Id']);
	show_contri_album_audio($con,$albumId);
	break;

	case 'album_details':
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	album_details($con, $album_id);
	break;

	case 'showapplication':
	$app_search = mysqli_real_escape_string($con,$_POST['app_search']);
	show_application_list($con,$app_search);
	break;

	case 'showapplication2':
	$app_search2 = mysqli_real_escape_string($con,$_POST['app_search2']);
	show_application_list2($con,$app_search2);
	break;

	case 'showapp_song':
	$app_id = mysqli_real_escape_string($con,$_POST['app_id']);
	$app_song_search = mysqli_real_escape_string($con,$_POST['app_song_search']);
	show_application_songs($con,$app_song_search,$app_id);
	break;

	case 'showapp_song2':
	$app_id2 = mysqli_real_escape_string($con,$_POST['app_id2']);
	$app_song_search2 = mysqli_real_escape_string($con,$_POST['app_song_search2']);
	show_application_songs2($con,$app_song_search2,$app_id2);
	break;

	case 'showstylesongs':
	$name = mysqli_real_escape_string($con,$_POST['style_name']);
	$search_song = mysqli_real_escape_string($con,$_POST['search_gsong']);
	show_style_songs($con,$name,$search_song);
	break;

	case 'showstylesongs_mobile':
	$name = mysqli_real_escape_string($con,$_POST['style_name']);
	$search_song = mysqli_real_escape_string($con,$_POST['search_gsong_mobile']);
	show_style_songs_mobile($con,$name,$search_song);
	break;

	case 'createplist':
	$playlist_name = mysqli_real_escape_string($con,$_POST['playlist_name']);
	insert_plist($con,$playlist_name);
	break;
	case 'createplist2':
	$playlist_name2 = mysqli_real_escape_string($con,$_POST['playlist_name2']);
	insert_plist2($con,$playlist_name2);
	break;


	case 'updateplist':
	$pid = mysqli_real_escape_string($con,$_POST['pid']);
	$plist_name = mysqli_real_escape_string($con,$_POST['plist_name']);
	update_plist($con,$pid,$plist_name);
	break;
	

	case 'del_plist':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_plist($con,$id);
	break;

	case 'addtoplist':
	$audio_id = mysqli_real_escape_string($con,$_POST['audio_id']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	add_to_plist($con,$audio_id,$id);
	break;

	case 'showplist_songs':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	$plistname = mysqli_real_escape_string($con,$_POST['plistname']);
	$plist_song_search = mysqli_real_escape_string($con,$_POST['plist_song_search']);
	show_playlist_songs($con,$plist_song_search,$plistid,$plistname);
	break;

	case 'showplist_songs2':
	$plistid2 = mysqli_real_escape_string($con,$_POST['plistid2']);
	$plistname2 = mysqli_real_escape_string($con,$_POST['plistname2']);
	$plist_song_search2 = mysqli_real_escape_string($con,$_POST['plist_song_search2']);
	show_playlist_songs2($con,$plist_song_search2,$plistid2,$plistname2);
	break;

	case 'show_song_details':
	$alb_id = mysqli_real_escape_string($con,$_POST['alb_id']);
	$aud_id = mysqli_real_escape_string($con,$_POST['aud_id']);
	$us_id = mysqli_real_escape_string($con,$_POST['us_id']);
	show_song_details($con,$alb_id,$aud_id,$us_id);
	break;

	case 'show_song_details2':
	$alb_id = mysqli_real_escape_string($con,$_POST['alb_id']);
	$aud_id = mysqli_real_escape_string($con,$_POST['aud_id']);
	$us_id = mysqli_real_escape_string($con,$_POST['us_id']);
	show_song_details2($con,$alb_id,$aud_id,$us_id);
	break;

	case 'addto_plist':
	$audio_id = mysqli_real_escape_string($con,$_POST['audio_id']);
	$plistname = mysqli_real_escape_string($con,$_POST['plistname']);
	addto_plist($con,$audio_id,$plistname);
	break;

	case 'show_child_songs':
	$search_child_songs  = mysqli_real_escape_string($con,$_POST['search_child_songs']);
	show_child_songs($con,$search_child_songs);
	break;

	case 'show_child_songs2':
	$search_child_songs2  = mysqli_real_escape_string($con,$_POST['search_child_songs2']);
	show_child_songs2($con,$search_child_songs2);
	break;

	case 'show_child_songs3':
	$search_child_songs3  = mysqli_real_escape_string($con,$_POST['search_child_songs3']);
	show_child_songs3($con,$search_child_songs3);
	break;

	case 'show_child_songs4':
	$search_child_songs4  = mysqli_real_escape_string($con,$_POST['search_child_songs4']);
	show_child_songs4($con,$search_child_songs4);
	break;

	case 'show_child_songs5':
	$search_child_songs5  = mysqli_real_escape_string($con,$_POST['search_child_songs5']);
	show_child_songs5($con,$search_child_songs5);
	break;

	case 'show_child_songs6':
	$search_child_songs6  = mysqli_real_escape_string($con,$_POST['search_child_songs6']);
	show_child_songs6($con,$search_child_songs6);
	break;

	case 'removefromlist':
	$audio_id = mysqli_real_escape_string($con,$_POST['audio_id']);
	$pl_id = mysqli_real_escape_string($con,$_POST['id']);
	removefromlist($con,$audio_id,$pl_id);
	break;

	case'select_song':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	$p_ids = mysqli_real_escape_string($con,$_POST['p_ids']);
	save_song($con,$plistid,$p_ids);
	break;

	/*case'show_plist_cbox':
	$splist = mysqli_real_escape_string($con,$_POST['splist']);
	show_plist_cbox($con,$splist);
	break;*/


	
	case'show_plist_name':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	$plistmodaltitle = mysqli_real_escape_string($con,$_POST['plistmodaltitle']);
	show_plist_name($con,$plistmodaltitle,$plistid);
	break;

	case'save_selected_song':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	save_selected_song($con,$id,$plistid);
	break;

	case 'clear_album_songs':
	$alb_id_modal = mysqli_real_escape_string($con,$_POST['alb_id_modal']);
	clear_album_songs($con,$alb_id_modal);
	break;

	case 'clear_playlist_songs':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	clear_playlist_songs($con,$plistid);
	break;

	case 'show_dropdown_alb';
	show_dropdown_alb($con);
	break;

	case 'show_dropdown_style':
	show_dropdown_style($con);
	break;
	
	case 'show_dropdown_aud':
	show_dropdown_aud($con);
	break;

	case 'show_library_application':
	show_dropdown_app($con);
	break;

	case 'show_playlist_songs':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	$plist_song_search = mysqli_real_escape_string($con,$_POST['plist_song_search']);
	show_playlist_songs($con,$plistid,$plist_song_search);
	break;

	case 'load_playlist_info':
	$plistid = mysqli_real_escape_string($con,$_POST['plistid']);
	show_playlist_info($con, $plistid);
	break;


	
	case 'show_select_style':
	show_dropdown_style2($con);
	break;

	case 'load_tbl_audio':
	$splist = mysqli_real_escape_string($con,$_POST['splist']);
	$select_style = mysqli_real_escape_string($con,$_POST['select_style']);
	load_tbl_audio($con,$select_style,$splist);
	break;


	case 'addmultisongs':
	$audio_id =  mysqli_real_escape_string($con,$_POST["audio_id"]);
	$newplistid = mysqli_real_escape_string($con,$_POST["newplistid"]);
	savemultisongs($con,$audio_id,$newplistid);
	break;

	case 'load_album_selection':
	$searchalbums =  mysqli_real_escape_string($con,$_POST["searchalbums"]);
	load_album_selection($con,$searchalbums);
	break;

	case 'show_style_select':
	$style =  mysqli_real_escape_string($con,$_POST["style_value"]);
	load_style_select($con,$style);
	break;

	case 'show_style_select2':
	$style =  mysqli_real_escape_string($con,$_POST["style_value2"]);
	load_style_select2($con,$style);
	break;

	case 'show_app_select':
	$app =  mysqli_real_escape_string($con,$_POST["app_value"]);
	load_app_select($con,$app);
	break;

	case 'show_app_select2':
	$app =  mysqli_real_escape_string($con,$_POST["app_value2"]);
	load_app_select2($con,$app);
	break;

	case 'get_dateupload':
	$songid =  mysqli_real_escape_string($con,$_POST["songid"]);
	get_dateupload($con,$songid);
	break;

	case 'get_dateupload2':
	$songid =  mysqli_real_escape_string($con,$_POST["songid2"]);
	get_dateupload2($con,$songid);
	break;

	case 'save_supporting_files':
	$pdfpath = mysqli_real_escape_string($con, $_POST["pdfpath"]);
	$wordpath = mysqli_real_escape_string($con, $_POST["wordpath"]);
	$link_name = mysqli_real_escape_string($con, $_POST["link_name"]);
	$s_link = mysqli_real_escape_string($con, $_POST["s_link"]);
	save_supporting_files($con,$pdfpath,$wordpath,$link_name,$s_link);
	break;

	case 'get_max_au':
	$sql = mysqli_query($con,"SELECT count(id) from tbl_audios");
	if (mysqli_num_rows($sql)>0) {
	$row = mysqli_fetch_assoc($sql);
	echo $row['count(id)'];
	}
	break;

	case 'get_max_au2':
	$sql = mysqli_query($con,"SELECT count(audio_id) from tbl_audios");
	if (mysqli_num_rows($sql)>0) {
	$row = mysqli_fetch_assoc($sql);
	echo $row['count(audio_id)'];
	}
	break;

	case 'show_supporting_files2':
	$id = mysqli_real_escape_string($con,$_POST['group_id2']);
	show_supporting_files2($con,$id);
	break;

	case 'edit_file':
	$file_id = mysqli_real_escape_string($con,$_POST['file_id']);
	$fn = mysqli_real_escape_string($con,$_POST['file_name']);
	$fd = mysqli_real_escape_string($con,$_POST['file_desc']);
	$ln= mysqli_real_escape_string($con,$_POST['link_name']);
	$link = mysqli_real_escape_string($con,$_POST['link']);
	edit_supporting_file($con, $file_id, $fn, $fd, $ln, $link);
	break;

	case 'edit_file2':
	$file_id = mysqli_real_escape_string($con,$_POST['file_id2']);
	$fn = mysqli_real_escape_string($con,$_POST['file_name2']);
	$fd = mysqli_real_escape_string($con,$_POST['file_desc2']);
	$ln= mysqli_real_escape_string($con,$_POST['link_name2']);
	$link = mysqli_real_escape_string($con,$_POST['link2']);
	edit_supporting_file2($con, $file_id, $fn, $fd, $ln, $link);
	break;

	case 's_files':
		$group_id = mysqli_real_escape_string($con,$_POST['group_id']);
		s_files($con, $group_id);
	break;

	case 's_files2':
		$group_id = mysqli_real_escape_string($con,$_POST['group_id2']);
		s_files2($con, $group_id);
	break;

	case 'total_album_songs':
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	total_album_songs($con,$album_id);
	break;
}
 