<?php
session_start();
if (!isset($_SESSION['users'])) {
  @header('location:../');
}
include_once("../dbconnection.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="user.css">
  <link href="../css/w3.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <link href="../css/w3.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="music_player_album/audio.css">
  <link rel="icon" type="icon/png" href="../img/musicicon.png">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="add_song_admin.js"></script>
  <script src="../js/marvin.js"></script>
  <script src="../js/album.js"></script>
  <script src="geo_loc.js"></script>
  <script src="../js/w3.js"></script>
  <script src="music_player_album/audio-index.js"></script>

<style type="text/css">

.imgs {
  width: 50px; /* You can set the dimensions to whatever you want */
  height: 50px;
  object-fit: cover;
}

.user-mng h3{
  font-weight: 500;
  padding-left: 100px;
  font-size: 16px;
}

.modal-add-music{
  max-width: 1000px;
  width: 100%;
}

.dropdown {
   position: relative;
   display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.select_type{
  background-color: #f4f4f4;
  padding: 20px;
  width: 100%;
  height: 78px;
  border: 1px solid #d5d5d5;
  font-size: 18px;
}

.oji-3{
  width: 32%; 
  padding: 1%;
}

.pad-1{
  padding: 2%;
}

.modal-small{
    max-width: 500px;
    width: 100%;
}

.modal-medium{
    max-width: 700px;
    width: 100%;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #304756;
   text-align: center;
   color: #f2f2f2;
}

</style>
</head>
<body onload="album_musics_playlist(); total_album_songs(); ">
<input type="hidden" name="album_id" id="album_id" value="<?php echo $_POST['album_id']; ?>">
<div class="sidenav">
	<ul>
      <li class="w3-hover-shadow">
      <a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <a href="albums.php"><span class="fa fa-image fa-3x" style="color: #8eadab;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <a href="show_playlist.php"><span class="fa fa-play fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
    </li>
  </ul>
</div>

<div class="main">

	<div class="container-fluid hero hero-db hero-admin">
		<div class="row header">
			<div class="col-lg-3 col-2 head">
				<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
			</div>
			<div class="col-lg-9 head">
        <div class="dropdown pull-right">
          <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
          <div class="dropdown-content">
            <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
            <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
          </div>
        </div>
      </div>
		</div>  	  	
	</div>

	<div class="container-fluid user-mng">
		<div class="row">
      <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-3">
              <p class="title" style="text-transform: uppercase;"><?php echo $_POST['album_name']; ?> 
              <span class="badge default-fs" id="total_album_songs"></span> 
              </p>
            </div>
          <div class="col-lg-9">
            <div class="input-group">
                <input type="show" name="album_search" id="album_search" oninput="album_musics_playlist();" placeholder="Search Artist, Music, Genre...">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
              </div>
          </div>
        </div>
      </div>
		</div>
	</div>

	<div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
		<div class="row">
			<div class="col-lg-12 table-responsive">
				<table class="table table-striped table-hover" id="player">
          <tr><td colspan="10" class="text-left"><span>Click on the column title to sort</span></td></tr>
          <tr id="labels">
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(1)')"style="width: 15%; cursor:pointer">Title/Album</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(2)')" style="cursor:pointer">Description</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(3)')" style="cursor:pointer">Genre</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(4)')" style="cursor:pointer">Composer</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(5)')" style="cursor:pointer">Application</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(6)')" style="cursor:pointer; width: 11.5%;">Supporting Files</td>
            <td class="text-left" onclick="w3.sortHTML('#player', '.item', 'td:nth-child(7)')" style="cursor:pointer">Contributor</td>
            <td class="text-center" style="">Action</td>
          </tr>
          <tbody id="tbl_album_music" style=""></tbody>
        </table>
			</div>
		</div>
	</div>
  <hr>
	<footer class="container-fluid">
		<?php include('../footer.php'); ?>
	</footer>
  <!-- MUSIC PLAYER -->
<div class="footer none">
  <div class="row" style="padding-left: 110px; padding-right: 20px; padding-bottom: 10px;">
        <div class="col-sm-12">
          <div class="col-sm-12">
            <h4 style="margin-bottom: 0px;">
            <marquee id="music_title" class="col-sm-12 default-fs">LAMPSTAND STUDIO</marquee>
            </h4>
          </div>
          <input type="range" min="0"  max="100" class="player_slider" onmousemove="move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'));" onmouseup="move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">

          <div class="col-sm-12 row">
            <div class="col-sm-2">
                <div class="text-white"><span id="counter_strike">00:00</span>/<span id="timer_audio">00:00</span></div>
            </div>
            <div class="col-sm-8 text-center">
                <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg text-white"></i></a>
                <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="fa fa-play-circle fa-lg text-white"></i></a>
                <a href="#prev" onclick="next_aud();" class="btn-musics "><i class="fa fa-forward fa-lg text-white"></i></a>
            </div>
            <div class="col-sm-2 text-white">
                  <a href="#vol"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg text-white"></i></a>
                  <input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);">
            </div>
          </div>
        </div>

      <div id="music_audio" style="width: 100%;">
      <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud2();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
      Sorry, your browser does not support audio
      </audio>
      <div id="show_progress"></div>
      </div>
  </div>
</div>
</div>
<!-- Add Musics to the Album--> 
<div class="modal fade" id="addSong2" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <p class="title">ADD NEW MUSIC</p>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body" >
          <input type="hidden" id="songid2" disabled="">
              <input type="hidden" id="music_path2" disabled="">
              <input type="hidden" id="duration2" disabled="">
              <input type="file" name="uploaded_songs2" id="uploaded_songs2" style="display: none;" /> 

          <div class="container">
            <div class="row btn-pair">
              <div class="col-lg-6 right"><button type="button" class="btn btn-dark btn-small" id="btn-select2" onclick="$('#uploaded_songs2').click();">Select Song</button></div>
              <div class="col-lg-6 left">
                <div class="form-group">
                  <select class="btn btn-dark btn-small" id="album_lists2">

                  </select>
                </div>
               <!-- <button type="button" class="btn btn-dark btn-small" id="alb_select" onclick="addto_album();">Album</button>-->
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <input type="text" placeholder="Title" class="input-small" name="song_name2" id="song_name2" autocomplete="on"/>
                <br>
                <input type="text" placeholder="Composer" class="input-small" name="composer2" id="composer2" autocomplete="on"/>
              </div>
              <div class="col-lg-4"><textarea placeholder="Description" class="input-small big" name="description2" id="description2" autocomplete="on"></textarea></div>
              <div class="col-lg-4"><textarea placeholder="Keywords" class="input-small big" name="keyword2" id="keyword2" autocomplete="on"></textarea></div>
              <div class="col-lg-12 text-center" id="song_preview2" style="padding-left: 0px; padding-right: 0px;"></div>
            </div>
          </div>

          <div class="container-fluid divider">
            <div class="container">
              <p class="title">Style</p>
            </div>
          </div>

          <div class="container pad-1">
            <input type="hidden" id="style_value2">
            <div class="col-lg-12" id="style_select2">
              
            </div>
          </div>

          <div class="container-fluid divider">
            <div class="container">
              <p class="title">Application</p>
            </div>
          </div>

          <div class="container pad-1">
            <input type="hidden" id="app_value2">
            <div class="col-lg-12" id="app_select2">
              
            </div>
          </div>

          <div class="container-fluid divider">
            <div class="container">
              <p class="title">Supporting Files</p>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <input type="file" multiple name="multi_upload_file[]" id="multi_upload_file" style="display: none;">
                      <input type="hidden" name="group_id2" id="group_id2">
              <div class="col-lg-4 right"><button type="button" class="btn btn-dark btn-small" id="btn_select" onclick="$('#multi_upload_file').click();">Select Files</button></div>

              <div class="col-lg-3 select">
                <div class="btn btn-dark btn-small label">Available From</div>
              </div>
              <div class="col-lg-5 select">
                <div class="form-group">
                  <input type="date" class="form-control" name="date_upload2" id="date_upload2" size="16">
                </div>
              </div>
            </div>
          </div>

          <div class="container-fluid">
            <table class="table table-borderless">
              <head>
                <tr class="titles">
                <td>File Name</td>
                <td>File Description</td>
                <td>Link</td>
                <td>Link Name</td>
                <td>Task</td>
              </tr>
              </head>
              <tbody id="s_files_view2">
                
              </tbody>
            </table>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark btn-small" name="submitsong2" id="submitsong2" onclick="submitsong2();">Save</button>
        </div>

      </div>
    </div></div>
<div id="EditFileModal2" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
      <p class="title">Edit Supporting File</p>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
        <div class="modal-body">
          <div class="container">
            <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
                    <label>File Name:</label>
                    <input type="show" name="file_name2" id="file_name2" placeholder="File Name" class="form-control" />
                </div>
                <div class="form-group">
                    <label>File Description:</label>
                    <input type="show" name="file_description2" id="file_description2" placeholder="File Description" class="form-control" />
                 </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
                    <label>Link Name:</label>
                    <input type="show" name="link_name2" id="link_name2" placeholder="Link Name" class="form-control" />
                  </div>
                  <div class="form-group">
                    <label>Link:</label>
                    <input type="show" name="link2" id="link2" placeholder="Link" class="form-control" />
                  </div>
          </div>
        </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="old_name2" id="old_name2"/>
          <input type="hidden" name="edit_action2" id="edit_action2"/>
          <input type="hidden" name="file_id2" id="file_id2"/>
          <input type="hidden" name="file_ext2" id="file_ext2"/>
          <button type="button" class="btn btn-dark btn-small" style="font-family: MavenPro-Regular;" onclick="edit_file2();">Update</button>
        </div>
    </div>
  </div>
</div>

 <!-- View Music Information -->
<div id="music_info_modal" class="modal fade ">  
  <div class="modal-dialog modal-medium">  
    <input type="hidden" id="music_id">
    <div class="modal-content" id="music_info"> 

    </div>  
  </div>  
</div>
<script>

function check_all_validate() {

    if (document.getElementById('chk_album_musics').checked) {
    document.getElementById('btn-delete-am').disabled= false;
      } else {
    document.getElementById('btn-delete-am').disabled= true;
      }

    }

function count_check_am(){
  var chks = $('.select_am').filter(':checked').length

  if(chks > 0){
    document.getElementById('btn-delete-am').disabled= false;
  }else{
    document.getElementById('btn-delete-am').disabled= true;
  }

}

$(document).ready(function(){
    $("#chk_album_musics").change(function(){
      if(this.checked){
        $(".select_am").each(function(){
        this.checked=true;
        })       
      }else{
        $(".select_am").each(function(){
        this.checked=false;
        })              
      }
    });

    //Delete All
//Disapprove All
$('#btn-delete-am').click(function(){
  swal({
    title: "Are you sure?",
    text: "You want to Delete Selected data?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete",
    closeOnConfirm: false
  },
  function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Library!", "Please select atleast one", "info");
   }
   else
   {
    $.ajax({
     url:'delete_all_music.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
      if(data == 404){
        swal("Music Libray!", "Unable to delete", "info");
      }else{
      swal("Music Library!", "Deleted", "info");
      album_musics();
      document.getElementById('btn-delete-am').disabled= true;
      }
      
     }
     
    });
   }
 });
  
 });

});
</script>

</body>
</html>