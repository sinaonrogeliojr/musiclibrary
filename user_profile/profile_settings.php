<?php session_start(); ?>
<script type="text/javascript">
refresh_account();
load_pictures();
</script>
<div class="w3-animate-opacity">
<div class="card glass">
	<div class="card-body" >
		<h3><i class="fa fa-lg fa-gear"></i> Profile Settings</h3>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-3" style="margin-top: 5px;">
		<div class="img_con">
		   <div class="text-center" id="profile_pic"></div>
		  	<div class="middle">
		    	<button class="btn btn-success animated fadeIn" data-toggle="modal" data-target="#change_profile_pic">Change Profile Picture</button>
		  </div>
		</div>
	</div>

	<div class="col-sm-7 " style="margin-top: 5px;">
		<div id="my_data"></div>
	</div>
	<div class="col-sm-5">
		<div id="choose"></div>
		<input type="hidden" name="old" id="old">
		<input type="hidden" name="id" id="id">
		
	</div>
</div>
</div>






<style type="text/css">
	.card{
		margin-top: 10px;
		color: #fff;
		padding-top: 10px;
		padding-left: 10px;
		padding-bottom: 10px;

	}
	.glass{
		border-radius: 10px;
		background-color: rgba(0,0,0,0.22);
	}


#profile_pic {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.img_con{
	padding-top: 5px;
	padding-left: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	border:solid 5px #193c52db;
	background-color: #193c52;
	border-radius: 5px;

}

.img_con:hover #profile_pic {
  opacity: 0.8;
}

.img_con:hover .middle {
  opacity: 1;
}


</style>