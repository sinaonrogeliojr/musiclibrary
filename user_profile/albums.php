<?php
include_once("session.php");
$user_id = $_SESSION['users'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="user.css">
  <link href="../css/w3.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <link href="../css/w3.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="music_player/audio.css">
  <link rel="icon" type="icon/png" href="../img/musicicon.png">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="add_song.js"></script>
  <script src="../js/marvin.js"></script>
  <script src="../js/album.js"></script>
  <script src="geo_loc.js"></script>
  <script src="../js/w3.js"></script>
  <script src="music_player/audio-index.js"></script>
  <style type="text/css">
    .imgs {
      width: 50px; /* You can set the dimensions to whatever you want */
      height: 50px;
      object-fit: cover;
      }
    
    
    .img_music2 {
      width: 200px; /* You can set the dimensions to whatever you want */
      height:200px;    
    }

    .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #304756;
   text-align: center;
   color: #f2f2f2;
}

.select_type{
  background-color: #f4f4f4;
  padding: 20px;
  width: 100%;
  height: 78px;
  border: 1px solid #d5d5d5;
  font-size: 18px;
  }

  .oji-3{
    width: 32%; 
    padding: 1%;
  }
  .pad-1{
    padding: 2%;
  }

  .scrollers{
      max-height:450px; 
      overflow:auto;
    }

  @media only screen and (max-width: 600px) {
    .oji-3{
    width: 100%; 
    padding: 1%;
    }
  }
  

  </style>
</head>
<body onload="albums(); load_pictures(); uploading(); upload_picture(); upload_picture2();">

<div class="sidenav">
  <ul>
      <li class="w3-hover-shadow">
      <a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <a href="albums.php"><span class="fa fa-image fa-3x" style="color: #8eadab;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <a href="show_playlist.php"><span class="fa fa-play fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
      
    </li>
  </ul>
</div>

<div class="main">
  <div class="container-fluid hero hero-db hero-admin">
    <div class="row header">
      <div class="col-lg-3 col-2 head">
        <a href="#" onclick="albums();"><img class="img-fluid" src="../img/logo.png"/></a>
      </div>
      <div class="col-lg-9 head">
        <div class="dropdown pull-right">
          <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
          <div class="dropdown-content">
            <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
            <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
          </div>
        </div>
      </div>
    </div>        
  </div>

  <div class="container-fluid user-mng">
    <div class="row">
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <p class="title">My Albums</p>
          </div>
          <div class="col-lg-8">
            <div class="input-group">
                <input type="show" oninput="albums();" name="search" id="search" placeholder="Search Album...">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <a>
          <button type="button" class="btn btn-dark btn-small" title="Add Album" data-toggle="modal" data-target="#newAlbumModal"><span class="fa fa-plus"></span> Add Album
          </button>
        </a>
      </div>
      <div class="col-lg-2">
        <a>
           <button type="button" class="btn btn-dark btn-small" id="btn-delete" disabled="">
            <span class="fa fa-trash fa-lg"></span> Delete
          </button>
        </a>
      </div>
    </div>
  </div>
  <div></div>
  <div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
    <div class="row">
      <div class="col-lg-12 table-responsive">
        <table class="table table-striped table-hover" id="musics">
          <tr><td colspan="7" class="text-left"><span>Click on the column title to sort</span></td></tr>
          <tr id="labels">
            <td class="text-left">
              <div class="form-check-inline">
              <label class="form-check-label" for="chk_accounts">
                <input type="checkbox" name="chk_accounts" id="chk_accounts" onclick="check_all_validate();">
              </label>
            </div>
            </td>
            <td class="text-center" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(2)')"style="cursor:pointer">Image</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(3)')" style="cursor:pointer">Album Name</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(4)')" style="cursor:pointer">Description</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(5)')" style="cursor:pointer">Songs</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(6)')" style="cursor:pointer">Date</td>
            <td class="text-left" style="">Action</td>
          </tr>
          <tbody id="tbl_album" style="">
            
          </tbody>
          </table>
      </div>
    </div>
  </div>
  <hr>
  <!--<div class="container-fluid related">
    <div class="row">
      <div class="col-lg-6 recents">
        <div class="row">
          <div class="col-lg-10">
            <h4>Recently Played</h4>
          </div>

          <div class="col-lg-2">
            <a href="#" class="see-all">See All</a>
          </div>

          <div class="col-lg-12"><hr></div>

          <div class="col-lg-3"><a href="#"><img src="../img/img-3.jpg"></a></div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/play-big.png"></a></div>

          <div class="col-lg-3 content">
            <a href="#"><h6>Smetana From The Homeland 2</h6></a>
            <a href="#"><p>Smetana From The Homeland</p></a>
          </div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/add-big.png"></a></div>

          <div class="col-lg-2"><p>2:23</p></div>
        </div>

        <div class="row">
          <div class="col-lg-3"><a href="#"><img src="../img/img-4.jpg"></a></div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/play-big.png"></a></div>

          <div class="col-lg-3 content">
            <a href="#"><h6>Smetana From The Homeland 2</h6></a>
            <a href="#"><p>Smetana From The Homeland</p></a>
          </div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/add-big.png"></a></div>

          <div class="col-lg-2"><p>2:23</p></div>
        </div>

        <div class="row">
          <div class="col-lg-3"><a href="#"><img src="../img/img-5.jpg"></a></div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/play-big.png"></a></div>

          <div class="col-lg-3 content">
            <a href="#"><h6>Smetana From The Homeland 2</h6></a>
            <a href="#"><p>Smetana From The Homeland</p></a>
          </div>

          <div class="col-lg-2"><a href="#"><img src="../img/icons/add-big.png"></a></div>

          <div class="col-lg-2"><p>2:23</p></div>
        </div>
      </div>

      <div class="col-lg-6 suggested-artists">
        <div class="row">
          <div class="col-lg-10">
            <h4>Suggested Related Artist</h4>
          </div>

          <div class="col-lg-2">
            <a href="#" class="see-all">See All</a>
          </div>

          <div class="col-lg-12"><hr></div>

          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-3.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-4.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-5.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>
          </div>
          
          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-3.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-4.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4"><a href="#"><img src="../img/img-5.jpg"></a></div>
              <div class="col-lg-8 content">
                <a href="#"><h6>Smetana From The Homeland 2</h6></a>
                <a href="#"><p>Smetana From The Homeland</p></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>-->

  <footer class="container-fluid">
    <?php include('../footer.php'); ?>
  </footer>
</div>

<?php include('my_modals.php'); ?>

<script type="text/javascript">

function count_check(){
  var chks = $('.select_albums').filter(':checked').length

  if(chks > 0){
    document.getElementById('btn-delete').disabled= false;
  }else{
    document.getElementById('btn-delete').disabled= true;
  }
}

function check_all_validate() {
    if (document.getElementById('chk_accounts').checked) {

      document.getElementById('btn-delete').disabled= false;
        } else {
      document.getElementById('btn-delete').disabled= true;
        }
    }

  $(document).ready(function(){
    $("#chk_accounts").change(function(){
      if(this.checked){
        $(".select_albums").each(function(){
        this.checked=true;
        })       
      }else{
        $(".select_albums").each(function(){
        this.checked=false;
        })              
      }
    });

    //Delete All
//Disapprove All
$('#btn-delete').click(function(){
  swal({
    title: "Are you sure?",
    text: "You want to Delete Selected data?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete",
    closeOnConfirm: false
  },
  function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {
    $.ajax({
     url:'delete_all_album.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
      if(data == 404){
        swal("Music Libray!", "Unable to delete", "info");
      }else{
      swal("Music Libray!", "Deleted", "info");
      albums();
      document.getElementById('btn-delete').disabled= true;
      }
      
     }
     
    });
   }
 });
  
 });

});
</script>
</body>
</html>