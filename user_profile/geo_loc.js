function getgeoloc(){
    if(navigator.geolocation){
        // timeout at 60000 milliseconds (60 seconds)
        var options = {enableHighAccuracy:true,maximumAge:30000,timeout:27000};
        geoLoc = navigator.geolocation;
        watchID = geoLoc.watchPosition(showLocation, errorHandler, options);
     }else{
        alert("Sorry, browser does not support geolocation!");
     }
		}


	  var watchID;
	  var geoLoc;
	  function showLocation(position) {
	      var latitude = position.coords.latitude;
	      var longitude = position.coords.longitude;
	      get_add(latitude, longitude);         
	  }
	  
	  function errorHandler(err){
	    if(err.code == 1) {
	      alert("Error: Access is denied!");
	    }else if( err.code == 2) {
	      alert("Error: Position is unavailable!");
	    }
	  	}
         

function get_add(latitude, longitude){
  	$.get("https://maps.google.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false", function(data){
    $(data).find("formatted_address").each(function(){
        var unitData = $(this).text();
        $("#current_loc2").val("Latitude : " + latitude + " Longitude: " + longitude + "<br>");
        $("#current_loc2").val(unitData);
        return false;
        });     
  		});
			}