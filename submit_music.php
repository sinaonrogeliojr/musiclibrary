<?php 
session_start();
if (isset($_SESSION['admin'])) {
 @header('location:admin_panel');
}
else if (isset($_SESSION['users'])) {
  @header('location:user_profile/');
}
else if (isset($_SESSION['email_add'])) {
  @header('location:guest_profile/');
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Music Library</title>
  <?php include_once("header.php"); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/2.0.6/wavesurfer.min.js"></script>
<!-- <link href="https://fonts.googleapis.com/css?family=Electrolize" rel="stylesheet"> -->
</head>
<style type="text/css">
  .img_resize{
    width: 120px;
    text-shadow:2px 2px 4px #555;
  }
  .img-text {
      top: 50%;
      color: #fff;
}

.img-subscribe{
  background-attachment: fixed;
    background-position:center;
    background-repeat: no-repeat;
    background-size: cover;
    background-image: url('img/index_img/4th_bg.jpg');
}

.border-left{
  border-right: 1px solid #e7e7e7;
}

#login_header{
  height: 230px; 
  /* Center and scale the image nicely */
  background-position:center;
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;
  background-image: url('img/main-bg.png');
}

</style>
<body onload="play_list(play_now); show_music(); scroll();" id="wrap" class="bg-second-page">
 
<div>

<nav class="navbar navbar-expand-md fixed-top" id="menu-nav">
    <a href="index.php"><img src="img/slogo3.png" id="logo_nav" class="img-fluid" width="210"></a>


  <!-- Brand
  <a class="font-m1 w3-wide nav-a   btn btn-outline-secondary mr-3 " style="border:none;" href="#bg-blur">HOME</a>
  <a class="w3-wide font-m1  nav-a  btn btn-outline-secondary" style="border:none;"  href="#bg_second">ABOUT</a>
   <a class="w3-wide font-m1  nav-a  btn btn-outline-secondary" style="border:none;"  href="#connect">CONNECT</a> -->
  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
  <span class="fa fa-bars text-dark"></span>
  </button>
  <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">

    <ul class="navbar-nav ml-auto">
       <li class="nav-item mr-2">
        <a href="login.php" style="text-decoration: none; top: 2%;" class="nav-a font-m1 w3-wide "><span class="fa fa-user"></span> LOGIN&nbsp;|</a>
      </li>

      <li class="nav-item mr-2">
        <a href="register.php" style="text-decoration: none; top: 2%;" class="nav-a font-m1 w3-wide">REGISTER</a>
      </li>

      <!--<li class="nav-item mr-2 bg-success">
        <a href="register.php" style="text-decoration: none; top: 2%;" class="nav-a font-m1 w3-wide"><span class="fa fa-search fa-lg"></span></a>
      </li>-->

    </ul>
  </div>
</nav>


<div class="container-fluid" id="login_header">
  <div class="row" >
    <div class="col-sm-12 text-center text-white"  style="margin-top: 15%;  ">
      
    </div>
  </div>
</div>
</div>

<div class="row">

  <div class="col-sm-12 row text-center" style="background-color: #fff; margin-bottom: 4%;">
    <div class="col-sm-2"></div>
    <div class="col-sm-8 ">
      <h1 style="font-size: 60px; line-height: 150%; text-shadow:2px 2px 4px #555; font-family: MavenPro-Regular; font-weight: bolder; ">SUBMIT MUSIC</h1>
      <p>
        If you are a composer, producer, publisher or have a music catalog and would like to have your music considered by Lampstand Music for inclusion in our broadcast library, you can now send us a sample of your music using the following form.
      </p>
      <div class="col-sm-5 float-left" style=" margin-top: 3%; height:auto;">
        <img class="img-responsive" width="100%" src="img/vin.jpg">
      </div>
      <div class="col-sm-7 float-right" style=" margin-top: 3%; height:auto; background-color: #f1f1f1;">
         <h4>Here's what you need:</h4><br>
        <p class="text-left" style="line-height: 20px;">
        1.The music must be original. Arrangements of Public music are also acceptable. <br><br>
        2.You must own and control 100% of the music copyright and be authorized to assign the copyright to lampstand Music as publisher.<br><br>
        3.Files should be MP3 format with minimun bitrate of 196k.<br><br>
        4.You can only upload 10 files at a time.
      </p>
      </div>
      <div class="col-sm-6 float-left" style=" margin-top: 3%;">
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" style="height: 50px; background-color: #f1f1f1;">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Name" style="height: 50px; background-color: #f1f1f1;">
        </div>
      </div>
      <div class="col-sm-6 float-right" style=" margin-top: 3%;">
        <div class="form-group">
          <textarea class="form-control" id="exampleTextarea" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 115px; background-color: #f1f1f1;" placeholder="Please tell us a little about you and your song(s)."></textarea>
        </div>
      </div>
      <div class="col-sm-12 float-right" style="">
        <button class="btn btn-default btn-lg" style="width: 100%; height:100%; line-height: 20px; background-color: #f1f1f1;">Click here to start uploading your MP3s.<br> 
          <span style="font-size:12px;">Please note that at this time we can only accept MP3 files.</span>
        </button>
      </div>

    </div>
    <div class="col-sm-2"></div>
  </div>

  <div class="col-sm-12">
    <div class="bg-fourth-page img-subscribe w3-padding" id="connect">
      <div class="container-fluid " style="margin-bottom:50px;">
        <div class="row" style="padding-top: 5%; height: 340px;">
          <div class="col-sm-12 row">
            <div class="col-sm-8"></div>
            <div class="col-sm-4">
              <p>
                <strong style="font-weight: bolder; font-size: 50px; color: #fff; text-shadow:2px 2px 4px #555;">PLAY MUSIC</strong><br>
                <strong style="font-weight: bolder; font-size: 50px; color: #fff; text-shadow:2px 2px 4px #555;">EVERYWHERE</strong>
              </p>
              <button class="btn btn-lg btn-primary">SUBSCRIBE NOW</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12 text-center">
    <div class="jumbotron">
      <p style="font-size: 40px;"><span class="fa fa-envelope" style="color: #949d9c;"></span> <b>JOIN OUR MAILING LIST</b></p>
      <p class="lead">Sign Up For Our Monthly Newsletter and Learn About Our Newest Releases, Greatest Hits and More.</p>
      <div class="col-sm-12 row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 text-center">
              <form>
                  <div class="input-group mb-3 input-group-lg">
                    <input type="email" class="form-control" required placeholder="Email" id="email_address">
                    <div class="input-group-prepend">
                      <span class="input-group btn btn-lg btn-primary" onclick="subscribe_now();">SUBSCRIBE NOW</span>&nbsp;<span id="loading" class="text-dark"></span>
                    </div>
                  </div>
                </form>
            </div>
            <div class="col-sm-3"></div>
          </div>
    </div>
  </div>
  </div>
<div class="p-5 text-center text-white" style="background-color: #282e3a; text-decoration: none; margin-top: -3%; ">

  <div>
    <b><a href="#">HOME</a> | <a href="submit_music.php">SUBMIT MUSIC</a> | <a href="#">PRICING</a> | <a href="#">ABOUT US</a> | <a href="#">CONTACT</a></b><br>
    <a href="#">FAQ</a> | <a href="#">TERMS OF USE</a> | <a href="#">PRIVACY POLICY</a><br>
  </div>
  &copy; 2018. LAMPSTAND STUDIO | ALL RIGHTS RESERVED
</div>

<?php 
// include_once("music_player.php");
 ?>
</body>
</html>


 <script type="text/javascript">
   $('[data-toggle="tooltip"]').tooltip(); 
 </script>
<?php include_once("modal.php"); ?>
<!-- Timer -->
<!-- uname/teamtester5gh@gmail.com
  pwd/testing5!
 -->