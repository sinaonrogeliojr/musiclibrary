/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.6.12-log : Database - music_library
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`music_library` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `music_library`;

/*Table structure for table `tbl_about_content` */

DROP TABLE IF EXISTS `tbl_about_content`;

CREATE TABLE `tbl_about_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` longtext,
  `body` longtext,
  `image` varchar(255) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_about_content` */

insert  into `tbl_about_content`(`id`,`header`,`body`,`image`,`date_update`,`active`) values 
(1,'Lampstand Studio is an initiative of a Christian household to provide a platform where good quality, original Christian music can be shared with members of the Lampstand Studio community. We aim to give households a place where they can download and share','The contributors to this site share their music free of charge so that others can be blessed by the word of the Lord through song and music. The music on this site has been recorded in amateur or pro-am studios as a labour of love but is still of a listenable standard. A spirit of fellowship and connection underpins the project.','img/guitar.jpg',NULL,1);

/*Table structure for table `tbl_album` */

DROP TABLE IF EXISTS `tbl_album`;

CREATE TABLE `tbl_album` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `album_id` varchar(255) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  `album_description` text,
  `album_artwork` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date_released` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`user_id`),
  KEY `album_id` (`album_id`),
  CONSTRAINT `userid` FOREIGN KEY (`user_id`) REFERENCES `tbl_user_profile` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_album` */

insert  into `tbl_album`(`id`,`album_id`,`album_name`,`album_description`,`album_artwork`,`user_id`,`date_released`) values 
(99,'album-6225','Christian Songs','Christian Musics and Gospel Songs	','','user-8415817','2018-10-08'),
(100,'album-339957','new','NONE','','user-8415817','2018-10-08'),
(101,'album-8710056','sample','NONE','','user-0005','2018-10-08'),
(108,'album-8410145','sample','asdasdas','img/album_img/242.JPG','user-8415817','2018-10-09'),
(109,'album-2110814','DRACARYS','Fire and Blood!','img/album_img/566.jpg','user-591635','2019-11-12'),
(110,'album-3210922','My album','test',NULL,'user-0003','2020-05-07'),
(111,'album-5611091','test','dasdasd','','user-0003','2020-05-07');

/*Table structure for table `tbl_application` */

DROP TABLE IF EXISTS `tbl_application`;

CREATE TABLE `tbl_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_application` */

insert  into `tbl_application`(`id`,`application_id`,`application_name`) values 
(1,'app-001','Item'),
(2,'app-002','Chorus'),
(3,'app-003','Busking'),
(4,'app-004','Background'),
(5,'app-005','Devotional');

/*Table structure for table `tbl_audience` */

DROP TABLE IF EXISTS `tbl_audience`;

CREATE TABLE `tbl_audience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audience_id` varchar(255) DEFAULT NULL,
  `audience_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_audience` */

insert  into `tbl_audience`(`id`,`audience_id`,`audience_name`) values 
(1,'audience-001','All'),
(2,'audience-002','Children'),
(3,'AUD-0337','Teens');

/*Table structure for table `tbl_audios` */

DROP TABLE IF EXISTS `tbl_audios`;

CREATE TABLE `tbl_audios` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `music` text,
  `song_name` varchar(255) DEFAULT NULL,
  `composer` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `artist` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `album_id` varchar(255) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `approved_date` datetime DEFAULT NULL,
  `audience_id` varchar(255) NOT NULL,
  `application_id` varchar(255) NOT NULL,
  `duration` time NOT NULL,
  `file_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `albumid` (`album_id`),
  KEY `genid` (`genre`),
  CONSTRAINT `albumid` FOREIGN KEY (`album_id`) REFERENCES `tbl_album` (`album_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_audios` */

insert  into `tbl_audios`(`id`,`audio_id`,`music`,`song_name`,`composer`,`description`,`keyword`,`artist`,`genre`,`album_id`,`upload_date`,`active`,`approved_date`,`audience_id`,`application_id`,`duration`,`file_id`) values 
(63,'song-6341','uploads/uploadedsongs/Though I am Just a Kid (Singing For My Lord).mp3','asds','da','sdasda','da','0','Children','album-8710056','2018-10-08 00:00:00',1,'2018-10-12 15:21:25','1','Chorus','04:22:00','GROUP-2188'),
(64,'song-936351','','asd','asd','asd','asdad','0','Instrumental','album-8710056','2018-10-08 00:00:00',1,'2018-10-12 15:21:25','1','Busking','00:00:00','GROUP-7692'),
(65,'song-276456','uploads/uploadedsongs/Hillsong Young & Free - Wake (Live) HD.mp3','Wake','Hillsong Young & Free','Hillsong Young & Free - Wake (Live) HD','wake','0','Instrumental, Modern Ensemble','album-6225','2018-10-08 00:00:00',1,'2018-10-12 15:21:25','1','Devotional','04:35:00','GROUP-2947'),
(66,'song-896557','uploads/uploadedsongs/One Way - Hillsong Worship (192  kbps) (Mp3Converter.net).mp3','One Way','Hillsong Worship','One Way - Hillsong Worship','JESUS','0','Contemporary, Modern Ensemble','album-6225','2018-10-08 00:00:00',1,'2018-10-12 15:21:25','1','Chorus','03:35:00','GROUP-2694'),
(67,'song-356696','uploads/uploadedsongs/Cornerstone Acoustic version-HD.mp3','Cornerstone','Rogelio Jr.','Cornerstone Acoustic version-HD','pogi','0','Contemporary, Modern Ensemble','album-339957','2018-10-08 00:00:00',1,'2018-10-12 15:21:25','1','Busking, Devotional','02:39:00','GROUP-4974'),
(68,'song-576752','../uploads/uploadedsongs/When The Fight Calls (Live) - Hillsong Young & Free (128  kbps) (Mp3Converter.net).mp3','Hillsong','Hillsong','adasd','dasdas','0','Solo','album-8710056','2018-10-12 00:00:00',1,'2018-10-12 15:21:25','1','Chorus','04:31:00','GROUP-4227'),
(69,'song-316835','../uploads/uploadedsongs/Our God - Jason Waller (Acoustic Cover).mp3','sada','sampke','sdas','dsad','0','Instrumental, Sunday School','album-8710056','2018-10-11 00:00:00',1,'2018-10-12 15:21:25','1','Busking','05:03:00','GROUP-9526'),
(70,'song-826999','../uploads/uploadedsongs/How Can It Be - Lauren Daigle.mp3','How Can It Be','Lauren Daigle','How Can It Be by Lauren Daigle','hcib','0','Acoustic','album-2110814','2019-11-12 00:00:00',2,'0000-00-00 00:00:00','1','Devotional','04:10:00','GROUP-9979'),
(71,'song-877099','../uploads/uploadedsongs/How Can It Be - Lauren Daigle.mp3','How Can It Be ','How Can It Be ','How Can It Be ','How Can It Be ','0','Acoustic, Solo','album-3210922','2020-05-07 00:00:00',0,'0000-00-00 00:00:00','1','Background, Busking','04:10:00','GROUP-4391'),
(72,'song-987142','../uploads/uploadedsongs/How Can It Be - Lauren Daigle.mp3','How Can It Be ','How Can It Be ','How Can It Be ','How Can It Be ','0','Acoustic, Solo','album-3210922','2020-05-07 00:00:00',0,'0000-00-00 00:00:00','1','Background, Busking','04:10:00','GROUP-4391');

/*Table structure for table `tbl_download_list` */

DROP TABLE IF EXISTS `tbl_download_list`;

CREATE TABLE `tbl_download_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `datetrans` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_download_list` */

/*Table structure for table `tbl_downloads` */

DROP TABLE IF EXISTS `tbl_downloads`;

CREATE TABLE `tbl_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date_` datetime DEFAULT NULL,
  `geo_location` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_downloads` */

insert  into `tbl_downloads`(`id`,`audio_id`,`user_id`,`date_`,`geo_location`) values 
(2,'song-936351','user-0002','2018-10-15 15:28:35',''),
(3,'song-316835','user-0002','2018-10-15 15:29:50',''),
(4,'song-356696','user-0002','2018-10-15 15:30:47',''),
(5,'song-896557','user-0002','2018-10-15 15:30:57',''),
(6,'song-276456','user-0002','2018-10-15 15:31:00',''),
(7,'song-936351','user-0002','2018-10-15 15:31:37','');

/*Table structure for table `tbl_faq_content` */

DROP TABLE IF EXISTS `tbl_faq_content`;

CREATE TABLE `tbl_faq_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `status` int(1) DEFAULT '0',
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_faq_content` */

insert  into `tbl_faq_content`(`id`,`title`,`content`,`status`,`date_update`) values 
(1,'How do I add my own music?','Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.',1,'2018-10-05 16:00:09'),
(2,'Are there any fees for song submission?','Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.',1,NULL),
(3,'Are all the songs downloadable?','Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.',1,NULL),
(17,'SAMPLE FAQ\'s TITLE.','SAMPLE FAQ\'s CONTENT.',1,'2018-10-05 20:49:56');

/*Table structure for table `tbl_genre` */

DROP TABLE IF EXISTS `tbl_genre`;

CREATE TABLE `tbl_genre` (
  `gen_id` int(255) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_genre` */

insert  into `tbl_genre`(`gen_id`,`genre_name`) values 
(6,'Acoustic'),
(7,'Contemporary'),
(8,'Vocal'),
(9,'Children'),
(10,'Instrumental'),
(11,'Solo'),
(12,'Modern Ensemble'),
(13,'Orchestral or Classical Ensemble'),
(14,'Sunday School'),
(15,'All');

/*Table structure for table `tbl_played_songs` */

DROP TABLE IF EXISTS `tbl_played_songs`;

CREATE TABLE `tbl_played_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `date_play` datetime DEFAULT NULL,
  `location` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_played_songs` */

insert  into `tbl_played_songs`(`id`,`audio_id`,`user_id`,`date_play`,`location`) values 
(1,'song-356696','index','2018-10-11 16:56:51',NULL),
(2,'song-576752','index','2018-10-11 16:56:58',NULL),
(3,'song-276456','user-0002','2018-10-11 17:02:05',NULL),
(4,'song-316835','user-0002','2018-10-11 17:03:14',NULL),
(5,'song-576752','user-0002','2018-10-11 17:03:22',NULL),
(7,'song-896557','user-8415817','2018-10-11 17:18:15',NULL),
(8,'song-896557','user-8415817','2018-10-11 17:20:12',NULL),
(9,'song-276456','user-8415817','2018-10-11 17:20:18',NULL),
(10,'song-276456','user-0001','2018-10-11 18:38:26',NULL),
(11,'song-316835','user-0001','2018-10-11 18:38:41',NULL),
(12,'song-896557','user-0001','2018-10-11 18:38:41',NULL),
(13,'song-576752','user-0001','2018-10-11 18:38:47',NULL),
(14,'song-356696','user-0001','2018-10-11 18:38:48',NULL),
(15,'song-6341','user-0001','2018-10-11 18:38:51',NULL),
(16,'song-936351','user-0001','2018-10-11 18:38:53',NULL),
(17,'song-276456','user-0002','2018-10-29 12:59:51',NULL),
(18,'song-936351','user-0002','2018-10-29 13:00:50',NULL),
(19,'song-6341','user-0002','2018-10-29 13:00:53',NULL),
(20,'song-356696','user-0002','2018-10-29 13:00:56',NULL),
(21,'song-896557','user-0002','2018-10-29 13:01:45',NULL),
(22,'song-576752','user-0002','2018-10-29 13:01:49',NULL),
(23,'song-896557','user-0002','2018-10-29 13:03:45',NULL),
(24,'song-576752','user-0002','2018-10-29 13:08:56',NULL),
(25,'song-896557','user-0002','2018-10-29 13:09:01',NULL),
(26,'song-896557','user-0001','2019-11-12 22:34:08',NULL),
(27,'song-276456','user-0001','2019-11-12 22:35:23',NULL);

/*Table structure for table `tbl_playlist` */

DROP TABLE IF EXISTS `tbl_playlist`;

CREATE TABLE `tbl_playlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `user_id` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_playlist` */

insert  into `tbl_playlist`(`id`,`name`,`date_created`,`user_id`) values 
(25,'FAVORITES','2018-10-29 12:34:52','user-0002');

/*Table structure for table `tbl_playlist_info` */

DROP TABLE IF EXISTS `tbl_playlist_info`;

CREATE TABLE `tbl_playlist_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `playlist_id` int(11) DEFAULT NULL,
  `date_` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_playlist_info` */

insert  into `tbl_playlist_info`(`id`,`audio_id`,`user_id`,`user_type`,`playlist_id`,`date_`,`is_active`) values 
(17,'66','user-0002',3,25,'2018-10-29 13:01:40',1),
(18,'68','user-0002',3,25,'2018-10-29 13:01:42',1);

/*Table structure for table `tbl_recently_played` */

DROP TABLE IF EXISTS `tbl_recently_played`;

CREATE TABLE `tbl_recently_played` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `audio_id` varchar(255) DEFAULT NULL,
  `date_play` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_recently_played` */

insert  into `tbl_recently_played`(`id`,`user_id`,`audio_id`,`date_play`) values 
(1,'user-0002','song-671571','2018-10-07 22:13:51'),
(2,'user-0002','song-4338','2018-10-07 22:14:06'),
(3,'user-591635','song-831721','2018-10-07 22:20:09'),
(4,'user-591635','song-671571','2018-10-07 22:20:12'),
(5,'user-591635','song-951699','2018-10-07 22:20:20'),
(6,'user-591635','song-931825','2018-10-07 22:20:23'),
(7,'user-591635','song-4338','2018-10-07 22:20:24'),
(8,'user-0002','song-4338','2018-10-07 23:43:22'),
(9,'user-0002','song-671571','2018-10-07 23:43:30'),
(10,'user-0002','song-885078','2018-10-08 17:13:37'),
(11,'user-0002','song-885078','2018-10-08 17:15:41'),
(12,'user-0002','song-885078','2018-10-08 17:15:55'),
(13,'user-0002','song-885078','2018-10-08 17:16:34'),
(14,'user-0002','song-936351','2018-10-08 21:32:45'),
(15,'user-0002','song-6341','2018-10-08 21:38:51'),
(16,'user-0002','song-356696','2018-10-08 21:38:51'),
(17,'user-0002','song-6341','2018-10-09 11:45:42'),
(18,'user-0002','song-6341','2018-10-09 11:45:43'),
(19,'user-0002','song-6341','2018-10-09 11:45:44'),
(20,'user-0002','song-6341','2018-10-09 11:45:44'),
(21,'user-0002','song-6341','2018-10-09 11:45:44'),
(22,'user-0002','song-6341','2018-10-09 11:45:44'),
(23,'user-0002','song-6341','2018-10-09 11:45:44'),
(24,'user-0002','song-6341','2018-10-09 11:45:45'),
(25,'user-0002','song-6341','2018-10-09 11:45:45'),
(26,'user-0002','song-936351','2018-10-09 12:12:24'),
(27,'user-0002','song-6341','2018-10-09 12:12:38'),
(28,'user-0002','song-6341','2018-10-09 12:16:37'),
(29,'user-0002','song-6341','2018-10-09 12:17:34'),
(30,'user-0002','song-936351','2018-10-09 12:17:41'),
(31,'user-0002','song-896557','2018-10-09 12:18:10'),
(32,'user-0002','song-356696','2018-10-09 15:50:43'),
(33,'user-0002','song-276456','2018-10-09 15:50:59'),
(34,'user-0002','song-936351','2018-10-11 17:00:23'),
(35,'user-0002','song-936351','2018-10-11 17:00:26'),
(36,'user-0002','song-6341','2018-10-11 17:00:35'),
(37,'user-0002','song-276456','2018-10-11 17:00:50'),
(38,'user-0002','song-276456','2018-10-11 17:02:05'),
(39,'user-0002','song-316835','2018-10-11 17:03:14'),
(40,'user-0002','song-576752','2018-10-11 17:03:22'),
(41,'user-0002','song-276456','2018-10-29 12:59:51'),
(42,'user-0002','song-936351','2018-10-29 13:00:50'),
(43,'user-0002','song-6341','2018-10-29 13:00:53'),
(44,'user-0002','song-356696','2018-10-29 13:00:56'),
(45,'user-0002','song-896557','2018-10-29 13:01:45'),
(46,'user-0002','song-576752','2018-10-29 13:01:49'),
(47,'user-0002','song-896557','2018-10-29 13:03:45'),
(48,'user-0002','song-576752','2018-10-29 13:08:56'),
(49,'user-0002','song-896557','2018-10-29 13:09:01');

/*Table structure for table `tbl_shuffle_index` */

DROP TABLE IF EXISTS `tbl_shuffle_index`;

CREATE TABLE `tbl_shuffle_index` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `order_row` varchar(255) DEFAULT NULL,
  `order_sort` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_shuffle_index` */

insert  into `tbl_shuffle_index`(`id`,`order_row`,`order_sort`) values 
(4,'song_name','DESC');

/*Table structure for table `tbl_subscribers` */

DROP TABLE IF EXISTS `tbl_subscribers`;

CREATE TABLE `tbl_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `date_trans` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_subscribers` */

insert  into `tbl_subscribers`(`id`,`name`,`email`,`date_trans`,`status`) values 
(2,'tester1','rogelio@gmail.com','2018-08-25 13:12:19',0),
(3,'rogelio','sinaonrogeliojr@gmail.com','2018-08-25 13:13:36',0);

/*Table structure for table `tbl_suggesttions` */

DROP TABLE IF EXISTS `tbl_suggesttions`;

CREATE TABLE `tbl_suggesttions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_suggesttions` */

/*Table structure for table `tbl_supporting_file` */

DROP TABLE IF EXISTS `tbl_supporting_file`;

CREATE TABLE `tbl_supporting_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_file_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_supporting_file` */

insert  into `tbl_supporting_file`(`file_id`,`user_id`,`new_file_id`,`file_name`,`file_description`,`link_name`,`link`,`group_id`,`file_path`) values 
(102,'user-8415817','file-2910128','ROGELIO-resume.docx','','','','GROUP-1665','../uploads/uploaded_files/ROGELIO-resume.docx'),
(98,'user-8415817','file-119736','application-letter-resume.docx','ads','asd','asdas','GROUP-3836','../uploads/uploaded_files/application-letter-resume.docx'),
(101,'user-8415817','file-4410063','application letter_rocky.docx','','','','GROUP-9285','../uploads/uploaded_files/application letter_rocky.docx'),
(99,'user-8415817','file-779825','application letter_rogelio.docx','','','','GROUP-3836','../uploads/uploaded_files/application letter_rogelio.docx'),
(100,'user-8415817','file-189968','jhienmedina (2).docx','','','','GROUP-5986','../uploads/uploaded_files/jhienmedina (2).docx');

/*Table structure for table `tbl_user_profile` */

DROP TABLE IF EXISTS `tbl_user_profile`;

CREATE TABLE `tbl_user_profile` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `b_date` varchar(20) DEFAULT NULL,
  `email_add` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` int(1) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `picture` text,
  `pwd_code` varchar(255) DEFAULT NULL,
  `is_approve` int(1) NOT NULL DEFAULT '0',
  `is_notify` int(1) DEFAULT '0',
  `last_log` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_profile` */

insert  into `tbl_user_profile`(`id`,`user_id`,`fn`,`mn`,`ln`,`gender`,`b_date`,`email_add`,`username`,`password`,`user_type`,`is_active`,`picture`,`pwd_code`,`is_approve`,`is_notify`,`last_log`) values 
(7,'user-0001','Rogelio Jr.s','MARQUEZ','Sinaons','Male','1996-12-31','admin@gmail.com','admin','1234',1,1,'/avatar/pict-316.png','1111',1,1,'2019-11-12 22:33:39'),
(8,'user-0002','Starks','GUEST','Eddard','Male',NULL,'guest@gmail.com','guest','1234',3,1,'/avatar/pict-335.JPG','1112',1,0,'2018-10-29 13:31:33'),
(9,'user-0003','Sinaon','Dalloran','Rogelio Jr.','Male','2018-06-27','rogelio@yahoo.com','rogelio','12345',2,1,'/avatar/243.jpg','1111',1,1,'2020-05-07 14:22:46'),
(10,'user-0004','Juan ','A.','Delacruz','Male','2018-06-27','juan@yahoo.com','user','12345',2,1,'/avatar/pict-996.png','1111',1,1,NULL),
(11,'user-0005','USER','USER','USER','Male','2018-06-27','user@gmail.com','user','1234',3,1,'/avatar/pict-763.png','1111',1,1,'2018-10-11 17:16:04'),
(12,'user-211268','Liam','bob','MacKay','Male','2018-07-05','liam@sitesuport.com.au','liam','GreatWhite18!',3,1,'/avatar/pict-627.jpg','8M57s0l6',1,1,NULL),
(13,'user-691387','Marvin','N/A','Louel','N/A','2018-09-19','vin@ymail.com','N/A','Marvin6490',2,1,'/avatar/pict-292.jpg',NULL,1,1,NULL),
(14,'user-191413','Vin','N/A','Diesel','N/A','N/A','guest@gmaidl.com','N/A','12345',3,1,'/avatar/pict-604.jpg',NULL,1,0,'2018-09-26 16:28:09'),
(15,'user-8415817','Liam','N/A','Mackay','Male','N/A','oji@gmail.com','N/A','12345',3,1,'/avatar/pict-173.JPG',NULL,1,1,'2018-10-12 21:01:30'),
(16,'user-591635','John','N/A','Snow','N/A','N/A','johnsnow@gmail.com','N/A','123456789',2,1,'/avatar/pict-902.jpg',NULL,1,0,'2019-11-12 22:40:48'),
(17,'user-441712','Eddard','N/A','Stark','N/A','N/A','eddard@gmail.com','N/A','1234567',2,0,NULL,'3M19S4L5',0,0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
