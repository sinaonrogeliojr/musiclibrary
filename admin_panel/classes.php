<?php 
date_default_timezone_set('Australia/Brisbane');
require("../api/fpdf.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

function edit_supporting_file($con, $id, $fn, $fd, $ln, $link) 
{
	$sql = mysqli_query($con, "UPDATE tbl_supporting_file SET file_name = '$fn', file_description = '$fd', link_name = '$ln', link = '$link' WHERE file_id = '$id'");

	if ($sql) 
	{
		echo 1;
	}
}

function remove_file($con,$path,$id){
	$sql = mysqli_query($con, "DELETE from tbl_supporting_file where new_file_id='$id' ");
	if ($sql) {
		echo 1;
	}
}

function gen_groupid($con){
$my_id = $_SESSION['admin'];
$query = mysqli_query($con, "SELECT MAX(group_id) FROM tbl_supporting_file where user_id='$my_id'");
$row = mysqli_fetch_assoc($query);
if (mysqli_num_rows($query)>0) {
 		echo'GROUP-'.rand(1,9).rand(1,9).rand(1,9).rand(1,9);
}else{
  		echo'GROUP-0001';
}

}

function get_dateupload($con, $songid){

	$sql = mysqli_query($con,"SELECT * FROM tbl_audios where id = '$songid'");

		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				echo date('Y-m-d', strtotime($row['upload_date']));
			}	
		}else{
			    echo date('Y-m-d');
		}
}


function load_album_selection($con,$searchalbums)
{
	$myid = $_SESSION['admin'];
	if ($searchalbums == "" || $searchalbums == null) {
	$sql = mysqli_query($con, "SELECT * from tbl_album where user_id='$myid' ORDER BY album_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>
		<ul class="list-group list-group-lg " ondblclick="selectalb('<?php echo ucfirst($row['album_name'])?>','<?php echo $row['album_id']?>');" style="padding-top: 0px; ">
          <li class="list-group-item text-white lister-album">   
            <div class="pull-right"> 
            	<button class="btn btn-dark btn-small" onclick="selectalb('<?php echo ucfirst($row['album_name'])?>','<?php echo $row['album_id']?>');">
            		<i class="fa fa-plus"></i>
            	</button>
            </div> 
            <a href="#" class=" m-r-sm pull-left" style=" margin-top: -3px; text-decoration: none;"> 
            	<img src="<?php echo '../'.$row['album_artwork']?>" width="40" height="35">
            		<span><?php echo ucfirst($row['album_name']); ?></span> 
           	</a> 
          </li>
        </ul> 
	<?php
	}
	?>
	<?php
	}	
	else
	{
	echo '<div id="no_songs" class="text-center text-white col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;" onclick="addto_album();">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> Add New Album!</label></strong>
		</div>';
		}
	}
	else
	{
	$sql = mysqli_query($con, "SELECT * from tbl_album where user_id='$myid' AND concat(album_name) like '%$searchalbums%' ORDER BY album_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>
		<ul class="list-group list-group-lg " ondblclick="selectalb('<?php echo ucfirst($row['album_name'])?>','<?php echo $row['album_id']?>');" style="padding-top: 0px; ">
          <li class="list-group-item text-white lister-album" style="height: 60px;">   
            <div class="pull-right"> 
            	<button class="btn btn-success" onclick="selectalb('<?php echo ucfirst($row['album_name'])?>','<?php echo $row['album_id']?>');">
            		<i class="fa fa-plus"></i>
            	</button>
            </div> 
            <a href="#" class=" m-r-sm pull-left" style=" margin-top: -3px; text-decoration: none;"> 
            	<img src="<?php echo '../'.$row['album_artwork']?>" width="40" height="35">
            		<span><?php echo ucfirst($row['album_name']); ?></span> 
           	</a> 
          </li>
        </ul> 
	<?php
	}
	?>
	<?php
	}
	else
	{
		echo '<div id="no_songs" class="text-center text-white col-sm-12" style="margin-top: 120px; font-size:15px; font-family:Stardust;">
		  <strong><span class="fa fa-music fa-5x"></span> <br><label style="font-size: 45px;"> No Records Found!</label></strong>
		</div>';
	}
	}
	}

function show_supporting_files($con,$id) 
{	
	$user = $_SESSION['admin'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_supporting_file WHERE user_id = '$user' and group_id = '$id' ");
	if (mysqli_num_rows($sql)>0) {
			while ($rows = mysqli_fetch_assoc($sql)) 
	{ ?>

		<tr class="tracks">
		<td><?php echo $rows['file_name']?></td>
		<td><?php echo $rows['file_description']?></td>
		<td><?php echo $rows['link_name'] ?></td>
		<td><?php echo $rows['link']?></td>
		<td style="text-align: center;">
			<div class="btn-group btn-justified">
				<a href="#" class="btn btn-normal btn-dark"  data-toggle="modal" data-target="#EditFileModal" 
				id="edit" title="Edit" onclick="get_file_data('<?php echo $rows['new_file_id']?>')"><span class="fa fa-edit fa-lg"></span> </a>

				<a href="#" class="btn btn-normal btn-dark" id="delte" title="Delete" onclick="remove_file('<?php echo $rows['new_file_id']?>','../uploads/uploaded_files/<?php echo $rows['file_name']?>');">
					<span class="fa fa-trash fa-lg"></span> 
		        </a>
			</div>
	       	
		</td>
	</tr>
	
<?php
	}
	}else{
			
	}
	
}
function load_style_select($con, $style){
	?>
	    <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT t1.*,t2.* from tbl_genre t1 left join tbl_audios t2 on t1.genre_name=  t2.`genre` ORDER BY t1.genre_name ASC ");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
                <label class="form-check-label oji-3"> 

                	<?php 

                	if(strcmp($style, $row['genre_name'])){ ?>

                	<input onclick="get_style_value();" checked="true" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	
                	<span class="text-ellipsis style-name">
					<?php echo $row['genre_name']?> 
                	</span>

                	<?php	}else{ ?>

                	<input onclick="get_style_value();" type="checkbox" value="<?php echo $row['genre_name'] ?>">
                	 
                	<span class="text-ellipsis style-name">

                		<?php echo $row['genre_name']?> 
                	</span>

                	<?php
                		}

                	?>

                	 
      			</label> 
      			
	          <?php
	        }
	       ?>
	   </div>
	<?php
	}


	function load_app_select($con,$app){
	?>
        <div class="col-lg-12"> 
	      <?php 
	        $sql = mysqli_query($con,"SELECT t1.*,t2.* from tbl_application t1 left join tbl_audios t2 on t1.application_name=  t2.`application_id` ORDER BY t1.application_name ASC");
	        while ($row = mysqli_fetch_assoc($sql)) {
	          ?>
				<label class="form-check-label oji-3"> 
				<?php 

                	

                	if(strcmp($app, $row['application_name'])){ ?>

                	<input onclick="get_app_value();" type="checkbox"  checked="true" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php	}else{ ?>

                		<input onclick="get_app_value();" type="checkbox" value="<?php echo $row['application_name'] ?>">
                
                	<span>
                		<?php echo ucfirst($row['application_name'])?> 
                	</span> 

                	<?php
                		}

                	?>
      			</label> 
	          <?php
	        }
	       ?>
	    </div>
	<?php
	}


function id_gen_all($con,$tbl,$row_name,$alias_id){
	$sql = mysqli_query($con,"SELECT max(".$row_name.") from ".$tbl."");
	$row = mysqli_fetch_assoc($sql);
	return $alias_id.'-'.rand(0,9).($row['max('.$row_name.')'] + 1).rand(0,9).rand(1,10);
}

function insert_genre($con,$genre,$gen_id){
	if ($gen_id == "") {
	$sql = mysqli_query($con,"INSERT INTO tbl_genre values(gen_id,'$genre')");
	if ($sql) {
		echo 1;
	}
	}
	else
	{
	$sql = mysqli_query($con,"UPDATE tbl_genre set genre_name='$genre' where gen_id='$gen_id'");
	if ($sql) {
		echo 2;
	}
	}
}

function insert_app($con,$app_name,$app_id){
	$new_id = id_gen_all($con,'tbl_application','id','APP');
	if ($app_id == "") {
	$sql = mysqli_query($con,"INSERT INTO tbl_application values(id,'$new_id','$app_name')");
	if ($sql) {
		echo 1;
	}
	}
	else
	{
	$sql = mysqli_query($con,"UPDATE tbl_application set application_name='$app_name' where application_id='$app_id'");
	if ($sql) {
		echo 2;
	}
	}
}

function insert_audiences($con,$audience_type,$audience_id){
	$new_id = id_gen_all($con,'tbl_audience','id','AUD');
	if ($audience_id == "") {
	$sql = mysqli_query($con,"INSERT INTO tbl_audience values(id,'$new_id','$audience_type')");
	if ($sql) {
		echo 1;
	}
	}
	else
	{
	$sql = mysqli_query($con,"UPDATE tbl_audience set audience_name='$audience_type' where audience_id='$audience_id'");
	if ($sql) {
		echo 2;
	}
	}
}

function display_genre($con){
	$sql = mysqli_query($con,"SELECT * from tbl_genre order by genre_name asc");
	if (mysqli_num_rows($sql)>0) {
		?>
		<table class="table table-hover  table-light ">
			<thead class="thead-dark">
				<tr>
					<th>Genre Name</th>
					<th class="text-right">Action</th>
				</tr>
			</thead>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
		<tbody>
			<tr class="tracks w3-hover-shadow">
				<td><?php echo $row['genre_name'] ?></td>
				<td class="text-right">
					<div class="dropdown " style="margin-bottom: 0px;">
						<button type="button" class="btn btn-dark dropdown-toggle w3-round-xxlarge w3-border-black btn-small" data-toggle="dropdown">
						  <i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu">
						  <a class="dropdown-item w3-hover-green" href="#" onclick="update_genre('<?php echo $row['genre_name'] ?>','<?php echo $row['gen_id'] ?>');"><i class="fa fa-pencil"></i> Edit</a>
						  <a class="dropdown-item w3-hover-red" href="#" onclick="delete_genre('<?php echo $row['gen_id'] ?>');"><i class="fa fa-trash"></i> Delete</a>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
		<?php
		}
		?>
		</table>
		<?php
	}
}

function display_audiences($con){
	$sql = mysqli_query($con,"SELECT * from tbl_audience order by audience_name ASC");
	if (mysqli_num_rows($sql)>0) {
		?>
		<table class="table table-hover table-light ">
			<thead class="thead-dark">
				<tr id="labels">
					<th>Audience Type</th>
					<th class="text-right">Action</th>
				</tr>
			</thead>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
		<tbody>
			<tr class="tracks w3-hover-shadow">
				<td><?php echo $row['audience_name'] ?></td>
				<td class="text-right">
					<div class="dropdown " style="margin-bottom: 0px; width: 60%;">
						<button type="button" class="btn btn-dark dropdown-toggle w3-round-xxlarge w3-border-black btn-small" data-toggle="dropdown">
						 <i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu">
						  <a class="dropdown-item w3-hover-green" href="#" onclick="update_audience('<?php echo $row['audience_name'] ?>','<?php echo $row['audience_id'] ?>');"><i class="fa fa-pencil"></i> Edit</a>
						  <a class="dropdown-item w3-hover-red" href="#" onclick="delete_audience('<?php echo $row['audience_id'] ?>');"><i class="fa fa-trash"></i> Delete</a>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
		<?php
		}
		?>
		</table>
		<?php
	}
}


function display_app($con){
	$sql = mysqli_query($con,"SELECT * from tbl_application order by application_name asc");
	if (mysqli_num_rows($sql)>0) {
		?>
		<table class="table table-hover table-light ">
			<thead class="thead-dark">
				<tr>
					<th>Application Name</th>
					<th class="text-right">Action</th>
				</tr>
			</thead>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
		?>
		<tbody>
			<tr class="w3-hover-shadow tracks">
				<td><?php echo $row['application_name'] ?></td>
				<td class="text-right">
					<div class="dropdown " style="margin-bottom: 0px; width: 70%;">
						<button type="button" class="btn btn-dark dropdown-toggle w3-round-xxlarge w3-border-black btn-small" data-toggle="dropdown">
						  <i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu">
						  <a class="dropdown-item w3-hover-green" href="#" onclick="update_app('<?php echo $row['application_name'] ?>','<?php echo $row['application_id'] ?>');"><i class="fa fa-pencil"></i> Edit</a>
						  <a class="dropdown-item w3-hover-red" href="#" onclick="delete_app('<?php echo $row['application_id'] ?>');"><i class="fa fa-trash"></i> Delete</a>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
		<?php
		}
		?>
		</table>
		<?php
	}
}

function delete_genre($con,$id){
	$sql = mysqli_query($con,"DELETE from tbl_genre where gen_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function delete_auds($con,$id){
	$sql = mysqli_query($con,"DELETE from tbl_audience where audience_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function delete_application($con,$id){
	$sql = mysqli_query($con,"DELETE from tbl_application where application_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//update  account name
function update_name($con,$id,$fn,$mn,$ln){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set fn='$fn',mn='$mn',ln='$ln' where user_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function update_bdate($con,$bdate,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set b_date='$bdate' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}



function update_email($con,$email,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set email_add='$email' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_username($con,$username,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set username='$username' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}


function update_pwd($con,$pwd,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set password='$pwd' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_album($con,$new_name,$id){
	$sql = mysqli_query($con,"UPDATE tbl_album set album_name='$new_name' where album_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function delete_album($con,$id){
	$sql = mysqli_query($con,"DELETE from tbl_album where album_id='$id'");
	if ($sql) {
		echo 1;
	}
}

function upload_music($con,$link2,$song_name,$composer,$genres,$album,$ud,$audience,$application,$duration){

	$random_id = new_id($con);
	$sql = mysqli_query($con,"INSERT INTO tbl_audios values(id,'$random_id','$link2','$song_name','$composer',0,'$genres','$album', '$ud',0,'','$audience','$application','$duration')");
	if ($sql) {
		echo 1;
	}
	else
	{
		echo 2;
	}
}

function new_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_audios");
	$row = mysqli_fetch_assoc($sql);
	return 'song-'.rand(1,9).rand(1,9).$row['max(id)'].rand(1,9).rand(1,9);
}

//Session update
function refresh_account($con){
	$id = $_SESSION['admin'];
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$id' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['admin'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
			$_SESSION['user_pic'] = '../avatar/def.png';
					
		}
	else
	{
		$_SESSION['user_pic'] = '../'.$row['picture'];
	}

	$dt1 = date_create($row['age']);
	$dt2 = date('Y-m-d');
	$dt3 = date_create($dt2);	
	$age = date_diff($dt1,$dt3);
	?>
	<div class="list-group">

			<li onclick="update_name('<?php echo $_SESSION['fn'] ?>','<?php echo $_SESSION['mn'] ?>','<?php echo $_SESSION['ln'] ?>','<?php echo $_SESSION['admin'] ?>');" class="list-group-item w3-hover-shadow" data-toggle="tooltip" data-placement="left" title="Name"><i class="w3-margin-right fa fa-user fa-lg"></i> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?>
			</li>
			<li onclick="update_email('<?php echo $_SESSION['email_add'] ?>');" class="list-group-item w3-hover-shadow" data-toggle="tooltip" data-placement="left" title="E-mail"><i class="w3-margin-right fa fa-at fa-lg"></i> <?php echo $_SESSION['email_add'] ?>	
			</li>
			<li onclick="update_pwd('<?php echo $_SESSION['password'] ?>');"  data-toggle="tooltip" data-placement="left" title="Change your password" class="list-group-item w3-hover-shadow"><i class="w3-margin-right fa fa-lock fa-lg"></i> Change Password
			</li>
		</div>
		<script type="text/javascript">
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		});
		</script>
	<?php
}


function user_account_type($type){
	if ($type == 2) {
		return 'Contributor';
	}
	else if ($type == 3) {
		return 'Guest User';
	}
}

function app_disapp($con,$id){
$ss = mysqli_query($con,"SELECT * from tbl_user_profile where id='$id'");
$row = mysqli_fetch_assoc($ss);
if ($row['is_approve'] == 1) {
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set is_approve=0 where id='$id'");
$query3=mysqli_query($con, "SELECT fn,email_add,password FROM tbl_user_profile where id='$id'");
	$row3 = mysqli_fetch_assoc($query3);

	//send email
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row3['email_add']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Hi <'.$row3['fn'].'>.';
	    $mail->Body    = '<p>Unfortunately we are not able to approve your registration request for Lampstand Studio. If this status changes, we will advise you.</p>

                            <p>Lampstand Studio is a closed community and registration is generally only available via invitation or membership qualification. It is not available to the general public.</p>
                            
                            <p>If your registration request has not been approved, it is likely due to either or both of the above criteria.</p>
                            
                            Kind Regards,<br>
                            The Lampstand Studio Team';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
	
}
else
{
$sql = mysqli_query($con,"UPDATE tbl_user_profile set is_approve=1 where id='$id'");

$query2=mysqli_query($con, "SELECT fn,email_add,password FROM tbl_user_profile where id='$id'");
	$row2 = mysqli_fetch_assoc($query2);

	//send email
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row2['email_add']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Hi <'.$row2['fn'].'>.';
	    $mail->Body    = '<p>We are pleased welcome you to the Lampstand Studio Community!
                            You now have access to original music written and recorded by RFI members and provided on this site for all to enjoy.<p>
                            <p>YOur login details are:<br>
                            Username:  <b><em>'.$row2['email_add'].'</em></b><br>
                            Password:  <b><em>'.$row2['password'].'</em></b><br><br>
                            You can login on any device at www.lampstandstudio.com</p>
                            
                            <p>Once logged in,  you can search and filter the songs on the site and even build your own playlists. You can also download the songs to your device. Some songs may even have supporting files such as scores that you are also able to download and use.<p>
                            
                            <p>Lampstand Studio is a "house to house" project developed and maintained by a number of RFI households. It is not associated with the RFI Lampstand website in any way except that Lampstand Studio is only available to RFI members.<p>
                            
                            <p>We trust that you the music on the site and that it is a blessing to your family.<p>
                            
                            <p>Attached to this email is a user guide that will help you get the most out of Lampstand Studio.<p>
                            
                            <p>Happy listening and downloading.<p>
                            
                            Kind Regards,<br>
                            
                            The Lampstand Studio Team';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
}
echo 1;
}

function user_approve($data,$id){
	if ($data == 0) {
		return '<a  href="#disapp"  data-trigger="hover" data-toggle="tooltip" title="Approved/Disapproved" data-placement="left"><i id="'.$id.'" onclick="app_disapp($(this).attr(\'id\'));" class="fa fa-thumbs-down text-danger w3-xlarge"></i></a> <span class="'.$id.'">Disapproved</span>';
	}
	else if($data == 1)
	{
		return '<a href="#app" data-trigger="hover" data-toggle="tooltip" title="Approved/Disapproved" data-placement="left"><i id="'.$id.'" onclick="app_disapp($(this).attr(\'id\'));"  class="fa fa-thumbs-up text-primary w3-xlarge"></i></a> <span class="'.$id.'">Approved</span>';
	}
}
//email approve
function subscribe_approve($data,$id){
	if ($data == 0) {
		return '<a  href="#disapp"  data-trigger="hover" data-toggle="tooltip" title="Subscribed/Not Subscribed" data-placement="left"><i id="'.$id.'" onclick="subcribe_app_disapp($(this).attr(\'id\'));" class="fa fa-thumbs-down text-danger w3-xlarge"></i></a> <span class="'.$id.'">Not Subscribed</span>';
	}
	else if($data == 1)
	{
		return '<a href="#app" data-trigger="hover" data-toggle="tooltip" title="Subscribed/Not Subscribed" data-placement="left"><i id="'.$id.'" onclick="subcribe_app_disapp($(this).attr(\'id\'));"  class="fa fa-thumbs-up text-primary w3-xlarge"></i></a> <span class="'.$id.'">Subscribed</span>';
	}
}

function subcribe_app_disapp($con,$id){
$ss = mysqli_query($con,"SELECT * from tbl_subscribers where id='$id'");
$row = mysqli_fetch_assoc($ss);
if ($row['status'] == 1) {
	$sql = mysqli_query($con,"UPDATE tbl_subscribers set status=0 where id='$id'");
	$query3=mysqli_query($con, "SELECT name,email FROM tbl_subscribers where id='$id'");
	$row3 = mysqli_fetch_assoc($query3);

	//send email to disapproved
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row3['email']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Subscriber welcome '.$row3['email'].':';
	    $mail->Body    = '<p>Hi '.$row3['name'].',</p>
						<p>You have unsubscribed from the Lampstand Monthly Updates. You will no longer receive the monthly update email. </p>

						<p>You can re-subscribe at any time via your account management area.</p>

						<p>Kind Regards,</p>
						<p>The Lampstand Studio Team</p>';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
	
}
else
{
$sql = mysqli_query($con,"UPDATE tbl_subscribers set status=1 where id='$id'");

$query2=mysqli_query($con, "SELECT name,email FROM tbl_subscribers where id='$id'");
	$row2 = mysqli_fetch_assoc($query2);

	//send email to approved
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row2['email']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Subscriber welcome '.$row2['email'].':';
	    $mail->Body    = ' <p>Hi '.$row2['name'].',</p>
							<p>Thanks for subscribing to the Lampstand Studio Monthly update. Once a month we will send you a list of any new songs that have been added to the site since the last update. </p>

							<p>If no new music is added, you will not receive an email. </p>

							<p>These are the only emails that you will receive from Lampstand Studio apart from any technical notifications if we are going to be doing upgrades or maintenance.</p>
							<p>You can manage your subscription from within the account section of your login.</p>

							<p>Enjoy your free music!</p>

							<p>Kind Regards,</p>
							<p>The Lampstand Studio Team</p>
	    				';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
}
echo 1;
}

function get_max_limit($con,$tbl){
$sql = mysqli_query($con,"SELECT count(id) from ".$tbl."");
$row = mysqli_fetch_assoc($sql);
return $row['count(id)'];
}

function show_accounts($con,$search,$start,$limit,$page){
$start_from = ($page - 1)*$limit; 
if ($search == "" || $search == null) {
		$sql =mysqli_query($con,"SELECT * from tbl_user_profile where user_type=2 or user_type=3 or user_type=4 order by fn,ln asc LIMIT ".$start_from.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			?>
			<script>
				$(document).ready(function(){
					$("#chk_accounts").change(function(){
					    if(this.checked){
					      $(".select_accounts").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_accounts").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});
			</script>

			<table class="table table-hover table-light table-responsive-sm" id="usersTable">
			<thead class="thead-dark">
				<tr id="labels">
					<th>
					<div class="form-check-inline">
				      <label class="form-check-label" for="chk_accounts">
				        <input type="checkbox" name="chk_accounts" id="chk_accounts" onclick="check_all_validate();"> Check All
				      </label>
				    </div>						
					</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Name</td>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(2)')" style="cursor:pointer;">User Type</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(3)')" style="cursor:pointer;">Status</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(4)')" style="cursor:pointer;">Email</th>
					<th>Password</th>
					<th>Action</th>
				</tr>	
			</thead>
			<?php
			$num_id = 1;
			while ($row = mysqli_fetch_assoc($sql)) {
				$temp_id = 'input1'.$num_id++;
				$temp_btn_id = 'btn1'.$num_id++;
				if ($row['picture'] == "") {
					$pict = '../avatar/def.png';
					}
					else
					{
						$pict = '../'.$row['picture'];
					}

					if ($row['user_type'] == 2) {
						$user_type = strtoupper('Contributor');
					}
					else if ($row['user_type'] == 3) {
						$user_type = strtoupper('Guest User');
					}
					else if ($row['user_type'] == 4) {
						$user_type = strtoupper('INACTIVE');
					}
					$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$user_type."</p>";
					$toll_title="<center>".$row['fn'].' '.$row['ln']."</center>";
				?>
				<tbody id="<?php echo $row['user_id'] ?>">
					<tr class="tracks item" id="<?php echo $row["id"]; ?>">
						<!--<td class="text-left" style="padding: 1%;">
							<img class="imgs img-responsive" src="<?php echo $pict; ?>" alt=""></td>-->
						<td class="text-left"><p class="default-fs" style="margin-left: 18%;"><input type="checkbox" onclick="count_check();" name="id[]" class="select_accounts" value="<?php echo $row["id"]; ?>" /></p></td>
						<td class="text-capitalize db-content text-left"><p class="default-fs" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>" style="margin-left: 7%;"><?php echo $row['fn'].' '.$row['ln'];  ?></p></td>
						<td class="text-capitalize db-content text-left"><p class="default-fs" style="margin-left: 10%;"><?php echo user_account_type($row['user_type']); ?></p></td>
						<td class="text-left db-content"><p class="default-fs" style="margin-left: 10%;"><?php echo user_approve($row['is_approve'],$row['id']); ?></p></td>
						<td class="db-content text-left"><p class="default-fs" style="margin-left: 7%;"><?php echo $row['email_add']; ?></p></td>
						<td class="text-left db-content">
							<span style="margin-left: 7%;"><input id="<?php echo $temp_id ?>" disabled="" style="border:none; background-color: transparent;" type="password" class="db-content default-fs"name="" value="<?php echo $row['password']; ?>"/></span>

							<span class="fa fa-eye fa-lg <?php echo $temp_id ?>" onclick="toggle_pwd('<?php echo $temp_id ?>','<?php echo $_SESSION['password'] ?>','<?php echo $temp_btn_id ?>')">	
							</span>
							<span class="fa fa-edit fa-lg" onclick="update_password_client('<?php echo $row['user_id'] ?>','<?php echo $row['fn'].' '.$row['ln'] ?>',$(this).attr('id'));" style="display: none;" id="<?php echo $temp_btn_id ?>">
							</span>	
							
						</td>
						<td class="text-center">
							<div class="btn-group btn-justified">
								<button class="btn btn-dark btn-small default-fs" title="User Type" onclick="change_user_type(<?php echo $row['user_type'] ?>,'<?php echo $row['user_id'] ?>');"><i class="fa fa-user-circle fa-lg"></i> 
								</button>
								<button  class="btn btn-dark btn-small default-fs" title="Edit Info" onclick="get_client_info('<?php echo $row['user_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['email_add'] ?>')"><i class="fa fa-edit fa-lg"></i>
								</button>
								<button class="btn btn-dark btn-small default-fs" title="Delete User" onclick="delete_users('<?php echo $row['user_id'] ?>');"><i class="fa fa-trash fa-lg"></i>
								</button>
							</div>
						</td>
					</tr>
				</tbody>
				<script type="text/javascript">
				enable_tooltips();	
				</script>	
			<?php
			}

			?>
			</table>
			<?php
			$tab = get_max_limit($con,'tbl_user_profile') - 1;
			$total = ceil($tab/20);
			?>
			<div class="float-right">
			<ul class="pagination">
			<?php
			for ($i=1; $i <=$total ; $i++) { 
			?>
			 <li class="page-item"><a class="page-link" href="#hh" onclick="next_record('<?php echo $i ?>')"><?php echo $i ?></a></li>
			<?php
			}
			?>
			</ul>
			</div>
			<?php

		}
		else
		{
			echo '<div class="alert glass">No Records Found...</div>';
		}
	}
	else
	{

		$sql =mysqli_query($con,"SELECT * from tbl_user_profile where concat(fn,' ',ln) like '%$search%' and (user_type=2 or user_type=3 or user_type = 4) or email_add like '%$search%'  and (user_type=2 or user_type=3 or user_type = 4) LIMIT 0,3");
		if (mysqli_num_rows($sql)>0) {
			?>
			<script>
				$(document).ready(function(){
					$("#chk_accounts").change(function(){
					    if(this.checked){
					      $(".select_accounts").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_accounts").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});

			</script>
			<table class="table table-hover table-light table-responsive-sm" id="usersTable">
			<thead class="thead-dark">
				<tr id="labels">
					<th>
					<div class="form-check-inline">
				      <label class="form-check-label" for="chk_accounts">
				        <input type="checkbox" name="chk_accounts" id="chk_accounts" onclick="check_all_validate();"> Check All
				      </label>
				    </div>						
					</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Name</td>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(2)')" style="cursor:pointer;">User Type</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(3)')" style="cursor:pointer;">Status</th>
					<th onclick="w3.sortHTML('#usersTable', '.item', 'td:nth-child(4)')" style="cursor:pointer;">Email</th>
					<th>Password</th>
					<th>Action</th>
				</tr>	
			</thead>
			<?php
			$num_id = 1;

			while ($row = mysqli_fetch_assoc($sql)) {
				$temp_id = 'input1'.$num_id++;
				$temp_btn_id = 'btn1'.$num_id++;
				if ($row['picture'] == "") {
					$pict = '../avatar/def.png';
					}
					else
					{
						$pict = '../'.$row['picture'];
					}

					if ($row['user_type'] == 2) {
						$user_type = strtoupper('Contributor');
					}
					else if ($row['user_type'] == 3) {
						$user_type = strtoupper('Guest User');
					}
					else if ($row['user_type'] == 4) {
						$user_type = strtoupper('INACTIVE');
					}
					$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$user_type."</p>";
					$toll_title="<center>".$row['fn'].' '.$row['ln']."</center>";
				?>
				<tbody id="<?php echo $row['user_id'] ?>">
					<tr class="tracks item" id="<?php echo $row["id"]; ?>">
						<!--<td class="text-left" style="padding: 1%;">
							<img class="imgs img-responsive" src="<?php echo $pict; ?>" alt=""></td>-->
						<td class="text-left"><p class="default-fs" style="margin-left: 18%;"><input type="checkbox" onclick="count_check();" name="id[]" class="select_accounts" value="<?php echo $row["id"]; ?>" /></p></td>
						<td class="text-capitalize db-content text-left"><p class="default-fs" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>" style="margin-left: 7%;"><?php echo $row['fn'].' '.$row['ln'];  ?></p></td>
						<td class="text-capitalize db-content text-left"><p class="default-fs" style="margin-left: 10%;"><?php echo user_account_type($row['user_type']); ?></p></td>
						<td class="text-left db-content"><p class="default-fs" style="margin-left: 10%;"><?php echo user_approve($row['is_approve'],$row['id']); ?></p></td>
						<td class="db-content text-left"><p class="default-fs" style="margin-left: 7%;"><?php echo $row['email_add']; ?></p></td>
						<td class="text-left db-content">
							<span style="margin-left: 7%;"><input id="<?php echo $temp_id ?>" disabled="" style="border:none; background-color: transparent;" type="password" class="db-content default-fs"name="" value="<?php echo $row['password']; ?>"/></span>

							<span class="fa fa-eye fa-lg <?php echo $temp_id ?>" onclick="toggle_pwd('<?php echo $temp_id ?>','<?php echo $_SESSION['password'] ?>','<?php echo $temp_btn_id ?>')">	
							</span>
							<span class="fa fa-edit fa-lg" onclick="update_password_client('<?php echo $row['user_id'] ?>','<?php echo $row['fn'].' '.$row['ln'] ?>',$(this).attr('id'));" style="display: none;" id="<?php echo $temp_btn_id ?>">
							</span>	
							
						</td>
						<td class="text-center">
							<div class="btn-group btn-justified">
								<button class="btn btn-dark btn-small default-fs" title="User Type" onclick="change_user_type(<?php echo $row['user_type'] ?>,'<?php echo $row['user_id'] ?>');"><i class="fa fa-user-circle fa-lg"></i> 
								</button>
								<button  class="btn btn-dark btn-small default-fs" title="Edit Info" onclick="get_client_info('<?php echo $row['user_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['email_add'] ?>')"><i class="fa fa-edit fa-lg"></i>
								</button>
								<button class="btn btn-dark btn-small default-fs" title="Delete User" onclick="delete_users('<?php echo $row['user_id'] ?>');"><i class="fa fa-trash fa-lg"></i></i> 
								</button>
							</div>
						</td>
					</tr>
				</tbody>
				<script type="text/javascript">
				enable_tooltips();
				</script>	
			<?php
			}

			?>
			</table>
			<?php
		}
		else
		{
			echo '<div class="alert glass">No Records Found...</div>';
		}	
	}
}

	
function update_user($con,$user,$uid){

	$sql = mysqli_query($con,"UPDATE tbl_user_profile set user_type='$user' where user_id='$uid'");
	if ($sql) {

		$query3=mysqli_query($con, "SELECT fn,email_add,password FROM tbl_user_profile where user_id='$uid'");
		$row3 = mysqli_fetch_assoc($query3);

		$userlevel = '';

		if($user == 2){
			$userlevel = 'Contributor';
		}else if($user == 3){
			$userlevel = 'Guest';
		}else{
			$userlevel = 'Inactive';
		}
		//echo $userlevel;
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row3['email_add']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Hi <'.$row3['fn'].'>.';
	    $mail->Body    = '<p>
						Your Lampstand Studio user level has been changed to:<br>
						'.$userlevel.'
	    				</p>

                            Kind Regards,<br>
                            
                            The Lampstand Studio Team';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
		echo 1;
	}
}


function pict_session($con,$myid){
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$myid' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['admin'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
		$pict = '../avatar/def.png';
					
		}
		else
		{
			$_SESSION['user_pic'] = '../'.$row['picture'];
		}

		echo $_SESSION['user_pic'];
}



function update_pf($con,$path,$myid){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set picture='$path' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

//oji classes

//Accept Song
function accept_song($con,$id){
	$date_today = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"UPDATE tbl_audios set active= 1, approved_date = '$date_today' where audio_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//Accept Song
function disapprove_song($con,$id){
	$date_today = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"UPDATE tbl_audios set active= 2 where audio_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//Post ADmin Song
function post_music($con,$id){
	$date_today = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"UPDATE tbl_audios set active= 1, approved_date = '$date_today' where audio_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//Unpost ADmin Song
function unpost_music($con,$id){
	$date_today = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"UPDATE tbl_audios set active= 0, approved_date = '$date_today' where audio_id='$id'");
	if ($sql) {
		echo 1;
	}
}
//Delete Song
function delete_music($con,$id,$music){
	$sql = mysqli_query($con, "DELETE from tbl_audios where audio_id='$id' and music='$music'");
	if ($sql) {
		echo 1;
	}
}

//show download details
function download_details($con,$id){

	$sql = "SELECT t1.*, t2.* FROM tbl_downloads t1 LEFT JOIN tbl_user_profile t2 ON t1.`user_id` = t2.`user_id` 
	WHERE t1.`audio_id` = '$id' ORDER by t1.`date_` DESC";

	$result = mysqli_query($con, $sql);
	$counter = 0;
	if(mysqli_num_rows($result) > 0)
	{
	     while($row = mysqli_fetch_array($result))
	     {
	     	$counter++;
	     	$datetime = $row['date_']
	    ?>
	    	<tr class="w3-hover-shadow">
				<td><?php echo $counter; ?>.</td>
				<td><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></td>
				<td>
					<em><?php echo strtoupper($row['geo_location']); ?></em>
				</td>
				<td class="text-center">
					<span class="float-right w3-large"> <?php echo date('M d, Y', strtotime($row['date_'])); ?> &nbsp; <span class="badge w3-large w3-card-4 w3-green w3-medium pull-right ml3"> <?php echo time_ago($datetime); ?></span></span>
				</td>
			</tr>
	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-default"><span class="fa fa-download fa-5x"></span> <br> No Data Found!</strong>
		</div>
		</td>
	<?php  
	}

}

function download_music($con,$search3, $filter, $top)
{
	if ($search3 == "" || $search3 == null) {

	$counter = 0;

	$result = mysqli_query($con, "SELECT count(audio_id) as down FROM tbl_downloads");
			$row = mysqli_fetch_assoc($result);
			$count = $row['down'];

	$sql = mysqli_query($con, "SELECT t2.*, COUNT(t1.`audio_id`) AS downloads, t1.`audio_id` as aud_id, t4.`fn`, t4.`ln`,t4.`picture`, t1.`geo_location` FROM tbl_downloads t1 
		LEFT JOIN tbl_audios t2 ON t1.`audio_id` = t2.`audio_id`
		LEFT JOIN tbl_album t3 ON t2.`album_id` = t3.`album_id` 
		LEFT JOIN tbl_user_profile t4 ON t3.`user_id` = t4.`user_id` 
		GROUP BY t1.`audio_id` ORDER BY downloads DESC LIMIT $top");

			if($count > 0){

				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;

					if ($row['picture'] == "") {
						$img = '../img/contributor.png';				
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					$tooltip ="<p><img src='".$img."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";


  						if($filter == 0){ 
  						//Popular Songs ?>

  						<tr class="tracks" onclick="v_details('<?php echo $row['aud_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['downloads'];?>')">
  							<td><?php echo $counter; ?>.</td>
  							<td><?php echo ucfirst( $row['song_name']) ?></td>
  							<td class="text-center">
  								<span class="badge w3-card-4 w3-red w3-medium ml3"> <i class="fa fa-download"></i> <?php echo number_format($row['downloads']); ?>
								</span>
							</td>
  						</tr>

  					<?php
  						}else if($filter == 1){ 
  						//Contributor ?>

  						<tr class="tracks" onclick="v_details('<?php echo $row['aud_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['downloads'];?>')">
  							<td><?php echo $counter; ?>.</td>
  							<td><?php echo ucfirst( $row['song_name']) ?></td>
  							<td>
  								<img data-toggle="popover" data-html="true" data-title="Contributor" data-trigger="hover" data-content="<?php echo $tooltip ?>" src="<?php echo $img; ?>" class="img-album img_music"  > &nbsp;<b><?php echo $row['fn'].' '.$row['ln']; ?></b>



  							</td>
  							<td class="text-center"><span class="badge w3-card-4 w3-red w3-medium ml3"> <i class="fa fa-download"></i> <?php echo number_format($row['downloads']); ?>
							</span></td>
  						</tr>
  						<script type="text/javascript">
						enable_tooltips();	
						</script>
  					<?php
  						}
			
			 }

			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}
			
	}
	else
	{

		$counter = 0;

	$result = mysqli_query($con, "SELECT count(audio_id) as down FROM tbl_downloads");
			$row = mysqli_fetch_assoc($result);
			$count = $row['down'];

	$sql = mysqli_query($con, "SELECT t2.*, COUNT(t1.`audio_id`) AS downloads, t1.`audio_id` as aud_id, t4.`fn`, t4.`ln`,t4.`picture`, t1.`geo_location` FROM tbl_downloads t1 
		LEFT JOIN tbl_audios t2 ON t1.`audio_id` = t2.`audio_id`
		LEFT JOIN tbl_album t3 ON t2.`album_id` = t3.`album_id` 
		LEFT JOIN tbl_user_profile t4 ON t3.`user_id` = t4.`user_id`
		where concat(t2.`song_name`, geo_location, fn,ln) like '%$search3%' GROUP BY t1.`audio_id` ORDER BY downloads DESC");

			if($count > 0){

				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;

					if ($row['picture'] == "") {
						$img = '../img/contributor.png';				
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";


  						if($filter == 0){ 
  						//Popular Songs ?>

  						<tr onclick="v_details('<?php echo $row['aud_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['downloads'];?>')">
  							<td><?php echo $counter; ?>.</td>
  							<td><?php echo ucfirst( $row['song_name']) ?></td>
  							<td class="text-center">
  								<span class="badge w3-card-4 w3-red w3-medium ml3"> <i class="fa fa-download"></i> <?php echo number_format($row['downloads']); ?>
								</span>
							</td>
  						</tr>

  					<?php
  						}else if($filter == 1){ 
  						//Contributor ?>

  						<tr onclick="v_details('<?php echo $row['aud_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['downloads'];?>')">
  							<td><?php echo $counter; ?>.</td>
  							<td><?php echo ucfirst( $row['song_name']) ?></td>
  							<td><img data-toggle="popover" data-html="true" data-title="Contributor" data-trigger="hover" data-content="<?php echo $tooltip ?>" src="../img/img.png" class="img-album w3-round"  width="40" style="background-image: url(<?php echo $img ?>);"> &nbsp;<b><?php echo $row['fn'].' '.$row['ln']; ?></b></td>
  							<td class="text-center"><span class="badge w3-card-4 w3-red w3-medium ml3"> <i class="fa fa-download"></i> <?php echo number_format($row['downloads']); ?>
							</span></td>
  						</tr>
  						<script type="text/javascript">
						enable_tooltips();	
						</script>

  					<?php
  						}
			
			 }


			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}
		
	}
	
}

function new_uploads($con,$search4)
{
	if ($search4 == "" || $search4 == null) {
	$counter = 0;
	$date_ = date('Y-m-d');
	$sql = mysqli_query($con, "SELECT t1.*, t3.`fn`,t3.`ln`,t4.`genre_name` From tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.`album_id`=t2.`album_id`
		LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
		LEFT JOIN tbl_genre t4 on t1.`genre` = t4.`gen_id`
		where active = 0 and t1.upload_date <= '$date_' order by upload_date DESC");

			if(mysqli_num_rows($sql) > 0){ ?>

				<script>

				$(document).ready(function(){
					$("#chk_uploads").change(function(){
					    if(this.checked){
					      $(".select_uploads").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_uploads").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});

			</script>

				<div class="table-responsive">
			    <table class="table table-hover table-light table-responsive-sm ">
			    	<thead class="thead-dark">
						<tr id="labels">
							<td class="text-left">
								<div class="form-check-inline">
							      <label class="form-check-label" for="chk_uploads">
							        <input type="checkbox" name="chk_uploads" id="chk_uploads" onclick="check_all_validate();"> Check All
							      </label>
							    </div>
							</td>
							<td class="text-left">TITLE</td>
							<td class="text-left">CONTRIBUTOR</td>
							<td class="text-left">GENRE</td>
							<td class="text-left">Action</td>
						</tr>
					</thead>
			    	<tbody>
			    	

				<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;
					$datetime = $row['upload_date']
			 		?>
					<tr class="w3-hover-shadow tracks" ondblclick="upload_data('<?php echo $row['audio_id']?>')">
							<td class="text-left">
								<p class="default-fs" style="margin-left: 13%;"><input type="checkbox" onclick="count_check();" name="id[]" class="select_uploads" value="<?php echo $row["id"]; ?>" />
							</td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 6%;"><?php echo ucfirst( $row['song_name']) ?></p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 6%;"><?php echo ucfirst( $row['fn'].' '.$row['ln']) ?></p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 5%;"><?php echo $row['genre']; ?></p></td>
  							<td class="text-left">
  								<p class="default-fs" style="margin-left: 5%;" onclick="upload_data('<?php echo $row['audio_id']?>')"><i class="fa fa-eye fa-lg" ></i> View</p>
							</td>
  						</tr>
					<?php
			 } ?>

			 </tbody>
			    </table>
			</div>
			<?php
			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
				</div>

			<?php
			}	
	}
	else
	{
		$counter = 0;
		$date_ = date('Y-m-d');
		$sql = mysqli_query($con, "SELECT t1.*, t3.`fn`,t3.`ln`,t4.`genre_name` From tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.`album_id`=t2.`album_id`
		LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
		LEFT JOIN tbl_genre t4 on t1.`genre` = t4.`gen_id`
		where active = 0 and t1.upload_date <= '$date_' and concat(song_name,fn,ln,genre,description,application_id,keyword) like '%$search4%' and active = 0 order by upload_date DESC");

			if(mysqli_num_rows($sql) > 0){?>

				<script>

				$(document).ready(function(){
					$("#chk_uploads").change(function(){
					    if(this.checked){
					      $(".select_uploads").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_uploads").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});

			</script>

				<div class="table-responsive">
			    <table class="table table-hover table-light table-responsive-sm ">
			    	<thead class="thead-dark">
						<tr id="labels">
							<td class="text-left">
								<div class="form-check-inline">
							      <label class="form-check-label" for="chk_uploads">
							        <input type="checkbox" name="chk_uploads" id="chk_uploads" onclick="check_all_validate();"> Check All
							      </label>
							    </div>
							</td>
							<td class="text-left">TITLE</td>
							<td class="text-left">CONTRIBUTOR</td>
							<td class="text-left">GENRE</td>
							<td class="text-left">Action</td>
						</tr>
					</thead>
			    	<tbody>
			    	

				<?php

				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;

			 		?>
					<tr class="w3-hover-shadow tracks" ondblclick="upload_data('<?php echo $row['audio_id']?>')">
							<td class="text-left">
								<p class="default-fs" style="margin-left: 13%;"><input type="checkbox" onclick="count_check();" name="id[]" class="select_uploads" value="<?php echo $row["id"]; ?>" />
							</td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 6%;"><?php echo ucfirst( $row['song_name']) ?></p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 6%;"><?php echo ucfirst( $row['fn'].' '.$row['ln']) ?></p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 5%;"><?php echo $row['genre']; ?></p></td>
  							<td class="text-left">
  								<p class="default-fs" style="margin-left: 5%;" onclick="upload_data('<?php echo $row['audio_id']?>')"><i class="fa fa-eye fa-lg" ></i> View</p>
							</td>
  						</tr>
					<?php
			}
			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
				</div>

			<?php
			}
	}	
}

//show upload details
function upload_details($con,$id){

	$sql = "SELECT t1.*,t2.*,t3.* FROM tbl_audios t1 
	 LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	 LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	 where t1.`audio_id`='$id'";

	$result = mysqli_query($con, $sql);
	$counter = 0;
	if(mysqli_num_rows($result) > 0)
	{
	     while($row = mysqli_fetch_array($result))
	     {
	     	$counter++;
	     	$datetime = $row['upload_date']
	    ?>
				<div class="bg-dark text-center w3-slim text-capitalize text-white w3-padding rounded-top" style="opacity: 0.9;">
							<?php 
					if (strlen($row['song_name'])>13) {
						?>
						<h4 class="w3-padding glass w3-wide ml3 text-white" ><b><marquee><?php echo ucfirst( $row['song_name']) ?></marquee></b></h4>
					<?php
					}
					else
					{
					?>
						<h4 class="w3-padding glass  w3-wide ml3 text-white" ><b><?php echo ucfirst( $row['song_name']) ?></b></h4>
					<?php	
					}
					?>
				<div id="myAudio" class="col-sm-12 text-center">
              
          		</div>
				</div>
				<div class="text-center bg-dark rounded-bottom " style="padding-top:10px; padding-bottom: 15px;">
					<h3 class="w3-padding glass  w3-medium ml3 text-white" >
						<div class="table-responsive text-center">
							<table class="table table-hover">
							<tr>
								<td>Contributor:</td>
								<td><b><?php echo $row['fn'].' '.$row['ln']; ?></b></td>
							</tr>
							<tr>
								<td>Album:</td>
								<td><b><?php echo ucfirst( $row['album_name']) ?></b></td>
							</tr>
							<tr>
								<td>Composer:</td>
								<td><b><?php echo ucfirst( $row['composer']) ?></b></td>
							</tr>
							<tr>
								<td>Genre:</td>
								<td><b><?php echo ucfirst( $row['genre']) ?></b></td>
							</tr>
							<tr>
								<td>Application:</td>
								<td><b><?php echo ucfirst( $row['application_id']) ?></b></td>
							</tr>
							<tr>
								<td>Description:</td>
								<td><b><?php echo ucfirst( $row['description']) ?></b></td>
							</tr>
							<tr>
								<td>Date Uploaded:</td>
								<td><b><?php echo date('M d Y', strtotime($row['upload_date'])); ?></b>&nbsp; <span class="badge w3-small w3-card-4 w3-green w3-small ml3"> <?php echo time_ago($datetime); ?></span>
								</td>
							</tr>

							</table>
						</div>

					</h3>
					<div class="text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-dark btn-small" onclick="playAudio('../<?php echo $row['music'] ?>');"><span class="fa fa-play"></span> Play</button>
							<button type="button" class="btn btn-dark btn-small" onclick="accept_song('<?php echo $row['audio_id']?>')"><span class="fa fa-thumbs-up"></span> Approve
							</button>
							<button type="button" class="btn btn-dark btn-small" onclick="disapprove_song('<?php echo $row['audio_id']?>')"><span class="fa fa-thumbs-down"></span> Disapprove</button>
						</div>
					</div>
				</div>
	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-default"><span class="fa fa-upload fa-5x"></span> <br> No Data Found!</strong>
		</div>
		</td>
	<?php  
	}
 
}

//load_album_details
function load_album_details($con,$id){

	$sql = "SELECT t1.*,t2.*,t3.* FROM tbl_audios t1 
	 LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	 LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	 where t2.`album_id`='$id' ORDER BY active DESC";

	$result = mysqli_query($con, $sql);
	$counter = 0;
	if(mysqli_num_rows($result) > 0)
	{
	     while($row = mysqli_fetch_array($result))
	     {
	     	$counter++;
	    ?>
	    	<li class="list-group-item glass w3-hover-shadow">

				<span class="w3-large"><?php echo $counter; ?>.&nbsp;<?php echo $row['song_name']; ?>
				</span> 
				<span class="float-right w3-large">

					<?php 
						if($row['active'] == 1 ){ ?>
							<span class="badge w3-large w3-card-4 w3-green w3-medium pull-right ml3"> 
								Approved
							</span>
					<?php }
						elseif($row['active'] == 0 ){ ?>
							<span class="badge w3-large w3-card-4 w3-yellow w3-medium pull-right ml3"> 
								Pending
							</span>

					<?php	}else{ ?>

							<span class="badge w3-large w3-card-4 w3-red w3-medium pull-right ml3"> 
								 Disapproved
							</span>
					<?php
						}
					?>
					
				</span>

			</li>

	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No music found!</strong>
		</div>
		</td>
	<?php  
	}

}

function load_albums($con,$search5){

	if ($search5 == "" || $search5 == null) {

		$sql = mysqli_query($con,"SELECT a.*,b.*,c.*,count(c.id), a.`album_id` as ab from tbl_album a left join tbl_user_profile b on a.user_id=b.user_id left join tbl_audios c on a.album_id=c.album_id group by a.album_id order by a.`date_released` DESC");

			if(mysqli_num_rows($sql) > 0){ 
				?>
				<script type="text/javascript">
					$("[data-toggle='tooltip']").tooltip();
					$("[data-toggle='popover']").popover();
				</script>
				<table class="table table-light table-hover table-responsive-sm">
					<thead class="thead-dark">
						<tr id="labels">
							<td>Image</td>
							<td>Album Name</td>
							<td>Posted By</td>
							<td>Action</td>
							<td></td>
						</tr>
					</thead>
				<?php
				 while ($row = mysqli_fetch_assoc($sql)) {
				 	$userid = $row['user_id'];
			 		$myid = $_SESSION['admin'];	
			 		$pict ='';
			 		if ($row['album_artwork'] == "") {
			 			$pict = "../img/slogo3.png";
			 		}
			 		else
			 		{
			 			$pict = '../'.$row['album_artwork'];
			 		}

			 		$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
					$toll_title="<center>".user_account_type($row['user_type'])."</center>";
	 			?>
	 			<tbody>
	 				<tr class="tracks" ondblclick="album_details('<?php echo $row['ab'] ?>','<?php echo $row['album_name']; ?>','<?php echo $row['count(c.id)'] ?>');">
	 					<td class="">
	 						<img class="img_music img-responsive" src="<?php echo $pict; ?>" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>">

	 					</td>
	 					<td class="w3-medium pt-3"><?php echo $row['album_name'] ?></td>
	 					<td class="w3-medium text-capitalize pt-3"><?php echo $row['fn'].' '.$row['ln'] ?></td>
	 					<td class="text-center">
	 						<div class="dropdown">
	 						<button class="btn btn-dark btn-normal w3-round dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	 						<?php 

								if($myid == $userid){ ?>
									<ul class="dropdown-menu animated text-center"> 
			  							<li class="dropdown-item w3-hover-green" onclick="edit_album('<?php echo $row['ab'] ?>','<?php echo $row['fn'].' '.$row['ln']; ?>', '<?php echo $row['album_name'] ?>');"><span class="fa fa-edit"></span> Edit	 
			  							</li> 
			  							<li class="dropdown-item w3-hover-red" onclick="delete_album('<?php echo $row['ab'] ?>');"> <span class="fa fa-trash"></span> Delete</li> 
			             			 </ul>
							<?php
								}else{ ?>
								<ul class="dropdown-menu animated  text-center"> 
									<li class="dropdown-item w3-hover-red" onclick="album_details('<?php echo $row['ab'] ?>','<?php echo $row['album_name']; ?>','<?php echo $row['count(c.id)'] ?>');"><span class="fa fa-eye"></span> View</li> 
								 </ul>
							<?php
								}

							?>	
		 					</div>
		 				</td>
		 				<td></td>
	 				</tr>
	 			</tbody>
	 			<?php
			 }
			 ?>
				 </table>
			 <?php
			 	}
			 	else
			 		{ 
			 		?>

			 		<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-image fa-5x"></span> <br> No Data Found!</strong>
				</div>
			<?php
			 	}
			 
	}else{

		$sql = mysqli_query($con,"SELECT a.*,b.*,c.*,count(c.id), a.`album_id` as ab from tbl_album a left join tbl_user_profile b on a.user_id=b.user_id left join tbl_audios c on a.album_id=c.album_id where concat(album_name,fn,ln,mn) like '%$search5%' group by a.album_id order by a.`date_released` DESC");
		if(mysqli_num_rows($sql) > 0){ 
				?>
				<script type="text/javascript">
					$("[data-toggle='tooltip']").tooltip();
					$("[data-toggle='popover']").popover();
				</script>
				<table class="table table-light table-hover table-sm">
					<thead class="thead-dark">
						<tr id="labels">
							<td>Image</td>
							<td>Album Name</td>
							<td>Posted By</td>
							<td>Action</td>
							<td></td>
						</tr>
					</thead>
				<?php
				 while ($row = mysqli_fetch_assoc($sql)) {
				 	$userid = $row['user_id'];
			 		$myid = $_SESSION['admin'];	
			 		$pict ='';
			 		if ($row['album_artwork'] == "") {
			 			$pict = "../img/slogo3.png";
			 		}
			 		else
			 		{
			 			$pict = '../'.$row['album_artwork'];
			 		}

			 		$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
					$toll_title="<center>".user_account_type($row['user_type'])."</center>";
	 			?>
	 			<tbody>
	 				<tr class="tracks" ondblclick="album_details('<?php echo $row['ab'] ?>','<?php echo $row['album_name']; ?>','<?php echo $row['count(c.id)'] ?>');">
	 					<td class="">
	 						<img class="img_music img-responsive" src="<?php echo $pict; ?>" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>">

	 					</td>
	 					<td class="w3-medium pt-3"><?php echo $row['album_name'] ?></td>
	 					<td class="w3-medium text-capitalize pt-3"><?php echo $row['fn'].' '.$row['ln'] ?></td>
	 					<td class="text-center">
	 						<div class="dropdown">
	 						<button class="btn btn-dark btn-normal w3-round dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	 						<?php 

								if($myid == $userid){ ?>
									<ul class="dropdown-menu animated text-center"> 
			  							<li class="dropdown-item w3-hover-green" onclick="edit_album('<?php echo $row['ab'] ?>','<?php echo $row['fn'].' '.$row['ln']; ?>', '<?php echo $row['album_name'] ?>');"><span class="fa fa-edit"></span> Edit	 
			  							</li> 
			  							<li class="dropdown-item w3-hover-red" onclick="delete_album('<?php echo $row['ab'] ?>');"> <span class="fa fa-trash"></span> Delete</li> 
			             			 </ul>
							<?php
								}else{ ?>
								<ul class="dropdown-menu animated  text-center"> 
									<li class="dropdown-item w3-hover-red" onclick="album_details('<?php echo $row['ab'] ?>','<?php echo $row['album_name']; ?>','<?php echo $row['count(c.id)'] ?>');"><span class="fa fa-eye"></span> View</li> 
								 </ul>
							<?php
								}

							?>	
		 					</div>
		 				</td>
		 				<td></td>
	 				</tr>
	 			</tbody>
	 			<?php
			 }
			 ?>
				 </table>
			 <?php
			 	}
			
		 	else{ ?>

		 		<div class="alert alert-default container text-center" >
			  <strong class="text-danger"><span class="fa fa-image fa-5x"></span> <br> No album found!</strong>
			</div>
				<?php
		 	}
		
		 }

}

function update_music($con,$song_name,$composer,$genre,$albumname,$audiencename,$applicationname,$id){
	$sql = mysqli_query($con,"UPDATE tbl_audios set song_name='$song_name',composer='$composer',genre='$genre',album_id='$albumname',audience_id='$audiencename',application_id='$applicationname'  where id='$id'");
	if ($sql) {
		echo 1;
	}
	
}
	
function load_mymusic($con,$search_music2, $status){
	if ($search_music2 == "" || $search_music2 == null) {

		$myid = $_SESSION['admin'];

		if($status == 1){

			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' and b.active = 1 order by song_name asc LIMIT 0,100");

		}else if($status == 2){

			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' and b.active = 0 order by song_name asc LIMIT 0,100");

		}
		else if($status == 0){
			
			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' order by song_name asc LIMIT 0,100");
		}
			if(mysqli_num_rows($sql) > 0){ 
				?>
				<table class="table table-hover table-light table-responsive-sm">
					<thead class="thead-dark">
						<tr id="labels">
							<td>Image</td>
							<td>Title</td>
							<td>Duration</td>
							<td>Status</td>
							<td>Action</td>
						</tr>					
					</thead>
				<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$status = $row['active'];

					$pict ='';
			 		if ($row['album_artwork'] == "") {
			 			$pict = "../img/slogo3.png";
			 		}
			 		else
			 		{
			 			$pict = '../'.$row['album_artwork'];
			 		}

					$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
					$toll_title="<center>".$row['song_name']."</center>";

							?>
					<tbody>
						<tr ondblclick="choose_play('../<?php echo $row['album_artwork'] ?>','../<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>');">
							<td class="w3-medium text-capitalize pt-3"> 

								<img class="img_music img-responsive" src="<?php echo $pict; ?>" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>">

								</td>
							<td class="w3-medium text-capitalize pt-3"><?php echo $row['song_name'] ?></td>
							<td class="w3-medium text-center text-capitalize pt-3"><?php echo $row['duration'] ?></td>
							<td class="w3-medium text-capitalize pt-3">
								<?php 
										if($status == 1){ ?>
											<span class="badge badge-pill badge-info">Posted</span>
									<?php
										}else if($status == 0){ ?>
											<span class="badge badge-pill badge-warning">Unposted</span>
									<?php
										}else{ ?>
											<span class="text-danger pull-right"></span>
									<?php
										}		
									?>
							</td>
							<td class="w3-medium text-capitalize pt-3">
								<div class="dropdown pull-right" style="margin-bottom: 0px;">
						<button type="button" class="btn btn-dark btn-small w3-round-xxlarge w3-border w3-border-black dropdown-toggle" data-toggle="dropdown">
						  <i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu">

							<?php 
								if($status == 1){ ?>

								<a class="dropdown-item text-info" href="#">
							   		<i class="fa fa-check-circle"></i> Posted
							   </a>

							<?php } elseif($status == 0) { ?>

								<a class="dropdown-item text-info" href="#" onclick="post_music('<?php echo $row['audio_id']; ?>')">
							   		<i class="fa fa-check-circle"></i> Post
							   </a>

							<?php }?>

							<?php 
								if($status == 0){ ?>

								<a class="dropdown-item text-warning" href="#">
							   		<i class="fa fa-times"></i> Unposted
							   </a>

							<?php } elseif($status == 1) { ?>

								<a class="dropdown-item text-warning" href="#" onclick="unpost_music('<?php echo $row['audio_id']; ?>')">
							   		<i class="fa fa-times"></i> Unpost
							   </a>

							<?php }?>

						   <a class="dropdown-item text-default" href="#" onclick="edit_audio('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['album_id'] ?>','<?php echo $row['file_id'] ?>')"><span class="fa fa-edit"></span> Edit</a> 

						   <a class="dropdown-item text-danger" href="#" onclick="delete_music('<?php echo $row['audio_id']; ?>','<?php echo $row['music']; ?>')">
						   	<i class="fa fa-trash"></i> Delete
						   </a>

						</div>
					</div>
					
							</td>
						</tr>
					</tbody>

				<?php
				}
				?>
				</table>
				<script type="text/javascript">
					enable_tooltips();
				</script>
				<?php
				}

			else
				{ ?>
				

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}

	}else{

		$myid = $_SESSION['admin'];

		if($status == 1){

			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' and b.active = 1 and concat(song_name,composer,artist) LIKE '%$search_music2%' order by song_name asc LIMIT 0,100");

		}else if($status == 2){

			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' and b.active = 0 and concat(song_name,composer,artist) LIKE '%$search_music2%' order by song_name asc LIMIT 0,100");

		}
		else if($status == 0){
			
			$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id where a.user_id = '$myid' and concat(song_name,composer,artist) LIKE '%$search_music2%' order by song_name asc LIMIT 0,100");
		}
		
			if(mysqli_num_rows($sql) > 0){ 
				?>
				<table class="table table-hover table-light table-sm">
					<thead class="thead-dark">
						<tr>
							<th class="w3-small">IMAGE</th>
							<th class="w3-small">TITLE</th>
							<th class="w3-small">STATUS</th>
							<th class="text-center w3-small">ACTION</th>
						</tr>
					</thead>
				<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$status = $row['active'];

					$pict ='';
			 		if ($row['album_artwork'] == "") {
			 			$pict = "../img/slogo3.png";
			 		}
			 		else
			 		{
			 			$pict = '../'.$row['album_artwork'];
			 		}

					$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
					$toll_title="<center>".$row['song_name']."</center>";
							?>
					<tbody>
						<tr ondblclick="choose_play('../<?php echo $row['album_artwork'] ?>','../<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>');">
							<td class="w3-medium text-capitalize pt-3"> 

								<img class="img_music img-responsive" src="<?php echo $pict; ?>" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>">

								</td>
							<td class="w3-medium text-capitalize pt-3"><?php echo $row['song_name'] ?></td>
							<td class="w3-medium text-center text-capitalize pt-3"><?php echo $row['duration'] ?></td>
							<td class="w3-medium text-capitalize pt-3">
								<?php 
										if($status == 1){ ?>
											<span class="badge badge-pill badge-info">Posted</span>
									<?php
										}else if($status == 0){ ?>
											<span class="badge badge-pill badge-warning">Unposted</span>
									<?php
										}else{ ?>
											<span class="text-danger pull-right"></span>
									<?php
										}		
									?>
							</td>
							<td class="w3-medium text-capitalize pt-3">
								<div class="dropdown pull-right" style="margin-bottom: 0px;">
						<button type="button" class="btn btn-dark btn-small w3-round-xxlarge w3-border w3-border-black dropdown-toggle" data-toggle="dropdown">
						  <i class="fa fa-cog"></i>
						</button>
						<div class="dropdown-menu">

							<?php 
								if($status == 1){ ?>

								<a class="dropdown-item text-info" href="#">
							   		<i class="fa fa-check-circle"></i> Posted
							   </a>

							<?php } elseif($status == 0) { ?>

								<a class="dropdown-item text-info" href="#" onclick="post_music('<?php echo $row['audio_id']; ?>')">
							   		<i class="fa fa-check-circle"></i> Post
							   </a>

							<?php }?>

							<?php 
								if($status == 0){ ?>

								<a class="dropdown-item text-warning" href="#">
							   		<i class="fa fa-times"></i> Unposted
							   </a>

							<?php } elseif($status == 1) { ?>

								<a class="dropdown-item text-warning" href="#" onclick="unpost_music('<?php echo $row['audio_id']; ?>')">
							   		<i class="fa fa-times"></i> Unpost
							   </a>

							<?php }?>

						   <a class="dropdown-item text-default" href="#" onclick="edit_audio('<?php echo $row['id'] ?>','<?php echo $row['genre'] ?>','<?php echo $row['song_name'] ?>','<?php echo $row['composer'] ?>','<?php echo $row['description'] ?>','<?php echo $row['keyword'] ?>','<?php echo $row['album_name'] ?>','<?php echo $row['application_id'] ?>','<?php echo $row['music'] ?>','<?php echo $row['duration'] ?>','<?php echo $row['album_id'] ?>','<?php echo $row['file_id'] ?>')"><span class="fa fa-edit"></span> Edit</a> 

						   <a class="dropdown-item text-danger" href="#" onclick="delete_music('<?php echo $row['audio_id']; ?>','<?php echo $row['music']; ?>')">
						   	<i class="fa fa-trash"></i> Delete
						   </a>

						</div>
					</div>
					
							</td>
						</tr>
					</tbody>

				<?php
				}
				?>
				</table>
				<?php
				}
				
				else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}
	}
}


function edit_client_pwd($con,$user_id,$pwd){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set password='$pwd' where user_id='$user_id'");
	if ($sql) {
		echo 1;
	}
}


function update_client_informaion($con,$fn,$ln,$email,$user_id){
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set fn='$fn',ln='$ln',email_add='$email' where user_id='$user_id'");
	if ($sql) {
		echo 1;
	}
}


function subscribers($con,$search){
if ($search == "" || $search == null) {
		$sql = mysqli_query($con, "SELECT * FROM tbl_user_profile where user_type = 3 order by fn ASC");
		$counter=0;
		if (mysqli_num_rows($sql)>0) {
			
			?>

			<script>
				$(document).ready(function(){
					$("#chk_subs").change(function(){
					    if(this.checked){
					      $(".select_subs").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_subs").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});
			</script>

			<table class="table table-hover table-light table-responsive-sm " id="subs">
			<thead class="thead-dark">
				<tr id="labels">
					<th>
						<div class="form-check-inline">
					      <label class="form-check-label" for="chk_subs">
					        <input type="checkbox" name="chk_subs" id="chk_subs" onclick="check_all_subs();"> Check All
					      </label>
					    </div>
					</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Name</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(2)')" style="cursor:pointer">Email</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(3)')" style="cursor:pointer">Status</th>
				</tr>	
			</thead>
			<?php
			$num_id = 1;

			while ($row = mysqli_fetch_assoc($sql)) {
				$counter ++;
				?>
				<tbody>
					<tr id="tracks" class="item">
						<td class="text-left"><p class="default-fs" style="margin-left: 8.5%;"><input type="checkbox" onclick="count_check_subs();" name="id[]" class="select_subs" value="<?php echo $row["id"]; ?>" /></p></td>
						<td class="text-left"><p class="db-content"><?php echo $row['fn']. ' ' .$row['ln'] ?></p></td>
						<td class="text-left"><p class="db-content"><?php echo $row['email_add']; ?></p></td>
						<td class="text-left db-content">
							<p>
							<?php 
								if($row['is_notify'] == 1){
							?>
								<b><label class="text-info"><span class="fa fa-thumbs-up"></span> Subscribed</label></b>
							<?php
								}else{
							?>
								<b><label class="text-danger"><span class="fa fa-thumbs-down"></span> Not Subscribe</label></b>
							<?php
								}
							?>
							</p>
						</td>

					</tr>
				</tbody>
				<script type="text/javascript">
				enable_tooltips();	
				</script>	
			<?php
			}
			?>
			</table>
			<?php
		}
		else
		{
			echo '<tr>
			<td colspan="4">No Subscriber Found!</tr>
			</tr>';
		}
	}
	else
	{

		$sql = mysqli_query($con, "SELECT * FROM tbl_user_profile where user_type = 3  and concat(email_add,fn,ln) like '%$search%' ORDER BY fn asc");
		if (mysqli_num_rows($sql)>0) {
			$counter=0;
			?>
			<script>
				$(document).ready(function(){
					$("#chk_subs").change(function(){
					    if(this.checked){
					      $(".select_subs").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_subs").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});
			</script>

			<table class="table table-hover table-light table-responsive-sm " id="subs">
			<thead class="thead-dark">
				<tr id="labels">
					<th>
						<div class="form-check-inline">
					      <label class="form-check-label" for="chk_subs">
					        <input type="checkbox" name="chk_subs" id="chk_subs" onclick="check_all_subs();"> Check All
					      </label>
					    </div>
					</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Name</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(2)')" style="cursor:pointer">Email</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(3)')" style="cursor:pointer">Status</th>
				</tr>	
			</thead>
			<?php
			while ($row = mysqli_fetch_assoc($sql)) {
				$counter++;
				?>
				<tbody>
					<tr id="tracks" class="item">
						<td class="text-left"><p class="default-fs" style="margin-left: 10.5%;"><input type="checkbox" onclick="count_check_subs();" name="id[]" class="select_subs" value="<?php echo $row["id"]; ?>" /></p></td>
						<td class="text-left"><p class="db-content"><?php echo $row['fn']. ' ' .$row['ln'] ?></p></td>
						<td class="text-left"><p class="db-content"><?php echo $row['email_add']; ?></p></td>
						<td class="text-left db-content">
							<p>
							<?php 
								if($row['is_notify'] == 1){
							?>
								<b><label class="text-info"><span class="fa fa-thumbs-up"></span> Subsribed</label></b>
							<?php
								}else{
							?>
								<b><label class="text-danger"><span class="fa fa-thumbs-down"></span> Not Subscribe</label></b>
							<?php
								}
							?>
							</p>
						</td>

					</tr>
				</tbody>
				<script type="text/javascript">
				enable_tooltips();	
				</script>	
			<?php
			}
			?>
			</table>
			<?php
		}
		else
		{
			echo '<tr>
			<td colspan="4">No Subscriber Found!</tr>
			</tr>';
		}	
	}
}

//load_album_details
function load_subs_info($con,$id){

	$sql = "SELECT t1.*,t2.* FROM tbl_user_profile t1 
	LEFT JOIN tbl_subscribers t2 on t1.`user_id` = t2.`contri_id` 
	where t2.`user_id` = '$id'";

	$result = mysqli_query($con, $sql);
	if(mysqli_num_rows($result) > 0)
	{ ?>

		<table class="table table-hover table-light table-responsive-sm ">
					<thead>
						<tr class="w3-small bg-dark text-white">
							<th>Image</th>
							<th>Name</th>
							<th>Date</th>
						</tr>
					</thead>
					<?php 
	     while($row = mysqli_fetch_array($result))
	     {
	     	if ($row['picture'] == "") {
				$pict = '../avatar/def.png';
				}
			else
				{
					$pict = '../'.$row['picture'];
				}

			$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$pict.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	    ?>
			<tbody>
				<tr class="w3-small w3-hover-shadow"  data-toggle="popover" data-html="true" data-title="Contributor" data-trigger="hover" data-content="<?php echo $tooltip ?>">
					<td>
						<img src="../img/img.png" class="img-album w3-round"  width="40" style="background-image: url(<?php echo $pict ?>);">
				    </td>
				    <td>
				    	<?php echo $row['fn'].' '.$row['ln'];  ?>
				    </td>
				    <td>
				    	<?php echo date('M d, Y H:i A', strtotime($row['date_subs'])); ?>
				    </td>
				</tr>
			</tbody>
	    <?php  
	     }
	     ?>
	     </table>
	     <script type="text/javascript">
				enable_tooltips();	
				</script>
	<?php }
	else
	{
	?>
		<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No music found!</strong>
		</div>
		</td>
	<?php  
	}

}


function time_ago($timestamp)  
 {  
      $time_ago = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $current_time - $time_ago;  
      $seconds = $time_difference;  
      $minutes = round($seconds / 60 );           // value 60 is seconds  
      $hours   = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800);          // 7*24*60*60;  
      $months  = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
      if($seconds <= 60)  
      {  
     return "Just Now";  
      }  
      else if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return "1 minute ago";  
     }  
     else  
           {  
       return "$minutes minutes ago";  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return "1 hour ago";  
     }  
           else  
           {  
       return "$hours hrs ago";  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return "yesterday";  
     }  
           else  
           {  
       return "$days days ago";  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return "a week ago";  
     }  
           else  
           {  
       return "$weeks weeks ago";  
     }  
   }  
       else if($months <=12)  
      {  
     if($months==1)  
           {  
       return "a month ago";  
     }  
           else  
           {  
       return "$months months ago";  
     }  
   }  
      else  
      {  
     if($years==1)  
           {  
       return "1 year ago";  
     }  
           else  
           {  
       return "$years years ago";  
     }  
   }  
 }

function exist_user($con,$email){
	$sql = mysqli_query($con,"SELECT * from tbl_user_profile where email_add='$email'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else
	{
		return 0;
	}
}

function generate_unique_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_user_profile");
	$row = mysqli_fetch_assoc($sql);
	$rand_gen = rand(1,9).rand(1,9).($row['max(id)'] + 1).rand(1,9).rand(1,9);
	return 'user-'.$rand_gen;
}


function add_direct_client($con,$fn,$ln,$email,$user_type){
$new_id = generate_unique_id($con);
$pwd = 'MUSIC-'.rand(100,200).rand(3,50).rand(1,10);
$sql = mysqli_query($con,"INSERT INTO tbl_user_profile values(id,'$new_id','$fn','N/A','$ln','N/A','N/A','$email','N/A','$pwd','$user_type',1,null,null,1,0,null)");
if ($sql) {
	echo 1;
}
}

function delete_users($con,$user_id){
	$sql = mysqli_query($con,"DELETE from tbl_user_profile where user_id='$user_id'");
	if ($sql) {
		echo 1;
	}
}


//New UPDATE ROGELIO

function show_style($con){
?>
<option selected disabled>Style</option>
<?php
$sql = mysqli_query($con,"SELECT * from tbl_genre");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option value="<?php echo $row['gen_id']; ?>"><?php echo $row['genre_name']; ?></option>
<?php
}
}

function show_album($con){
	?>
<option selected disabled>Album</option>
<?php
$myid = $_SESSION['admin'];
$sql = mysqli_query($con,"SELECT * from tbl_album where user_id='$myid'");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option value="<?php echo $row['album_id'] ?>"><?php echo $row['album_name']; ?></option>
<?php
}
}

function show_audience($con){
	?>
<option selected disabled>Audience</option>
<?php
$sql = mysqli_query($con,"SELECT * from tbl_audience");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option value="<?php echo $row['audience_id']; ?>"><?php echo $row['audience_name']; ?></option>
<?php
}
}

function show_app($con){
	?>
<option selected disabled>Application</option>
<?php
$sql = mysqli_query($con,"SELECT * from tbl_application");
while ($row = mysqli_fetch_assoc($sql)) {
?>
<option value="<?php echo $row['application_id']; ?>"><?php echo $row['application_name']; ?></option>
<?php
}
}


function send_emailer($title,$description,$composer,$genre,$posted_by){

		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
		    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
		    $mail->Password = 'testing5!';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587;                                    // TCP port to connect to

		    $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		        )
		    );
		    //Recipients
		    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
		    $mail->addAddress('johnluisb14@gmail.com');     // Add a recipient
		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Music Library';
		    $mail->Body    = '<h1>Music Library</h1><br><p>TITLE: '.$title.'</p><p>COMPOSER: '.$composer.'</p><p>GENRE: '.$genre.'</p><p>DESCRIPTION: '.$description.'</p><br><p>POSTED BY: '.$posted_by.'</p>';
		    
		    $mail->AltBody = '(Music Library) You have Music Request!';

		    $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
}

function show_music_library($con,$search3)
{
	if ($search3 == "" || $search3 == null) {

	$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$id = $row['audio_id'];
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

	if ($row['picture'] == "") {
		$img = '../img/contributor.png';
	}
	else
	{
		$img = '../'.$row['picture'];
	}

	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	$toll_title="<center>".'Contributor'."</center>";
	?>
			<tr class="label item">
				<td class="text-left">
					<h6 class="db-content default-fs"  style="margin-left: 40%;">
						<input type="checkbox" onclick="count_check_music();" name="id[]" class="select_musics" value="<?php echo $row["music"]; ?>" />
					</h6>
				</td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b>
                   		<?php echo ucfirst($row['song_name'])?> <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['fn'].' ' .$row['ln'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
               	<td class="text-center">
               		<div class="btn-group btn-justified text-center" id="btns"> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
	               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
					</div>
           		</td>
            </tr>
	<?php
	}
	?>
	<script type="text/javascript">
		enable_tooltips();
	</script>
	<?php
	}
		else
		{ ?>
			<td colspan="8" class="text-center">
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
			</td>

			
	<?php	
		}
	}
	else
	{

		$sql =mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
			right join tbl_audios t2 on t1.album_id=t2.album_id 
			right join tbl_album t3 on t2.album_id=t3.album_id 
			right join tbl_user_profile t4 on t3.user_id=t4.user_id 
			where t2.active = 1 and concat(song_name,genre,fn,ln,composer,description,application_id,keyword) like '%$search3%'  order by song_name");

		if (mysqli_num_rows($sql)>0) {
			?>
				
			<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$id = $row['audio_id'];
				    $date_ = date('Y-m-d');
					$app_date = $row['app_date'];
					$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
					$group_id = $row['file_id'];
					$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
					$rows = mysqli_fetch_assoc($query);
					$total_files = $rows['files'];
					if ($row['picture'] == "") {
						$img = '../img/contributor.png';
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

					$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
					$toll_title="<center>".'Contributor'."</center>";

					?>
					<tr class="track item">
						<td class="text-left">
							<h6 class="db-content default-fs"  style="margin-left: 40%;">
								<input type="checkbox" onclick="count_check_music();" name="id[]" class="select_musics" value="<?php echo $row["music"]; ?>" />
							</h6>
						</td>
		                <td class="text-left">
		                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b>
                   		<?php echo ucfirst($row['song_name'])?> <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
		                </td>
		                <td class="text-left">
		                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
		                </td>
		                
		               	<td class="text-left">
		               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
		           		</td>
		           		<td class="text-left">
		                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
		               	</td>
		               	<td class="text-left">
		               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
		           		</td>
		           		<td class="text-left">
		               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['fn'].' ' .$row['ln'])?></h6>
		           		</td>
		           		<td class="text-left">
		           			<h6 class="db-content default-fs default_margin" >
								<?php 
								if($total_files == 1){ ?>
									<?php echo $total_files; ?>&nbsp; File

								<?php }
								else if($total_files == 0){ ?>
									NONE
								<?php }
								else{ ?>
									<?php echo $total_files; ?>&nbsp; Files
								<?php
								}
								?>	
							</h6>			
					</td>
		               	<td class="text-center">
		               		<div class="btn-group btn-justified text-center" id="btns"> 
			               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
			               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
							</div>
		           		</td>
		            </tr>
					<?php
					}
				?>
				<script type="text/javascript">
					enable_tooltips();
				</script>
			<?php
		}
		else
		{
			?>
			<td colspan="8" class="text-center">
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
			</td>
	<?php
		}
	}
}

function music_details($con,$id){
	$sql = mysqli_query($con,"SELECT t1.*,t3.*, t4.`genre_name`,t2.`album_name`, t2.`album_artwork`,count(t5.`file_name`) as files,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.album_id = t2.album_id 
		LEFT JOIN tbl_user_profile t3 on t2.user_id = t3.user_id
		LEFT JOIN tbl_genre t4 on t1.genre = t4.gen_id 
		LEFT JOIN tbl_supporting_file t5 on t1.file_id = t5.group_id 
		WHERE t1.`audio_id` = '$id'");
	$row = mysqli_fetch_assoc($sql);
	$description = $row['description'];
	$song_name = $row['song_name'];
	$composer = $row['composer'];
	$artist = $row['artist'];
	$genre = $row['genre'];
	$application_id = $row['application_id'];
	$files = $row['files'];
	$group_id = $row['file_id'];
	$album_name = $row['album_name'];
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));

	if($row['picture'] == ''){
			$img = '../img/contributor.png';
			}else{
				$img = '../'.$row['picture'];
			}

			$tooltip ="<p><img src='".$img."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
			$toll_title="<center>".'Contributor'."</center>";

			if($row['album_artwork'] == ''){
			$album_image = '../img/logo2.png';
			}else{
				$album_image = '../'.$row['album_artwork'];
			}

			$tooltip_album ="<p><img src='".$album_image."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
			$album_title="<center>".'ALBUM'."</center>";
	?>	

		<div class="modal-header music-dark mtxt-light">
		Music Information: <?php echo $song_name; ?> 
		<?php if($detect_month != 1){ ?>
			<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
		<?php }else{ ?>

		<?php } ?>
		<button class="close" data-dismiss="modal">&times;</button></div>
		<style type="text/css">
			.padding-0{
				padding: 0px;
			}
		</style>
		<div class="modal-body"> 
				<div class="row">
					<div class="col-lg-12">
						<div class="coupon">
						  <div class="container" style="line-height: 25px;"><br>
						  	<div class="col-lg-12 table-responsive">
       	 						<table class="table table-sm table-hover">
       	 							<tr>
       	 								<td><span class="text-default">Album: </span></td>
       	 								<td>
       	 									<span class="text-default" data-toggle="popover" data-html="true" data-title="<?php echo $album_title ?>" data-trigger="hover" data-content="<?php echo $tooltip_album ?>">
						    					<?php echo $row['album_name']; ?>
						    				</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Description:</span> </td>
       	 								<td><span class="text-default"><b><?php echo $description; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Contributor:</span> </td>
       	 								<td><span class="text-default" ><b data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>"><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Composer:</span></td>
       	 								<td><span class="text-default" ><b><?php echo $composer; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Genres:</span></td>
       	 								<td><span><strong style="font-weight: bolder;"><?php echo $genre; ?></strong></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Application:</span> </td>
       	 								<td><span class="text-default" ><b><?php echo $application_id; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Date Uploaded:</span></td>
       	 								<td><span class="text-default" ><b><?php echo date('M d, Y', strtotime($row['upload_date'])); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Supporting Files:</span> </td>
       	 								<td>
       	 									<span class="text-default" >
										    	<b>
											    	<?php
											    		if($files < 1){ ?>
											    			None
											    	<?php } elseif($files = 1){ ?>
											    		<?php echo $files ?> file
											    	<?php } else{ ?>
											    		<?php echo $files ?> files
											    	<?php } ?>
										    	</b>
										    </span>		
										</td>
       	 							</tr>
       	 							<tr>
       	 								<td colspan="2">
       	 									<div class="list-group">
												<?php 
													$query = mysqli_query($con, "SELECT file_name,file_path FROM tbl_supporting_file where group_id = '$group_id'");
													while ($rows = mysqli_fetch_assoc($query)) { ?>

														<li class="list-group-item d-flex justify-content-between align-items-center">
													    <span class="text-primary"><b><span class="fa fa-file-text-o"></span>&nbsp;<?php echo $rows['file_name'];  ?></b></span> 
													    <span class="text-primary">
													    	<a href="<?php echo $rows['file_path'] ?>" title="Download <?php echo $rows['file_name'] ?>" download >
															<span class="fa fa-download"></span> 
															</a>
													    </span>
													  </li>										
													<?php }	?>
											</div>
       	 								</td>

       	 							</tr>
       	 						</table>
       	 					</div>
						  </div>
						</div>
					</div>
				</div>
		</div>  
		<script type="text/javascript">
				enable_tooltips();	
				</script>
<?php
}

//About Content
function about_content($con){

	$sql = mysqli_query($con, "SELECT * FROM tbl_about_content where active = 1 limit 1");
	$row = mysqli_fetch_assoc($sql);
	$id = $row['id'];
	$header = $row['header'];
	$body = $row['body']; 
	$image = '../'.$row['image'];
	?>

	<style type="text/css">
		#update_header {
  		display: none;
		}

		#large:hover #update_header {
		  display: inline-block;
		}

		#update_body {
  		display: none;
		}
		#body:hover #update_body {
		  display: inline-block;
		}

		#update_image {
  		display: none;
		}
		#image:hover #update_image {
		  display: inline-block;
		  margin-bottom: 3%;
		}
	</style>

	<div class="col-lg-12" >
	<h1 style="margin-left: 3%;">About <br>Lampstand Music.</h1>
	

	<p id="large" style="margin-left: 3%;"><?php echo $header; ?>
		<button class="btn btn-small btn-dark" id="update_header" onclick="get_about_header('<?php echo $id; ?>')"><span class="fa fa-edit fa-lg"></span> Update Header</button>
	</p>
	<p style="margin-left: 3%;" id="body"><?php echo $body; ?>
		<button class="btn btn-small btn-dark" id="update_body" onclick="get_about_body('<?php echo $id; ?>')"><span class="fa fa-edit fa-lg"></span> Update Body</button>
	</p>

	</div>
	<!--<div class="col-lg-4 text-center" id="image">
		<button class="btn btn-small btn-dark btm-block" id="update_image" onclick="get_about_image('<?php //echo $id; ?>', '<?php// echo $image ?>')"><span class="fa fa-edit fa-lg"></span> Update Image</button>
		<img class="img-fluid" src="<?php //echo $image; ?>" >
		
	</div>-->

<?php
}


function update_about_header($con,$id,$new_header){
	$sql = mysqli_query($con,"UPDATE tbl_about_content set header='$new_header' where id='$id'");
	if ($sql) {
		echo 1;
	}
}

function load_update_header($con,$id){
$sql = mysqli_query($con, "SELECT * FROM tbl_about_content where id='$id' limit 1 "); 
$row = mysqli_fetch_assoc($sql);?>
<div class="col-lg-12">
	<textarea placeholder="Enter About Header" class="input-small big" name="new_header" id="new_header"><?php echo $row['header'] ?></textarea>
</div>
<?php
}

function update_about_body($con,$id,$new_body){
	$sql = mysqli_query($con,"UPDATE tbl_about_content set body='$new_body' where id='$id'");
	if ($sql) {
		echo 1;
	}
}

function load_update_body($con,$id){
$sql = mysqli_query($con, "SELECT * FROM tbl_about_content where id='$id' limit 1 "); 
$row = mysqli_fetch_assoc($sql);?>
<div class="col-lg-12">
	<textarea placeholder="Enter New Body" class="input-small big" name="new_body" id="new_body"><?php echo $row['body'] ?></textarea>
</div>
<?php
}

function faqs_content($con){
	$sql = mysqli_query($con, "SELECT * FROM tbl_faq_content order by id asc limit 5 "); 
	?>
	<table class="table table-hover table-light table-responsive-sm " id="subs">
			<thead class="thead-dark">
				<tr id="labels">
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Title</th>
					<th class="text-left" onclick="w3.sortHTML('#subs', '.item', 'td:nth-child(2)')" style="cursor:pointer">Content</th>
					<th class="text-left">Action</th>
				</tr>	
			</thead>
			<tbody>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>

		<tr class="tracks item">
			<td><?php echo $row['title']; ?></td>
			<td><?php echo $row['content']; ?></td>
			<td>
				<div class="btn-group btn-justified">
					<button class="btn btn-small btn-dark" onclick="edit_faqs('<?php echo $row['id'] ?>')"><span class="fa fa-edit fa-lg"></span></button>
					<button class="btn btn-small btn-dark" onclick="delete_faqs('<?php echo $row['id'] ?>')"><span class="fa fa-trash fa-lg"></span></button>
				</div>
			</td>
		</tr>
	<?php
	}
	?>
	</tbody>
	</table>

<?php
}


function save_faqs($con,$title,$content){
	$date_ = date('Y-m-d H:i:s');
		$sql = mysqli_query($con, "INSERT INTO tbl_faq_content(title,content,date_update) values('$title','$content','$date_')");
		if($sql){
			echo 1;
		}
}

function update_faqs($con,$id,$title,$content){
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "UPDATE tbl_faq_content set title = '$title', content = '$content', date_update = '$date_' WHERE id='$id'");
		if($sql){
			echo 1;
		}
}

function load_update_faqs($con,$id){
$sql = mysqli_query($con, "SELECT * FROM tbl_faq_content where id='$id' limit 1 "); 
$row = mysqli_fetch_assoc($sql);?>
<div class="col-lg-12">
	<input type="show" id="up_title" name="up_title" placeholder="Enter New Title" value="<?php echo $row['title'] ?>">
</div>
<div class="col-lg-12">
	<textarea placeholder="Enter New Content" class="input-small big" name="up_content" id="up_content"><?php echo $row['content'] ?></textarea>
</div>
<?php
}


function delete_faqs($con,$id){
	$sql = mysqli_query($con, "DELETE FROM tbl_faq_content where id='$id'");

	if($sql){
		echo 1;
	}

}


function music_played($con,$search,$top_plays)
{
	if ($search == "" || $search == null) {

	$counter = 0;
	$sql = mysqli_query($con, "SELECT t1.`audio_id`,t2.`song_name`, COUNT(t1.`audio_id`) AS total_plays FROM tbl_played_songs t1 LEFT JOIN tbl_audios t2 ON t1.`audio_id`=t2.`audio_id` GROUP BY t1.`audio_id` ORDER BY total_plays DESC
 		LIMIT $top_plays");

			if (mysqli_num_rows($sql)>0) {

				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;
  					 ?>

  						<tr class="tracks" onclick="played_details('<?php echo $row['audio_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['total_plays'];?>')">
  							<td></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 7%;"><?php echo $counter; ?>.</p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 4%;"><?php echo ucfirst( $row['song_name']) ?></p></td>
  							<td class="text-left">
  								<span class="badge w3-card-4 w3-red w3-medium ml3" title="Total Play" style="margin-left: 2%;"> <i class="fa fa-play"></i> <?php echo number_format($row['total_plays']); ?>
								</span>
							</td>
  						</tr>
  						<script type="text/javascript">
						enable_tooltips();	
						</script>
			<?php
			 }

			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}
			
	}
	else
	{

	$counter = 0;
	$sql = mysqli_query($con, "SELECT t1.`audio_id`,t2.`song_name`, COUNT(t1.`audio_id`) AS total_plays FROM tbl_played_songs t1 LEFT JOIN tbl_audios t2 ON t1.`audio_id`=t2.`audio_id` WHERE t2.`song_name` like '%$search%' GROUP BY t1.`audio_id` ORDER BY total_plays DESC LIMIT $top_plays"); 

			if (mysqli_num_rows($sql)>0) {

				while ($row = mysqli_fetch_assoc($sql)) {
					$counter ++;

  					 ?>

  						<tr class="tracks" onclick="played_details('<?php echo $row['audio_id'];?>','<?php echo $row['song_name'];?>','<?php echo $row['total_plays'];?>')">
  							<td></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 7%;"><?php echo $counter; ?>.</p></td>
  							<td class="text-left"><p class="default-fs" style="margin-left: 4%;"><?php echo ucfirst( $row['song_name']) ?></p></td>
  							<td class="text-left">
  								<span class="badge w3-card-4 w3-red w3-medium ml3" title="Total Play" style="margin-left: 2%;"> <i class="fa fa-play"></i> <?php echo number_format($row['total_plays']); ?>
								</span>
							</td>
  						</tr>
  						<script type="text/javascript">
						enable_tooltips();	
						</script>
			<?php
			 }
			}else{ ?>

				<div class="alert alert-default container text-center" >
				  <strong class="text-danger"><span class="fa fa-music fa-5x"></span> <br> No Data Found!</strong>
				</div>

			<?php
			}
		
	}
	
}
function save_play($con, $audio_id){
	$user = $_SESSION['admin'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "INSERT INTO tbl_played_songs(audio_id,user_id,date_play,location) VALUES('$audio_id','$user','$date_',null)");
	if($sql){
		echo 1;
	}
}


//show download details
function load_played_details($con,$id){

	$sql = "SELECT t1.`user_id`, t1.`audio_id`, t2.`fn`, t2.`ln`,t1.`date_play` FROM tbl_played_songs t1 LEFT JOIN tbl_user_profile t2 ON t1.`user_id` = t2.`user_id` WHERE t1.`audio_id` = '$id' ORDER BY t1.`date_play` DESC";

	$result = mysqli_query($con, $sql);
	$counter = 0;
	if(mysqli_num_rows($result) > 0)
	{
	     while($row = mysqli_fetch_array($result))
	     {
	     	$counter++;
	     	$datetime = $row['date_play']
	    ?>
	    	<tr class="tracks item">
				<td class="text-left"><?php echo $counter; ?>.</td>
				<td class="text-left">
					<?php 
						$name = '';
						if($row['fn'] == null AND  $row['ln'] == null){
							$name = 'Played on Index';
						}else{
							$name = strtoupper($row['fn'].' '.$row['ln']);
						}
						echo strtoupper($name);
					?>
				</td>
				<td class="text-left">
					<span class=" w3-large"> <?php echo date('M d, Y', strtotime($row['date_play'])); ?> &nbsp; <span class="badge w3-small w3-card-4 w3-green pull-right ml3"> <?php echo time_ago($datetime); ?></span></span>
				</td>
			</tr>
	    <?php  
	     }
	}
	else
	{
	?>
		<td colspan="3" class="text-center"><div class="alert alert-default alert-dismissible" >
		  <strong class="text-default"><span class="fa fa-download fa-5x"></span> <br> No Data Found!</strong>
		</div>
		</td>
	<?php  
	}
}

function load_user_logs($con, $search){

	if ($search == "" || $search == null) {

		$sql = mysqli_query($con, "SELECT concat(fn,' ', ln) as name, user_type, last_log,picture from tbl_user_profile where user_type != 1 order by last_log DESC limit 20");
		if (mysqli_num_rows($sql)>0) {

			while ($row = mysqli_fetch_assoc($sql)) { 

				if ($row['picture'] == "") {
				$pict = '../avatar/def.png';
				}
				else
				{
				$pict = '../'.$row['picture'];
				}

				if ($row['user_type'] == 2) {
				$user_type = strtoupper('Contributor');
				}
				else if ($row['user_type'] == 3) {
				$user_type = strtoupper('Guest User');
				}
				else if ($row['user_type'] == 4) {
				$user_type = strtoupper('INACTIVE');
				}
				$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$user_type."</p>";
				$toll_title="<center>".$row['name']."</center>";

			?>

				<tr class="item tracks">
					<td></td>
					<td class="text-left"><p class="default-fs" style="margin-left: 5%;" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>"><?php echo $row['name']; ?></p></td>
					<td class="text-left">
						<p class="default-fs" style="margin-left: 5%;">
						<?php 
						if($row['user_type'] == 2){
							echo 'Contributor';
						}else if($row['user_type'] == 3){
							echo 'Guest';
						}
						?>		
						</p>					
					</td>
					<td class="text-left">
						<p class="default-fs" style="margin-left: 3%;">
						<?php 
						if($row['last_log'] != null){ ?>
							<?php echo $row['last_log']; ?> <label class="badge badge-info"> <?php echo time_ago($row['last_log']); ?> </label> 
						<?php }else{ ?>
							Never Sign in.
						<?php }
						?>
					</p>

						
					</td>
				</tr>

			<?php } ?>

			<script type="text/javascript">
				enable_tooltips();	
				</script>	

		<?php }else{ ?>
			<td colspan="4" class="text-center">
			  <strong class="text-default"><span class="fa fa-sign-in fa-5x"></span> <br> No User Log Found!</strong>
			</td>
		<?php }

	}else{

		$sql = mysqli_query($con, "SELECT concat(fn,' ', ln) as name, user_type, last_log,picture from tbl_user_profile where user_type != 1 and concat(ln,fn) like '%$search%' order by last_log DESC limit 20");
		if (mysqli_num_rows($sql)>0) {

			while ($row = mysqli_fetch_assoc($sql)) { 
				if ($row['picture'] == "") {
				$pict = '../avatar/def.png';
				}
				else
				{
				$pict = '../'.$row['picture'];
				}

				if ($row['user_type'] == 2) {
				$user_type = strtoupper('Contributor');
				}
				else if ($row['user_type'] == 3) {
				$user_type = strtoupper('Guest User');
				}
				else if ($row['user_type'] == 4) {
				$user_type = strtoupper('INACTIVE');
				}
				$tooltip ="<p><img src='".$pict."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$user_type."</p>";
				$toll_title="<center>".$row['name']."</center>";
			?>

				<tr class="item tracks">
					<td></td>
					<td class="text-left"><p class="default-fs" style="margin-left: 5%;" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>"><?php echo $row['name']; ?></p></td>
					<td class="text-left">
						<p class="default-fs" style="margin-left: 7%;">
						<?php 
						if($row['user_type'] == 2){
							echo 'Contributor';
						}else if($row['user_type'] == 3){
							echo 'Guest';
						}
						?>		
						</p>					
					</td>
					<td class="text-left">
						<p class="default-fs" style="margin-left: 7%;">
						<?php 
						if($row['last_log'] != null){ ?>
							<?php echo $row['last_log']; ?> <label class="badge badge-info"> <?php echo time_ago($row['last_log']); ?> </label> 
						<?php }else{ ?>
							Never Sign in.
						<?php }
						?>
					</p>
						
					</td>
				</tr>

			<?php } ?>

			<script type="text/javascript">
				enable_tooltips();	
				</script>	
			
		<?php }else{ ?>
			<td colspan="4" class="text-center">
			  <strong class="text-default"><span class="fa fa-sign-in fa-5x"></span> <br> No User Log Found!</strong>
			</td>
		<?php }
	}
	

}

?>