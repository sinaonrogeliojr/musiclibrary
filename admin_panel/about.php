<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
	
  </style>
</head>
<body onload="load_about_content(); refresh_account(); load_pictures();//upload_about_image(); ">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title"><i class="fa fa-lg fa-info"></i> Manage About Content</p>
		</div>
	</div>
	<hr>
	<div class="row" style=" margin-top: -30px;">
		<div class="col-lg-12 w3-round" style="">
			<div class="row" id="about_content"></div>
		</div>
	</div>
</div>

<footer class="container-fluid">
<?php include('../footer.php'); ?>
</footer>
</div>

<!-- UPDATE HEADER MODAL -->
<div class="modal fade" role="dialog" id="update_header_modal" data-keyboard="false" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Update Header <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<input type="hidden" id="header_id">
				<div class="container">
					<div class="row">
						<div class="col-lg-12" id="load_update_header">
							
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<div class="container">
					<button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
					<button class="btn btn-dark btn-small" onclick="update_header_about();"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- UPDATE BODY MODAL -->
<div class="modal fade" role="dialog" id="update_body_modal" data-keyboard="false" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Update Body <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<input type="hidden" id="body_id">
				<div class="container">
					<div class="row">
						<div class="col-lg-12" id="load_update_body">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<div class="container">
					<button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
					<button class="btn btn-dark btn-small" onclick="update_body_about();"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
