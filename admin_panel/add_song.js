var url = 'function.php';

function get_path(index){
    $("#music_path").val(index);
  }

function submitsong2(){
    //var song_path = document.getElementById('uploaded_songs'); 
    var songid = $("#songid").val();
    var style = $("#style_value").val();
    var app = $("#app_value").val();
    var song_name = $("#song_name2").val();
    var composer = $("#composer2").val();
    var album = $("#selectedalb_id").val();
    var description = $("#description").val();
    var keyword = $("#keyword").val();
    var extension = $('#uploaded_songs').val().split('.').pop().toLowerCase();


    var song_path = $("#link2").val();
    var duration = $("#myduration").val();
    var song_path = $("#music_path").val();
    var duration = $("#duration").val();
    var date_upload = $("#date_upload").val();
    var file_id = $("#group_id").val();
    var song_preview2 = document.getElementById('song_preview2');
    var group_id = $("#group_id").val();
    
      if(song_name == '')  
      {  

       swal("Oops", "Please Input Song Name!", "warning"); 
       $("#song_name2").focus();
        return false;
       
      } 
      else if(composer == '')  
      {  
        swal("Oops", "Please Input Composer!", "warning"); 
        $("#composer2").focus();
         return false;
      } 
      else if(album == '')  
      {  
        swal("Oops", "Please Select Album!", "warning"); 
        $("#alb_select").focus();
         return false;
      } 
      else if(date_upload == '')  
      {  
        swal("Oops", "Please Input Date!", "warning"); 
        $("#date_upload").focus();
         return false;
      } 
      /*else if(jQuery.inArray(extension, ['mp3']) == -1)  
      {  
        swal("Oops", "Please Select Song", "warning");  
        return false;
      }*/
      else 
      {
     /* var mydata ='action=upload_song' + '&song_name=' + song_name + '&composer=' + composer + '&description=' + description + 
                   '&album=' + album +'&keyword=' + keyword + '&song_path=' + song_path + '&duration=' + duration + '&date_upload=' + date_upload;
      alert(mydata);*/
      $.ajax({
      url:"addsong.php",
      method:"POST",
      data:{style:style,app:app,song_name:song_name,composer:composer,album:album,description:description,keyword:keyword,song_path:song_path,duration:duration,date_upload:date_upload,file_id:file_id,songid:songid,group_id:group_id},
      cache:false,
      success:function(data){
      // alert(data);
      console.log(data);
      if (data == 1) 
      {
      swal("Success", "Song has been Added!", "success");
      $('#addmusic').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id').val('');
      onload('#playlists');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      mymusic();
      song_able2();
      }
      else if(data == 2){
      swal("Success", "Song has been Updated!", "success");
      $('#addmusic').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id').val('');
      $('#display_supporting_files').html('');
      onload('#playlists');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      mymusic();
      song_able2();
      }
      }
      });
      }  
      }

function selectalb(album_name,album_id){
  document.getElementById("alb_select").innerHTML = album_name;
  $("#selectedalb_id").val(album_id);
  $("#select_album_modal").modal('hide');
  //swal("Success", "Album Selected!", "success");
}

function create_album(){
  $("#select_album_modal").modal('hide');
  $("#createAlbumModal").modal('show');
}

function cancel_select(){
  $("#select_album_modal").modal('hide');
    $("#addmusic").modal('show');
}

function load_albums(){
  var searchalbums = document.getElementById('searchalbums');
  var mydata = 'action=load_album_selection' + '&searchalbums=' + searchalbums.value;
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#tbl_album_select").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#tbl_album_select").html(data);},800); 
  }
  }); 
}

function addto_album(){
  $("#addmusic").modal('hide');
  load_albums();
  $("#select_album_modal").modal('show');
}


function get_style_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#style_select input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;

  $("#style_value").val(selected);
}

function get_app_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#app_select input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(',' + ' ') ;

  $("#app_value").val(selected);
}

function generate_group_id(){
  var mydata = 'action=gen_groupid';
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  success:function(data){
    //alert(data);
    $("#group_id").val(data.trim());
  }
  }); 

}

function display_files()
{
  var id = document.getElementById('group_id');
  var mydata = 'action=show_supporting_files' + '&group_id=' + id.value;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_supporting_files").html(data);},800); 
    }
  }); 
}

function clear_form(){
  $("#song_preview2").html('');
  $("#alb_select").html('<i class="fa fa-folder-o"></i> &nbsp;Album');
  $("#selectedalb_id").val('');
  $("#style_value").val('');
  $("#app_value").val('');
  document.getElementById('btn-select2').disabled = false;
  $("#composer2").val('');
  $("#description").val('');
  $("#keyword").val('');
  $("#duration").val('');
  $("#music_path").val('');
  $("#display_supporting_files").html('');

}

function load_style_select(){
var style = document.getElementById('style_value');
//alert(style.value);
var mydata = 'action=show_style_select' + '&style_value=' + style.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#style_select").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#style_select").html(data);},800); 
}
});
}

function load_app_select(){
var app = document.getElementById('app_value');
var mydata = 'action=show_app_select'+ '&app_value=' + app.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#app_select").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#app_select").html(data);},800); 
}
});
}

function get_dateupload(){
    var songid = document.getElementById("songid");
    var mydata = 'action=get_dateupload' +'&songid=' + songid.value;
    //alert(mydata);
     $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        //alert(data);
          $("#date_upload").val(data.trim());
      }
    }); 
  }

function upload_music22(){
 $(document).ready(function(){
  $(document).on('change', '#uploaded_songs', function(){
    var name = document.getElementById("uploaded_songs").files[0].name;
    var form_data = new FormData();
    var ext1 = name.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext1, ['mp3']) == -1) 
      {
      swal("Error", "Invalid File","error")
      return false;
      }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploaded_songs").files[0]);
    var f = document.getElementById("uploaded_songs").files[0];
    var fsize = f.size||f.fileSize;
      if(fsize > 70000000)
      {
      alert("Invalid, Large File Size!");
      }
      else
      {
      form_data.append("uploaded_songs", document.getElementById('uploaded_songs').files[0]);
      
      // alert(form_data);
      $.ajax({
      url:"uploadsong2.php",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend:function(){
      $('#song_preview2').html("<label class='text-success col-sm-12'>Music Uploading...</label>");
      },   
      success:function(data)
      {
      
      $('#btn-select2').removeClass('btn btn-dark');
      $('#song_preview2').html(data);
      document.getElementById('submitsong2').disabled=false;
      document.getElementById('btn-select2').disabled=true;
      $('#btn-select2').addClass('btn btn-dark');
      //swal("Success", "File Uploaded", "success");
      }
      });
      }
      });
     });
}
function multi_upload_file(){
  $(document).ready(function(){
    $(document).on('change', '#multi_upload_file', function(){

      var filedata = document.getElementById("multi_upload_file");
      var group_id = document.getElementById("group_id");
      //alert(group_id.value);
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var filename = document.getElementById("multi_upload_file").files[i].name;
        var formdata = new FormData();
        var ext1 = filename.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['pdf','doc','docx','txt']) == -1) 
        {
          swal("Error","Invalid File","error");
          $("#multi_upload_file").val('');
          return false;
        }

        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multi_upload_file").files[i]);
        var f = document.getElementById("multi_upload_file").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          swal("Warning", "Large File Size","warning");
        }
        else
        {
          formdata.append("multi_upload_files", document.getElementById('multi_upload_file').files[i]);
          formdata.append("group_id", group_id.value);

          $.ajax({
            url:"multi_upload_file.php",
            method:"POST",
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#file_preview').html('<span><img src="../img/loder1.gif" width="20"> </span>');
            },   
            success:function(data)

            { 
              display_files2();
              //alert(data);
              $('#file_preview').text("");
              setTimeout(function(){ $('#file_preview').append(data);}, 1000);
              //$('#display_supporting_files').html(data);
              $('#pdf_btn').removeClass('btn btn-info');
              //document.getElementById('multi_upload_file').disabled= true;
              $('#pdf_btn').addClass('btn btn-dark');
              swal("Success", "Files Uploaded", "success");
              
              var upload_file = $('#upload_file').val();

              file_able();
              
            }
          });
        }
      }
    });
  });
}

function file_able(){
  $("#select_file_modal").modal('hide');
  $("#addmusic").modal('show');
  $("#link_name").val('');
  $("#s_link").val('');
  //$("#word_prev").html('');
  //$("#file_preview").html('');
  $("#pdf_btn").removeClass('btn btn-dark');
  $("#pdf_btn").addClass('btn btn-info');
  document.getElementById('multi_upload_file').disabled= false;
  //$("#word_btn").removeClass('btn btn-dark');
  //$("#word_btn").addClass('btn btn-primary');
  //document.getElementById('upload_word').disabled= false;
}

function display_files2()
{
  var id = document.getElementById('group_id');
  var mydata = 'action=show_supporting_files' + '&group_id=' + id.value;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_supporting_files").html(data);},800); 
    }
  }); 
}

function get_file_data(new_file_id) 
{
  $('#edit_action').val("update_file");
  var mydata = 'new_file_id=' + new_file_id;

  $.ajax({
   url:"get_files_data.php",
   method:"POST",
   data:mydata,
   dataType:"JSON",
   success:function(data)
   {
    var str = data.file_name;
    var fn = str.split(".");

    $('#file_id').val(data.file_id);
    $('#file_name').val(fn[0]);
    $('#file_description').val(data.file_description);
    $('#link_name').val(data.link_name);
    $('#link').val(data.link);

    $('#file_ext').val(fn[1]);
    $('#addmusic').modal('hide');
    $('#EditFileModal').modal('show');
   }
  });
}

function edit_file(){

  var file_id = $("#file_id").val();
  var file_name = $("#file_name").val();
  var file_desc = $("#file_description").val();
  var link_name = $("#link_name").val();
  var link = $("#link").val();
  var link_ext = $("#file_ext").val();

  var fn = file_name + '.' + link_ext; 

  var mydata = 'action=edit_file' + '&file_id=' + file_id + '&file_name=' + fn + '&file_desc=' + file_desc + 
                '&link_name=' + link_name + '&link=' + link;
  $.ajax({
    url:url,
    method:"POST",
    data:mydata,
    success:function(data)
    {
      if (data == 1) 
      {
        swal("Success", "Successfully Updated Supporting Files", "info");
        $('#EditFileModal').modal('hide');
        $('#addmusic').modal('show');
        display_files();
      }
    }
  });
}

function remove_file(id,path){ 
  var path ='action=remove_file'+ '&path=' + path + '&id=' + id;
  //alert(path);
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this File?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#34464a",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "File has been Removed!", "success");
      display_files();
      //$('#file_preview').html('');
      //$('#pdf_btn').removeClass('btn btn-dark');
      //$('#pdf_btn').addClass('btn btn-info');
      //$('#upload_file').val('');
      //document.getElementById('upload_file').disabled= false;
    } 
    else  
    {  
      return false;  
    } 
    /*$('#btn-select2').removeClass('btn btn-dark');
    document.getElementById('uploaded_songs').disabled= false;
    $('#btn-select2').addClass('btn btn-primary');
    $('#uploaded_songs').val('');*/
    }  
    });  
    });
  }

 