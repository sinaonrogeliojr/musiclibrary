<!--  -->
<nav class="navbar navbar-expand-lg fixed-top w3-card-4 " style="padding:0px; background-color: #29425a;" id="mainNav">
  
   <a class="navbar-brand ml2 w3-hover-opacity w3-xlarge animated slideInLeft bg-info" style="margin:0px; padding:0px;" onclick="pager('homepage');" href="#">
      <img src="../img/slogo3.png" class="img-fluid p-2" width="123" style="padding:0px; margin: 0px;">
    </a> 
   
  <button id="openNav" class="w3-button w3-hover-dark-grey w3-xlarge" style=" padding:15px; display: none; background-color: transparent;" onclick="w3_open();">&#9776;</button>
  <button id="closeNav" class="w3-button w3-hover-dark-grey  w3-xlarge" onclick="w3_close()" style=" padding:15px;  background-color: transparent;">&#9776;</button>

  <ul class="navbar-nav ml-auto droper mr-3">
      <li class="">
          <a href="#login" class="" onclick="$('#sidenav').toggle();">
            <div id="img_pf_pic">
            </div>
         
          <span class="fa fa-bars text-white fa-lg" id="bar_logo"></span>
        </a>
        <div class="side-drop w3-card-4 text-center">
          <div id="img_pf">
             
          </div>
         <div class="text-center">           
            <div class="text-capitalize"><p><?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></p></div>
            <a href="../logout.php" class="btn btn-dark btn-block"><i class="fa fa-sign-out"></i>Logout</a>
         </div>
        </div>
      </li>
    </ul> 
</nav>

<div class="w3-sidebar w3-bar-block w3-card-4 w3-animate-left mobile-nav" style="background-color:#000;  z-index:10; color: #4cb6cb; " id="mySidebar">
    <div style="margin-top: 80px;">
      <a href="#homepage" onclick="pager('homepage');" data-toggle="collapse" class="hovers  logo-label w3-bar-item w3-button text-nav p-3 w3-hover-dark-grey">
       <i class="icon-toggle fa fa-user-circle fa-lg " ></i><span class="text-label "> User Management </span>
      </a>

      <a href="#subscribers" onclick="pager('subscribers');" data-toggle="collapse" class="w3-bar-item w3-button logo-label  p-3 w3-hover-dark-grey">
       <i class="icon-toggle fa fa-users fa-lg" ></i> <span class="text-label">Subscribers</span>
      </a>

      <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button logo-label p-3 w3-hover-dark-grey">
        <i class="icon-toggle fa fa-music fa-lg" ></i><span class="text-label"> Music Management</span></i>
      </a>

      <div id="settings" class="collapse">
        <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('my_music');"><i class="icon-toggle fa fa-music" ></i> <span class="text-label">My Music</span></a>
        <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('album');"><i class="icon-toggle fa fa-image" ></i> <span class="text-label">Album</span></a>
        <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('music_playlist');"><i class="icon-toggle fa fa-play-circle" ></i> <span class="text-label">Music Library</span></a>
        <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('new_uploads');"><i class="icon-toggle fa fa-upload" ></i> <span class="text-label">Pending Uploads</span></a>
        <a href="#settings" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('download');"><i class="icon-toggle fa fa-download" ></i> <span class="text-label">Downloads</span></a>
      </div>

      <a href="#settings2" data-toggle="collapse" class="w3-bar-item w3-button logo-label  p-3 w3-hover-dark-grey">
       <i class="icon-toggle fa fa-cog fa-lg" ></i> <span class="text-label">Settings</span> 
      </a>

      <div id="settings2" class="collapse">
        <a href="#settings2" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('genre');"> <i class="icon-toggle fa fa-gears" ></i> <span class="text-label">Options</span></a>
        <a href="#settings2" data-toggle="collapse" class="w3-bar-item w3-button  logo-label   w3-hover-dark-grey" onclick="pager('account_settings');"> <i class="icon-toggle fa fa-shield" ></i> <span class="text-label">Account Settings</span></a>
      </div>

    </div>
  </div>
