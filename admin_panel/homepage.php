<script type="text/javascript">
user_account();
</script>

<style type="text/css">
	.left-round{
		border-radius: 15px 0px 0px 15px;
	}
	.right-round{
		border-radius: 0px 15px 15px 0px;
	}
	.link_cursor{
		cursor: pointer;
	}
	
</style>

<div class="w3-animate-opacity">
<div class="card glass">
	<div  class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<h3>User Management </h3>
			</div>
			<div class="col-sm-2 text-right pr-1" style="margin-top: 10px;"	>
				<button class="btn btn-dark w3-round-xxlarge " data-toggle="modal" data-target="#add_user"><i class="fa fa-user-circle"></i> Add User</button>
			</div>
			<div class="col-sm-4 pl-1" style="margin-top: 10px;">
				<div class="form-group ">
					<div class="input-group">
						<span class="btn btn-dark bg-dark input-group-addon text-white" onclick="user_account();"><i class="fa fa-search"></i></span>
						<input type="text" oninput="user_account();" name="search" id="search" class="form-control " placeholder="Search..." autocomplete="off">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="user_list"></div>
<br>
	<div class="w3-round">
		<div id="tbl_users"></div>
	</div>
</div>

<div class="modal fade" role="dialog" id="add_user" data-keyboard="false" >
	<div class="modal-dialog">
		<div class="modal-content w3-round-large">
			<div class="modal-header music-dark mtxt-light">Add User <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-6">
						<label class="text-dark">Firstname</label>
						<div class="form-group">
							<input type="text" class="form-control text-capitalize" name="add_fn" id="add_fn" placeholder="Enter firstname...">
						</div>
					</div>
					<div class="col-sm-6">
						<label class="text-dark">Lastname</label>
						<div class="form-group">
							<input type="text" class="form-control text-capitalize" name="add_ln" id="add_ln" placeholder="Enter lastname...">
						</div>
					</div>
					<div class="col-sm-12">
						<label class="text-dark">E-mail</label>
						<div class="form-group">
							<input type="text" class="form-control" name="add_email" id="add_email" placeholder="Enter E-mail...">
						</div>
					</div>
					<div class="col-sm-12">
						<label class="text-dark">User Type</label>
						<div class="form-group">
							<select class="form-control" id="add_type" name="add_type">
								<option selected="" disabled="">Select user type</option>
								<option value="2">Contributor</option>
								<option value="3">Guest User</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button class="btn btn-success" onclick="add_client();"><i class="fa fa-save"></i> Save</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_users(id){
	var mydata = 'action=delete_user' + '&user_id=' + id;
	swal({
	  title: "Are you sure?",
	  text: "You wan to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Delete",
	  closeOnConfirm: false
	},
	function(){
	 $.ajax({
	 	type:"POST",
	 	url:url,
	 	data:mydata,
	 	cache:false,
	 	success:function(data){
	 	// alert(data);
		if (data.trim() == 1) 
		{
			$("#"+id+"").addClass('w3-card-4 animated zoomOutDown');
			swal.close();
			setTimeout(function(){
			user_account();
			},100);
		}
		else
		{
			swal("Sorry!","Active Users won\'t be able to delete !","error");
			$("#"+id+"").addClass('w3-red');
			setTimeout(function(){
			$("#"+id+"").removeClass('w3-red');
			},1000);
		}
	 	}
	 });
	  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
	});
	}
</script>