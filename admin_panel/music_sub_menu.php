<div id="settings" class="collapse">
	<li class="w3-hover-shadow" title="Music Library" style="background: #616f7f;" onclick="window.location='music_playlist.php'">
		<a href="#settings" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-play-circle fa-2x" ></i>
		</a>
	</li>
	<li class="w3-hover-shadow" title="Music Played" style="background: #616f7f;" onclick="window.location='played_song.php'">
		<a href="#settings" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-music fa-2x" ></i>
		</a>
	</li>
	<li class="w3-hover-shadow" title="Pending Uploads" style="background: #616f7f;" onclick="window.location='new_uploads.php'">
		<a href="#settings" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-upload fa-2x" ></i>
		</a>
	</li>
	<li class="w3-hover-shadow" title="Downloads" style="background: #616f7f;" onclick="window.location='download.php'">
		<a href="#settings" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa fa-download fa-2x" ></i>
		</a>
	</li>
</div>