var url = 'function.php';

function generate_group_id(){
  var mydata = 'action=gen_groupid';
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  success:function(data){
    //alert(data);
    $("#group_id").val(data.trim());
  }
  }); 

}

function get_dateupload(){
    var songid = document.getElementById("songid");
    var mydata = 'action=get_dateupload' +'&songid=' + songid.value;
    //alert(mydata);
     $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        //alert(data);
          $("#date_upload").val(data.trim());
      }
    }); 
  }

function save_able(){
  $('#btn-chooser').removeClass('btn btn-secondary');
  document.getElementById('album_image').disabled= false;
  $('#btn-chooser').addClass('btn btn-primary');
  $('#album_image').val('');
  $('#image_preview').html('');
}

function save_album_able(){
  $('#btn-chooser2').removeClass('btn btn-secondary');
  document.getElementById('album_image2').disabled= false;
  $('#btn-chooser2').addClass('btn btn-primary');
  $('#album_image2').val('');
  $('#image_preview2').html('');
  $('#album_name2').val('');
}

function upload_picture(){
  $(document).ready(function(){
  $(document).on('change', '#album_image', function(){
    var name = document.getElementById("album_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
    {
    alert("Invalid File");
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("album_image").files[0]);
    var f = document.getElementById("album_image").files[0];
    var fsize = f.size||f.fileSize;
    if(fsize > 2000000)
    {
    alert("File Size is very big");
    }
    else
    {
    form_data.append("album_image", document.getElementById('album_image').files[0]);
    $.ajax({
    url:"upload.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
    $('#image_preview').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
    $('#btn-chooser').removeClass('btn btn-primary');
    $('#image_preview').html(data);
    document.getElementById('album_image').disabled= true;
    $('#btn-chooser').addClass('btn btn-secondary');
    }
    });
    }
    });
    });
    }


function upload_picture2(){
  $(document).ready(function(){
  $(document).on('change', '#album_image2', function(){
    var name = document.getElementById("album_image2").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
    {
    swal("Error!", "Invalid File!", "warning");
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("album_image2").files[0]);
    var f = document.getElementById("album_image2").files[0];
    var fsize = f.size||f.fileSize;
    if(fsize > 2000000)
    {
    swal("Warning!", "Large File Size!", "warning");
    }
    else 
    {
    form_data.append("album_image2", document.getElementById('album_image2').files[0]);
    $.ajax({
    url:"upload_img.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
    $('#image_preview2').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
    $('#btn-chooser2').removeClass('btn btn-primary');
    $('#image_preview2').html(data);
    document.getElementById('album_image2').disabled= true;
    $('#btn-chooser2').addClass('btn btn-secondary');
    }
    });
    }
    });
    });
    }

function remove_img(){
    var path = $('#remove_img').data("path") + 'action=remove_image';
    swal({
    title: "Are you sure?",
    text: "Do you want to remove this Image?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    }, 
    function(){
    $.ajax({  
    url:url,  
    type:"POST",  
    data:path,
    cache:false,  
    success:function(data){  
    if(data != '')  
    {  
    swal("Deleted!", "Image has been deleted !", "success");
    $('#image_preview').html('');
    }
    else  
    {  
    return false;  
    }  
    $('#btn-chooser').removeClass('btn btn-secondary');
    document.getElementById('album_image').disabled= false;
    $('#btn-chooser').addClass('btn btn-primary');
    $('#album_image').val('');
    }  
    });  
    });
    }
 

function remove_img2(){
    var path = $('#remove_img2').data("path") + 'action=remove_image';
    swal({
    title: "Are you sure?",
    text: "Do you want to remove this Image?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    }, 
    function(){
    $.ajax({  
    url:url,  
    type:"POST",  
    data:path,
    cache:false,  
    success:function(data){  
    if(data != '')  
    {  
    swal("Deleted!", "Image has been deleted !", "success");
    $('#image_preview2').html('');
    }
    else  
    {  
    return false;  
    }  
    $('#btn-chooser2').removeClass('btn btn-secondary');
    document.getElementById('album_image2').disabled= false;
    $('#btn-chooser2').addClass('btn btn-primary');
    $('#album_image2').val('');
    }  
    });  
    });
    }


function submit_album2()
{
  var album_image2 = $('#album_image2').val(); 
  var album_name2 = $('#album_name2').val();
  var extension = $('#album_image2').val().split('.').pop().toLowerCase();
  var imglink = $("#imglink").val();   
  
  if(album_image2 == '')  
  {  
    swal("Error!", "Please Select Album Image", "warning");
    return false;  
  } 
  else if(album_name2 == '')  
  {  
    swal("Error!", "Please Input Album Name", "warning"); 
    return false;  
  }  
  else if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)  
  {  
    swal("Error!", "Invalid Image File", "warning");  
    $('#album_image').val('');  
    return false;  
  }
  else 
  {
    var mydata = 'imglink=' + imglink + '&album_name2=' + album_name2;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:"save_album2.php",
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data);
        if (data == 1) 
        {
          swal("Success!", "Album Created", "success");  
          $('#createAlbumModal').modal('hide');
          $('#select_album_modal').modal('show');
           load_albums();
           show_album();
          save_album_able();
          mymusic();
        }
      }
    });
  }  
}

function load_albums(){
  var searchalbums = document.getElementById('searchalbums');
  var mydata = 'action=load_album_selection' + '&searchalbums=' + searchalbums.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#tbl_album_select").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
    // alert(data);
  setTimeout(function(){$("#tbl_album_select").html(data);},800); 
  }
  }); 
}

function load_style_select(){
var style = document.getElementById('style_value');
//alert(style.value);
var mydata = 'action=show_style_select' + '&style_value=' + style.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#style_select").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#style_select").html(data);},800); 
}
});
}


function load_app_select(){
var app = document.getElementById('app_value');
var mydata = 'action=show_app_select'+ '&app_value=' + app.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#app_select").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#app_select").html(data);},800); 
}
});
}


function submitsong(){
    var uploaded_song = $('#uploaded_song').val(); 
    var songname = $('#songname').val();
    var compose = $('#compose').val();
    var artis = $('#artis').val();
    var extension = $('#uploaded_song').val().split('.').pop().toLowerCase();
    var link1 = $("#link1").val();
    var genres = $("#genres").val();   
    var album = $("#id_album").val(); 
      // alert(al);
      if(uploaded_song == '')  
      {  
      alert("Please Select Song");  
      return false;  
      } 
      else if(songname == '')  
      {  
      alert("Please Input Song Name");  
      return false;  
      } 
      else if(compose == '')  
      {  
      alert("Please Input Composer");  
      return false;  
      }
      else if(artis == '')  
      {  
      alert("Please Input Artist");  
      return false;  
      } 
      else if(jQuery.inArray(extension, ['mp3']) == -1)  
      {  
      alert('Invalid Song File');  
      $('#uploaded_song').val('');  
      return false; 
      }
      else if (genres == null || genres == "" || genres == "genres") 
      {
      alert('Please Select genres');    
      }
      else 
      {
      var mydata = 'link1=' + link1 + '&songname=' + songname + '&compose=' + compose + '&artis=' + artis + '&genres=' + genres + '&album=' + album;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:"save_song.php",
      data:mydata,
      cache:false,
      success:function(data){
      console.log(data);
      if (data == 1) 
      {
      $('#addmusic').modal('hide');
      $('#songname').val('');
      $('#compose').val('');
      $('#artis').val('');
      onload('#playlists');
      song_able();
      }
      }
      });
      }  
      }

function check(){
   $(function() { 
  $('#ssstyle[type="checkbox"]').bind('click',function() {
    $('#ssstyle[type="checkbox"]').not(this).prop("checked", false);
  });
});

}

/*
function appcheck(){
   $(function() { 
  $('#apps[type="radio"]').bind('click',function() {
    $('#apps[type="radio"]').not(this).prop("checked", false);
  });
});

}*/

function check2(){
  $(function() { 
  $('#app[type="checkbox"]').bind('click',function() {
    $('#app[type="checkbox"]').not(this).prop("checked", false);
  });
});

}


// function get_song_duration(total){
// var seconds = "0" + (Math.floor(total) - minutes * 60);
// var seconds = "0" + Math.floor(total % 60);
// var minutes = "0" + Math.floor(total / 60);
// var seconds = "0" +  Math.floor(total - minutes * 60);
// //concat minutes and seconds
// var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
// $("#duration").val(dur);
// alert(dur);
// }

function get_style_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#style_select input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(','+' ') ;

  $("#style_value").val(selected);
}

function get_app_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#app_select input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(',' + ' ') ;

  $("#app_value").val(selected);
}

function clear_form(){
  $("#song_preview2").html('');
  $("#alb_select").html('<i class="fa fa-folder-o"></i> &nbsp;Album');
  $("#selectedalb_id").val('');
  $("#style_value").val('');
  $("#app_value").val('');
  document.getElementById('btn-select2').disabled = false;
  $("#composer2").val('');
  $("#description").val('');
  $("#keyword").val('');
  $("#duration").val('');
  $("#music_path").val('');
  $("#display_supporting_files").html('');

}

  function submitsong2(){
    //var song_path = document.getElementById('uploaded_songs'); 
    var songid = $("#songid").val();
    var style = $("#style_value").val();
    var app = $("#app_value").val();
    var song_name = $("#song_name2").val();
    var composer = $("#composer2").val();
    var album = $("#selectedalb_id").val();
    var description = $("#description").val();
    var keyword = $("#keyword").val();
    var extension = $('#uploaded_songs').val().split('.').pop().toLowerCase();


    var song_path = $("#link2").val();
    var duration = $("#myduration").val();
    var song_path = $("#music_path").val();
    var duration = $("#duration").val();
    var date_upload = $("#date_upload").val();
    var file_id = $("#group_id").val();
    var song_preview2 = document.getElementById('song_preview2');
    var group_id = $("#group_id").val();
    
      if(song_name == '')  
      {  

       swal("Oops", "Please Input Song Name!", "warning"); 
       $("#song_name2").focus();
        return false;
       
      } 
      else if(composer == '')  
      {  
        swal("Oops", "Please Input Composer!", "warning"); 
        $("#composer2").focus();
         return false;
      } 
      else if(album == '')  
      {  
        swal("Oops", "Please Select Album!", "warning"); 
        $("#alb_select").focus();
         return false;
      } 
      else if(date_upload == '')  
      {  
        swal("Oops", "Please Input Date!", "warning"); 
        $("#date_upload").focus();
         return false;
      } 
      /*else if(jQuery.inArray(extension, ['mp3']) == -1)  
      {  
        swal("Oops", "Please Select Song", "warning");  
        return false;
      }*/
      else 
      {
     /* var mydata ='action=upload_song' + '&song_name=' + song_name + '&composer=' + composer + '&description=' + description + 
                   '&album=' + album +'&keyword=' + keyword + '&song_path=' + song_path + '&duration=' + duration + '&date_upload=' + date_upload;
      alert(mydata);*/
      $.ajax({
      url:"addsong.php",
      method:"POST",
      data:{style:style,app:app,song_name:song_name,composer:composer,album:album,description:description,keyword:keyword,song_path:song_path,duration:duration,date_upload:date_upload,file_id:file_id,songid:songid,group_id:group_id},
      cache:false,
      success:function(data){
      // alert(data);
      console.log(data);
      if (data == 1) 
      {
      swal("Success", "Song has been Added!", "success");
      $('#addmusic').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id').val('');
      onload('#playlists');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      mymusic();
      song_able2();
      }
      else if(data == 2){
      swal("Success", "Song has been Updated!", "success");
      $('#addmusic').modal('hide');
      $('#song_name2').val('');
      $('#composer2').val('');
      $('#artist2').val('');
      $('#group_id').val('');
      $('#display_supporting_files').html('');
      onload('#playlists');
      document.getElementById('submitsong2').disabled= false;
      document.getElementById('btn-select2').disabled = false;
      mymusic();
      song_able2();
      }
      }
      });
      }  
      }
 

function selectalb(album_name,album_id){
  document.getElementById("alb_select").innerHTML = album_name;
  $("#selectedalb_id").val(album_id);
  $("#addmusic").modal('show');
  $("#select_album_modal").modal('hide');
  //swal("Success", "Album Selected!", "success");
}


function addto_album(){
  $("#addmusic").modal('hide');
  load_albums();
  $("#select_album_modal").modal('show');
}

function cancel_select(){
  $("#select_album_modal").modal('hide');
    $("#addmusic").modal('show');
}

function create_album(){
  $("#select_album_modal").modal('hide');
  $("#createAlbumModal").modal('show');
}

function cancelcreate(){

  $("#createAlbumModal").modal('hide');
    $("#select_album_modal").modal('show');
}

function selectfile(){
  $("#addmusic").modal('hide');
    $("#select_file_modal").modal('show');
}

function cancel_select_files(){
  $("#select_file_modal").modal('hide');
  $("#addmusic").modal('show');
}



function ms(){
  $(document).ready(function(){
    $("#").click(function(){
        $("p").hide();
    });
    $("#ms").click(function(){
        $("#s3").show();
    });
});
}

function show_genres(){
  $.ajax({
    type:"POST",
    url:"show_genres.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_genre").html(data);
    }
    });
    }

function genres(){
  $.ajax({
    type:"POST",
    url:"genres.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_genres").html(data);
    }
    });
    }

function show_musics(){
  $.ajax({
    type:"POST",
    url:"show_musics.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_songs").html(data);
    $("#search3").show();
    }
    });
    }

function show_musics2(){
  $.ajax({
    type:"POST",
    url:"show_musics2.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_songs2").html(data);
    $("#search3a").show();
    }
    });
    }


function show_playlist(){
  $.ajax({
    type:"POST",
    url:"show_my_musics.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_playlist").html(data);
    }
    });
    }


function show_my_playlist(){
  $.ajax({
    type:"POST",
    url:"show_my_playlist.php",
    data:"",
    cache:false,
    success:function(data){
    $("#my_playlist").html(data);
    }
    });
    }

  function song_able2(){
  $('#btn-select2').removeClass('btn btn-dark');
  document.getElementById('uploaded_songs').disabled= false;
  $('#btn-select2').addClass('btn btn-dark');
  $('#uploaded_songs').val('');
  $('#song_preview2').html('');
  $('#keyword').val('');
  $('#description').val('');

  /*$('#genres2').val('Genres');
  $('#album2').val('Album');*/
  }

function upload_file(){
 $(document).ready(function(){
  $(document).on('change', '#upload_file', function(){
    var filename = document.getElementById("upload_file").files[0].name;
    var form_data = new FormData();
    var ext1 = filename.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext1, ['pdf','doc','docx']) == -1) 
      {
      swal("Error","Invalid File","error");
      $("#upload_file").val('');
      return false;
      }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload_file").files[0]);
    var f = document.getElementById("upload_file").files[0];
    var fsize = f.size||f.fileSize;
      if(fsize > 50000000)
      {
      swal("Warning", "Large File Size","warning");
      }
      else
      {
      form_data.append("upload_file", document.getElementById('upload_file').files[0]);
      
      //alert(form_data);
      $.ajax({
      url:"upload_file.php",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend:function(){
      $('#file_preview').html('<span><img src="../img/load.gif" width="20"> </span>');
      },   
      success:function(data)

      {
      //alert(data);
      $('#file_preview').html(data);
      $('#pdf_btn').removeClass('btn btn-info');
      document.getElementById('upload_file').disabled= true;
      $('#pdf_btn').addClass('btn btn-dark');
      swal("Success", "File Uploaded", "success");
      }
      });
      }
      });
     });
      }



/*function display_files()
{
  var id = document.getElementById('group_id');
  var mydata = 'action=show_supporting_files' + '&group_id=' + id.value;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_supporting_files").html(data);},800); 
    }
  }); 
}*/


function edit_file(){

  var file_id = $("#file_id").val();
  var file_name = $("#file_name").val();
  var file_desc = $("#file_description").val();
  var link_name = $("#link_name").val();
  var link = $("#link").val();
  var link_ext = $("#file_ext").val();

  var fn = file_name + '.' + link_ext; 

  var mydata = 'action=edit_file' + '&file_id=' + file_id + '&file_name=' + fn + '&file_desc=' + file_desc + 
                '&link_name=' + link_name + '&link=' + link;
  $.ajax({
    url:url,
    method:"POST",
    data:mydata,
    success:function(data)
    {
      if (data == 1) 
      {
        swal("Success", "Successfully Updated Supporting Files", "info");
        $('#EditFileModal').modal('hide');
        $('#addmusic').modal('show');
        display_files();
      }
    }
  });
}

function close_edit(){
  $('#EditFileModal').modal('hide');
  $('#addmusic').modal('show');
}

function get_file_data(new_file_id) 
{
  $('#edit_action').val("update_file");
  var mydata = 'new_file_id=' + new_file_id;

  $.ajax({
   url:"get_files_data.php",
   method:"POST",
   data:mydata,
   dataType:"JSON",
   success:function(data)
   {
    var str = data.file_name;
    var fn = str.split(".");

    $('#file_id').val(data.file_id);
    $('#file_name').val(fn[0]);
    $('#file_description').val(data.file_description);
    $('#link_name').val(data.link_name);
    $('#link').val(data.link);

    $('#file_ext').val(fn[1]);
    $('#addmusic').modal('hide');
    $('#EditFileModal').modal('show');
   }
  });
}

/*function edit_file(new_file_id){


  var file_id = (new_file_id);
  alert(file_id);
  $.ajax({
   url:"edit.php",
   method:"post",
   data:{file_id:file_id},
   dataType:"json",
   success:function(data)
   {
    $('#EditFileModal').modal('show');
    $('#file_id').val(file_id);
    $('#file_name').val(data.file_name);
    $('#file_description').val(data.file_description);
   }
  });

}*/

function closeEdit(){
  $("#File_name").val('');
  $("#Edit_fileModal").modal('hide');
  $("#select_file_modal").modal('show');
}


function remove_song(path1){ 
  var path = 'action=remove_song'+ '&path1=' + path1;
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this Song?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "Song has been Removed!", "success");
      $('#song_preview2').html('');
    } 
    else  
    {  
      return false;  
    } 
    $('#btn-select2').removeClass('btn btn-dark');
    document.getElementById('uploaded_songs').disabled= false;
    document.getElementById('submitsong2').disabled= true;
    document.getElementById('btn-select2').disabled = false;
    $('#btn-select2').addClass('btn btn-primary');
    $('#uploaded_songs').val('');
    }  
    });  
    });
  }
/*

function upload_word(){
 $(document).ready(function(){
  $(document).on('change', '#upload_word', function(){
    var filename = document.getElementById("upload_word").files[0].name;
    var form_data = new FormData();
    var ext1 = filename.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext1, ['doc','docx']) == -1) 
      {
      swal("Error","Invalid File","error");
      $("#upload_word").val('');
      return false;
      }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("upload_word").files[0]);
    var f = document.getElementById("upload_word").files[0];
    var fsize = f.size||f.fileSize;
      if(fsize > 50000000)
      {
      swal("Warning", "Large File Size","warning");
      }
      else
      {
      form_data.append("upload_word", document.getElementById('upload_word').files[0]);
      
      //alert(form_data);
      $.ajax({
      url:"upload_word.php",
      method:"POST",
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend:function(){
      $('#word_prev').html('<span><img src="../img/loder1.gif" width="20"> </span>');
      },   
      success:function(data)

      {
      //alert(data);
      $('#word_prev').html(data);
      $('#word_btn').removeClass('btn btn-primary');
      document.getElementById('upload_word').disabled= true;
      $('#word_btn').addClass('btn btn-dark');
      swal("Success", "File Uploaded", "success");
      }
      });
      }
      });
     });
      }*/

/*
function remove_word(path){ 
  var path = 'action=remove_word'+ '&path=' + path;
    swal({
    title: "Are you sure?",
    text: "Do you want to Remove this File?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){  
  $.ajax({  
    url:url,  
    type:"POST",  
    data:path,  
    success:function(data){  
    if(data != '')  
    {  
      swal("Removed!", "File has been Removed!", "success");
      $('#word_prev').html('');
      $('#word_btn').removeClass('btn btn-dark');
      $('#word_btn').addClass('btn btn-primary');
      $('#upload_word').val('');
      document.getElementById('upload_word').disabled= false;
    } 
    else  
    {  
      return false;  
    } 
    }  
    });  
    });
  }*/

function save_sfiles(){
  var upload_file = $('#upload_file').val();
  if (upload_file == '') {
    swal("Error","Please Upload File","error");
    return false;
  }else{
    swal("Success","File Has Been Saved","success");
    file_able();
    display_files();
  }

  /*else{
  //var mydata = 'action=save_supporting_files' + '&pdfpath=' + pdfpath + '&wordpath=' + wordpath + '&link_name=' + link_name + '&s_link=' + s_link;
  //alert(mydata);
  $.ajax({  
    url:"savesupport.php",  
    type:"POST",  
    data:{pdfpath:pdfpath,wordpath:wordpath,link_name:link_name,s_link:s_link,},  
    success:function(data){   
     alert(data);
      $('#sfiles_').html(data);
      swal("Success", "File has been Saved!", "success");
      document.getElementById('btn_select').disabled = true;
      file_able();
      $("#avail").removeClass('btn btn-dark');
      $("#avail").addClass('btn btn-success');

    
    }  
    });
    }*/  
}


function file_able(){
  $("#select_file_modal").modal('hide');
  $("#addmusic").modal('show');
  $("#link_name").val('');
  $("#s_link").val('');
  //$("#word_prev").html('');
  //$("#file_preview").html('');
  $("#pdf_btn").removeClass('btn btn-dark');
  $("#pdf_btn").addClass('btn btn-info');
  document.getElementById('multi_upload_file').disabled= false;
  //$("#word_btn").removeClass('btn btn-dark');
  //$("#word_btn").addClass('btn btn-primary');
  //document.getElementById('upload_word').disabled= false;
}





/*function multiple_files(){
  $(document).ready(function(){

 $('#multiple_files').change(function(){
  var error_images = '';
  var form_data = new FormData();
  var files = $('#multiple_files')[0].files;
  if(files.length > 10)
  {
    //swal("Warning","You can not select more than 10 files","warning"); 
   error_images += 'You can not select more than 10 files';
  }
  else
  {
   for(var i=0; i<files.length; i++)
   {
    var name = document.getElementById("multiple_files").files[i].name;
    var ext = name.split('.').pop().toLowerCase();
    if(jQuery.inArray(ext, ['pdf','doc','docx']) == -1) 
    {
      //swal("Warning","Invalid File","warning"); 
     error_images += '<p>Invalid '+i+' File</p>';
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("multiple_files").files[i]);
    var f = document.getElementById("multiple_files").files[i];
    var fsize = f.size||f.fileSize;
    if(fsize > 2000000)
    {
      //swal("Warning","Large File Size","warning"); 
     error_images += '<p>' + i + ' File Size is very big</p>';
    }
    else
    {
     form_data.append("file[]", document.getElementById('multiple_files').files[i]);
    }
   }
  }
  if(error_images == '')
  {
   $.ajax({
    url:"upload_file.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#error_multiple_files').html('<br /><label class="text-primary">Uploading...</label>');
    },   
    success:function(data)
    {
       swal("Success","File Uploaded!","success"); 
     $('#error_multiple_files').html('<br /><label class="text-success">Uploaded</label>');
     //load_file_data();
    }
   });
  }
  else
  {
    swal("Error","Error!","error"); 
   $('#multiple_files').val('');
   $('#error_multiple_files').html("<span class='text-danger'>"+error_images+"</span>");
   return false;
  }
 });  
  });
}*/

/*
function multiple_files()
{
  $(document).ready(function(){
    $(document).on('change', '#multiple_files', function(){

      var filedata = document.getElementById("multiple_files")
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var name = document.getElementById("multiple_files").files[i].name;
        var form_data = new FormData();
        var ext1 = name.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['pdf','docx','doc']) == -1) 
        {
          alert("Invalid File");
        }
        
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multiple_files").files[i]);
        var f = document.getElementById("multiple_files").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          alert("Large File Size");
        }
        else
        {
          form_data.append("multiple_files", document.getElementById('multiple_files').files[i]);

          $.ajax({
            url:"multi_upload_file.php",
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#file_table').html("<label class='text-success'>Music Uploading...</label>");
            },   
            success:function(data)
            { 
              $('#file_table').text("");
              setTimeout(function(){ $('#file_table').append(data);}, 1000);
              alb_song_multi_disable();
            }
          });
        }
      }
    });
  });
}
*/


/*function multi_upload_file(){
  $(document).ready(function(){
    $(document).on('change', '#multi_upload_file', function(){

      var filedata = document.getElementById("multi_upload_file");
      var group_id = document.getElementById("group_id");
      //alert(group_id.value);
      var len = filedata.files.length;

      for (var i = 0; i < len; i++) 
      {
        var filename = document.getElementById("multi_upload_file").files[i].name;
        var formdata = new FormData();
        var ext1 = filename.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext1, ['pdf','doc','docx','txt']) == -1) 
        {
          swal("Error","Invalid File","error");
          $("#multi_upload_file").val('');
          return false;
        }

        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("multi_upload_file").files[i]);
        var f = document.getElementById("multi_upload_file").files[i];
        var fsize = f.size||f.fileSize;

        if(fsize > 50000000)
        {
          swal("Warning", "Large File Size","warning");
        }
        else
        {
          formdata.append("multi_upload_files", document.getElementById('multi_upload_file').files[i]);
          formdata.append("group_id", group_id.value);

          $.ajax({
            url:"multi_upload_file.php",
            method:"POST",
            data: formdata,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              $('#file_preview').html('<span><img src="../img/loder1.gif" width="20"> </span>');
            },   
            success:function(data)

            { 
              //alert(data);
              $('#file_preview').text("");
              setTimeout(function(){ $('#file_preview').append(data);}, 1000);
              //$('#display_supporting_files').html(data);
              $('#pdf_btn').removeClass('btn btn-info');
              //document.getElementById('multi_upload_file').disabled= true;
              $('#pdf_btn').addClass('btn btn-dark');
              swal("Success", "Files Uploaded", "success");
              var upload_file = $('#upload_file').val();
              file_able();
              display_files();
            }
          });
        }
      }
    });
  });
}*/

  function date_asc(){
    var mydata = 'action=showdate_asc' +'&asc=' +asc;
    //alert(mydata);
     $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_playlist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
          show_playlist();
      }
    }); 
  }



function pager(index){
    $('#music_playlist').load(index+'.php');
    }


function play_music(path,song_name,composers,artists){
   /*alert(path+' '+song_name+' '+composers+' '+artists);*/
   $("#foot").show();
    //$("#play_musics").modal('show');
    $("#music_name").html('<div class="divMarquee">'+'<h5>Song Name: ('+song_name+').mp3</h5></div>');
    $("#music_composer").html('<h6>Composer: '+composers+'</h6>');
    $("#music_artist").html('<h7>Artist: '+artists+'</h7>');
    $("#music_player").html('<audio controls controlsList="nodownload" class="w3-green"><source src="../'+path+'" type="audio/mp3"></audio>');
    }


/*  function delete_music(index,music){
  var mydata ='action=del_music'+'&id=' + index +'&music=' + music;
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_playlist();
    }
    else
    {
    alert(data);
    }
    }
    });
    });
    }*/


function delete_music(index,music){

  var mydata ='action=del_music'+'&id=' + index +'&music=' + music;
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_playlist();
    mymusic();
    song_playlist();
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    }   

function delete_album(id,album_artwork){
  var mydata ='action=del_album'+'&id=' + id +'&album_artwork=' +album_artwork;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Album?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Album has been deleted !", "success");
    show_album();
    }
    else
    {
    swal("Oops", "Cannot Delete Album!", "error");
    }
    }
    });
    });
    }

function edit_album(id,album_name,album_artwork){
    $("#update_alb").modal('show');
    $("#albname").val(album_name);
    $("#album_path_backup").val(album_artwork);
    $("#album_artwork").val(album_artwork);
    $("#id").val(id);
    $("#album_name").focus();
    $("#album_artwork").focus();    
    $("#btn_cancel").show('fast');
    }

function edit_audio(id,genres,song_name,composer,description,keyword,album_name,application_id,music_path,duration,album_id,group_id){
    $("#songid").val(id);
    $("#style_value").val(genres);
    $("#song_name2").val(song_name);
    $("#composer2").val(composer);
    $("#description").val(description);
    $("#keyword").val(keyword);
    $("#alb_select").html(album_name);
    $("#app_value").val(application_id);
    $("#music_path").val(music_path);
    $("#duration").val(duration);
    $("#selectedalb_id").val(album_id);
    $("#group_id").val(group_id);
    $("#title_form").html('UPDATE MUSIC');
    $("#addmusic").modal('show');

    document.getElementById('btn-select2').disabled = true;
     document.getElementById('submitsong2').disabled= false;
    load_style_select(); 
    load_app_select(); 
    display_files();
    get_dateupload();

    }




function save_music(){
  var song_name = $("#sname").val();
  var composer = $("#scomp").val();
  var artist = $("#sart").val();
  var genre = $("#sgen").val();
  var albumname = $("#salb").val();
  var audiencename = $("#saud").val();
  var applicationname = $("#sapp").val();
  var id = $("#songid").val();
  if (genre == "") 
  {
  $("#sgen").focus();
  }
  if (albumname == "") 
  {
  $("#salb").focus();
  }
  if (audiencename == "") 
  {
  $("#saud").focus();
  }
  if (applicationname == "") 
  {
  $("#sapp").focus();
  }
  if (song_name == "") 
  {
  $("#sname").focus();
  }
  if (composer == "") 
  {
  $("#scomp").focus();
  }
  if (artist == "") 
  {
  $("#sart").focus();
  }
  else
  {
  var mydata ='action=insert_music' + '&song_name=' +  song_name + '&composer=' +  composer + '&artist=' +  artist + '&genre=' +  genre + '&albumname=' +  albumname + '&audiencename=' +  audiencename + '&applicationname=' +  applicationname +'&id=' + id;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
    $("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
    },
    success:function(data){
    if (data == 1) 
    {
    show_playlist();
    $("#sgen").val('');
    $("#sname").val('');
    $("#composer").val('');
    $("#artist").val('');
    $("#id").val('');
    }
    else if (data == 2) 
    {
    swal("Success", "Song has been Updated", "success");
    show_playlist();
    mymusic();
    $("#editmusic").modal('hide');
    $("#sgen").val('');
    $("#sname").val('');
    $("#scomp").val('');
    $("#sart").val('');
    $("#id").val(''); 
    }
    else
    {
    alert(data);
    }
    $("#loader").html('');
    }
    });
    }
    }

function save_alb(){
  var album_name = $("#albname").val();
  var album_artwork = $("#album_artwork").val();
  var id = $("#id").val();
  if (album_name == "") 
  {
  swal("Oops","Please Input Album Name","warning"); 
  $("#alb_name").focus();
  return false;
  }
  else
  {
  var mydata = 'album_name=' +  album_name + '&action=insert_album' + '&id=' + id;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
    $("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
    },
    success:function(data){
    if (data == 1) 
    {

    show_album();
    $("#albname").val('');
    $("#album_artwork").val('');
    $("#id").val('');
    }
    else if (data == 2) 
    {
    swal("Success","Successfully Updated","success"); 
    show_album();
    $("#albname").val('');
    $("#album_artwork").val('');
    $("#id").val('');
    $("#update_alb").modal('hide') 
    }
    else
    {
    alert(data);
    }
    $("#loader").html('');
    }
    });
    }
    }


function show_album(){
  $.ajax({
    type:"POST",
    url:"show_album.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_album").html(data);
    }
    });
    }

  function refresh_account(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=refresh_account',
    cache:false,
    beforeSend:function(){
      $("#my_data").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#my_data").html(data);},900);
    }
  });
} 
function update_name(fn,mn,ln,id){
  $("#choose").hide('fast');
  $('#fn').val(fn);
  $('#mn').val(mn);
  $('#ln').val(ln);
  $('#id').val(id);
  $("#form-names").show('fast');
}

function name(fn,mn,ln,id){
  if (fn == "") 
  {
    $('#fn').focus();
  }
  else if (mn == "") 
  {
    $('#mn').focus();
  }
  else if (ln == "") 
  {
    $('#ln').focus();
  }
  else
  {
  var mydata = 'action=update_account' + '&fn=' + fn + '&mn=' + mn + '&ln=' + ln + '&id=' + id;
  // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
      $('#edit_name_modal').modal('hide');
      swal("Success","Your Name has been changed!","success");
      
    }
    }
  });
  }
}


function bdates(bdate){
  if (bdate == "") 
  {
    $('#bdate').focus();
  }
  else
  {
  var mydata = 'action=update_bdate' + '&bdate=' + bdate;
   // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
      $('#edit_bdate_modal').modal('hide');
      swal("Success","Your Birthday has been changed!","success");
    }
    }
  });
  }
}

function email_add(email){
  if (email == "") 
  {
    $('#email').focus();
  }
  else
  {
  var mydata = 'action=update_email' + '&email=' + email;
   // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
     $('#edit_email_modal').modal('hide');
     swal("Success","Email has been changed!","success");
    }
    }
  });
  }
}

function usernames(username){
  if (username == "") 
  {
    $('#username').focus();
  }
  else
  {
  var mydata = 'action=update_username' + '&username=' + username;
   // alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      refresh_account();
      $('#edit_uname_modal').modal('hide');
      swal("Success","Username has been changed!","success");
    }
    else
    {
      swal("Opps","Invalid Username!","error");
    }
    }
  });
  }
}

function update_bdate(bdates){
  $('#bdate').val(bdates);
  $("#form-bd").show('fast');
  $("#choose").hide('fast');
}


function update_uname(uname){
  $('#username').val(uname);
  $("#form-username").show('fast');
  $("#choose").hide('fast');
}


function update_email(emails){
  $('#email').val(emails);
  $("#form-email").show('fast');
  $("#choose").hide('fast');
}

function cancel_updates(){
  $("#o_pwd").val('');
  $("#n_pwd").val('');
  $("#c_pwd").val('');
  $("#old").val('');
  $("#edit_pass_modal").modal('hide');
}


function update_pwd(index){
  $("#old").val(index);
  $("#form-pwd").show('fast');
  $("#choose").hide('fast');
}

function update_user(index){
  var fn,mn,ln,id,bdate,email;
  if (index == 'name') 
  {
  fn = $('#fn').val();
  mn = $('#mn').val();
  ln = $('#ln').val();
  id =$('#id').val();
  name(fn,mn,ln,id);
  }
  else if(index == 'bdate')
  {
  bdate = $("#bdate").val();
  bdates(bdate);
  }
  else if (index == 'email') 
  {
  email = $("#email").val();
  email_add(email);
  }
  else if (index == 'username') 
  {
  var username = $("#username").val();
  usernames(username);
  }
  else if (index == 'pwd') 
  {
  var o_pwd,n_pwd,c_pwd,old;
  o_pwd = $("#o_pwd").val();
  n_pwd = $("#n_pwd").val();
  c_pwd = $("#c_pwd").val();
  old = $("#old").val();
  change_pass(o_pwd,c_pwd,n_pwd,old);
  }
}

function change_pass(o_pwd,c_pwd,n_pwd,old){
  if (o_pwd == "") 
  {
    $("#o_pwd").focus();
  }
  else if (o_pwd != old) 
  {
    swal("Sorry","Password Doesn\'t Match !","error");
    $("#o_pwd").focus();
  }
  else if (n_pwd == "") 
  {
    $("#n_pwd").focus();
  }
  else if (c_pwd == "") 
  {
    $("#c_pwd").focus();
  }
  else if (c_pwd != n_pwd) 
  {
    swal("Sorry","Please confirm your password!","error");
    $("#c_pwd").focus();
  }
  else
  {
    var mydata = 'action=update_pwd' + '&pwd=' + n_pwd;
 // alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      // alert(data);
      if (data == 1) {
        // alert(data);
        refresh_account();
        $('#edit_pass_modal').modal('hide');
        swal("Success","Password has been changed!","success");
      }
      }
    });
  }
  
}

function uploading(){
  $(document).ready(function(){
  $(document).on('change', '#user_img', function(){
    var name = document.getElementById("user_img").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("user_img").files[0]);
    var f = document.getElementById("user_img").files[0];
    var fsize = f.size||f.fileSize;

      if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
      swal("Sorry","Invalid file format !","error");
        }
      else if(fsize > 2000000){
       swal("Sorry","File is too large !","error");
        }
      else
        {
        form_data.append("user_img", document.getElementById('user_img').files[0]);
        //alert(form_data);
        $.ajax({
          url:"uploadpic.php",
          method:"POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend:function(){
            $('#user_img_preview').html("<label class='text-success'>Image Uploading...</label>");
            },   
          success:function(data)
            {
            $("#btn-up").hide('fast');
            $('#user_img_preview').html(data);
            $("#btn_save_del").show('fast');
            }
            });
            }
            });
  });
}


 function load_pictures(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=update_picture',
      cache:false,
      success:function(data){
         //alert(data);
        $("#img_pf").html('<a href="#" onclick="pager(\'account_settings\');"> <img src="../img/img.png"  style="background-image: url('+data.trim()+');" class="w3-circle w3-hover-opacity img-album img-fluid w3-margin-bottom" width="100"></a>');
        $("#img_pf_pic").html('<img src="../img/img.png" style="background-image: url('+data.trim()+');" id="logo_img" class="ad img-album w3-circle animated bounceIn">');
        $("#profile_pic").html(' <img src="../img/img.png" style="background-image: url('+data.trim()+'); margin-bottom:5px;" class="img-album r r-2x img-full" width="220">')
       

      }
    });
  }

  function remove_image(path,img){
    var mydata = 'action=del_image' + '&path=' + path;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
        // alert(path); 
        if (data == 1) 
        {
          $("#btn-up").show('fast');
          $("#user_img").val('');
          $("#btn_save_del").hide('fast');
          $('#user_img_preview').html(' <img src="../'+img+'"  class="r r-2x img-full animated fadeInLeft" >');
        }
      }
    });
  }

  function save_image(path){
    alert(path);
    var mydata = 'action=save_image' + '&path=' + path;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#profile_pic").html('<br><br><center><img src="../img/load.gif" width="30" class="img-fluid w3-margin-bottom"></center>');
      },
      success:function(data){
      if (data == 1) 
      {
      swal("Success","Successfully Updated","success");
      load_pictures();
      $("#change_profile_pic").modal("hide"); 
      $("#user_img").val('');
      $("#btn_save_del").hide('fast');
      $("#btn-up").show('fast');
      }
      else{
         swal("Success","Successfully Updated","success");
          load_pictures();
      $("#change_profile_pic").modal("hide"); 
      $("#user_img").val('');
      $("#btn_save_del").hide('fast');
      $("#btn-up").show('fast');
      }
    }
  });
  }











/* 6/22/2018 */
  function music_list(){
      var search1 = $("#search1").val();
      var mydata = 'action=showplaylist' + '&search1=' + search1;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_playlist").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_playlist").html(data);},800); 
      }
      }); 
      }

  function mymusic(){
      var mymusic_search = $("#mymusic_search").val();
      var mydata = 'action=showmymusic' + '&mymusic_search=' + mymusic_search;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_mymusic").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_mymusic").html(data);},800); 
      }
      }); 
      }


  function genre_list2(){
      var search2a = $("#search2a").val();
      var mydata = 'action=showgenre2' + '&search2a=' + search2a;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_genre").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_genre").html(data);},800); 
      }
      }); 
      }


  function genre_list(){
      var search2 = $("#search2").val();
      var mydata = 'action=showgenre' + '&search2=' + search2;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_styles").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_styles").html(data);},800); 
      }
      }); 
      }


function sortsongs_desc(){
  $("#sort_id").val('1');
  songs_list(); 
}

function sortsongs_asc(){
  $("#sort_id").val('0');
  songs_list(); 
}

function composer_desc(){
  $("#composer_id").val('1');
  songs_list(); 
}

function composer_asc(){
  $("#composer_id").val('0');
  songs_list(); 
}  

function artist_desc(){
  $("#artist_id").val('1');
  songs_list(); 
}

function artist_asc(){
  $("#artist_id").val('0');
  songs_list(); 
}  

function sort_title(){
  var ascending = document.getElementById('ascending');
  var descending = document.getElementById('descending');
  var mydata = '&ascending=' + ascending.value + '&descending=' +descending.value;
  alert (mydata);
}


  

 function songs_list(){
      //var audience_id = document.getElementById('audience_id');
      loadapplications();
      var search3 = document.getElementById('search3');
      var show_applications = document.getElementById('show_applications');
      var mydata = 'action=showsongs' + '&search3=' + search3.value + '&show_applications=' + show_applications.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_songs").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_songs").html(data);},800); 
      }
      }); 
      }

 function songs_list2(){
      var show_applications = document.getElementById('show_applications');
      var search3a = document.getElementById('search3a');
      var mydata = 'action=showsongs2' + '&search3a=' + search3a.value + '&show_applications=' + show_applications.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_songs2").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_songs2").html(data);},800); 
      }
      }); 
      }

 function topic_list(){
      var search4 = $("#search4").val();
      var mydata = 'action=showtopics' + '&search4=' + search4;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_genres").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_genres").html(data);},800); 
      }
      }); 
      }

 function contributors_list(){
      var search5 = $("#search5").val();
      var mydata = 'action=showcontributors' + '&search5=' + search5;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_contributors").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_contributors").html(data);},800); 
      }
      }); 
      }

 function albums_list(){
      var search6 = $("#search6").val();
      var mydata = 'action=showalbumslist' + '&search6=' + search6;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_albums").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_albums").html(data);},800); 
      }
      }); 
      }



function song_info(id,song_name,composer,genre,application,album_name,duration,upload_date){
    $("#info_modal").modal('show');
    $("#info_id").val(id);
    $("#song_name_info").val(song_name);
    $("#composer_info").val(composer);
    $("#genre_info").val(genre);
    $("#info_app").val(application);
    $("#album_info").val(album_name);
    $("#info_duration").val(duration);
    $("#ud_info").val(upload_date);
    };

function song_info2(id,genre,song_name,composer,artist,album_name,music,upload_date){
    $("#info_modal2").modal('show');
    $("#info_id2").val(id);
    $("#song_name_info2").val(song_name);
    $("#composer_info2").val(composer);
    $("#album_info2").val(album_name);
    $("#artist_info2").val(artist);
    $("#genre_info2").val(genre);
    $("#path_info2").val(music);
    $("#ud_info2").val(upload_date);
    };



function audience_child(){
  $("#audience_id").val('1');
  $('input[name=aud_all]').attr('checked',false);
  $('input[name=aud_dis]').attr('checked',false);
  songs_list();
}

function audience_all(){
  $("#audience_id").val('0');
  $('input[name=aud_all]').attr('checked',true);
  $('input[name=aud_child]').attr('checked',false); 
  $('input[name=aud_dis]').attr('checked',false);
  songs_list();
}

function aud_disabled(){
  $("#audience_id").val('3');
   $('input[name=aud_dis]').attr('checked',true);
   $('input[name=aud_child]').attr('checked',false); 
  $('input[name=aud_all]').attr('checked',false);
   songs_list();
}

function audience_child1(){
  $("#audience_id").val('1');
  $('input[name=aud_all1]').attr('checked',false);
  $('input[name=aud_dis1]').attr('checked',false);
  songs_list2();
}

function audience_all1(){
  $("#audience_id").val('0');
  $('input[name=aud_all1]').attr('checked',true);
  $('input[name=aud_child1]').attr('checked',false); 
  $('input[name=aud_dis1]').attr('checked',false);
  songs_list2();
}

function aud_disabled1(){
  $("#audience_id").val('3');
   $('input[name=aud_dis1]').attr('checked',true);
   $('input[name=aud_child1]').attr('checked',false); 
  $('input[name=aud_all1]').attr('checked',false);
   songs_list2();
}


function audience_child2(){
  $("#audience_id").val('1');
  $('input[name=aud_all2]').attr('checked',false);
  $('input[name=aud_dis2]').attr('checked',false);
  composer_list();
}

function audience_all2(){
  $("#audience_id").val('0');
  $('input[name=aud_all2]').attr('checked',true);
  $('input[name=aud_child2]').attr('checked',false); 
  $('input[name=aud_dis2]').attr('checked',false);
  composer_list();
}

function aud_disabled2(){
  $("#audience_id").val('3');
   $('input[name=aud_dis2]').attr('checked',true);
   $('input[name=aud_child2]').attr('checked',false); 
  $('input[name=aud_all2]').attr('checked',false);
   composer_list();
}


function audience_child3(){
  $("#audience_id").val('1');
  $('input[name=aud_all3]').attr('checked',false);
  composer_list2();
}

function audience_all3(){
  $("#audience_id").val('0');
  $('input[name=aud_all3]').attr('checked',true);
  $('input[name=aud_child3]').attr('checked',false); 
  artist();
}

function audience_child4(){
  $("#audience_id").val('1');
  $('input[name=aud_all4]').attr('checked',false);
  artist();
}

function audience_all4(){
  $("#audience_id").val('0');
  $('input[name=aud_all4]').attr('checked',true);
  $('input[name=aud_child4]').attr('checked',false); 
  artist();
}

function audience_child5(){
  $("#audience_id").val('1');
  $('input[name=aud_all5]').attr('checked',false);
  artist2();
}

function audience_all5(){
  $("#audience_id").val('0');
  $('input[name=aud_all5]').attr('checked',true);
  $('input[name=aud_child5]').attr('checked',false); 
  artist2();
}

function audience_child6(){
  $("#audience_id").val('1');
  $('input[name=aud_all6]').attr('checked',false);
  style();
}

function audience_all6(){
  $("#audience_id").val('0');
  $('input[name=aud_all6]').attr('checked',true);
  $('input[name=aud_child6]').attr('checked',false); 
  style();
}

function audience_child7(){
  $("#audience_id").val('1');
  $('input[name=aud_all7]').attr('checked',false);
  style2();
}

function audience_all7(){
  $("#audience_id").val('0');
  $('input[name=aud_all7]').attr('checked',true);
  $('input[name=aud_child7]').attr('checked',false); 
  style2();
}

function audience_child8(){
  $("#audience_id").val('1');
  $('input[name=aud_all8]').attr('checked',false);
  date_list();
}

function audience_all8(){
  $("#audience_id").val('0');
  $('input[name=aud_all8]').attr('checked',true);
  $('input[name=aud_child8]').attr('checked',false); 
  date_list();
}

function audience_child9(){
  $("#audience_id").val('1');
  $('input[name=aud_all9]').attr('checked',false);
  date_list2();
}

function audience_all9(){
  $("#audience_id").val('0');
  $('input[name=aud_all9]').attr('checked',true);
  $('input[name=aud_child9]').attr('checked',false); 
  date_list2();
}

function show_composer(){
  $.ajax({
    type:"POST",
    url:"composer.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_composer").html(data);
    $("#search7").show();
    }
    });
    }

 function composer_list(){
      var show_applications = document.getElementById('show_applications');
      var search7 = document.getElementById('search7');
      var mydata = 'action=showcomposer' + '&search7=' + search7.value + '&show_applications=' + show_applications.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_composer").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_composer").html(data);},800); 
      }
      }); 
      }

function show_composer2(){
  $.ajax({
    type:"POST",
    url:"composer2.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_composer2").html(data);
    $("#search7a").show();
    }
    });
    }

 function composer_list2(){
      var show_applications = document.getElementById('show_applications');
      var search7a = document.getElementById('search7a');
      var mydata = 'action=showcomposer2' + '&search7a=' + search7a.value + '&show_applications=' + show_applications.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_composer2").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_composer2").html(data);},800); 
      }
      }); 
      }


function artist(){
      var audience_id = document.getElementById('audience_id');
      var artistsearch = document.getElementById('artistsearch');
      var mydata = 'action=showartist' + '&artistsearch=' + artistsearch.value + '&audience_id=' + audience_id.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_artist").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_artist").html(data);},800); 
      }
      }); 
      }

function artist2(){
      var audience_id = document.getElementById('audience_id');
      var artistsearch2 = document.getElementById('artistsearch2');
      var mydata = 'action=showartist2' + '&artistsearch2=' + artistsearch2.value + '&audience_id=' + audience_id.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_artist2").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_artist2").html(data);},800); 
      }
      }); 
      }

function library_style(){
      var show_applications = document.getElementById('show_applications');
      var stylesearch = document.getElementById('stylesearch');
      var mydata = 'action=showstyle' + '&stylesearch=' + stylesearch.value + '&show_applications=' + show_applications.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_style").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_style").html(data);},800); 
      }
      }); 
      }

function style2(){
      var show_applications = document.getElementById('show_applications');
      var stylesearch2 = document.getElementById('stylesearch2');
      var mydata = 'action=showstyle2' + '&stylesearch2=' + stylesearch2.value + '&show_applications=' + show_applications.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_style2").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_style2").html(data);},800); 
      }
      }); 
      }

function show_date(){
  $.ajax({
    type:"POST",
    url:"date_added.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_date_list").html(data);
    $("#search8").show();
    }
    });
    }

 function date_list(){
      var show_applications = document.getElementById('show_applications');
      var search8 = document.getElementById('search8');
      var mydata = 'action=showdate_list' + '&search8=' + search8.value + '&show_applications=' + show_applications.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_date_list").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_date_list").html(data);},800); 
      }
      }); 
      }

function show_date2(){
  $.ajax({
    type:"POST",
    url:"date_added2.php",
    data:"",
    cache:false,
    success:function(data){
    $("#tbl_date_list2").html(data);
    $("#search8a").show();
    }
    });
    }

 function date_list2(){
      var show_applications = document.getElementById('show_applications');
      var search8a = document.getElementById('search8a');
      var mydata = 'action=showdate_list2' + '&search8a=' + search8a.value + '&show_applications=' + show_applications.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_date_list2").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
      success:function(data){
      setTimeout(function(){$("#tbl_date_list2").html(data);},800); 
      }
      }); 
      }

 function description(){
      var description = document.getElementById('description');
      var mydata = 'action=showdescription' + '&description=' + description.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_description").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_description").html(data);},800); 
      }
      }); 
      }


function delete_music_list(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_musics(data);
    $('input[name=ml_title]:checked', '#ml_form').val()
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    } 

function delete_music_list2(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_musics2(data);
    $('input[name=ml_title2]:checked', '#ml_form2').val()
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    } 


function delete_music_list3(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_composer(data);
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    } 


function delete_music_list4(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_composer2(data);
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    } 

function delete_music_list5(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_date(data);
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    }


function delete_music_list6(audio_id,music){
  var mydata ='action=del_music_list' + '&audio_id=' + audio_id +'&music=' + music;
  //alert(mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Music has been deleted !", "success");
    show_date2(data);
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    }


function download_music(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_musics();
      swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function downloadall(id){
  //alert(id);
  var mydata = 'action=downloadall_music' + '&id=' + id;
  alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      //show_musics();
      swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });




}

/*function download_song(id,audio_id){
  alert(audio_id);
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_musics();
      swal("Music Library","Thank You For Downloading!","success");
    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}*/


function download_music2(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_musics2();
      swal("Music Library","Thank You For Downloading!","success");

    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function download_music3(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_composer();
      swal("Music Library","Thank You For Downloading!","success");

    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function download_music4(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_composer2();
      swal("Music Library","Thank You For Downloading!","success");

    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function download_music5(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_date();
      swal("Music Library","Thank You For Downloading!","success");

    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function download_music6(id){
  //alert(id);
  //d_id = document.getElementById('d_id');
  var current_loc = document.getElementById('current_loc');
  var mydata = 'action=download_music' + '&id=' + id + '&current_loc=' + current_loc.value;
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    // alert(data);
    if (data == 1) {
      show_date2();
      swal("Music Library","Thank You For Downloading!","success");

    }
    else
    {
      swal("Opps","Error On Downloading Music","error");
    }
    }
  });
}

function foot(){
    $("#foot").hide('fast');

  $('addmusic').on('hidden.bs.modal', function(){
    $('#btn-select2').removeClass('btn btn-secondary');
    document.getElementById('uploaded_songs').disabled= false;
    $('#btn-select2').addClass('btn btn-primary');
    $('#uploaded_songs').val('');
    $('#song_preview2').html('');
    $('#genres2').val('Genres');
    $('#album2').val('Album');

  })
}


/*2018/06/28*/


function contrisongs(id){
  $("#c_id").val(id);
  load_contri_info();
  load_contri_songs();
  load_contri_albums();
  $("#viewSongsModal").modal('show');

}

function load_contri_albums(){
  var conalbums = document.getElementById('conalbums');
  var c_id = document.getElementById('c_id');
  var mydata = 'action=contri_albums' + '&c_id=' + c_id.value + '&conalbums=' + conalbums.value;
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#contributor_albums").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#contributor_albums").html(data);},800); 
  }
  }); 
}

function load_contri_info(){
  var c_id = document.getElementById('c_id');
  var mydata = 'action=contri_details' + '&c_id=' + c_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#contri_info").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
  success:function(data){
  setTimeout(function(){$("#contri_info").html(data);},800); 
  }
  }); 
}

function load_contri_songs(){
  var consongs = document.getElementById('consongs');
  var c_id = document.getElementById('c_id');
  var mydata = 'action=contri_songs' + '&c_id=' + c_id.value + '&consongs=' + consongs.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#contributor_songs").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#contributor_songs").html(data);},800); 
  }
  }); 
}

/*2018/06/29*/


function view_album_audio(album_id)
{
  var mydata = 'action=show_album_audio' + '&Id=' + album_id;
  alert(mydata);
  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#load_album_audio").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      $("#album_audio_modal").modal('show');
      setTimeout(function(){$("#load_album_audio").html(data);},800);  
    }
  }); 
}










function viewalbum_modal(album_id){
  //alert(album_id);
  $("#album_song_id").val(album_id);
  showalbumsongs();
  $("#albumsong_modal").modal('show');
  load_album_info();
}

function load_album_info(){
  var album_song_id = document.getElementById('album_song_id');
  var mydata = 'action=con_album_details' + '&album_song_id=' + album_song_id.value;
   //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  success:function(data){
  setTimeout(function(){$("#conalbum_info").html(data);},800); 
  }
  }); 
}

function showalbumsongs(){
  var search_album_songs = document.getElementById('search_album_songs');
  var album_song_id = document.getElementById('album_song_id');
  var mydata = 'action=show_album_songs' + '&album_song_id=' + album_song_id.value + '&search_album_songs=' + search_album_songs.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:alb_url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#album_song").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#album_song").html(data);},800);  
    }
  }); 
}

  function application_list(){
      var app_search = $("#app_search").val();
      var mydata = 'action=showapplication' + '&app_search=' + app_search;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_app_list").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_app_list").html(data);},800); 
      }
      }); 
      }

  function application_list2(){
      var app_search2 = $("#app_search2").val();
      var mydata = 'action=showapplication2' + '&app_search2=' + app_search2;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_app_list2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_app_list2").html(data);},800); 
      }
      }); 
      }

function open_app(application_id){
  //alert(application_id);
  //alert(application_name);
  $('#app_id').val(application_id);
  load_app_song();
  //$('#app_name').val(application_name);
  $('#las1').show('fast');
  $('#las3').show('fast');

  $('#las2').hide('fast');
  $('#tbl_app_list').hide('fast');
  $('#app_song_search').show('fast');
  $('#app_song').show('fast');
  
  $('#app_search').hide('fast');
  $('#aps1').hide('fast');
  $('#aps2').hide('fast');
}

function open_app2(application_id){
  //alert(application_id);
  //alert(application_name);
  $('#app_id2').val(application_id);
  load_app_song2();
  //$('#app_name').val(application_name);
  $('#las1a').show('fast');
  $('#las3a').show('fast');
  $('#app_song_search2').show('fast');
  $('#app_song2').show('fast');
  
  $('#gridhide').hide('fast');
  $('#app_search2').hide('fast');
  $('#tbl_app_list2').hide('fast');
  $('#sehide').hide('fast');

  $('#aps2a').hide('fast');
}


function load_app_song(){
      var app_song_search = document.getElementById('app_song_search');
      var app_id = document.getElementById('app_id');
      var mydata = 'action=showapp_song' + '&app_song_search=' + app_song_search.value + '&app_id=' + app_id.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#app_song").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#app_song").html(data);},800); 
      }
      }); 
}


function load_app_song2(){
      var app_song_search2 = document.getElementById('app_song_search2');
      var app_id2 = document.getElementById('app_id2');
      var mydata = 'action=showapp_song2' + '&app_song_search2=' + app_song_search2.value + '&app_id2=' + app_id2.value;
      // alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#app_song2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#app_song2").html(data);},800); 
      }
      }); 
}


function loadapplications(){
var mydata = 'action=show_library_application';
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#showapplications").html('<br><br><center><img src="../img/loder1.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#showapplications").html(data);},800); 
}
});
}

function stylemusics (genre_name){
  //alert (genre_name);
  $("#stylename").val(genre_name);
  load_style_songs();
  getgeoloc();
  $("#stylemodal").modal('show');
}

function show_styles (genre_name,gen_id){
  //alert (genre_name);
  $("#stylename").val(genre_name);
  $("#style_id").val(gen_id)
  load_style_songs();
  load_style_info();
  $("#stylemodal").modal('show');
}

function load_style_info(){
  var gen_id = document.getElementById('style_id');
  var mydata = 'action=style_details' + '&gen_id=' + gen_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  success:function(data){
  setTimeout(function(){$("#style_header").html(data);},800); 
  }
  }); 
}

function load_style_songs(){
  var search_style_songs = document.getElementById('search_style_songs');
  //var stylename = document.getElementById('stylename');
  var style_id = document.getElementById('style_id');
  var mydata = 'action=showstylesongs' + '&style_id=' + style_id.value + '&search_style_songs=' + search_style_songs.value;
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#stylesong_list").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#stylesong_list").html(data);},800); 
  }
  }); 
}


function song_playlist()
{
  var plist_search = $("#plist_search").val();
  var mydata = 'action=show_song_plist' + '&plist_search=' + plist_search;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_song_plist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_song_plist").html(data);},800);  
    }
  }); 
}


function song_playlist2()
{
  var plist_search2 = $("#plist_search2").val();
  var mydata = 'action=show_song_plist2' + '&plist_search2=' + plist_search2;

  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_song_plist2").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_song_plist2").html(data);},800);  
    }
  }); 
}

function N_cancel(){
  $("#album_name").val('');
}

function cancelplist(){
  $("#newPlaylistModal").modal('hide');
  $("#playlist_name").val('');
}

function cancelplist2(){
  $("#newPlaylistModal2").modal('hide');
  $("#playlist_name2").val('');
  $("#addtoModal").modal('show');
}

function cancelpupdate(){
  $("#update_plist").modal('hide');
  $("#plist_name").val('');
}

function cplist(){
  $("#addtoModal").modal('hide');
  $("#newPlaylistModal2").modal('show');

}

function submit_playlist2()
{ 
  var playlist_name2 = document.getElementById('playlist_name2');
  if(playlist_name2 == '')  
  { 
    swal("Oops", "Please Enter Playlist Name!", "warning");  
  }  
  else 
  {
    var mydata = 'action=createplist2' + '&playlist_name2=' + playlist_name2.value;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#tblcplist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
      },
      success:function(data){
          swal("Success", "Playlist Created", "success");
          song_playlist();
          song_playlist2();
          $('#newPlaylistModal2').modal('hide');
          $('#playlist_name2').val('');
           $("#addtoModal").modal('show');
           load_add_to();
        
      }
    });
  }  
}


function submit_plist()
{ 
  var playlist_name = $('#playlist_name').val(); 
  if(playlist_name == '')  
  { 
    swal("Oops", "Please Enter Playlist Name!", "warning");  
    $("#playlist_name").focus();
    return false;  
  }  
  else 
  {
    var mydata = 'action=createplist' + '&playlist_name=' + playlist_name;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#tbl_song_plist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
      },
      success:function(data){
          swal("Success", "Playlist Created", "success");
          song_playlist();
          song_playlist2();
          $('#newPlaylistModal').modal('hide');
          $('#playlist_name').val('');
        
      }
    });
  }  
}

function edit_plist(id,name){
    $("#update_plist").modal('show');
    $("#pid").val(id);
    $("#plist_name").val(name);
    $("#plist_name").focus();    
    }

function submit_plist2()
{ 
  var pid = $('#pid').val();
  var plist_name = $('#plist_name').val(); 
  if(plist_name == '')  
  { 
    swal("Oops", "Please Enter Playlist Name!", "warning");  
    $("#plist_name").focus();
    return false;  
  }  
  else 
  {
    var mydata = 'action=updateplist' + '&plist_name=' + plist_name + '&pid=' + pid;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#tbl_song_plist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
      },
      success:function(data){
          swal("Success", "Successfully Updated", "success");
          song_playlist();
          song_playlist2();
          $('#update_plist').modal('hide');
          $('#plist_name').val('');
        
      }
    });
  }  
}

function delete_plist(id){
    var mydata ='action=del_plist'+'&id=' + id;
    swal({
    title: "Are you sure?",
    text: "Do you want to delete this Playlist?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    }, 
    function(){
    $.ajax({  
    url:url,  
    type:"POST",  
    data:mydata,
    cache:false,  
    success:function(data){  
    if(data != '')  
    {  
    swal("Deleted!", "Playlist has been deleted !", "success");
    song_playlist();
    song_playlist2();
    }
    else  
    {  
    return false;  
    }  
    
    }  
    });  
    });
    }

function add_to(audio_id){
  //alert(audio_id);
  $("#audio_id").val(audio_id)
  $("#addtoModal").modal('show');
  load_add_to();
}

function load_add_to(){
  var audio_id = document.getElementById('audio_id');
  var plistname = document.getElementById('plistname');
  var mydata = 'action=addto_plist' + '&audio_id=' + audio_id.value + '&plistname=' + plistname.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_addplist").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_addplist").html(data);},800); 
      }
      }); 
}




function c_plist(){
  $("#newPlaylistModal").modal('show');
  $("#addtoModal").modal('hide');
}

function addto_playlist2(id)
 { 
  var audio_id = document.getElementById('audio_id');
  var mydata = 'action=addtoplist' + '&audio_id=' + audio_id.value + '&id=' + id;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#tbl_song_plist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
      },
      success:function(data){
        //alert(data);
       if (data == 404) {
        swal("Error", "This Song is already exist in List!", "error");
       }
        else{
             swal("Success", "Song has been added to Playlist", "success");
          $("#addtoModal").modal('hide');
        }
      }
    });
  
}


function addto_playlist(id)
 { 
  var audio_id = $('#audio_id').val();
  if(audio_id == '')  
  { 
    swal("Oops", "Please Enter Playlist Name!", "warning");  
    $("#plist_name").focus();
    return false;  
  }  
  else 
  {
    var mydata = 'action=addtoplist' + '&audio_id=' + audio_id + '&id=' + id;
    //alert(mydata);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      $("#tbl_song_plist").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
      },
      success:function(data){
        //alert(data);
       if (data == 404) {
        swal("Error", "This Song is already exist in List!", "error");
       }
        else{
             swal("Success", "Song has been added to Playlist", "success");
          $("#addtoModal").modal('hide');
        }
      }
    });
  }  
}


function viewmy_plist(id,name){
  $("#plistid").val(id);
  $("#nameplist").val(name);
  $("#nameplist").html('<label style="text-transform: capitalize;"> '+name+' </label>');
  $("#p_list").show('fast');
   $("#plistid2").val(id);
  $("#plistname2").val(name);
  $("#pl1").hide('fast');
  $("#pl2a").hide('fast');
  $("#pl5").hide('fast');
  $("#plist_search").hide('fast');
  $("#tbl_song_plist").hide('fast');

  $("#pl4").show('fast');
  $("#pl3").show('fast');
  $("#pl6").show('fast');
  $("#tbl_plist_songs").show('fast');
  $("#plist_song_search").show('fast');
  load_plist_songs();
}

function viewmy_plist2(id,name){
  $("#plistid2").val(id);
  $("#plistname2").val(name);
  $("#plistname2").html('<label style="text-transform: capitalize;"> '+name+' </label>');
   $("#p_list2").show('fast');

  $("#pl5a").hide('fast');
  $("#pl1a").hide('fast');
  $("#plist_search2").hide('fast');
  $("#pl2a1").hide('fast');

  $("#pl4a").show('fast');
  $("#pl3a").show('fast');
  $("#plist_song_search2").show('fast');

  $("#tbl_song_plist2").hide('fast');
  $("#tbl_plist_songs2").show('fast');
  load_plist_songs2();
}

function load_plist_songs(){
      var plist_song_search = document.getElementById('plist_song_search');
      var plistid = document.getElementById('plistid');
      var plistname = document.getElementById('plistname');
      var mydata = 'action=showplist_songs' + '&plist_song_search=' + plist_song_search.value + '&plistid=' + plistid.value + '&plistname=' + plistname.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_plist_songs").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top:120px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_plist_songs").html(data);},800); 
      }
      }); 
}

function load_plist_songs2(){
      var plist_song_search2 = document.getElementById('plist_song_search2');
      //var plistid2 = document.getElementById('plistid2');
      //var plistname2 = document.getElementById('plistname2');
      var mydata = 'action=showplist_songs2' + '&plist_search2=' + plist_search2.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_plist_songs2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_plist_songs2").html(data);},800); 
      }
      }); 
}

function song_details(audio_id,album_id,user_id){
  //alert(audio_id);
  $("#aud_id").val(audio_id);
  $("#alb_id").val(album_id);
  $("#us_id").val(user_id);
  $("#song_details").modal('show');
  load_details();
}

function enable_tooltips(){
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
    $('[data-toggle="tooltip"]').tooltip(); 
});
}


function load_details(){
  var alb_id = document.getElementById('alb_id');
  var aud_id = document.getElementById('aud_id');
  var us_id = document.getElementById('us_id');
  var mydata = 'action=show_song_details' + '&alb_id=' + alb_id.value + '&aud_id=' + aud_id.value + '&us_id=' + us_id.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_det").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_det").html(data);},800);  
    }
  }); 
}


function song_details2(audio_id,album_id,user_id){
  //alert(audio_id);
  $("#aud_id2").val(audio_id);
  $("#alb_id2").val(album_id);
  $("#us_id2").val(user_id);
  $("#song_details2").modal('show');
  load_details2();
}

function load_details2(){
  var alb_id = document.getElementById('alb_id2');
  var aud_id = document.getElementById('aud_id2');
  var us_id = document.getElementById('us_id2');
  var mydata = 'action=show_song_details2' + '&alb_id=' + alb_id.value + '&aud_id=' + aud_id.value + '&us_id=' + us_id.value;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
      $("#tbl_det2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');
    },
    success:function(data){
      setTimeout(function(){$("#tbl_det2").html(data);},800);  
    }
  }); 
}


 function load_children_songs(){
      var search_child_songs = $("#search_child_songs").val();
      var mydata = 'action=show_child_songs' + '&search_child_songs=' + search_child_songs;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs").html(data);},800); 
      }
      }); 
      }

 function load_children_songs2(){
      var search_child_songs2 = $("#search_child_songs2").val();
      var mydata = 'action=show_child_songs2' + '&search_child_songs2=' + search_child_songs2;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs2").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs2").html(data);},800); 
      }
      }); 
      }

 function load_children_songs3(){
      var search_child_songs3 = $("#search_child_songs3").val();
      var mydata = 'action=show_child_songs3' + '&search_child_songs3=' + search_child_songs3;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs3").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs3").html(data);},800); 
      }
      }); 
      }

 function load_children_songs4(){
      var search_child_songs4 = $("#search_child_songs4").val();
      var mydata = 'action=show_child_songs4' + '&search_child_songs4=' + search_child_songs4;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs4").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs4").html(data);},800); 
      }
      }); 
      }

 function load_children_songs5(){
      var search_child_songs5 = $("#search_child_songs5").val();
      var mydata = 'action=show_child_songs5' + '&search_child_songs5=' + search_child_songs5;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs5").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs5").html(data);},800); 
      }
      }); 
      }

 function load_children_songs6(){
      var search_child_songs6 = $("#search_child_songs6").val();
      var mydata = 'action=show_child_songs6' + '&search_child_songs6=' + search_child_songs6;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_child_songs6").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_child_songs6").html(data);},800); 
      }
      }); 
      }




function rem_on_list(audio_id,id){
    var mydata ='action=removefromlist'+'&audio_id=' + audio_id + '&id=' + id ;
    //alert(mydata);
    swal({
    title: "Are you sure?",
    text: "Do you want to remove this song?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    }, 
    function(){
    $.ajax({  
    url:url,  
    type:"POST",  
    data:mydata,
    cache:false,  
    success:function(data){  
    if(data != '')  
    {  
    swal("Deleted!", "Song has been removed!", "success");
    load_plist_songs();
    song_playlist2();
    load_playlist_info();
    song_playlist()
    }
    else  
    {  
    return false;  
    }  
   
    }  
    });  
    });
    }

   

    function rem_on_list2(audio_id,id){
    var mydata ='action=removefromlist'+'&audio_id=' + audio_id + '&id=' + id ;
    //alert(mydata);
    swal({
    title: "Are you sure?",
    text: "Do you want to remove this song?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    }, 
    function(){
    $.ajax({  
    url:url,  
    type:"POST",  
    data:mydata,
    cache:false,  
    success:function(data){  
    if(data != '')  
    {  
    swal("Deleted!", "Song has been removed!", "success");
     load_plist_songs2();
    }
    else  
    {  
    return false;  
    }  
   
    }  
    });  
    });
    }

function addplistsongs(){
  //alert(name);
  $("#playlistsongsmodal").modal('hide');
  $("#addplistsongs").modal('show');
  load_ptitle();
  //load_plistcbox();
  load_tbl_audios();
}


function load_ptitle(){
  var plistid = document.getElementById('plistid');
  var plistmodaltitle = document.getElementById('plistmodaltitle');
  var mydata = '&action=show_plist_name' + '&plistmodaltitle=' + plistmodaltitle.value + '&plistid=' + plistid.value;
  $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      setTimeout(function(){$("#plist_title").html(data);},800); 
      }
      }); 
      }

/*function load_plistcbox(){
  var splist = $("#splist").val();
  var mydata = 'action=show_plist_cbox' + '&splist=' + splist;
    //alert(mydata);
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){$("#plistcbox").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
    success:function(data){
    setTimeout(function(){$("#plistcbox").html(data);},800); 
    }
    });  s
    }*/

function savemulti(){
  var audio_id = [];
  var newplistid = $("#newplistid").val();
  //alert(newplistid);
  $(':checkbox:checked').each(function(i){
    audio_id[i] = $(this).val();
   });
   
   if(audio_id.length === 0) //tell you if the array is empty
   {
    swal("Error", "Please Select Song", "warning");
   }
   else
   {
    $.ajax({
     url:'savemulti.php',
     method:"POST",
     data:{audio_id:audio_id,newplistid:newplistid},
     success:function(data)
     {
      //alert(data);
      if (data == 404) 
      {
      swal("Success", "Song has been added to Playlist", "success");
      load_tbl_audios();
      $("#playlistsongsmodal").modal('show');
      $("#addplistsongs").modal('hide');
      stopplay8();
      }
      else{
      swal("Success", "Song has been added to Playlist", "success");
      load_playlist_songs()
          load_tbl_audios();
          //load_plist_songs();
          load_playlist_info();
          song_playlist();
          song_playlist2();
          stopplay8();
          $("#playlistsongsmodal").modal('show');
          $("#addplistsongs").modal('hide');
      }
     }
     
    });
   }
}




function addmultiplesongs(){
  var p_ids = document.getElementById('p_ids');
  var plistid = document.getElementById('plistid');
    
    var mydata = 'action=select_song' + '&p_ids=' + p_ids.value + '&plistid=' + plistid.value;
      alert(mydata);
      $.ajax({
      url: url,
      method: "POST",
      data:mydata,
      beforeSend:function(){
      $("#plistcbox").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');
      },
      success:function(data){
      //alert(data);
      if (data == 404) {
      swal("Error", "This Song is already exist in List!", "error");
      load_plistcbox();
      }
      else{
      swal("Success", "Song has been added to Playlist", "success");
      $("#addplistsongs").modal('hide');
      load_plist_songs();
      }
      }
      });
      }

     
function select_songs(index){
  //alert(audio_id);
var plistid = document.getElementById('plistid');
var mydata ='action=save_selected_song'+'&id=' + index + '&plistid=' + plistid.value; 
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){
$("#plistcbox").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');
},
success:function(data){
//alert(data);
if (data == 404) {
swal("Error", "This Song is already exist in List!", "error");
load_tbl_audios();

}
else{
swal("Success", "Song has been added to Playlist", "success");
//load_plistcbox();
load_tbl_audios();
load_plist_songs();
load_playlist_info();
song_playlist();
song_playlist2();
}
}
});
}

function emptyplist(){
  var playlist_ = document.getElementById('v_pid');
  var mydata ='action=clear_songs' + '&v_pid=' + playlist_.value;
  swal({
    title: "Are you sure?",
    text: "Do you want to delete this Music?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Playlist has been deleted !", "success");
    show_my_playlist();
    }
    else
    {
      alert(data);
    }
    }
    });
    });
    } 



function p_music_list(){
var search_song = document.getElementById('search_song');
var albid = document.getElementById('ab-id');
var mydata = 'action=show_music_playlist' + '&albid=' + albid.value + '&search_song=' + search_song.value;
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#display_musics").html('<td colspan="7" class="text-center" style="margin-top: 10px;"><img src="../img/giphy.gif" class="img-fluid" width="30"></td>');},
success:function(data){
setTimeout(function(){$("#display_musics").html(data);},800); 
}
}); 
}


function load_dropdown_alb(){
var mydata = 'action=show_dropdown_alb';
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#dropdownalbum").html('<br><br><center><img src="../img/loder1.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#dropdownalbum").html(data);},800); 
}
});
}

function load_dropdown_style(){
var mydata = 'action=show_dropdown_style';
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#dropdownstyle").html('<br><br><center><img src="../img/loder1.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#dropdownstyle").html(data);},800); 
}
});
}

function load_dropdown_aud(){
var mydata = 'action=show_dropdown_aud';
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#dropdownaud").html('<br><br><center><img src="../img/loder1.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#dropdownaud").html(data);},800); 
}
});
}

function load_dropdown_app(){
var mydata = 'action=show_dropdown_app';
//alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#dropdownapp").html('<br><br><center><img src="../img/loder1.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#dropdownapp").html(data);},800); 
}
});
}

function load_dropdowns(){
  load_dropdown_alb();
  load_dropdown_style();
  load_dropdown_aud();
  load_dropdown_app();
}

function view_playlist_songs(id,name,nums){
  //alert(id);
  //alert(name);
  //alert(nums);
  //$("#songnumber").val(nums);  
  //$("#modal_pl_title").val(name);
  $("#plistid").val(id);
  $("#playlistsongsmodal").modal('show');
  load_playlist_songs();
  load_playlist_info();
  load_select_style();
}

function load_playlist_songs(){
      var plistid = document.getElementById('plistid');
      var plist_song_search = document.getElementById('plist_song_search');
      var mydata = 'action=show_playlist_songs' + '&plist_song_search=' + plist_song_search.value + '&plistid=' + plistid.value;
      //alert(mydata);
      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#tbl_plist_songs").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#tbl_plist_songs").html(data);},800); 
      }
      }); 
}




function empty_playlist(){
  var plistid = document.getElementById('plistid');
  var mydata ='action=clear_playlist_songs' + '&plistid=' + plistid.value;
  //alert (mydata);
  swal({
    title: "Are you sure?",
    text: "Do you want to Empty this Playlist?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes",
    closeOnConfirm: false
    },
  function(){
    $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
    swal("Deleted!", "Songs has been deleted !", "success");
    load_playlist_songs();
    load_playlist_info();
    load_plist_songs();
    song_playlist();
    song_playlist2();
    }
    else
    {
      swal("Error", "No Items Deleted", "info");
    }
    }
    });
    });
    } 


function load_playlist_info(){
  var plistid = document.getElementById('plistid');
  var mydata = 'action=load_playlist_info' + '&plistid=' + plistid.value;
   //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  success:function(data){
  setTimeout(function(){$("#playlist_header").html(data);},800); 
  }
  }); 
}

function load_select_style(){
  var mydata = 'action=show_select_style';
  //alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#select_style").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
  success:function(data){
  setTimeout(function(){$("#select_style").html(data);},800); 
  }
  }); 
}

 

function load_tbl_audios(){
  var splist = document.getElementById('splist');
  var select_style = document.getElementById('select_style');
  var mydata = 'action=load_tbl_audio' +  '&select_style=' + select_style.value + '&splist=' + splist.value;
  //alert(mydata);

      $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){$("#plistcbox").html('<br><br><center><img src="../img/giphy.gif" class="img-fluid" width="30" style="padding-bottom:15px; margin-top: 120px;"></center>');},
      success:function(data){
      setTimeout(function(){$("#plistcbox").html(data);},800); 
      }
      }); 
      }



/*
function load_audio2(){
var genres = document.getElementById('genres2');
var mydata = 'action=load_audio2' + '&genres2=' + genres.value;
alert(mydata);
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
beforeSend:function(){$("#plistcbox").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="30"></center>');},
success:function(data){
setTimeout(function(){$("#plistcbox").html(data);},800); 
}
}); 
}*/

function closeupload(){
  $("#song_name2").val('');
  $("#composer2").val('');
  $("#artist2").val('');
}


function Load_Contributor(){
  play_list(play_now);  
  play_list2(play_now2);  
  load_dropdowns(); 
  getgeoloc(); 
  load_pictures(); 
  uploading(); 
  upload_file(); 
  upload_picture(); 
  upload_picture2();  
  upload_music(); 
  upload_music22(); 
  foot(); 
  multi_upload_music(); 
  multi_upload_file();
}