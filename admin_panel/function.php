<?php 
session_start();
include_once("../dbconnection.php");
include_once("classes.php");
$myid = $_SESSION['admin'];

$action = mysqli_real_escape_string($con,$_POST['action']);
switch ($action) {

	case 'edit_file':
	$file_id = mysqli_real_escape_string($con,$_POST['file_id']);
	$fn = mysqli_real_escape_string($con,$_POST['file_name']);
	$fd = mysqli_real_escape_string($con,$_POST['file_desc']);
	$ln= mysqli_real_escape_string($con,$_POST['link_name']);
	$link = mysqli_real_escape_string($con,$_POST['link']);
	edit_supporting_file($con, $file_id, $fn, $fd, $ln, $link);
	break;

	case 'remove_file':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	unlink($path);
	remove_file($con,$path,$id);
	echo 1;
	break;

	case 'gen_groupid':
	gen_groupid($con);
	break;

	case 'get_dateupload':
	$songid =  mysqli_real_escape_string($con,$_POST["songid"]);
	get_dateupload($con,$songid);
	break;

	case 'delete_musics':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		$sql = mysqli_query($con,"DELETE FROM tbl_audios where audio_id='$id'");
		if ($sql) {
			echo 1;
		}
	break;

	case 'all_approve_music':
	$date_ = date('Y-m-d');
	$date_today = date('Y-m-d H:i:s');
	$sql = mysqli_query($con,"UPDATE tbl_audios set active = 1, approved_date = '$date_today' where active = 0 and upload_date <= '$date_'");
	if ($sql) {
		echo 1;
	}
	break;

	case 'music_library_tbl':
	$search_music = mysqli_real_escape_string($con,$_POST['search_music']);
	show_music_library($con,$search_music);
	break;

	case 'load_album_selection':
	$searchalbums =  mysqli_real_escape_string($con,$_POST["searchalbums"]);
	load_album_selection($con,$searchalbums);
	break;

	
	case 'insert_genre':
	$genre = mysqli_real_escape_string($con,$_POST['genre']);
	$gen_id = mysqli_real_escape_string($con,$_POST['gen_id']);
	insert_genre($con,$genre,$gen_id);
	break;
	
	case 'show_supporting_files':
	$id = mysqli_real_escape_string($con,$_POST['group_id']);
	show_supporting_files($con,$id);
	break;
	
	case 'show_style_select':
	$style =  mysqli_real_escape_string($con,$_POST["style_value"]);
	load_style_select($con,$style);
	break;

	case 'show_app_select':
	$app =  mysqli_real_escape_string($con,$_POST["app_value"]);
	load_app_select($con,$app);
	break;
	
	case 'insert_app':
	$app_name = mysqli_real_escape_string($con,$_POST['app_name']);
	$app_id = mysqli_real_escape_string($con,$_POST['app_id']);
	insert_app($con,$app_name,$app_id);
	break;

	case 'insert_audience':
	$audience_type = mysqli_real_escape_string($con,$_POST['audience_type']);
	$audience_id = mysqli_real_escape_string($con,$_POST['audience_id']);
	insert_audiences($con,$audience_type,$audience_id);
	break;
	
	case 'show_genre':
	display_genre($con);
	break;

	case 'show_aud':
	display_audiences($con);
	break;

	case 'show_apps':
	display_app($con);
	break;

	case 'del_genre':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_genre($con,$id);
	break;

	case 'del_aud':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_auds($con,$id);
	break;
	
	case 'del_app':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_application($con,$id);
	break;
	
	case 'update_account':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
	$mn = mysqli_real_escape_string($con,$_POST['mn']);
	$ln = mysqli_real_escape_string($con,$_POST['ln']);
	update_name($con,$id,$fn,$mn,$ln);
	break;

	case 'update_bdate':
	$bdate = mysqli_real_escape_string($con,$_POST['bdate']);
	update_bdate($con,$bdate,$myid);
	break;

	case 'update_email':
	$email = mysqli_real_escape_string($con,$_POST['email']);
	update_email($con,$email,$myid);
	break;

	case 'update_username':
	$username = mysqli_real_escape_string($con,$_POST['username']);
	update_username($con,$username,$myid);
	break;
	
	case 'update_pwd':
	$pwd = mysqli_real_escape_string($con,$_POST['pwd']);
	update_pwd($con,$pwd,$myid);
	break;
	
	case 'refresh_account':
	refresh_account($con,$myid);
	break;

	case 'show_users':
	$search = mysqli_real_escape_string($con,$_POST['search']);
	$start = mysqli_real_escape_string($con,$_POST['start']);
	$limit = mysqli_real_escape_string($con,$_POST['limit']);
	$page = mysqli_real_escape_string($con,$_POST['page']);
	show_accounts($con,$search,$start,$limit,$page);
	break;

	case 'update_user':
	$user = mysqli_real_escape_string($con,$_POST['user']);
	$uid = mysqli_real_escape_string($con,$_POST['uid']);
	update_user($con,$user,$uid);
	break;

	case 'del_image':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	unlink($path);
	echo 1;
	break;

	case 'update_picture':
	pict_session($con,$myid);
	break;

	case 'save_image':
	if ($_SESSION['user_pic'] == "../avatar/def.png") {
	$path = mysqli_real_escape_string($con,$_POST['path']);
	update_pf($con,$path,$myid);
	}
	else
	{
	unlink($_SESSION['user_pic']);
	$path = mysqli_real_escape_string($con,$_POST['path']);
	update_pf($con,$path,$myid);
	}
	
	break;

	case 'sound_audio':
		$id = $_POST['id'];
		$num = 1;
		$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");


		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
		break;

	case 'get_max_au':
		$sql = mysqli_query($con,"SELECT count(id) from tbl_audios");
		if (mysqli_num_rows($sql)>0) {
		$row = mysqli_fetch_assoc($sql);
		echo $row['count(id)'];
		}
	break;

	case 'get_array_set':
	$num = 1;
	$sql = mysqli_query($con,"SELECT * from tbl_audios");
	while ($row = mysqli_fetch_assoc($sql)) {
		echo $row['id'].',';
	}
	break;

	//oji function

	case 'download_music':
	$search = mysqli_real_escape_string($con,$_POST['search_musicd']);
	$filter = mysqli_real_escape_string($con,$_POST['filter_download']);
	$top = mysqli_real_escape_string($con,$_POST['top']);
	download_music($con,$search,$filter,$top);
	break;

	case 'download_details':
	$id = mysqli_real_escape_string($con,$_POST['aud_id']);
	download_details($con,$id);
	break;

	case 'new_uploads':
	$search = mysqli_real_escape_string($con,$_POST['search_upload']);
	new_uploads($con,$search);
	break;

	case 'upload_details':
	$id = mysqli_real_escape_string($con,$_POST['up_id']);
	upload_details($con,$id);
	break;

	case 'accept_song':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	accept_song($con,$aud);
	break;

	case 'disapprove_song':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	disapprove_song($con,$aud);
	break;

	case 'load_album_details':
	$id = mysqli_real_escape_string($con,$_POST['album_id1']);
	load_album_details($con,$id);
	break;

	case 'load_albums':
	$search = mysqli_real_escape_string($con,$_POST['search_album']);
	load_albums($con,$search);
	break;

	case 'update_album':
	$name = mysqli_real_escape_string($con,$_POST['e_album_name']);
	$id = mysqli_real_escape_string($con,$_POST['a_id']);
	update_album($con,$name,$id);
	break;

	case 'delete_album':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_album($con,$id);
	break;

	case 'load_mymusic':
	$search = mysqli_real_escape_string($con,$_POST['search_music2']);
	$status = mysqli_real_escape_string($con,$_POST['sort_status']);
	load_mymusic($con,$search,$status);
	break;

	case 'upload_song':
	$link2 = mysqli_real_escape_string($con,$_POST['link2']);
	$song_name = mysqli_real_escape_string($con,$_POST['song_name']);
	$composer = mysqli_real_escape_string($con,$_POST['composer']);
	$genres = mysqli_real_escape_string($con,$_POST['genres']);
	//$artist = mysqli_real_escape_string($con,$_POST['artist']);
	$album = mysqli_real_escape_string($con,$_POST['album']);
	$audience = mysqli_real_escape_string($con,$_POST['audience']);
	$application = mysqli_real_escape_string($con,$_POST['application']);
	$duration = mysqli_real_escape_string($con,$_POST['duration']);
	$ud = date('Y-m-d');
	upload_music($con,$link2,$song_name,$composer,$genres,$album,$ud,$audience,$application,$duration);
	break;

	case 'post_music':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	post_music($con,$aud);
	break;

	case 'unpost_music':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	unpost_music($con,$aud);
	break;

	/*case 'delete_music':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	delete_music($con,$aud);
	break; */

	case 'delete_music':
	$aud = mysqli_real_escape_string($con,$_POST['aud']);
	$music = mysqli_real_escape_string($con,$_POST['music']);
	unlink('../'.$music);
	delete_music($con,$aud,$music);
	break;
	
	case 'update_music':
	$song_name = mysqli_real_escape_string($con,$_POST['song_name']);
	$composer = mysqli_real_escape_string($con,$_POST['composer']);
	$genre = mysqli_real_escape_string($con,$_POST['genre']);
	$albumname = mysqli_real_escape_string($con,$_POST['albumname']);
	$audiencename = mysqli_real_escape_string($con,$_POST['audiencename']);
	$applicationname = mysqli_real_escape_string($con,$_POST['applicationname']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	update_music($con,$song_name,$composer,$genre,$albumname,$audiencename,$applicationname,$id);
	break;	

	case 'app_disapp':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	app_disapp($con,$id);
	break;

	case 'subcribe_app_disapp':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	subcribe_app_disapp($con,$id);
	break;

	case 'mng_client_password':
	$user_id = mysqli_real_escape_string($con,$_POST['user_id']);
	$pwd = mysqli_real_escape_string($con,$_POST['mng_pwd']);
	edit_client_pwd($con,$user_id,$pwd);
	break;

	case 'update_client_info':
	$user_id = mysqli_real_escape_string($con,$_POST['user_id']);
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
	$ln = mysqli_real_escape_string($con,$_POST['ln']);
	$email = mysqli_real_escape_string($con,$_POST['email']);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  echo 3;
	}
	else
	{
		update_client_informaion($con,$fn,$ln,$email,$user_id);
	}
	break;

	case 'subscribers':
	$search = mysqli_real_escape_string($con,$_POST['search_subs']);
	subscribers($con,$search);
	break;

	case 'load_subs_info':
	$id = mysqli_real_escape_string($con,$_POST['sub_id']);
	load_subs_info($con,$id);
	break;

	case 'add_user_client':
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
	$ln = mysqli_real_escape_string($con,$_POST['ln']);
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$user_type = mysqli_real_escape_string($con,$_POST['user_type']);

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		echo 3;
	}
	else if (exist_user($con,$email) > 0) {
	echo 2;
	}
	else 
	{
	add_direct_client($con,$fn,$ln,$email,$user_type);
	}
	break;

	case 'delete_user':
	$user_id = mysqli_real_escape_string($con,$_POST['user_id']);
	delete_users($con,$user_id);
	break;


	case 'show_style':
	show_style($con);
	break;

	case 'show_album':
	show_album($con);
	break;

	case 'show_audience':
	show_audience($con);
	break;

	case 'show_app':
	show_app($con);
	break;	

	case 'music_details':
	$music_id = mysqli_real_escape_string($con,$_POST['music_id']);
	music_details($con, $music_id);
	break;

	case 'about_content':
	about_content($con);
	break;

	case 'update_about_header':
	$id = mysqli_real_escape_string($con,$_POST['header_id']);
	$header = mysqli_real_escape_string($con,$_POST['new_header']);
	update_about_header($con, $id, $header);
	break;

	case 'update_about_body':
	$id = mysqli_real_escape_string($con,$_POST['body_id']);
	$body = mysqli_real_escape_string($con,$_POST['new_body']);
	update_about_body($con, $id, $body);
	break;

	case 'faqs_content':
		faqs_content($con);
	break;

	case 'save_faqs':
	$title = mysqli_real_escape_string($con,$_POST['title']);
	$content = mysqli_real_escape_string($con,$_POST['content']);
	save_faqs($con,$title,$content);
	break;

	case 'update_faqs':
	$id = mysqli_real_escape_string($con,$_POST['faqs_id']);
	$title = mysqli_real_escape_string($con,$_POST['up_title']);
	$content = mysqli_real_escape_string($con,$_POST['up_content']);
	update_faqs($con,$id,$title,$content);
	break;

	case 'load_update_faqs':
		$id = mysqli_real_escape_string($con,$_POST['faqs_id']);
		load_update_faqs($con,$id);
	break;

	case 'delete_faqs':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		delete_faqs($con,$id);
	break;

	case 'load_update_body':
		$id = mysqli_real_escape_string($con,$_POST['body_id']);
		load_update_body($con,$id);
	break;

	case 'load_update_header':
		$id = mysqli_real_escape_string($con,$_POST['header_id']);
		load_update_header($con,$id);
	break;

	case 'music_played':
	$search = mysqli_real_escape_string($con,$_POST['search_plays']);
	$top = mysqli_real_escape_string($con,$_POST['top_plays']);
	music_played($con,$search,$top);
	break;

	case 'save_play':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		save_play($con,$id);
	break;

	case 'load_played_details':
	$id = mysqli_real_escape_string($con,$_POST['id_song']);
	load_played_details($con,$id);
	break;

	case 'load_user_logs':
	$search = mysqli_real_escape_string($con,$_POST['search_logs']);
	load_user_logs($con,$search);
	break;
}
