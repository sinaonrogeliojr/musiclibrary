<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
  <script src="https://www.w3schools.com/lib/w3.js"></script>
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="admin.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
 <style type="text/css">

.imgs2 {
width: 50px; /* You can set the dimensions to whatever you want */
height: 50px;
object-fit: cover;
}
	/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555; 
}

#main-div{
	margin-left: 100px;
}

#view_profile{

}
#btn_bars{
	display: none;
}

.hide_th{

}
@media only screen and (max-width: 600px) {
	.sidenav{
	display: none;
	}
	#main-div{
	margin-left: 0px;
	}
	#view_profile{
	display: none;
	}
	#btn_bars{
	display: block;
	}
	.tracks{
		font-size: 10px;
	}
	.imgs {
    width: 50px; /* You can set the dimensions to whatever you want */
    height: 50px;
    object-fit: cover;
	}
	.labels{
		font-size: 12px;
	}
	.hide_th{
	display: none;
	}
}

th {
    cursor: pointer;
}

th, td {
    text-align: left;
    padding: 16px;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

  </style>
</head>
<body onload="user_logs(); load_pictures();">

<div class="sidenav collapse show" id="view_sidebar">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="User Logs"><a href="user_logs.php"><span class="fa fa-users fa-3x" style="color: #8eadab;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
		</li>
	</ul>
</div>

<div class="main" id="main-div">
<div class="container-fluid hero hero-db hero-admin ">
	<div class="row header">
		 
		<div class="col-lg-3 col-2 head">
			<a href="#" onclick="user_logs();"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<button type="button" class="btn btn-dark btn-collapse" id="btn_bars">
		  <span class="fa fa-bars text-white" data-toggle="collapse" data-target="#view_sidebar"></span>
		</button>
		<div class="col-lg-9 head" id="view_profile">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title">User Logs</p>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-4 input"><input type="show" oninput="user_logs();" name="search_logs" id="search_logs" placeholder="Search Users..."></div>
	</div>
</div>

<div class="container-fluid tracks-table">
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover table-light table-responsive-sm" id="user_logs">
				<thead class="thead-dark">
					<tr id="labels">
						<th></th>
						<th onclick="w3.sortHTML('#user_logs', '.item', 'td:nth-child(1)')" style="cursor:pointer;">Name</th>
						<th onclick="w3.sortHTML('#user_logs', '.item', 'td:nth-child(2)')" style="cursor:pointer;">User Type</th>
						<th onclick="w3.sortHTML('#user_logs', '.item', 'td:nth-child(3)')" style="cursor:pointer;">Last Login</th>
					</tr>	
				</thead>
				<tbody id="tbl_user_logs">
					
				</tbody>
			</table>
		</div>
	</div>
</div>

<footer class="container-fluid">
<?php include('../footer.php'); ?>
</footer>
</div>
</body>
</html>
