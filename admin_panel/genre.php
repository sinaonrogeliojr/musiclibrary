<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
	
  </style>
</head>
<body onload="user_account(); load_pictures(); show_genre(); show_application(); show_audience_now(); refresh_account(); load_pictures();">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title"><i class="fa fa-lg fa-gears"></i> Options</p>
		</div>
	</div>
	<hr>
	<div class="row" style=" margin-top: -60px;">
		<div class="col-lg-4 w3-round" style="">
			<h3 class="text-white glass w3-padding w3-round">Genres</h3>
			<div class="form-group">
				<div class="input-group">
					<span  class="input-group-addon btn btn-danger bg-danger text-white btn-small" id="btn_cancel" style="display: none; width: 25%;" onclick="cancel_update();" >Cancel</span>
					<input type="hidden" name="gen_id" id="gen_id">
					<input type="show" name="genre" id="genre" class="form-control" placeholder="Enter Genre...">
					<span class="input-group-addon btn btn-primary bg-primary text-white btn-small" style="width: 25%;" onclick="save_genre();">Save</span>
				</div>
			</div>
			<center><span id="loader"></span></center>
			<div id="tbl_genres"></div>

		</div>

		<div class="col-lg-4 w3-round" style="">
			<h3 class="text-white glass w3-padding w3-round">Application</h3>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon btn btn-danger bg-danger text-white text-center btn-small" id="btn_cancel_app" style="display: none; width: 25%;" onclick="cancel_update_app();">Cancel</span>
					<input type="hidden" name="app_id" id="app_id">
					<input type="show" name="app_name" id="app_name" class="form-control" placeholder="Enter application...">
					<span class="input-group-addon btn btn-primary bg-primary text-white btn-small" style="width: 25%;" onclick="save_application();">Save</span>
				</div>
			</div>
			<center><span id="loader"></span></center>
			<div id="tbl_app"></div>
		</div>

		<div class="col-lg -4 w3-round" >
			<h3 class="text-white glass w3-padding w3-round">Audience</h3>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon btn btn-danger bg-danger text-white btn-small" id="btn_cancel_aud" style="display: none;" onclick="cancel_update_aud();" style="width: 25%;">Cancel</span>
					<input type="hidden" name="audience_id" id="audience_id">
					<input type="show" name="audience_type" id="audience_type" class="form-control" placeholder="Enter audience...">
					<span class="input-group-addon btn btn-primary bg-primary text-white btn-small" style="width: 25%;"  onclick="save_audience();">Save</span>
				</div>
			</div>
			<center><span id="loader"></span></center>
			<div id="tbl_audience"></div>
		</div>
	</div>
</div>

<footer class="container-fluid">
<?php include('../footer.php'); ?>
</footer>
</div>
</body>
</html>
