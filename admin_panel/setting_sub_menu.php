<div id="management" class="collapse">
	<li class="w3-hover-shadow" title="Manage Genres, Application and Audience" style="background: #616f7f;" onclick="window.location='genre.php'">
		<a href="#management" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-microphone fa-2x" ></i>
		</a>
	</li>
	<li class="w3-hover-shadow" title="Manage About Content" style="background: #616f7f;" onclick="window.location='about.php'">
		<a href="#management" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-info fa-2x" ></i>
		</a>
	</li>
	<li class="w3-hover-shadow" title="Manage FAQ's Content" style="background: #616f7f;" onclick="window.location='faqs.php'">
		<a href="#management" data-toggle="collapse" style="color: #f1f1f1;">
		<i class="fa fa-question-circle fa-2x" ></i>
		</a>
	</li>
</div>