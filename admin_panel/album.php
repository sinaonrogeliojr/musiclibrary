<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
   <script type="text/javascript" src="../js/audio-index.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
  	.img_music {
    width: 60px; /* You can set the dimensions to whatever you want */
    height:60px;
    object-fit: cover;
	}
	.img_music2 {
    width: 200px; /* You can set the dimensions to whatever you want */
    height:200px;
    object-fit: cover;
	}
	.imgs_player {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.select_type{
	background-color: #f4f4f4;
	padding: 20px;
	width: 100%;
	height: 78px;
	border: 1px solid #d5d5d5;
	font-size: 18px;
}
  </style>
</head>
<body onload="load_albumsss(); load_pictures(); uploading(); upload_picture();">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<a href="genre.php"><span class="fa fa-gears fa-3x" style="color: #3a4b58;"></span></a>
		</li>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title">Albums</p>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-lg-3">
			<button class="btn btn-dark" data-target="#add_album" data-toggle="modal" static><i class="fa fa-plus"></i> Album</button>
		</div>
		<div class="col-lg-6 input"><input type="text" oninput="load_albumsss();" name="search_album" id="search_album" placeholder="Search Album..."></div>
		<div class="col-lg-3"><a onclick="load_mymusic();"><button type="button" class="btn btn-dark">SEARCH</button></a></div>
	</div>
</div>

<div class="container-fluid tracks-table ">
	<div class="row">
		<div class="col-lg-12" id="load_albums"></div>
	</div>
</div>
<br>
<footer class="container-fluid">

	<div class="row copyright">
		<div class="col-lg-12">
			<p class="nav-item"><span><a href="#">HOME</a></span><span class="fence">|</span><span><a href="#">ABOUT US</a></span><span class="fence">|</span><span><a href="#">CONTACT</a></span></p>
			<p ><span><a href="#">FAQ</a></span><span class="fence">|</span><span><a href="#">TERMS OF USE</a></span><span class="fence">|</span><span><a href="#">PRIVACY POLICY</a></span></p>
			<p ><span>© 2018, <a href="#">LAMPSTAND STUDIO</a></span><span class="fence">|</span><span>ALL RIGHTS RESERVED.</span></p>
		</div>
	</div>
</footer>
</div>
</body>
</html>
