<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
  <script src="https://www.w3schools.com/lib/w3.js"></script>
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="admin.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
 <style type="text/css">

.imgs2 {
width: 50px; /* You can set the dimensions to whatever you want */
height: 50px;
object-fit: cover;
}
	/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555; 
}

#main-div{
	margin-left: 100px;
}

#view_profile{

}
#btn_bars{
	display: none;
}

.hide_th{

}
@media only screen and (max-width: 600px) {
	.sidenav{
	display: none;
	}
	#main-div{
	margin-left: 0px;
	}
	#view_profile{
	display: none;
	}
	#btn_bars{
	display: block;
	}
	.tracks{
		font-size: 10px;
	}
	.imgs {
    width: 50px; /* You can set the dimensions to whatever you want */
    height: 50px;
    object-fit: cover;
	}
	.labels{
		font-size: 12px;
	}
	.hide_th{
	display: none;
	}
}

th {
    cursor: pointer;
}

th, td {
    text-align: left;
    padding: 16px;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

  </style>
</head>
<body onload="user_account(); load_pictures();">

<div class="sidenav collapse show" id="view_sidebar">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #8eadab;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="User Logs"><a href="user_logs.php"><span class="fa fa-users fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
		</li>
	</ul>
</div>

<div class="main" id="main-div">
<div class="container-fluid hero hero-db hero-admin ">
	<div class="row header">
		 
		<div class="col-lg-3 col-2 head">
			<a href="#" onclick="user_account();"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<button type="button" class="btn btn-dark btn-collapse" id="btn_bars">
		  <span class="fa fa-bars text-white" data-toggle="collapse" data-target="#view_sidebar"></span>
		</button>
		<div class="col-lg-9 head" id="view_profile">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title">User Management</p>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-2">
			<a data-toggle="modal" data-target="#add_user"><button type="button" class="btn btn-dark btn-normal"><span class="fa fa-user-plus fa-lg"></span> Add User</button></a>
		</div>
		<div class="col-lg-4 input"><input type="show" oninput="user_account();" name="search" id="search" placeholder="Search..."></div>
		<div class="col-lg-2">
			<button type="button" class="btn btn-dark btn-normal" id="btn-approve" disabled="">
				<span class="fa fa-thumbs-up fa-lg"></span> Approve
			</button>
		</div>
		<div class="col-lg-2">
			<button type="button" class="btn btn-dark btn-normal" id="btn-disapprove" disabled="">
				<span class="fa fa-thumbs-down fa-lg"></span> Disapprove
			</button>
		</div>
		<div class="col-lg-2">
			<button type="button" class="btn btn-dark btn-normal" id="btn-delete" disabled="">
				<span class="fa fa-trash fa-lg"></span> Delete
			</button>
		</div>
	</div>
</div>

<div class="container-fluid tracks-table ">
	<div class="row">
		<div class="col-lg-12" id="tbl_users">
		</div>
	</div>
</div>

<footer class="container-fluid">
<?php include('../footer.php'); ?>
</footer>
</div>
<div class="modal fade" role="dialog" id="add_user" data-keyboard="false" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header music-dark mtxt-light">Add User <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<div class="container">
				<div class="row">
					
						<div class="col-sm-6">
						<label class="text-dark">Firstname</label>
						<div class="form-group">
							<input type="show" class="form-control text-capitalize" name="add_fn" id="add_fn" placeholder="Enter firstname...">
						</div>
					</div>
					<div class="col-sm-6">
						<label class="text-dark">Lastname</label>
						<div class="form-group">
							<input type="show" class="form-control text-capitalize" name="add_ln" id="add_ln" placeholder="Enter lastname...">
						</div>
					</div>
					<div class="col-sm-12">
						<label class="text-dark">E-mail</label>
						<div class="form-group">
							<input type="show" class="form-control" name="add_email" id="add_email" placeholder="Enter E-mail...">
						</div>
					</div>
					<div class="col-sm-12">
						<label class="text-dark">User Type</label>
						<div class="form-group">
							<select class="form-control" id="add_type" name="add_type">
								<option selected="" disabled="">Select user type</option>
								<option value="2">Contributor</option>
								<option value="3">Guest User</option>
							</select>
						</div>
					</div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer text-center">
				<div class="container">
					<button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
					<button class="btn btn-dark btn-small" onclick="add_client();"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" role="dialog" id="edit_pwd" data-keyboard="false" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content w3-round-large">
      <div class="modal-header music-dark mtxt-light">Edit Client Password  <button class="close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body">
        <center><h3 id="c_name" class="mtxt-dark text-capitalize">Name</h3></center>
        <input type="hidden" name="mng_id" id="mng_id">
        <input type="hidden" name="id_btn" id="id_btn">
        <hr>
        <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
			          <label class="text-dark">New Password:</label>
			          <input type="password" class="form-control" name="mng_pwd" id="mng_pwd" >
			        </div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
			          <label class="text-dark">Verify New Password:</label>
			          <input type="password" class="form-control" name="verify_pwd" id="verify_pwd" placeholder="Verify New Password...">
			        </div>
				</div>

	        
			</div>
		</div>
        	
        </div>
        <div class="modal-footer">
        <button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
        <button class="btn btn-dark btn-small" onclick="update_account_pwd();"><i class="fa fa-save"></i> Save</button>
      </div>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
	function delete_users(id){
	var mydata = 'action=delete_user' + '&user_id=' + id;
	swal({
	  title: "Are you sure?",
	  text: "You wan to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Delete",
	  closeOnConfirm: false
	},
	function(){
	 $.ajax({
	 	type:"POST",
	 	url:url,
	 	data:mydata,
	 	cache:false,
	 	success:function(data){
	 	// alert(data);
		if (data.trim() == 1) 
		{
			$("#"+id+"").addClass('w3-card-4 animated zoomOutDown');
			swal.close();
			setTimeout(function(){
			user_account();
			},100);
		}
		else
		{
			swal("Sorry!","Active Users won\'t be able to delete !","error");
			$("#"+id+"").addClass('w3-red');
			setTimeout(function(){
			$("#"+id+"").removeClass('w3-red');
			},1000);
		}
	 	}
	 });
	  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
	});
	}

function count_check(){
	var chks = $('.select_accounts').filter(':checked').length

	if(chks > 0){
		document.getElementById('btn-approve').disabled= false;
		document.getElementById('btn-disapprove').disabled= false;
		document.getElementById('btn-delete').disabled= false;
	}else{
		document.getElementById('btn-approve').disabled= true;
		document.getElementById('btn-disapprove').disabled= true;
		document.getElementById('btn-delete').disabled= true;
	}
}

 function check_all_validate() {
        if (document.getElementById('chk_accounts').checked) {
            document.getElementById('btn-approve').disabled= false;
			document.getElementById('btn-disapprove').disabled= false;
			document.getElementById('btn-delete').disabled= false;
        } else {
            document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
			document.getElementById('btn-delete').disabled= true;
        }
    }

$(document).ready(function(){
 
 //Approve ALl
 $('#btn-approve').click(function(){
  
  swal({
	  title: "Are you sure?",
	  text: "You wan to Approve Selected data?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Approve",
	  closeOnConfirm: false
	},
	function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {

    $.ajax({
     url:'approve_all.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		swal("Music Libray!", "Unable to Approve", "info");
			
     	}else{
			swal("Music Libray!", "Approved", "info");
			user_account();
			 document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
			document.getElementById('btn-delete').disabled= true;
     	}
      
     }
     
    });
   }
 });

 });

//Disapprove All
$('#btn-disapprove').click(function(){
  
  swal({
	  title: "Are you sure?",
	  text: "You wan to Disapprove Selected data?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Approve",
	  closeOnConfirm: false
	},
	function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {

    $.ajax({
     url:'disapprove_all.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		swal("Music Libray!", "Unable to Disapprove", "info");
			
     	}else{
			swal("Music Libray!", "Disapproved", "info");
			user_account();
			 document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
			document.getElementById('btn-delete').disabled= true;
     	}
      
     }
     
    });
   }
 });
  
 }); 

//Delete All
//Disapprove All
$('#btn-delete').click(function(){
  swal({
	  title: "Are you sure?",
	  text: "You want to Delete Selected data?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Delete",
	  closeOnConfirm: false
	},
	function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {

    $.ajax({
     url:'delete_all_user.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		swal("Music Libray!", "Unable to delete", "info");
			
     	}else{
			swal("Music Libray!", "Deleted", "info");
			user_account();
			 document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
			document.getElementById('btn-delete').disabled= true;
     	}
      
     }
     
    });
   }
 });
  
 });

});

</script>
</body>
</html>
