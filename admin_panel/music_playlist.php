<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="music_player/audio.css">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="admin.css">
     <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
	<script src="music_player/audio-index.js"></script>

  
  <script src="https://www.w3schools.com/lib/w3.js"></script>

 
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
  	.img_music {
    width: 60px; /* You can set the dimensions to whatever you want */
    height:60px;
    object-fit: cover;
	}
	.img_music2 {
    width: 200px; /* You can set the dimensions to whatever you want */
    height:200px;
    object-fit: cover;
	}
	.imgs_player {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.select_type{
	background-color: #f4f4f4;
	padding: 20px;
	width: 100%;
	height: 78px;
	border: 1px solid #d5d5d5;
	font-size: 18px;
}
  </style>
</head>
<body onload="show_music_library(); load_pictures(); uploading(); upload_picture();">
<div class="sidenav">
  <ul>
    <li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
    </li>
    <li class="w3-hover-shadow" title="User Logs"><a href="user_logs.php"><span class="fa fa-users fa-3x" style="color: #3a4b58;"></span></a>
    </li>
    <li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
    </li>
    <li class="w3-hover-shadow" title="Music Management">
      <a href="#settings" data-toggle="collapse" style="color: #8eadab;">
            <i class="fa fa-music fa-3x" ></i>
        </a>
    </li>
    <?php include('music_sub_menu.php') ?>
    <li class="w3-hover-shadow" title="Content Management">
      <a href="#management" data-toggle="collapse" style="color: #3a4b58;">
            <i class="fa fa-gears fa-3x" ></i>
        </a>
    </li>
    <?php include('setting_sub_menu.php') ?>
    <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
      
    </li>

  </ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#" onclick="show_music_library();"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-3">
			<p class="title">Music Library</p>
		</div>
		<div class="col-lg-6 input"><input type="show" oninput="show_music_library();" name="search_music" id="search_music" placeholder="Search Title, Album, Application, Composer, Keyword..."></div>
		<div class="col-lg-3">
			<button type="button" class="btn btn-dark btn-normal" id="btn-delete" disabled="">
			<span class="fa fa-trash fa-lg"></span> Delete
		</button>
		</div>
		
	</div>
</div>
<div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
    <div class="row">
      <div class="col-lg-12 table-responsive">
        <table class="table table-striped table-hover" id="musics2">
          <tr><td colspan="10" class="text-left"><span>Click on the column Title to Sort</span></td></tr>
          <tr id="labels">
            <td class="text-left">
              <div class="form-check-inline">
              <label class="form-check-label" for="chk_musics">
                <input type="checkbox" name="chk_musicss" id="chk_musicss" onclick="check_all_validate_music();">
              </label>
            	</div>
            </td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(2)')"style="width: 15%; cursor:pointer">Title/Album</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(3)')" style="cursor:pointer">Description</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(4)')" style="cursor:pointer">Genre</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(5)')" style="cursor:pointer">Composer</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(6)')" style="cursor:pointer">Application</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(7)')" style="cursor:pointer">Contributor</td>
            <td class="text-left" onclick="w3.sortHTML('#musics2', '.item', 'td:nth-child(8)')" style="cursor:pointer; width: 11.5%;">Supporting Files</td>
            <td class="text-center" style="">Action</td>
          </tr>
          <tbody id="list_music" style="">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>
<br>
<footer class="container-fluid">
	<?php include('../footer.php'); ?>
</footer>
</div>
<div class="footer none" id="player">
	<div class="row" style="padding-left: 110px; padding-right: 20px; padding-bottom: 10px;">
	      <div class="col-sm-12">
	        <div class="col-sm-12">
	          <h4 style="margin-bottom: 0px;">
	          <marquee id="music_title" class="col-sm-12 default-fs">LAMPSTAND STUDIO</marquee>
	          </h4>
	        </div>
	        <input type="range" min="0"  max="100" class="player_slider" onmousemove="move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'));" onmouseup="move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">

	        <div class="col-sm-12 row">
	          <div class="col-sm-2">
	              <div class="text-white"><span id="counter_strike">00:00</span>/<span id="timer_audio">00:00</span></div>
	          </div>
	          <div class="col-sm-8 text-center">
	              <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg text-white"></i></a>
	              <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="fa fa-play-circle fa-lg text-white"></i></a>
	              <a href="#prev" onclick="next_aud();" class="btn-musics "><i class="fa fa-forward fa-lg text-white"></i></a>
	              
	          </div>
	          <div class="col-sm-2 text-white">
	                <a href="#vol"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg text-white"></i></a>
	                <input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);">
	          </div>
	        </div>
	      </div>

	    <div id="music_audio" style="width: 100%;">
	    <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
	    Sorry, your browser does not support audio
	    </audio>
	    <div id="show_progress"></div>
	    </div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">


function count_check_music(){
	var chks = $('.select_musics').filter(':checked').length

	if(chks > 0){
		document.getElementById('btn-delete').disabled= false;
	}else{
		document.getElementById('btn-delete').disabled= true;
	}
}

function check_all_validate_music() {
    if (document.getElementById('chk_musicss').checked) {
			document.getElementById('btn-delete').disabled= false;
        } else {
        	document.getElementById('btn-delete').disabled= true;
        }
    }

 $(document).ready(function(){
					$("#chk_musicss").change(function(){
					    if(this.checked){
					      $(".select_musics").each(function(){
					        this.checked=true;
					      })       
					    }else{
					      $(".select_musics").each(function(){
					        this.checked=false;
					      })              
					    }
					  });
					});

  $(document).ready(function(){
    $("#chk_accounts").change(function(){
      if(this.checked){
        $(".select_accounts").each(function(){
        this.checked=true;
        })       
      }else{
        $(".select_accounts").each(function(){
        this.checked=false;
        })              
      }
    });

  //Delete All
$('#btn-delete').click(function(){
  swal({
    title: "Are you sure?",
    text: "You want to Delete Selected data?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#34464a",
    confirmButtonText: "Delete",
    closeOnConfirm: false
  },
  function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Library!", "Please select atleast one", "info");
   }
   else
   {
    $.ajax({
     url:'../user_profile/delete_all_music.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
      if(data == 404){
        swal("Music Libray!", "Unable to delete", "info");
      }else{
      swal("Music Libray!", "Deleted", "info");
      show_music_library();
      document.getElementById('btn-delete').disabled= true;
      }
      
     }
     
    });
   }
 });
  
 });

});
</script>
