<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
}
.profile_pic {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.modal-small{
			max-width: 500px;
			width: 100%;
		}
  </style>
  <script type="text/javascript">
  	$(document).ready(function(){  

	$('#change_profile_pic').on('hidden.bs.modal', function () {
	$("#user_img_preview").html('');
	load_pictures();
	});

});
  </script>
</head>
<body onload="user_account(); uploading(); refresh_account(); load_pictures(); ">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<a href="genre.php"><span class="fa fa-gears fa-3x" style="color: #3a4b58;"></span></a>
		</li>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title"><i class="fa fa-lg fa-user"></i> Account Settings</p>
		</div>
	</div>
	<hr>
	<div class="row" style=" margin-top: -20px;">
		<div class="col-sm-3 mb-2">
			<div class="glass w3-padding w3-round">
				<div class="w3-display-container text-center">
					<div id="profile_pic">
					</div>
					<div class="w3-display-middle w3-display-hover">
						<button class="btn btn-dark animated fadeIn btn-small" data-toggle="modal" data-target="#change_profile_pic">Change Profile Picture</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 mb-2">
			<div id="my_data"></div>
		</div>
		<div class="col-sm-5">
			<div id="choose">
			</div>
			<input type="hidden" name="old" id="old">
			<input type="hidden" name="id" id="id">
			<?php include_once("forms.php"); ?>
		</div>
	</div>
</div>

<footer class="container-fluid">
	<div class="row copyright">
		<div class="col-lg-12">
			<p class="nav-item"><span><a href="#">HOME</a></span><span class="fence">|</span><span><a href="#">ABOUT US</a></span><span class="fence">|</span><span><a href="#">CONTACT</a></span></p>
			<p ><span><a href="#">FAQ</a></span><span class="fence">|</span><span><a href="#">TERMS OF USE</a></span><span class="fence">|</span><span><a href="#">PRIVACY POLICY</a></span></p>
			<p ><span>© 2018, <a href="#">LAMPSTAND STUDIO</a></span><span class="fence">|</span><span>ALL RIGHTS RESERVED.</span></p>
		</div>
	</div>
</footer>
</div>
<div class="modal fade" role="dialog" id="change_profile_pic" data-backdrop="static">
  <div class="modal-dialog modal-small">
    <div class="modal-content">
      <div class="modal-header text-white">Change Profile Picture
        <button class="cp_close close" data-dismiss="modal">&times;</button></div>
	      <div class="modal-body">
	        <input type="file" name="user_image" id="user_image" accept="image/*" style="display: none;">
	        <div class="text-center">
	        <div id="user_img_preview">
	          <img src="<?php echo $_SESSION['user_pic'] ?>" class="profile_pic img-album w3-round" style="margin-bottom:5px;">
	        </div>
	        <div id="btn-up">
	        	<button class="btn btn-dark btn-small" onclick="$('#user_image').click();"><i class="fa fa-image"></i> Upload Picture</button>
	        </div>
	        <div style="height: 10px;"></div>
	        </div>
	      </div>
    </div>
  </div>
</div>
</body>
</html>
