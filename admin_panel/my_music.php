<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
  <script type="text/javascript" src="add_song.js"></script>
   <script type="text/javascript" src="../js/audio-index.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
  	.img_music {
    width: 60px; /* You can set the dimensions to whatever you want */
    height:60px;
    object-fit: cover;
	}
	.img_music2 {
    width: 200px; /* You can set the dimensions to whatever you want */
    height:200px;    
    object-fit: cover;
	}
	.imgs_player {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.select_type{
	background-color: #f4f4f4;
	padding: 20px;
	width: 100%;
	height: 78px;
	border: 1px solid #d5d5d5;
	font-size: 18px;
	}

	.oji-3{
		width: 32%; 
		padding: 1%;
	}
	.pad-1{
		padding: 2%;
	}

	.scrollers{
	    max-height:450px; 
	    overflow:auto;
	  }

	@media only screen and (max-width: 600px) {
		.oji-3{
		width: 100%; 
		padding: 1%;
		}
	}
  </style>
</head>
<body onload="load_mymusic(); load_pictures(); upload_music22(); multi_upload_file(); display_files(); ">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<a href="genre.php"><span class="fa fa-gears fa-3x" style="color: #3a4b58;"></span></a>
		</li>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title">My Music</p>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-lg-3">
			<div class="input-group select_type">
				<span class="input-group-addon bg-dark text-info"><i class="fa fa-sort"></i></span>
				<select id="sort_status" onchange="load_mymusic();" oninput="load_mymusic();" class="form-control bg-dark text-white ">
					<option value="0">Status</option>
					<option value="1">Posted</option>
					<option value="2">Unposted</option>
				</select>
			</div>
		</div>
		<div class="col-lg-6 input"><input type="text" oninput="load_mymusic();" name="search_music2" id="search_music2" placeholder="Search Music..."></div>
		<div class="col-lg-3"><a><button type="button" class="btn btn-dark" data-target="#addSong" data-toggle="modal" onclick="generate_group_id(); display_files(); clear_form(); load_style_select(); load_app_select();   get_dateupload();">Add Song</button></a></div>
	</div>
</div>

<div class="container-fluid tracks-table ">
	<div class="row">
		<div class="col-lg-8" id="tbl_mymusic"></div>
		<div class="col-lg-4">
			<div class="row">
						<div class="bg-dark text-info w3-round w3-padding">
						<div id="" class="text-center">
						<span class="w3-large ml3" id="music_title">Music</span>
						</div>
						<div class="glass w3-round w3-padding text-center">
							<div id="album_artwork">
								<img src='../img/icon2.png' class="imgs_player img-responsive w3-round animated jello" alt="">
							</div>
						</div>
						<br>
						
						<div class=" text-center">
						  <div class="pt-2 w3-light-grey w3-round " oncontextmenu="return false;">
								<div class="d-flex">
								  <div class="p-3 pt-3">
								  	<a href="#prev" onclick="prev_aud();" class="text-text-dark "><i class="fa fa-backward "></i></a>
								  </div>
								   <div class="p-2  pt-3">
										<a href="#prev" onclick="next_aud();" class="text-text-dark "><i class="fa fa-forward "></i></a>
								  </div>
								  	<div id="music_audio" style="width: 100%;">
									  	<audio controls="" id="my_player" onplay="on_firs_play();" onpause="pause_media();" controlsList="">
												Sorry, your browser does not support audio
										</audio>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
<br>
<footer class="container-fluid">

	<div class="row copyright">
		<div class="col-lg-12">
			<p class="nav-item"><span><a href="#">HOME</a></span><span class="fence">|</span><span><a href="#">ABOUT US</a></span><span class="fence">|</span><span><a href="#">CONTACT</a></span></p>
			<p ><span><a href="#">FAQ</a></span><span class="fence">|</span><span><a href="#">TERMS OF USE</a></span><span class="fence">|</span><span><a href="#">PRIVACY POLICY</a></span></p>
			<p ><span>© 2018, <a href="#">LAMPSTAND STUDIO</a></span><span class="fence">|</span><span>ALL RIGHTS RESERVED.</span></p>
		</div>
	</div>
</footer>
</div>
<div class="modal fade" id="addSong">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<p class="title">ADD NEW MUSIC</p>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body scrollers" >
					<input type="hidden" id="songid">
			        <input type="hidden" id="music_path">
			        <input type="hidden" id="duration">
			        <input type="file" name="uploaded_songs" id="uploaded_songs" style="display: none;" /> 
			        <input type="hidden" name="selectedalb_id" id="selectedalb_id">

					<div class="container">
						<div class="row btn-pair">
							<div class="col-lg-6 right"><button type="button" class="btn btn-dark btn-small" id="btn-select2" onclick="$('#uploaded_songs').click();">Select Song</button></div>
							<div class="col-lg-6 left">
								<button type="button" class="btn btn-dark btn-small" id="alb_select" onclick="addto_album();">Album</button>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-4">
								<input type="text" placeholder="Title" class="input-small" name="song_name2" id="song_name2"/>
								<br>
								<input type="text" placeholder="Composer" class="input-small" name="composer2" id="composer2"/>
							</div>
							<div class="col-lg-4"><textarea placeholder="Description" class="input-small big" name="description" id="description"></textarea></div>
							<div class="col-lg-4"><textarea placeholder="Keywords" class="input-small big" name="keyword" id="keyword"></textarea></div>
							<div class="col-lg-12 text-center" id="song_preview2" style="padding-left: 0px; padding-right: 0px;"></div>
						</div>
					</div>

					<div class="container-fluid divider">
						<div class="container">
							<p class="title">Style</p>
						</div>
					</div>

					<div class="container pad-1">
						<input type="hidden" id="style_value">
						<div class="col-lg-12" id="style_select">
							
						</div>
					</div>

					<div class="container-fluid divider">
						<div class="container">
							<p class="title">Application</p>
						</div>
					</div>

					<div class="container pad-1">
						<input type="hidden" id="app_value">
						<div class="col-lg-12" id="app_select">
							
						</div>
					</div>

					<div class="container-fluid divider">
						<div class="container">
							<p class="title">Supporting Files</p>
						</div>
					</div>

					<div class="container">
						<div class="row">
							<input type="file" multiple name="multi_upload_file[]" id="multi_upload_file" style="display: none;">
                			<input type="hidden" name="group_id" id="group_id">
							<div class="col-lg-4 right"><button type="button" class="btn btn-dark btn-small" id="btn_select" onclick="$('#multi_upload_file').click();">Select Files</button></div>

							<div class="col-lg-3 select">
								<div class="btn-small label">Available Form</div>
							</div>
							<div class="col-lg-5 select">
								<div class="form-group">
									<select class="form-control">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<div class="container-fluid">
						<table class="table table-borderless">
							<head>
								<tr class="titles">
								<td>File Name</td>
								<td>File Description</td>
								<td>Link</td>
								<td>Link Name</td>
								<td>Task</td>
							</tr>
							</head>
							<tbody id="display_supporting_files">
								
							</tbody>
						</table>
					</div>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-dark btn-small" name="submitsong2" id="submitsong2" onclick="submitsong2(); get_path();">Save</button>
				</div>

			</div>
		</div>
	</div>

	<div id="select_album_modal" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog" >  
      <div class="modal-content w3-round-xlarge" style="background-color: #6C7A89;">  
      <div class="modal-header text-center SF-head">
        <h4 class="font-m1 w3-wide text-light SF-title text-center" style="width:100%; text-shadow:2px 2px 3px #333;">SELECT ALBUM</h4>  
        <a href="#"  class="close text-sm" data-dismiss="modal" onclick="cancel_select();">
          <i id="closehover" class="fa fa-times"></i></a>
        <hr class="SF-hr">
      </div> 
          <div class="modal-body">         
            <div class="text-center">
            <div class="row">
              <div class="col-sm-2"></div>
              <div class="col-sm-4">
                  <button class="btn btn-dark btn-small" onclick="create_album();"><i class="fa fa-plus"></i> Create New Album</button>
              </div>
              <div class="col-sm-6 form-group">
                 <div class="input-group">
                   <span class="input-group-addon w3-white btn btn-dark" onclick="load_albums();" style="border-radius:15px 0px 0px 15px;"><i class="fa fa-search"></i></span>
                     <input type="show" oninput="load_albums();" style="border-radius:0px 15px 15px 0px;" name="searchalbums" id="searchalbums" class=" form-control" placeholder="Search..." autocomplete="off" style="font-family: serif;">
                 </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body" style="margin: 0px; padding: 0px;">
                   <div class="scroll card-album" id="tbl_album_select"></div>
              </div>
            </div>
            </div>
          </div>  
      </div>  
    </div>  
  </div>

  <div id="createAlbumModal" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog ">  
      <div class="modal-content w3-round-xxlarge sa-bg AM-content" style="background-color: #6C7A89;">  
        <div class="modal-header text-center SF-head" >
          <h4 class="font-m1 w3-wide text-white text-center" style="width:100%; text-shadow:2px 2px 3px #333;">CREATE NEW ALBUM</h4>  
          <a href="#"  class="close" data-dismiss="modal" onclick="cancelcreate();">
            <i id="closehover" class="fa fa-times"></i></a>
          <hr class="SF-hr">
        </div>    
          <div class="modal-body AM-body">         

            <div id="image_preview2" class="text-center"> </div> <br/>
            <input type="file" name="album_image2" id="album_image2" style="display: none;" /> 
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <input type="text" name="album_name2" id="album_name2" class="form-control text-capitalize" placeholder="Album Name" required="true" maxlength="30" style="text-transform: capitalize;">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="">
                  <button class="btn btn-dark" id="btn-chooser2" onclick="$('#album_image2').click();"> 
                    <i class="fa fa-file fa-lg"></i> Select Image</button>
                </div>
              </div>
              <div class="col-sm-1"></div>

            <div class="col-sm-12">
              <hr class="AM-hr">
              <div class="text-right">
                <button class="btn btn-dark w3-round-xxlarge  font-m1" onclick="submit_album2();">Save Album</button>
                <button class="btn btn-danger  w3-round-xxlarge font-m1" data-dismiss="modal" onclick="cancelcreate();"> Cancel</button>  
              </div>
            </div>

            </div>
          </div>  
      </div>  
    </div>  
  </div>

  <div id="EditFileModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

    	<div class="modal-header">
			<p class="title">Edit Supporting File</p>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
        <div class="modal-body">
        	<div class="container">
        		<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
				            <label>File Name:</label>
				            <input type="show" name="file_name" id="file_name" placeholder="File Name" class="form-control" />
				        </div>
				        <div class="form-group">
				            <label>File Description:</label>
				            <input type="show" name="file_description" id="file_description" placeholder="File Description" class="form-control" />
				         </div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
				            <label>Link Name:</label>
				            <input type="show" name="link_name" id="link_name" placeholder="Link Name" class="form-control" />
				          </div>
				          <div class="form-group">
				            <label>Link:</label>
				            <input type="show" name="link" id="link" placeholder="Link" class="form-control" />
				          </div>
					</div>
				</div>
        	</div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="old_name" id="old_name"/>
          <input type="hidden" name="edit_action" id="edit_action"/>
          <input type="hidden" name="file_id" id="file_id"/>
          <input type="hidden" name="file_ext" id="file_ext"/>
          <button type="button" class="btn btn-dark btn-small" style="font-family: MavenPro-Regular;" onclick="edit_file();">Update</button>
        </div>
    </div>
  </div>
</div>
</body>
</html>