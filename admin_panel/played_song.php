<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
   <script type="text/javascript" src="../js/audio-index.js"></script>
<link href="../css/w3.css" rel="stylesheet" />
<script src="../js/w3.js"></script>
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="admin.css">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
  	.img_music {
    width: 60px; /* You can set the dimensions to whatever you want */
    height:60px;
    object-fit: cover;
	}
	.img_music2 {
    width: 200px; /* You can set the dimensions to whatever you want */
    height:200px;
    object-fit: cover;
	}
	.imgs_player {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.select_type{
	background-color: #f4f4f4;
	padding: 20px;
	width: 100%;
	height: 78px;
	border: 1px solid #d5d5d5;
	font-size: 18px;
}
  </style>
</head>
<body onload="load_pictures(); music_plays();">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="User Logs"><a href="user_logs.php"><span class="fa fa-users fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>
		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-3">
			<p class="title">Played Songs</p>
		</div>
		<div class="col-lg-2">
			<select id="top_plays" onchange="music_plays();" oninput="music_plays();" class="btn btn-dark btn-normal">
						<option value="100">Top</option>
						<option value="3">Top 3</option>
						<option value="5">Top 5</option>
						<option value="10">Top 10</option>
						<option value="20">Top 20</option>
						<option value="30">Top 30</option>
						<option value="40">Top 40</option>
						<option value="50">Top 50</option>
						<option value="60">Top 60</option>
						<option value="70">Top 70</option>
						<option value="80">Top 80</option>
						<option value="90">Top 90</option>
						<option value="100">Top 100</option>
					</select>
		</div>
		<div class="col-lg-5 input"><input type="show" oninput="music_plays();" name="search_plays" id="search_plays" placeholder="Search Music..."></div>
	</div>
</div>

<div class="container-fluid tracks-table ">
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
			    <table class="table table-hover table-light table-responsive-sm ">
			    	<thead class="thead-dark">
						<tr id="labels">
							<td class="text-left"></td>
							<td class="text-left">NO.</td>
							<td class="text-left">TITLE</td>
							<td class="text-left">TOTAL PLAYED</td>
						</tr>
					</thead>

			    	<tbody id="tbl_played">
			    	</tbody>
			    </table>
			</div>
		</div>
	</div>
</div>
<br>
<footer class="container-fluid">
	<?php include('../footer.php'); ?>
</footer>
</div>
 <!-- Download Details --> 
<div id="played_details" class="modal fade">  
  <div class="modal-dialog modal-lg">  
    <div class="modal-content">  
      <div class="modal-header"><p class="title"> 
      	<span class="fa fa-play"></span> Song Played Details :
      	<span id="song_name"></span>
     	  <span class="badge badge-danger" id="total_played"></span>  
      	</p>  
         <button type="button" class="close" data-dismiss="modal">&times;</button>  
      </div>  <input type="hidden" id="id_song">
      <div class="modal-body"> 
              <div class="row" style="margin-top: -33px;">
              	<div class="col-sm-12">
                  <div class="table-responsive">
                      <table class="table table-hover table-light table-responsive-sm" id="played">
                      <thead class="thead-dark">
                          <tr id="labels">
                            <th class="text-left" onclick="w3.sortHTML('#played', '.item', 'td:nth-child(1)')" style="cursor:pointer;">NO.</th>
                            <th class="text-left" onclick="w3.sortHTML('#played', '.item', 'td:nth-child(2)')" style="cursor:pointer;">NAME</th>
                            <th class="text-left" onclick="w3.sortHTML('#played', '.item', 'td:nth-child(3)')" style="cursor:pointer;">DATE</th>
                          </tr>
                        </thead>
                        <tbody id="play_details">
                          
                        </tbody>
                      </table>
                  </div>
      				</div>
            </div>
      </div>  
    </div>  
  </div>  
</div>
</body>
</html>
