//url
var url = 'function.php';
//tooltips
function enable_tooltips(){
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
    $('[data-toggle="tooltip"]').tooltip(); 
});
}

function save_genre(){
		var genre = $("#genre").val();
		var gen_id = $("#gen_id").val();
		if (genre == "") 
		{
			$("#genre").focus();
		}
		else
		{
			var mydata = 'genre=' +  genre + '&action=insert_genre' + '&gen_id=' + gen_id;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
				},
				success:function(data){
					// alert(data);
					if (data == 1) 
					{
						show_genre();
						$("#genre").val('');
						$("#gen_id").val('');
						$("#btn_cancel").hide('fast');
					}
					else if (data == 2) 
					{

						show_genre();
						$("#genre").val('');
						$("#gen_id").val('');
						$("#btn_cancel").hide('fast');	
					}
					else
					{
						alert(data);
					}
					$("#loader").html('');
				}
			});
		}
}


function save_application(){
		var app_name = $("#app_name").val();
		var app_id = $("#app_id").val();
		if (app_name == "") 
		{
			$("#app_name").focus();
		}
		else
		{
			var mydata = 'app_name=' +  app_name + '&action=insert_app' + '&app_id=' + app_id;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
				},
				success:function(data){
					// alert(data);
					if (data == 1) 
					{
						show_application();
						$("#app_name").val('');
						$("#app_id").val('');
						$("#btn_cancel_app").hide('fast');
					}
					else if (data == 2) 
					{

						show_application();
						$("#app_name").val('');
						$("#app_id").val('');
						$("#btn_cancel_app").hide('fast');
					}
					else
					{
						alert(data);
					}
					$("#loader").html('');
				}
			});
		}
}


function save_audience(){
		var audience_type = $("#audience_type").val();
		var audience_id = $("#audience_id").val();
		if (audience_type == "") 
		{
			$("#audience_type").focus();
		}
		else
		{
			var mydata = 'audience_type=' +  audience_type + '&action=insert_audience' + '&audience_id=' + audience_id;
			// alert(mydata);
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
				},
				success:function(data){
					// alert(data);
					if (data == 1) 
					{
						show_audience_now();
						$("#audience_type").val('');
						$("#audience_id").val('');
						$("#btn_cancel_aud").hide('fast');
					}
					else if (data == 2) 
					{

						show_audience_now();
						$("#audience_type").val('');
						$("#audience_id").val('');
						$("#btn_cancel_aud").hide('fast');
					}
					else
					{
						alert(data);
					}
					$("#loader").html('');
				}
			});
		}
}




function show_genre(){
mydata ='action=show_genre';
$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){

		},
		success:function(data){
		$("#tbl_genres").html(data);
		}
	});
}


function show_audience_now(){
mydata ='action=show_aud';
$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){

		},
		success:function(data){
		$("#tbl_audience").html(data);
		}
	});
}

function show_application(){
mydata ='action=show_apps';
$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){

		},
		success:function(data){
		// alert(data);
		
		$("#tbl_app").html(data);
		}
	});
}



function delete_genre(index){
	var mydata ='action=del_genre'+'&id=' + index;
	swal({
	  title: "Are you sure?",
	  text: "Do you want to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
			if (data == 1) 
			{
				swal("Deleted!", "Record has been deleted !", "success");
				show_genre();
			}
			else
			{
				alert(data);
			}
			}
		});
	 
	});
}

function delete_app(index){
	// alert(index);
	var mydata ='action=del_app'+'&id=' + index;
	swal({
	  title: "Are you sure?",
	  text: "Do you want to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
			// alert(data);
			if (data == 1) 
			{
				swal("Deleted!", "Record has been deleted !", "success");
				show_application();
			}
			else
			{
				alert(data);
			}
			}
		});
	 
	});
}


function delete_audience(index){
	// alert(index);
	var mydata ='action=del_aud'+'&id=' + index;
	swal({
	  title: "Are you sure?",
	  text: "Do you want to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
			// alert(data);
			if (data == 1) 
			{
				swal("Deleted!", "Record has been deleted !", "success");
				show_audience_now()
			}
			else
			{
				alert(data);
			}
			}
		});
	 
	});
}

function update_genre(genre,gen_ids){
		$("#genre").val(genre);
		$("#gen_id").val(gen_ids);
		$("#genre").focus();		
		$("#btn_cancel").show('fast');
}

function update_audience(aud_name,au_id){
		$("#audience_type").val(aud_name);
		$("#audience_id").val(au_id);
		$("#audience_type").focus();		
		$("#btn_cancel_aud").show('fast');
}

function cancel_update_aud(){
	$("#audience_type").val('');
	$("#audience_id").val('');
	$("#btn_cancel_aud").hide('fast');
}

function update_app(appname,appid){
		$("#app_name").val(appname);
		$("#app_id").val(appid);
		$("#app_name").focus();		
		$("#btn_cancel_app").show('fast');
}

function cancel_update_app(){
	$("#app_name").val('');
	$("#app_id").val('');
	$("#btn_cancel_app").hide('fast');
}


function cancel_update(){
	$("#genre").val('');
	$("#gen_id").val('');
	$("#btn_cancel").hide('fast');
}

//Admin account

function update_name(fn,mn,ln,id){
	$("#choose").hide('fast');
	$('#fn').val(fn);
	$('#mn').val(mn);
	$('#ln').val(ln);
	$('#id').val(id);
	$("#form-names").show('fast');
}



function refresh_account(){
	$.ajax({
		type:"POST",
		url:url,
		data:'action=refresh_account',
		cache:false,
		beforeSend:function(){
			$("#my_data").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');
		},
		success:function(data){
			setTimeout(function(){$("#my_data").html(data);},900);
		}
	});
}	



function name(fn,mn,ln,id){
	if (fn == "") 
	{
		$('#fn').focus();
	}
	else if (mn == "") 
	{
		$('#mn').focus();
	}
	else if (ln == "") 
	{
		$('#ln').focus();
	}
	else
	{
	var mydata = 'action=update_account' + '&fn=' + fn + '&mn=' + mn + '&ln=' + ln + '&id=' + id;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
		// alert(data);
		if (data == 1) {
			refresh_account();
			setTimeout(function(){cancel_updates();},1200);
		}
		}
	});
	}
}



function bdates(bdate){
	if (bdate == "") 
	{
		$('#bdate').focus();
	}
	else
	{
	var mydata = 'action=update_bdate' + '&bdate=' + bdate;
	 // alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
		// alert(data);
		if (data == 1) {
			refresh_account();
			setTimeout(function(){cancel_updates();},1200);
		}
		}
	});
	}
}



function email_add(email){
	if (email == "") 
	{
		$('#email').focus();
	}
	else
	{
	var mydata = 'action=update_email' + '&email=' + email;
	 // alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
		// alert(data);
		if (data == 1) {
			refresh_account();
			setTimeout(function(){cancel_updates();},1200);
		}
		}
	});
	}
}



function usernames(username){
	if (username == "") 
	{
		$('#username').focus();
	}
	else
	{
	var mydata = 'action=update_username' + '&username=' + username;
	 // alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
		// alert(data);
		if (data == 1) {
			refresh_account();
			setTimeout(function(){cancel_updates();},1200);
		}
		}
	});
	}
}


function update_bdate(bdates){
	$('#bdate').val(bdates);
	$("#form-bd").show('fast');
	$("#choose").hide('fast');
}


function update_uname(uname){
	$('#username').val(uname);
	$("#form-username").show('fast');
	$("#choose").hide('fast');
}


function update_email(emails){
	$('#email').val(emails);
	$("#form-email").show('fast');
	$("#choose").hide('fast');
}

function cancel_updates(){
	$('#fn').val('');
	$('#mn').val('');
	$('#ln').val('');
	$('#id').val('');
	$('#bdate').val('');
	$('#email').val('');
	$('#username').val('');
	$("#o_pwd").val('');
	$("#n_pwd").val('');
	$("#c_pwd").val('');
	$("#old").val('');
	$("#form-names").hide('fast');
	$("#form-pwd").hide('fast');
	$("#form-bd").hide('fast');
	$("#form-email").hide('fast');
	$("#form-username").hide('fast');
	$("#choose").show('fast');
}


function update_pwd(index){
	$("#old").val(index);
	$("#form-pwd").show('fast');
	$("#choose").hide('fast');
}

function update_user(index){
	var fn,mn,ln,id,bdate,email;
	if (index == 'name') 
	{
	fn = $('#fn').val();
	mn = $('#mn').val();
	ln = $('#ln').val();
	id =$('#id').val();
	name(fn,mn,ln,id);
	}
	else if(index == 'bdate')
	{
	bdate = $("#bdate").val();
	bdates(bdate);
	}
	else if (index == 'email') 
	{
	email = $("#email").val();
	email_add(email);
	}
	else if (index == 'username') 
	{
	var username = $("#username").val();
	usernames(username);
	}
	else if (index == 'pwd') 
	{
	var o_pwd,n_pwd,c_pwd,old;
	o_pwd = $("#o_pwd").val();
	n_pwd = $("#n_pwd").val();
	c_pwd = $("#c_pwd").val();
	old = $("#old").val();
	change_pass(o_pwd,c_pwd,n_pwd,old);
	}
}

function change_pass(o_pwd,c_pwd,n_pwd,old){
	if (o_pwd == "") 
	{
		$("#o_pwd").focus();
	}
	else if (o_pwd != old) 
	{
		swal("Sorry","Password Doesn\'t Match !","error");
		$("#o_pwd").focus();
	}
	else if (n_pwd == "") 
	{
		$("#n_pwd").focus();
	}
	else if (c_pwd == "") 
	{
		$("#c_pwd").focus();
	}
	else if (c_pwd != n_pwd) 
	{
		swal("Sorry","Please confirm your password!","error");
		$("#c_pwd").focus();
	}
	else
	{
		var mydata = 'action=update_pwd' + '&pwd=' + n_pwd;
 // alert(mydata);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
			// alert(data);
			if (data == 1) {
				// alert(data);
				refresh_account();
				setTimeout(function(){cancel_updates();},1200);
				swal("Success","Password has been changed!","success");
			}
			}
		});
	}
	
}

// user_accounts add limit and start
var start,limit;
start = 0;
start_val = 20;
limit = 20;
var page = 1;

function user_account(){
	
	var search = $("#search").val();
	var mydata = 'action=show_users' + '&search=' + search + '&start=' + start + '&limit=' + limit + '&page=' + page;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){$("#loader_side").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			setTimeout(function(){$("#tbl_users").html(data);},800);
			
		}
	});	
}

function next_record(pages){
	page = pages;
	user_account();
}

// user_accounts add limit and start
	
function change_user_type(index,id){
		var user_type = index;
		var img = '';
		$("#user_type").modal({'backdrop':'static'});

		if (user_type == 3) 
		{
			img = '<img src="../img/guest.png" class="img-fluid img-thumbnail" width="120"><h3 class="mtxt-dark">GUEST USER</h3>';
		}
		else if (user_type == 2) 
		{
			img = '<img src="../img/contributor.png" class="img-fluid img-thumbnail" width="120"><h3 class="mtxt-dark">CONTRIBUTOR</h3>';
		}
		$("#user_icon").html(img);
		$("#user").val(index);
		$("#uid").val(id);
	}

function choose_user_type(index){
	var user_type = index;
	var img = '';
	$("#user_type").modal({'backdrop':'static'});

	if (user_type == 3) 
	{
		img = '<img src="../img/guest.png" class="img-fluid img-thumbnail" width="120"><h3 class="mtxt-dark">GUEST USER</h3>';
	}
	else if (user_type == 2) 
	{
		img = '<img src="../img/contributor.png" class="img-fluid img-thumbnail" width="120"><h3 class="mtxt-dark">CONTRIBUTOR</h3>';
	}
	else if (user_type == 4) 
	{
		img = '<img src="../img/inactive.png" class="img-fluid img-thumbnail" width="120"><h3 class="mtxt-dark">INACTIVE</h3>';
	}
	$("#user_icon").html(img);
	$("#user").val(index);
}

function change_user(user,uid){
		// alert(user+' '+uid);
	var mydata = 'user=' + user + '&uid=' + uid + '&action=update_user';
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			if (data == 1) 
			{
				user_account();
				$("#user_type").modal('hide');
			}
		
		}
	});
}


function uploading(){
	$(document).ready(function(){
	$(document).on('change', '#user_image', function(){
    var name = document.getElementById("user_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("user_image").files[0]);
	var f = document.getElementById("user_image").files[0];
	var fsize = f.size||f.fileSize;

      if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
      swal("Sorry","Invalid file format !","error");
        }
      else if(fsize > 2000000){
       swal("Sorry","File is too large !","error");
        }
      else
        {
        form_data.append("user_image", document.getElementById('user_image').files[0]);
        $.ajax({
          url:"upload.php",
          method:"POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend:function(){
            $('#user_img_preview').html("<label class='text-success'>Image Uploading...</label>");
            },   
          success:function(data)
            {
            $("#btn-up").hide('fast');
            $('#user_img_preview').html(data);
            $("#btn_save_del").show('fast');
            }
            });
            }
            });
	});
}
 function load_pictures(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=update_picture',
      cache:false,
      success:function(data){
         // alert(data.trim());
        $("#img_pf").html('<img src='+data.trim()+' class="imgs2 img-responsive w3-circle animated bounceIn" alt="">');
        $("#img_pf_pic").html('<img src="../img/img.png" style="background-image: url('+data.trim()+');" id="logo_img" class="ad img-album w3-circle animated bounceIn">');
      	$("#profile_pic").html('<img src='+data.trim()+' class="profile_pic img-responsive animated bounceIn" alt="">')
      }
    });
  }

  function save_image(path){
		// alert(path);
	var mydata = 'action=save_image' + '&path=' + path;
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
			$("#profile_pic").html('<br><br><center><img src="../img/load.gif" width="30" class="img-fluid w3-margin-bottom"></center>');
			},
			success:function(data){
			if (data.trim() == 1) 
			{
			setTimeout(function(){
				load_pictures();
			},1000);
			$("#change_profile_pic").modal("hide");	
			$("#user_image").val('');
			$("#btn_save_del").hide('fast');
			$("#btn-up").show('fast');
			}
			else
			{
				alert(data);
			}
		}
	});
	}

	function remove_image(path,img){
		var mydata = 'action=del_image' + '&path=' + path;
		//alert(img);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
				// alert(path);	
				if (data == 1) 
				{
					$("#btn-up").show('fast');
					$("#user_image").val('');
					$("#btn_save_del").hide('fast');
					$('#user_img_preview').html('<img src='+img+' class="profile_pic img-responsive animated bounceIn" alt="">');
				}
			}
		});
	}


/*var num = 1;

 function open_sidebar(){
    if (num == 1) 
    {
      $(".sidenav").animate({width:'0px'});
      $(".sidenav").toggleClass('w3-animate-opacity');
      num = 0;
    }
    else
    {
      $(".sidenav").animate({width:'250px'});
      $(".sidenav").addClass('w3-animate-opacity');
       num = 1;
    }
   
  }
  
window.addEventListener("resize", function(){
if (screen.width >= 1366 || screen.height >= 768) 
  {
       $(".sidenav").animate({width:'250px'});
       $(".sidenav").toggleClass('w3-animate-opacity');
  }

});*/

//OJI JS

function v_details(aud_id,song,total){

$("#aud_id").val(aud_id);
$("#song_name").html(song);
$("#total").html(total);
load_details();
$("#download_details").modal({'backdrop':'static'});

}

function load_details(){
	var aud_id = document.getElementById('aud_id');
	var mydata = 'action=download_details' + '&aud_id=' + aud_id.value;
	// alert(mydata);
	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){$("#d_details").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
	success:function(data){
	setTimeout(function(){$("#d_details").html(data);},800); 
	}
	}); 
}

function played_details(aud_id,song,total){
$("#id_song").val(aud_id);
$("#song_name").html(song);
$("#total_played").html(total);
load_played_details();
$("#played_details").modal({'backdrop':'static'});

}

function load_played_details(){
	var aud_id = document.getElementById('id_song');
	var mydata = 'action=load_played_details' + '&id_song=' + aud_id.value;
	// alert(mydata);
	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){$("#play_details").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
	success:function(data){
	setTimeout(function(){$("#play_details").html(data);},800); 
	}
	}); 
}

function music_downloads(){
	var search = $("#search_musicd").val();
	var filter = document.getElementById('filter_download');
	var top = document.getElementById('top');
	var mydata = 'action=download_music' + '&search_musicd=' + search + '&filter_download=' + filter.value + '&top=' + top.value;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		//beforeSend:function(){$("#tbl_downloads").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){

			if(filter.value == 0){
				//default
				document.getElementById('contri').style.display = 'none';

			}else if (filter.value == 1){
				//contributor

				document.getElementById('contri').removeAttribute("style");

			}

			setTimeout(function(){$("#tbl_downloads").html(data);},800);
			
		}
	});	
}

function new_uploads(){
	var search = $("#search_upload").val();
	var mydata = 'action=new_uploads' + '&search_upload=' + search;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){$("#tbl_new_uploads").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			setTimeout(function(){$("#tbl_new_uploads").html(data);},800);
			
		}
	});	
}

function upload_details(){
	var aud_id = document.getElementById('up_id');
	var mydata = 'action=upload_details' + '&up_id=' + aud_id.value;
	// alert(mydata);
	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){$("#load_uinfo").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
	success:function(data){
	setTimeout(function(){$("#load_uinfo").html(data);},800); 
	}
	}); 
}

function upload_data(aud_id){
$("#up_id").val(aud_id);
upload_details();
$("#uploaded_data").modal({'backdrop':'static'});

}

//accept song
function accept_song(aud){

var mydata = 'action=accept_song' + '&aud=' + aud;

swal({
title: "Are you sure?",
text: "Do you want to Approve this Music?",
type: "info",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Administrator!", "Music has been approved !", "success");
$("#uploaded_data").modal('hide');
$("#myAudio").html('');
new_uploads();
}
else
{
alert(data);
}
}
});
})

}

//disapproved song
function disapprove_song(aud){

var mydata = 'action=disapprove_song' + '&aud=' + aud;

swal({
title: "Are you sure?",
text: "Do you want to Disapprove this Music?",
type: "error",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Administrator!", "Music has been disapproved !", "success");
$("#uploaded_data").modal('hide');
$("#myAudio").html('');
new_uploads();
}
else
{
alert(data);
}
}
});
})

}

function playAudio(aud) { 
    $("#myAudio").html('<audio controls controlsList="nodownload noremoteplayback" style="background-color:transparent;" id="my_Audio" autoplay="" src="'+aud+'"></audio>');
    //$("#s_name").val(name);
    //$("#now_play").show('fast');
    //$("#player").show('fast');
    //document.getElementById(id).style.backgroundColor = "#A9A9A9";
} 

function pause_(aud){
	var pause_ = document.getElementById('my_Audio');
	pause_.pause();
}

$(document).ready(function(){  

$('#uploaded_data').on('hidden.bs.modal', function () {

pause_();

})

});

function album_details(ids,name,total){

	$("#album_id1").val(ids);
	$("#a_name").html(name);
	$("#a_total").html(total);
	load_album_details();
	$("#album_details").modal({'backdrop':'static'});
}

function load_album_details(){
	var album_id = document.getElementById('album_id1');

	var mydata = 'action=load_album_details' + '&album_id1=' + album_id.value;
	// alert(mydata);
	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){$("#a_details").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
	success:function(data){
	setTimeout(function(){$("#a_details").html(data);},800); 
	}
	}); 
}

function upload_picture(){
	$(document).ready(function(){
	$(document).on('change', '#album_image', function(){
	var name = document.getElementById("album_image").files[0].name;
	var form_data = new FormData();
	var ext = name.split('.').pop().toLowerCase();
	if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
	{
	alert("Invalid File");
	}
	var oFReader = new FileReader();
	oFReader.readAsDataURL(document.getElementById("album_image").files[0]);
	var f = document.getElementById("album_image").files[0];
	var fsize = f.size||f.fileSize;
	if(fsize > 2000000)
	{
	alert("File Size is very big");
	}
	else
	{
	form_data.append("album_image", document.getElementById('album_image').files[0]);
	$.ajax({
	url:"upload_image.php",
	method:"POST",
	data: form_data,
	contentType: false,
	cache: false,
	processData: false,
	beforeSend:function(){
	$('#image_preview1').html("<label class='text-success'>Image Uploading...</label>");
	},   
	success:function(data)
	{
	$('#btn-chooser').removeClass('btn btn-primary');
	$('#image_preview1').html(data);
	document.getElementById('album_image').disabled= true;
	$('#btn-chooser').addClass('btn btn-secondary');
	}
	});
	}
	});
	});
}

function remove_img(){
	var path = $('#remove_img').data("path") + 'action=remove_image';
	swal({
	title: "Are you sure?",
	text: "Do you want to remove this Image?",
	type: "warning",
	showCancelButton: true,
	confirmButtonColor: "#34464a",
	confirmButtonText: "Yes",
	closeOnConfirm: false
	}, 
	function(){
		$.ajax({  
		url:url,  
		type:"POST",  
		data:path,
		cache:false,  
		success:function(data){  
		if(data != '')  
		{  
		swal("Deleted!", "Image has been deleted !", "success");
		$('#image_preview1').html('');
		}
		else  
		{  
		return false;  
		}  
		$('#btn-chooser').removeClass('btn btn-secondary');
		document.getElementById('album_image').disabled= false;
		$('#btn-chooser').addClass('btn btn-primary');
		$('#album_image').val('');
		}  
		});  
	});
}

function submit_album()
{
  var album_image = $('#album_image').val(); 
  var album_name = $('#album_name').val();
  var extension = $('#album_image').val().split('.').pop().toLowerCase();
  var link = $("#link").val();   
  
 
  if(album_name == '')  
  {  
  	 $('#album_name').focus();
  	swal("Music Library!", "Please enter album name !", "error");
  
    return false;  
  }  
  else if(album_image == '')  
  {  
    swal("Music Library!", "Please attach album artwork !", "error");
    return false;  
  } 
  else if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)  
  {  
    swal("Music Library!", "Invalid image file!", "info");
    $('#album_image').val('');  
    return false;  
  }
  else 
  {
    var mydata = 'link=' + link + '&album_name=' + album_name;
    $.ajax({
      type:"POST",
      url:"save_album.php",
      data:mydata,
      cache:false,
      success:function(data){
        console.log(data);
        if (data == 1) 
        {
          swal("Music Library!", "Album successfully saved.", "success");
          load_albumsss();
          $('#add_album').modal('hide');          
          $('#album_name').val('');
          save_able();
        }
      }
    });
  }  
}

function save_able(){
  $('#btn-chooser').removeClass('btn btn-secondary');
  document.getElementById('album_image').disabled= false;
  $('#btn-chooser').addClass('btn btn-primary');
  $('#album_image').val('');
  $('#image_preview1').html('');
}

function load_albumsss(){
	var search = $("#search_album").val();
	var mydata = 'action=load_albums' + '&search_album=' + search;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){$("#load_albums").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			// alert(data);
			setTimeout(function(){$("#load_albums").html(data);},800);
			
		}
	});	
}

function edit_album(a_id,name,album_name){

$("#a_id").val(a_id);
$("#e_name").html(name);
$("#e_album_name").val(album_name);
$("#update_album").modal({'backdrop':'static'});

}

//accept song
function update_album(){
var album_id = document.getElementById('e_album_name');
var id = document.getElementById('a_id');
var mydata = 'action=update_album' + '&e_album_name=' + album_id.value + '&a_id=' + id.value;

$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	success:function(data){
	if (data == 1) 
	{
	load_albumsss();
	$("#update_album").modal('hide');
	$("#e_album_name").val('');
	$("#e_name").html('');
	$("#e_album_id").val('');
	
	}
	else
	{
	alert(data);
	}
	}
});


}

//delete album
function delete_album(id){

var mydata = 'action=delete_album' + '&id=' + id;

swal({
title: "Are you sure?",
text: "Do you want to delete this album?",
type: "error",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
swal("Administrator!", "Album has been deleted !", "success");
load_albumsss();
}
else
{
alert(data);
}
}
});
})

}

function load_mymusic(){
	var search = $("#search_music2").val();
	var sort_status = document.getElementById('sort_status');
	var mydata = 'action=load_mymusic' + '&search_music2=' + search + '&sort_status=' + sort_status.value;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){$("#tbl_mymusic").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			setTimeout(function(){$("#tbl_mymusic").html(data);},800);
			
		}
	});	
}


function get_duration(total){
var seconds = "0" + (Math.floor(total) - minutes * 60);
var seconds = "0" + Math.floor(total % 60);
var minutes = "0" + Math.floor(total / 60);
var seconds = "0" +  Math.floor(total - minutes * 60);
//concat minutes and seconds
var dur = minutes.substr(-2) + ":" + seconds.substr(-2);
$("#duration").val(dur);
// alert(dur);
}

// function submitsong2(){
// 	alert("asdas");
// var uploaded_song2 = $('#uploaded_song2').val(); 
// var song_name = $('#song_name2').val();
// var composer = $('#composer2').val();
// var artist = $('#artist2').val();
// var extension = $('#uploaded_songs').val().split('.').pop().toLowerCase();
// var link2 = $("#link2").val();
// var genres = $("#genres2").val();   
// var album = $("#album2").val(); 
// var audience = $("#audience").val(); 
// var application = $("#application").val(); 
// var duration = $("#duration").val(); 

// if(uploaded_song2 == '')  
// {  
// swal("Oops", "Please Select Song !", "warning"); 
// $("#uploaded_song2").focus();
// } 
// if(song_name == '')  
// {  
// swal("Oops", "Please Input Song Name !", "warning"); 
// $("#song_name2").focus();
//  return false;
// } 
// if(composer == '')  
// {  
// swal("Oops", "Please Input Composer !", "warning"); 
// $("#composer2").focus();
// return false;
// }
// if(artist == '')  
// {  
// swal("Oops", "Please Input Artist !", "warning");
// $("#artist2").focus();
// return false;   
// } 
// if(jQuery.inArray(extension, ['mp3']) == -1)  
// {  
// swal("Oops", "Please Select Song", "warning");  
// return false;
// }
// if (genres == null || genres == "" || genres == "genres2") 
// {
// swal("Oops", "Please Select Genre !", "warning");    
// }
// else if (album == null || album == "" || album == "album2") 
// {
// swal("Oops", "Please Select Album !", "warning");    
// }
// else 
// {
// var mydata ='action=upload_song' + '&link2=' + link2 + '&song_name=' + song_name + '&composer=' + composer + '&artist=' + artist + '&genres=' + genres + '&album=' + album + '&audience=' + audience + '&application=' + application + '&duration=' + duration;
// //alert(mydata);
// $.ajax({
// type:"POST",
// url:url,
// data:mydata,
// cache:false,
// success:function(data){
// //alert(data);
// //console.log(data);
// if (data == 1) 
// {
// swal("Success", "Song has been Added!", "success");
// $('#addmusic').modal('hide');
// $('#song_name2').val('');
// $('#composer2').val('');
// $('#artist2').val('');
// load_mymusic();
// song_able2();
// }
// }
// });
// }  
// }

function upload_music2(){
$(document).ready(function(){
$(document).on('change', '#uploaded_song2', function(){
var name = document.getElementById("uploaded_song2").files[0].name;
var form_data = new FormData();
var ext1 = name.split('.').pop().toLowerCase();
if(jQuery.inArray(ext1, ['mp3']) == -1) 
{
alert("Invalid Song");
}
var oFReader = new FileReader();
oFReader.readAsDataURL(document.getElementById("uploaded_song2").files[0]);
var f = document.getElementById("uploaded_song2").files[0];
var fsize = f.size||f.fileSize;
if(fsize > 50000000)
{
alert("Invalid, Large File Size!");
}
else
{
form_data.append("uploaded_song2", document.getElementById('uploaded_song2').files[0]);

//alert(form_data);
$.ajax({
url:"uploadsong2.php",
method:"POST",
data: form_data,
contentType: false,
cache: false,
processData: false,
beforeSend:function(){
$('#song_preview2').html("<label class='text-success'>Music Uploading...</label>");
},   
success:function(data)
{
document.getElementById('submitsong2').disabled= false;
$('#btn-select2').removeClass('btn btn-primary');
$('#song_preview2').html(data);
$('#btn-select2').addClass('btn btn-secondary');
}
});
}
});
});
}

function remove_song(path1){ 
var path = 'action=remove_song'+ '&path1=' + path1;
swal({
title: "Are you sure?",
text: "Do you want to Remove this Song?",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){  
$.ajax({  
url:url,  
type:"POST",  
data:path,  
success:function(data){  
if(data != '')  
{  
swal("Removed!", "Song has been Removed!", "success");
$('#song_preview2').html('');
} 
else  
{  
return false;  
} 
$('#btn-select2').removeClass('btn btn-secondary');
document.getElementById('uploaded_song2').disabled= false;
document.getElementById('submitsong2').disabled= true;
$('#btn-select2').addClass('btn btn-primary');
$('#uploaded_song2').val('');
}  
});  
});
}

/*function song_able2(){
	$('#btn-select2').removeClass('btn btn-secondary');
	document.getElementById('uploaded_song2').disabled= false;
	$('#btn-select2').addClass('btn btn-primary');
	$('#uploaded_song2').val('');
	$('#song_preview2').html('');
	$('#genres2').val('Genres');
	$('#album2').val('Album');
}*/

  //post admin song
function post_music(aud){

var mydata = 'action=post_music' + '&aud=' + aud;
swal({
title: "Are you sure?",
text: "Do you want to Post this song?",
type: "info",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
load_mymusic();
swal("Administrator!", "Song has been posted !", "success");
$("#post_music").val('Unpost')
}
else
{
alert(data);
}
}
});
})

}

  //post admin song
function post_music(aud){

var mydata = 'action=post_music' + '&aud=' + aud;
swal({
title: "Are you sure?",
text: "Do you want to Post this song?",
type: "info",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
load_mymusic();
swal("Administrator!", "Song has been posted !", "success");
$("#post_music").val('Unpost')
}
else
{
alert(data);
}
}
});
})

}

  //post admin song
function unpost_music(aud){

var mydata = 'action=unpost_music' + '&aud=' + aud;
swal({
title: "Are you sure?",
text: "Do you want to Unpost this song?",
type: "info",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
load_mymusic();
swal("Administrator!", "Song has been Unposted !", "success");
$("#post_music").val('Unpost')
}
else
{
alert(data);
}
}
});
})

}

  //post admin song
function delete_music(index,music){

var mydata = 'action=delete_music' + '&aud=' + index + '&music=' + music;
swal({
title: "Are you sure?",
text: "Do you want to delete this song?",
type: "info",
showCancelButton: true,
confirmButtonColor: "#34464a",
confirmButtonText: "Yes",
closeOnConfirm: false
},
function(){
$.ajax({
type:"POST",
url:url,
data:mydata,
cache:false,
success:function(data){
if (data == 1) 
{
load_mymusic();
swal("Administrator!", "Song has been deleted !", "success");
$("#post_music").val('Unpost')
}
else
{
alert(data);
}
}
});
})

}

//update mymusic
function edit_music(id,genre,album_id,audience_id,application_id,song_name,composer,artist){
    $("#editmusic").modal('show');
    $("#songid").val(id);
    $("#sname").val(song_name);
    $("#scomp").val(composer);
    $("#sart").val(artist);
    $("#sgen").val(genre);
    $("#salb").val(album_id);
    $("#saud").val(audience_id);
    $("#sapp").val(application_id);
    }

function update_music(){
  var song_name = $("#sname").val();
  var composer = $("#scomp").val();
  var artist = $("#sart").val();
  var genre = $("#sgen").val();
  var albumname = $("#salb").val();
  var audiencename = $("#saud").val();
  var applicationname = $("#sapp").val();
  var id = $("#songid").val();
  if (genre == "") 
  {
  $("#sgen").focus();
  }
  if (albumname == "") 
  {
  $("#salb").focus();
  }
  if (audiencename == "") 
  {
  $("#saud").focus();
  }
  if (applicationname == "") 
  {
  $("#sapp").focus();
  }
  if (song_name == "") 
  {
  $("#sname").focus();
  }
  if (composer == "") 
  {
  $("#scomp").focus();
  }
  if (artist == "") 
  {
  $("#sart").focus();
  }
  else
  {
  var mydata ='action=update_music' + '&song_name=' +  song_name + '&composer=' +  composer + '&artist=' +  artist + '&genre=' +  genre + '&albumname=' +  albumname + '&audiencename=' +  audiencename + '&applicationname=' +  applicationname +'&id=' + id;
  //alert(mydata);
  $.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,
    beforeSend:function(){
    $("#loader").html('<span><img src="../img/load.gif" width="20"> </span>');
    },
    success:function(data){
	if (data == 1) 
    {
    load_mymusic();
    swal("Success", "Song has been Updated", "success");
    $("#editmusic").modal('hide');
    $("#sgen").val('');
    $("#sname").val('');
    $("#scomp").val('');
    $("#sart").val('');
    $("#id").val(''); 
    }
    else
    {
    alert(data);
    }
    $("#loader").html('');
    }
    });
    }
    }


///////////////////////////////////////////////////////////////////////////////////////New Update

    function app_disapp(control){
	var mydata = 'action=app_disapp' + '&id=' + control;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#"+control+"").toggleClass("fa-thumbs-down text-danger");
			$("#"+control+"").toggleClass("fa-thumbs-up  text-primary animated jackInTheBox");
			$("."+control+"").text('Please Wait...');
		},
		success:function(data){
			if (data == 1) 
			{
				user_account();
			}
			else
			{
				alert(data);
			}
		}
	});
}

function subcribe_app_disapp(control){
	var mydata = 'action=subcribe_app_disapp' + '&id=' + control;
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#"+control+"").toggleClass("fa-thumbs-down text-danger");
			$("#"+control+"").toggleClass("fa-thumbs-up  text-primary animated jackInTheBox");
			$("."+control+"").text('Please Wait...');
		},
		success:function(data){
			if (data == 1) 
			{
				subscribers();
			}
			else
			{
				alert(data);
			}
		}
	});
}

function toggle_pwd(id,mypwd,mybtn){
	var x = document.getElementById(id);
	  if (x.type === "password") {
		swal({
		title: "Security Alert",
		text: "Please enter your password:",
		type: "input",
		showCancelButton: true,
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Write something",
		inputType:'password'
		},
		function(inputValue){
		if (inputValue === false) return false;

		if (inputValue === "") {
		swal.showInputError("Please Enter your password!");
		return false
		}else if (inputValue == mypwd) 
		{
		swal("Thank you!", "Correct Password!", "success");
		$("."+id+"").toggleClass("fa-eye");
		$("."+id+"").toggleClass("fa-eye-slash animated bounceIn");
	
		$("#"+mybtn+"").show('fast');
		x.type = "show";
		x.focus();
		}
		else
		{
		swal.showInputError("Password doesn\'t match!");
		return false
		}
		
		});
	    } else {
	    $("."+id+"").toggleClass("fa-eye");
		$("."+id+"").toggleClass("fa-eye-slash animated bounceIn");
	        x.type = "password";
	        $("#"+mybtn+"").hide('fast');
	    
	    }
	
}

function update_password_client(user_id,client_name,btn_id){
$("#c_name").text(client_name);
$("#mng_id").val(user_id);
$("#mng_pwd").val('');
$("#id_btn").val(btn_id);
$("#edit_pwd").modal({'backdrop':'static'});
$("#mng_pwd").attr("placeholder", "Enter "+client_name+"\'s new password");
}

function update_account_pwd(){
		var id_btn,mng_id,mng_pwd,verify_pwd;
		id_btn = $("#id_btn").val();
		mng_id = $("#mng_id").val();
		mng_pwd = $("#mng_pwd").val();
		verify_pwd = $("#verify_pwd").val();

		if (mng_pwd == "") 
		{
			$("#mng_pwd").focus();
		}
		else if(verify_pwd == ''){
			$("#verify_pwd").focus();
		}
		else if(mng_pwd != verify_pwd){
			$("#mng_pwd").focus();
			$("#verify_pwd").val('');
			swal("Password is not Equal","Make sure the password is Equal","warning");
		}
		else
		{
			var mydata = 'action=mng_client_password' + '&user_id=' + mng_id + '&mng_pwd=' + mng_pwd;
			// alert(mydata);
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data.trim() == 1) 
					{
						user_account();
						$("#mng_pwd").val('');
						$("#verify_pwd").val('');
						$("#"+id_btn+"").hide('fast');
						$("#edit_pwd").modal('hide');

					}
					else
					{
						alert(data.trim());
					}
				}
			});
		}
	}

	function get_client_info(cid,cfn,cln,cemail){
		$("#cli_id").val(cid);
		$("#new_fn").val(cfn);
		$("#new_ln").val(cln);
		$("#new_mail").val(cemail);
		
		$("#edit_client_info").modal({'backdrop':'static'});
	}

	function update_client_info(){
		var cli_id,new_fn,new_ln,new_mail;
		cli_id = $("#cli_id").val();
		new_fn = $("#new_fn").val();
		new_ln = $("#new_ln").val();
		new_mail = $("#new_mail").val();

		if (new_fn == "") 
		{
			$("#new_fn").focus();
		}
		else if (new_ln == "") 
		{
			$("#new_ln").focus();
		}
		else if (new_mail == "") 
		{
			$("#new_mail").focus();
		}
		else
		{
			var mydata = 'action=update_client_info' + '&fn=' + new_fn + '&ln=' + new_ln + '&email=' + new_mail + '&user_id=' + cli_id;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data.trim() == 1) 
					{
						$("#edit_client_info").modal('hide');
						swal("Success","Account has been updated!","success");
						user_account();
					}
					else if (data.trim() == 3) 
					{
						swal("Oops!","Please enter valid e-mail address!","error");
						$("#new_mail").focus();
					}
					else
					{
						alert(data);
					}
				}
			});
		}

	}

	function subscribers(){
	var search = $("#search_subs").val();
	var mydata = 'action=subscribers' + '&search_subs=' + search;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){$("#tbl_subscribers").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			setTimeout(function(){$("#tbl_subscribers").html(data);},800);			
		}
	});	
}


function add_client(){
		var fn,ln,email,user_type;
		fn = $("#add_fn").val();
		ln = $("#add_ln").val();
		email = $("#add_email").val();
		user_type = $("#add_type").val();
		if (fn == "") 
		{
			$("#add_fn").focus();
		}
		else if (ln == "") 
		{
			$("#add_ln").focus();
		}
		else if (email == "") 
		{
			("#add_email").focus();
		}
		else if (user_type == 'Select user type' ||  user_type == null) 
		{
			 $("#add_type").focus();
		}
		else
		{
			var mydata = 'action=add_user_client' + '&fn=' + fn + '&ln=' + ln + '&user_type=' + user_type + '&email=' + email;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data.trim() == 1) 
					{
						//Successfuly added
						fn = $("#add_fn").val('');
						ln = $("#add_ln").val('');
						email = $("#add_email").val('');
						$("#add_user").modal('hide');
						user_type = $("#add_type").val('Select user type');
						swal("Success","User has been added","success");
						user_account();

					}
					else if (data.trim() == 2) 
					{
						//Already used email
						$("#add_email").focus();
						swal("Oops!","E-mail address is already used !","error");
					}
					else if (data.trim() == 3) 
					{
						//Invalid email address
						$("#add_email").focus();
						swal("Oops!","Please enter valid e-mail!","error");
					}
					else
					{
						console.log(data);
					}
				}
			});
		}

	}

//////////////////////////////////// OJI UPDATE 7/7/2018

function subs_info(id,total_subs){

		$("#sub_id").val(id);
		$("#total_subs").html(total_subs);
		load_subs_info();
		$("#subs_info").modal({'backdrop':'static'});
	}

function load_subs_info(){
	var sub_id = document.getElementById('sub_id');

	var mydata = 'action=load_subs_info' + '&sub_id=' + sub_id.value;

	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){$("#sub_details").html('<tr colspan="3"><td><center><img src="../img/load.gif" class="img-fluid" width="30"></center></td></tr>');},
	success:function(data){
	setTimeout(function(){$("#sub_details").html(data);},800); 
	}
	});
}


function show_style(){
mydata ='action=show_style';
$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){

	},
	success:function(data){
	$("#genres2").html(data);
	}
});
}

function show_album(){
mydata ='action=show_album';
$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){

	},
	success:function(data){
	$("#album2").html(data);
	}
});
}

function show_audience(){
mydata ='action=show_audience';
$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){

	},
	success:function(data){
	$("#audience").html(data);
	}
});
}

function show_app(){
mydata ='action=show_app';
$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){

	},
	success:function(data){
	$("#application").html(data);
	}
});
}


function show_dropdowns(){
	show_style();
	show_album();
	show_audience();
	show_app();
}

function show_music_library(){
	var search3 = $("#search_music").val();
	var mydata = 'action=music_library_tbl' + '&search_music=' + search3;
	// alert(mydata);
	$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	//beforeSend:function(){$("#tbl_songs").html('<div class="col-sm-12 text-center"><img src="../img/giphy.gif" class="img-fluid" width="30"></div>'); },
	success:function(data){
	setTimeout(function(){$("#list_music").html(data);},500); 
	}
	}); 
}

function music_info(id){
$("#music_id").val(id);
load_music_info();
$("#music_info_modal").modal({'backdrop':'static'});
}
function load_music_info(){
  var music_id = document.getElementById('music_id');
  var mydata = 'action=music_details' + '&music_id=' + music_id.value;
  // alert(mydata);
  $.ajax({
  type:"POST",
  url:url,
  data:mydata,
  cache:false,
  beforeSend:function(){$("#music_info").html('<img src="../img/giphy.gif" class="img-fluid" width="30">');},
  success:function(data){
  setTimeout(function(){$("#music_info").html(data);},800); 
  }
  }); 
}

function load_about_content(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=about_content',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#about_content").html(data);
        },100);
    }
  });
}

//UPDATE ABOUT HEADER
function get_about_header(id){
	$("#header_id").val(id);
	$("#update_header_modal").modal({'backdrop':'static'});
	load_update_header();
}

function load_update_header(){
	id = document.getElementById('header_id');
	mydata = 'action=load_update_header' + '&header_id=' + id.value;

	$.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#load_update_header").html(data);
        },100);
    }
  });
}

function update_header_about(){
		var id,new_header,mydata;
		id = $("#header_id").val();
		new_header = $("#new_header").val();
		if (new_header == "") 
		{
			$("#new_header").focus();
		}
		else
		{
			mydata = 'action=update_about_header' + '&header_id=' + id + '&new_header=' + new_header;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data == 1) 
					{
						//Successfuly Updated
						$("#update_header_modal").modal('hide');
						swal("Success","About Header has been Updated.","success");
						load_about_content()

					}
					else
					{
						console.log(data);
					}
				}
			});
		}

	}

//UPDATE ABOUT BODY
function get_about_body(id){
	$("#body_id").val(id);
	$("#update_body_modal").modal({'backdrop':'static'});
	load_update_body();
}

function load_update_body(){
	id = document.getElementById('body_id');
	mydata = 'action=load_update_body' + '&body_id=' + id.value;

	$.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#load_update_body").html(data);
        },100);
    }
  });
}

function update_body_about(){
		var id,new_body,mydata;
		id = $("#body_id").val();
		new_body = $("#new_body").val();
		if (new_body == "") 
		{
			$("#new_body").focus();
		}
		else
		{
			mydata = 'action=update_about_body' + '&body_id=' + id + '&new_body=' + new_body;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data == 1) 
					{
						//Successfuly Updated
						$("#update_body_modal").modal('hide');
						swal("Success","About Body has been Updated.","success");
						load_about_content()

					}
					else
					{
						console.log(data);
					}
				}
			});
		}

}

function load_faqs_content(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=faqs_content',
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#faqs_content").html(data);
        },100);
    }
  });
}

function save_faqs(){
		var title,content,mydata;
		title = $("#title").val();
		content = $("#content").val();
		if (title == "") 
		{
			$("#new_body").focus();
		}
		else if (content == "") 
		{
			$("#up_content").focus();
		}
		else
		{
			mydata = 'action=save_faqs' + '&title=' + title + '&content=' + content;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){
					if (data == 1) 
					{
						//Successfuly Updated
						$("#add_faqs").modal('hide');
						swal("Success","New FAQ's Successfully Saved.","success");
						load_faqs_content();

					}
					else
					{
						console.log(data);
					}
				}
			});
		}

}

function edit_faqs(id){
	$("#faqs_id").val(id);
	$("#update_faqs").modal({'backdrop':'static'});
	load_update_faqs();
}

function load_update_faqs(){
	id = document.getElementById('faqs_id');
	mydata = 'action=load_update_faqs' + '&faqs_id=' + id.value;

	$.ajax({
    type:"POST",
    url:url,
    data:mydata,
    cache:false,    
    success:function(data){
      setTimeout(function(){
        $("#load_update_faqs").html(data);
        },100);
    }
  });
}

function update_faqs(){
		var id,title,content,mydata;
		id = $("#faqs_id").val();
		title = $("#up_title").val();
		content = $("#up_content").val();
		if (title == "") 
		{
			$("#up_title").focus();
		}
		else if (content == "") 
		{
			$("#up_content").focus();
		}
		else
		{
			mydata = 'action=update_faqs' + '&faqs_id=' + id + '&up_title=' + title + '&up_content=' + content;
			$.ajax({
				type:"POST",
				url:url,
				data:mydata,
				cache:false,
				beforeSend:function(){},
				success:function(data){

				    if (data == 1) 
					{
						//Successfuly Updated
						$("#update_faqs").modal('hide');
						swal("Success","FAQ's Successfully Updated.","success");
						load_faqs_content();

					}
					else
					{
						console.log(data);
					}
				}
			});
		}

}

function delete_faqs(id){
	var mydata ='action=delete_faqs'+'&id=' + id;
	swal({
	  title: "Are you sure?",
	  text: "Do you want to delete this record ?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#34464a",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			success:function(data){
			if (data == 1) 
			{
				swal("Deleted!", "Record has been deleted !", "success");
				load_faqs_content();
			}
			else
			{
				alert(data);
			}
			}
		});
	 
	});
}

function music_plays(){
	var search = $("#search_plays").val();
	var top = document.getElementById('top_plays');
	var mydata = 'action=music_played' + '&search_plays=' + search + '&top_plays=' + top.value;
	// alert(mydata);
	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		//beforeSend:function(){$("#tbl_downloads").html('<br><br><center><img src="../img/load.gif" class="img-fluid" width="20"></center>');},
		success:function(data){
			setTimeout(function(){$("#tbl_played").html(data);},800);
		}
	});	
}

function save_play(id){
    //alert(path);
    var mydata = 'action=save_play' + '&id=' + id;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(data){
      if (data == 1) 
      {
     
      }
    }
  });
}

function user_logs(){
	var mydata,search;
	search = $('#search_logs').val();
	mydata = 'action=load_user_logs' + '&search_logs=' + search;

	$.ajax({
		type:'POST',
		url:url,
		data:mydata,
		cache:false,
		success:function(data){
			setTimeout(function(){$("#tbl_user_logs").html(data);},800);
		}
	});
}