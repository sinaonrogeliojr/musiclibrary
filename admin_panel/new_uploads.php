<?php
session_start();
if (!isset($_SESSION['admin'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script type="text/javascript" src="function.js"></script>
   <script type="text/javascript" src="../js/audio-index.js"></script>

  <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="admin.css">
  <link href="../css/w3.css" rel="stylesheet">

  <!-- FONT AWESOME -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <?php include_once("modal.php"); ?>
  <style type="text/css">
  	.imgs2 {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
	}
  	.img_music {
    width: 60px; /* You can set the dimensions to whatever you want */
    height:60px;
    object-fit: cover;
	}
	.img_music2 {
    width: 200px; /* You can set the dimensions to whatever you want */
    height:200px;
    object-fit: cover;
	}
	.imgs_player {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}
	.select_type{
	background-color: #f4f4f4;
	padding: 20px;
	width: 100%;
	height: 78px;
	border: 1px solid #d5d5d5;
	font-size: 18px;
}
  </style>
</head>
<body onload="new_uploads(); load_pictures();">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow" title="User Management"><a href="index.php"><span class="fa fa-user-circle fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="User Logs"><a href="user_logs.php"><span class="fa fa-users fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Subscribers"><a href="subscribers.php"><span class="fa fa-envelope fa-3x" style="color: #3a4b58;"></span></a>
		</li>
		<li class="w3-hover-shadow" title="Music Management">
			<a href="#settings" data-toggle="collapse" style="color: #8eadab;">
		        <i class="fa fa-music fa-3x" ></i>
		    </a>
		</li>
		<?php include('music_sub_menu.php') ?>
		<li class="w3-hover-shadow" title="Content Management">
			<a href="#management" data-toggle="collapse" style="color: #3a4b58;">
		        <i class="fa fa-gears fa-3x" ></i>
		    </a>
		</li>
		<?php include('setting_sub_menu.php') ?>

		<li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>

	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#" onclick="new_uploads();"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title">Pending Uploads</p>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-lg-6 input"><input type="show" oninput="new_uploads();" name="search_upload" id="search_upload" placeholder="Search New Upload..."></div>
		<div class="col-lg-2">
			<button class="btn btn-dark btn-normal" id="btn-approve" disabled=""><span class="fa fa-thumbs-up"></span> Approve</button>
		</div>
		<div class="col-lg-2">
			<button class="btn btn-dark btn-normal" id="btn-disapprove" disabled=""><span class="fa fa-thumbs-down"></span> Disapprove</button>
		</div>
	</div>

</div>

<div class="container-fluid tracks-table ">
	<div class="row">
		<div class="col-lg-12"  id="tbl_new_uploads">
			
		</div>
	</div>
</div>
<br>
<footer class="container-fluid">
<?php include('../footer.php'); ?>
</footer>
</div>
</body>
</html>
<script type="text/javascript">

	function count_check(){
	var chks = $('.select_uploads').filter(':checked').length

	if(chks > 0){
		document.getElementById('btn-approve').disabled= false;
		document.getElementById('btn-disapprove').disabled= false;
	}else{
		document.getElementById('btn-approve').disabled= true;
		document.getElementById('btn-disapprove').disabled= true;
	}
}

function check_all_validate() {
        if (document.getElementById('chk_uploads').checked) {
            document.getElementById('btn-approve').disabled= false;
			document.getElementById('btn-disapprove').disabled= false;
        } else {
            document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
        }
    }

$(document).ready(function(){
 
 //Approve ALl
 $('#btn-approve').click(function(){
  
  swal({
	  title: "Are you sure?",
	  text: "You wan to Approve Selected data?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Approve",
	  closeOnConfirm: false
	},
	function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {

    $.ajax({
     url:'approve_music_all.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		swal("Music Libray!", "Unable to Approve song.", "info");
			
     	}else{
			swal("Music Libray!", "Song approved.", "info");
			new_uploads();
			document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
     	}
      
     }
     
    });
   }
 });

 });

//Disapprove All
$('#btn-disapprove').click(function(){
  
  swal({
	  title: "Are you sure?",
	  text: "You want to Disapprove Selected data?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Approve",
	  closeOnConfirm: false
	},
	function(){
   var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    swal("Music Libray!", "Please select atleast one", "info");
   }
   else
   {

    $.ajax({
     url:'disapprove_music_all.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		swal("Music Libray!", "Unable to Disapprove music.", "info");
			
     	}else{
			swal("Music Libray!", "Music disapproved.", "info");
			new_uploads();
			document.getElementById('btn-approve').disabled= true;
			document.getElementById('btn-disapprove').disabled= true;
     	}
      
     }
     
    });
   }
 });
  
 }); 


});
</script>
