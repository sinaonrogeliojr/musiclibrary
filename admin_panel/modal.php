<?php include_once("../dbconnection.php"); ?>

<style type="text/css">
  .modal-small{
      max-width: 500px;
      width: 100%;
    }
   .modal-medium{
      max-width: 700px;
      width: 100%;
    }
</style>
<!-- View Music Information -->
<div id="music_info_modal" class="modal fade">  
  <div class="modal-dialog modal-medium">  
    <input type="hidden" id="music_id">
    <div class="modal-content" id="music_info"> 

    </div>  
  </div>  
</div>

<div class="modal fade" role="dialog" id="change_profile_pic">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">Change Profile Picture<button class="close" data-dismiss="modal"><i class="fa fa-remove fa-sm"></i></button></div>
			<div class="modal-body">
				<div></div>
				<input type="file" name="user_image" id="user_image" accept="image/*" style="display: none;">
				<div class="text-center">
				<div id="image_preview">
					<img src="../img/img.png" class="img-album w3-round" width="220" style="background-image: url('<?php echo $_SESSION['user_pic'] ?>'); margin-bottom:5px;">
				</div>
				<div id="btn-up">
				<br>
				<button class="btn btn-primary" onclick="$('#user_image').click();"><i class="fa fa-image"></i> Upload Picture</button>
				</div>
				
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">

</style>

 <!-- Download Details --> 
<div id="download_details" class="modal fade">  
  <div class="modal-dialog modal-lg modal_width">  
    <div class="modal-content" style="background-color: #6C7A89;">  
      <div class="modal-header bg-dark w3-text-white"><h5 class="calbum modal-title"> 
      	<span class="fa fa-download"></span> Download Details :
      	<span id="song_title"></span>
     	  <span class="badge badge-danger" id="total_plays"></span>  
      	</h5>  
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
      </div>  
      <div class="modal-body"> 
           <input type="hidden" id="song_id">
              <div class="row">
              	<div class="col-sm-12 scroll" id="list" >

                  <div class="table-responsive">
                      <table class="table table-hover table-light table-responsive-sm ">
                      <thead class="thead-dark">
                          <tr>
                            <th class="w3-small">NO.</th>
                            <th class="w3-small">NAME</th>
                            <th class="w3-small">LOCATION</th>
                            <th class="w3-small text-center">DATE</th>
                          </tr>
                        </thead>
                        <tbody id="d_details">
                          
                        </tbody>
                      </table>
                  </div>
      				</div>
            </div>
      </div>  
    </div>  
  </div>  
</div>

 <!-- Download Details --> 
<div id="uploaded_data" class="modal fade">  
  <div class="modal-dialog modal_width">  
    <div class="modal-content">  
      <div class="modal-header bg-dark w3-text-white"><h5 class="calbum modal-title"> 
        <span class="fa fa-music"></span> New Uploaded Music Details :
        </h5>  
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
      </div>  
      <div class="modal-body"> 
           <input type="hidden" id="up_id">
              <div class="col-sm-12 mb-2">
                <div class="w3-round w3-card-8" id="load_uinfo">
                  
                </div>
              </div>
      </div>  
    </div>  
  </div>  
</div>

 <!-- Album Details --> 
<div id="album_details" class="modal fade">  
  <div class="modal-dialog modal-lg modal_width">  
    <div class="modal-content">  
      <div class="modal-header bg-dark w3-text-white"><h5 class="calbum modal-title"> 
        <span class="fa fa-image"></span> Album Details : <span id="a_name"></span>&nbsp;<span class="badge badge-danger" id="a_total"></span>  
        <input type="hidden" id="album_id1">
          <span class="badge badge-danger"></span>  
        </h5>  
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
      </div>  
      <div class="modal-body" style="background-image: url(../img/main-bg.png);"> 
           <input type="hidden" id="aud_id">
              <div class="row">
                <div class="col-sm-12 scroll" id="list" >
                  <ul class="list-group text-left" id="a_details">

                  </ul>
              </div>
            </div>
      </div>  
    </div>  
  </div>  
</div>




 <!-- Add Album --> 
<div class="modal fade" role="dialog" id="add_album" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header music-dark mtxt-light">New Album <button class="close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body">
        <div class="container">
          <div id="image_preview1" class="text-center"> </div>
          <input type="file" name="album_image" id="album_image" style="display: none;" /> 

            <div class="row">
                <div class="col-sm-9">
                <label class="text-dark">New Name</label>
                <div class="form-group">
                  <input type="show" class="form-control" name="album_name" id="album_name" placeholder="New Album">
                </div>
                </div>
                <div class="col-sm-3">
                  <label class="text-dark">Image</label>
                  <button class="btn btn-dark btn-small" id="btn-chooser" onclick="$('#album_image').click();"> 
                        <i class="fa fa-file fa-lg"></i> Choose File</button>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer text-center">
        <div class="container">
        <button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
        <button class="btn btn-dark btn-small" onclick="submit_album();"><i class="fa fa-save"></i> Save</button>
      </div>
      </div>
    </div>
  </div>
</div>

 <!-- Update Album --> 
<div class="modal fade" role="dialog" id="update_album" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header music-dark mtxt-light">Update Album   <button class="close" data-dismiss="modal">&times;</button></div>

            <div class="modal-body">
              <div class="container"><br>
                <span class="fa fa-user"></span> Contributor:&nbsp;<span class="text-success" id="e_name"></span>  
                <input type="hidden" id="a_id">
                <span class="badge badge-danger"></span>
                  <div class="row">
                      <div class="col-sm-12">
                      <label class="text-dark">Album Name</label>
                      <div class="form-group">
                        <input type="show" name="e_album_name" id="e_album_name" class="form-control" placeholder="Album Name">
                      </div>
                      </div>
                  </div>
              </div>
            </div>
      <div class="modal-footer text-center">
        <div class="container">
        <button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
        <button class="btn btn-dark btn-small" onclick="update_album();"><i class="fa fa-save"></i> Save</button>
      </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .card-album{
   background-color: #13284eb8; 
   height: 400px;
   max-height: 400px;
  }
   .lister-album{
      background-color: rgba(3,3,3,0.1);
      border-radius: 0px;
      transition: background-color 0.3s;
      cursor: pointer;
    }
    .lister-album:hover{
      background-color: rgba(0,0,0,0.5);
    }
</style>

  <div id="createAlbumModal" class="modal fade" data-backdrop="static">  
    <div class="modal-dialog ">  
      <div class="modal-content w3-round-xxlarge sa-bg AM-content" style="background-color: #6C7A89;">  
        <div class="modal-header text-center SF-head" >
          <h4 class="font-m1 w3-wide text-white text-center" style="width:100%; text-shadow:2px 2px 3px #333;">CREATE NEW ALBUM</h4>  
          <a href="#"  class="close" data-dismiss="modal" onclick="cancelcreate();">
            <i id="closehover" class="fa fa-times"></i></a>
          <hr class="SF-hr">
        </div>    
          <div class="modal-body AM-body">         

            <div id="image_preview2" class="text-center"> </div> <br/>
            <input type="file" name="album_image2" id="album_image2" style="display: none;" /> 
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <input type="text" name="album_name2" id="album_name2" class="form-control text-capitalize" placeholder="Album Name" required="true" maxlength="30" style="text-transform: capitalize;">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="">
                  <button class="btn btn-dark" id="btn-chooser2" onclick="$('#album_image2').click();"> 
                    <i class="fa fa-file fa-lg"></i> Select Image</button>
                </div>
              </div>
              <div class="col-sm-1"></div>

            <div class="col-sm-12">
              <hr class="AM-hr">
              <div class="text-right">
                <button class="btn btn-dark w3-round-xxlarge  font-m1" onclick="submit_album2();">Save Album</button>
                <button class="btn btn-danger  w3-round-xxlarge font-m1" data-dismiss="modal" onclick="cancelcreate();"> Cancel</button>  
              </div>
            </div>

            </div>
          </div>  
      </div>  
    </div>  
  </div>

<?php
include('../dbconnection.php');
$my_id = $_SESSION['admin'];
?>
<!-- Add Musics --> 
<script type="text/javascript">
  
   
</script>
<div id="addmusic" class="modal fade" data-backdrop="static" style="overflow: auto;" >  
  <div class="modal-dialog modal-lg">  
    <div class="modal-content" style="background-color: #6C7A89;">  
      <div class="modal-header ">
        <h4 class="font-m1 w3-wide text-white AM-title text-center " id="title_form">ADD NEW MUSIC</h4>
        <a href="#" class="close AM-close  text-white" data-dismiss="modal" onclick="closeupload();">
          <i id="closehover" class="fa fa-times"></i></a>
        <hr class="AM-hr">
      </div>  
      <div class="modal-body"> 
        <input type="hidden" id="songid">
        <input type="hidden" id="music_path">
        <input type="hidden" id="duration">
        <input type="file" name="uploaded_songs" id="uploaded_songs" style="display: none;" /> 
        <input type="hidden" name="selectedalb_id" id="selectedalb_id">
       <div class="row">
         <div class="col-sm-6">
          <div class="form-group">
             <button class="btn btn-block w3-border btn-dark" style="color: white;" id="btn-select2" onclick="$('#uploaded_songs').click();">
              <i class="fa fa-music fa-lg" ></i> &nbsp;Select Song
            </button>
          </div>
           <div class="form-group">
             <input type="text" name="song_name2" id="song_name2" class="form-control " placeholder="Select Title" required maxlength="30">
           </div>
           <div class="form-group">
               <textarea class="form-control parsley-validated img-responsive AM-input2" name="description" id="description" rows="6" data-minwords="6" data-required="true" placeholder="Description"></textarea>
           </div>
         </div>

         <div class="col-sm-6">
             <div class="form-group">
               <button class="btn w3-border btn-block btn-dark" id="alb_select" onclick="addto_album();" style="color: white; ">
                  <i class="fa fa-folder-o"></i> &nbsp;Album 
               </button>
             </div>
             <div class="form-group">
              <input type="text" name="composer2" id="composer2" class="form-control " placeholder="Composer" required maxlength="30">
            </div>
            <div class="form-group">
                <textarea class="form-control parsley-validated img-responsive AM-input2" name="keyword" id="keyword" rows="6" data-minwords="6" data-required="true" placeholder="Key search words"></textarea>
            </div>
         </div>

         <div class="col-sm-12">
           <div class="card">
             <div class="card-header text-light" style="background-color: #585858;">STYLE</div>
             <div class="card-body">
               <input type="hidden" id="style_value">
                <div class="form-group" style="margin: 0 auto; ">
                      <div class="col-sm-12 AM-style2" style=" margin: 0 auto; border: none;" id="style_select"></div>
                  </div>
             </div>
           </div>
           <br>
            <div class="card">
             <div class="card-header text-light" style="background-color: #585858;">APPLICATION</div>
             <div class="card-body">
               <input type="hidden" id="app_value">
                <div class="form-group" style="margin: 0 auto; ">
                      <div class="col-sm-12 AM-style2" id="app_select"></div>
                  </div>
             </div>
           </div>
            <br>
            <div class="card">
             <div class="card-header text-light" style="background-color: #585858;">SUPPORTING FILES</div>
             <div class="card-body">
              <div class="row">
                <div class="col-sm-5">
               <button class="btn w3-border btn-block btn-dark" style="color: #fff;" id="btn_select" onclick="$('#multi_upload_file').click();">
                &nbsp;Select Files</button>
                <input type="file" multiple name="multi_upload_file[]" id="multi_upload_file" style="display: none;">
                <input type="hidden" name="group_id" id="group_id">
              </div>
              <div class="col-sm-2"></div>
              <div class="col-sm-5">
                <div class="form-group">
                    <div class="input-group">
                      <span class="btn w3-border input-group-addon bg-dark text-white AM-date1" style="background-color: #292f35; color: #fff;">Available from:</span>
                      <input type="date" class="input-md input-s datepicker-input form-control AM-date2" name="date_upload" id="date_upload" size="16">
                    </div>
                 </div>
              </div>
              </div>
             </div>
           </div>
           <br>
         </div>
         <div class="col-sm-12">
           <div class="bg-light" style="overflow-y: auto; height:280px; color: #000;">
              <table class="table table-responsive-sm table-hover table-light" style="">
                <thead style="background-color: #212529; border-color: #32383e; color: #fff;">
                  <tr>
                    <th style="">File Name</th>
                    <th style="">File Description</th>
                    <th style="" >Link Name</th>
                    <th style="">Link</th>
                    <th style=" text-align: center; ">Task</th>
                  </tr>
                </thead>
                <tbody id="display_supporting_files" >
                  
                </tbody>
              </table>
            </div>
         </div>
         <br>
           <div class="col-sm-12 text-center" id="song_preview2" style="padding-left: 0px; padding-right: 0px;"></div>
         <div class="col-sm-12" style="padding-top: 15px;">
          <hr>
          <button class="btn btn-dark col-sm-4 pull-right btn-rounded font-m1" disabled="" name="submitsong2" id="submitsong2" onclick="submitsong2();" >Save</button>
        </div>
       </div>
      </div>  
    </div>  
  </div>  
</div>

<div id="EditFileModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content AM-content" style="background-color: #6C7A89;">
      <div class="modal-header text-center AM-header">
        <h4 class="font-m1 w3-wide w3-xlarge text-white AM-title">Edit Supporting File</h4>  
        <a href="#"  class="pull-right AM-close" data-dismiss="modal" onclick="close_edit();">
          <i id="closehover" class="fa fa-times"></i></a>
        <hr class="AM-hr">
      </div>
        <div class="modal-body">
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">File Name:</label>
            <input type="text" name="file_name" id="file_name" placeholder="File Name" class="form-control" />
          </div>
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">File Description:</label>
            <input type="text" name="file_description" id="file_description" placeholder="File Description" class="form-control" />
          </div>
           <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">Link Name:</label>
            <input type="text" name="link_name" id="link_name" placeholder="Link Name" class="form-control" />
          </div>
          <div class="form-group">
            <label style="color: #fff; font-family: MavenPro-Regular; font-size: 17px;">Link:</label>
            <input type="text" name="link" id="link" placeholder="Link" class="form-control" />
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="old_name" id="old_name"/>
          <input type="hidden" name="edit_action" id="edit_action"/>
          <input type="hidden" name="file_id" id="file_id"/>
          <input type="hidden" name="file_ext" id="file_ext"/>
          <button type="button" class="btn rounded btn-dark" style="font-family: MavenPro-Regular;" onclick="edit_file();">Update</button>
        </div>
     
    </div>
  </div>
</div>

<style type="text/css">
    
  .length{
    width: 120px;
  }

.AM-title{
  text-shadow:2px 2px 4px #555; 
  font-size: 20px; 
  letter-spacing: 2px;
}
.AM-close{
  color: #dad7d7; 
  font-size: 20px; 
 
}
</style>

<!-- Update Music -->
<div class="modal fade" role="dialog" id="editmusic">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-dark text-white"> Update Music
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="height: auto;">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="songid" id="songid">

          <div class="row">
            <div class="col-sm-12">
              <div class="input-group" style="margin-bottom: 5px;">
              <span class="btn btn-primary input-group-addon bg-primary text-white length"><span class="fa fa-microphone"></span> &nbsp; Style:</span>
              <select class="btn btn-info option form-control" id="sgen">
                  <option selected disabled>Style</option>
                    <?php 
                    include_once("../dbconnection.php");
                    $sql = mysqli_query($con,"SELECT * from tbl_genre");
                    while ($row = mysqli_fetch_assoc($sql)) {
                    ?>
                    <option value="<?php echo $row['gen_id']; ?>"><?php echo $row['genre_name']; ?></option>
                    <?php
                    }
                    ?>  
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="input-group" style="margin-bottom: 5px;">
              <span class="btn btn-primary input-group-addon bg-primary text-white length"><span class="fa fa-image"></span> &nbsp; Album:</span>
              <select class="btn btn-info option form-control" id="salb">
                  <option selected disabled>Albums</option>
                    <?php 
                    include_once("../dbconnection.php");
                    $myid = $_SESSION['admin'];
                    $sql = mysqli_query($con,"SELECT * from tbl_album where user_id ='$myid'");
                    while ($row = mysqli_fetch_assoc($sql)) {
                    ?>
                    <option value="<?php echo $row['album_id'] ?>"><?php echo $row['album_name']; ?></option>
                    <?php
                    }
                    ?>
                </select>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-sm-12">
              <div class="input-group" style="margin-bottom: 5px;">
              <span class="btn btn-primary input-group-addon bg-primary text-white length"> <span class="fa fa-users"></span> &nbsp;Audience:</span>
              <select class="btn btn-info option form-control" id="saud">
                  <option selected disabled>Audience</option>
                    <?php 
                    include_once("../dbconnection.php");
                    $sql = mysqli_query($con,"SELECT * from tbl_audience");
                    while ($row = mysqli_fetch_assoc($sql)) {
                    ?>
                    <option value="<?php echo $row['audience_id'] ?>"><?php echo $row['audience_name']; ?></option>
                    <?php
                    }
                    ?>
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="input-group" style="margin-bottom: 5px;">
              <span class="btn btn-primary input-group-addon bg-primary text-white length"><span class="fa fa-list-alt"></span> &nbsp;Application:</span>
              <select class="btn btn-info option form-control" id="sapp">
                <option selected disabled>Application</option>
                <?php 
                include_once("../dbconnection.php");
                $sql = mysqli_query($con,"SELECT * from tbl_application");
                while ($row = mysqli_fetch_assoc($sql)) {
                ?>
                <option value="<?php echo $row['application_id'] ?>"><?php echo $row['application_name']; ?></option>
                <?php
                }
                ?>
              </select>
              </div>
            </div>
          </div>

              <div class="input-group" style="margin-bottom: 5px;">
                <span class="btn btn-primary input-group-addon bg-primary text-white length"><i class="fa fa-music"></i> &nbsp; Song Name:</span>
                <input type="text" name="sname" id="sname" class="form-control" placeholder="Enter Song Name..." style="text-transform: capitalize;">
              </div>
                
              <div class="input-group" style="margin-bottom: 5px;">
                <span class="btn btn-primary input-group-addon bg-primary text-white length"><i class="fa fa-microphone"></i> &nbsp; Composer:</span>
                <input type="text" name="scomp" id="scomp" class="form-control" placeholder="Enter Composer" style="text-transform: capitalize;">
              </div>
              </div>
            </div>

        </div>
            <div class="modal-footer text-center" style="height: auto; overflow: hidden;">
              <div class="col-sm-12" style="padding-left: 0px;">
                <button class="btn btn-primary " onclick="update_music();"><i class="fa fa-save"></i> Save</button>
                <span id="loader"></span>
                <button  class="btn btn-danger" data-dismiss="modal" >Cancel</button>
              </div>
            </div>
    </div>
  </div>
</div>

<!-- *********************************************************************************************new modal -->





<div class="modal fade" role="dialog" id="user_type">
  <div class="modal-dialog modal-sm bg-dark" >
    <div class="modal-content w3-round-large">
      <div class="modal-header music-dark mtxt-light">Change User Type <button class="close text-white w3-small" data-dismiss="modal"><i class="fa fa-remove"></i></button></div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-lg-12">
            <div class="form-group">
          <div class="text-center w3-padding">
            <div id="user_icon"></div>
          </div>
          <input type="hidden" name="uid" id="uid">
          <select class="form-control"  id="user" name="user" oninput="choose_user_type(this.value);">
            <option selected="" disabled="">Select User Type</option>
            <option value="2">CONTRIBUTOR</option>
            <option value="3">GUEST USER</option>
            <option value="4">INACTIVE</option>
          </select>
        </div>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
            <button class="btn btn-dark btn-small" onclick="change_user($('#user').val(),$('#uid').val());">Save</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" role="dialog" id="edit_client_info" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content w3-round-large">
      <div class="modal-header music-dark mtxt-light">Edit Client Information <button class="close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body">
        <input type="hidden" name="cli_id" id="cli_id">
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <label class="text-dark">Firstname</label>
              <div class="form-group">
                <input type="show" class="form-control text-capitalize" name="new_fn" id="new_fn" placeholder="Enter firstname...">
              </div>
            </div>
            <div class="col-sm-6">
              <label class="text-dark">Lastname</label>
              <div class="form-group">
                <input type="show" class="form-control text-capitalize" name="new_ln" id="new_ln" placeholder="Enter lastname...">
              </div>
            </div>
            <div class="col-sm-12">
              <label class="text-dark">E-mail</label>
              <div class="form-group">
                <input type="show" class="form-control" name="new_mail" id="new_mail" placeholder="Enter E-mail...">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer text-center">
        <div class="container">
          <button class="btn btn-dark btn-small" data-dismiss="modal">Cancel</button>
          <button class="btn btn-dark btn-small" onclick="update_client_info();"><i class="fa fa-save"></i> Save</button>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" role="dialog" id="subs_info" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content w3-round-large">
      <div class="modal-header music-dark mtxt-light">Subscriptions&nbsp;<span class="badge badge-success" id="total_subs"></span> <button class="close" data-dismiss="modal">&times;</button>
        <input type="hidden" name="sub_id" id="sub_id"><br>
      </div>
      <div class="modal-body" style="background-image: url(../img/main-bg.png);"> 
        <div class="col-sm-12 mb-2">
          <div class="w3-round w3-card-8" id="sub_details">
            
          </div>
        </div>
      </div>
      <div class="modal-footer music-dark">
        <button class="btn btn-danger btn-block" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>