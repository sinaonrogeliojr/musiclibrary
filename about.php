<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link href="css/w3.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="about.css">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/function.js"></script>
  <script src="js/audio-index.js"></script>
  <script src="js/sweetalert.min.js"></script>
  <script src="datatables/datatables.min.js"></script>
</head>
<body onload="show_music();">
<div class="container-fluid hero">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="index.php"><img class="img-fluid" src="img/logo.png"/></a>
		</div>
		<div class="col-lg-8 col-6 head w3-animate-right" id="default2"><p><img class="img-fluid fence" src="img/profile.png"><span><a href="login.php">LOGIN</a> <span class="fence">|</span> <a href="register.php">REGISTER</a></span></p></div>
		<div class="col-lg-1 col-4" id="default"><a href="#" class="search-icon" onclick="view_search();"><i class="fa fa-search fa-2x"></i></a></div>

		<div class="col-lg-4 col-4 none w3-animate-right" id="view_search">
			<div class="form-group w3-card-2">
	            <div class="input-group">
	            	<form action="search_query.php" class="form-control" method="post">
		                <input type="text" id="search_index" name="search_index2"  autocomplete="off" class="form-control" placeholder="Search...">
		                <span class="input-group-addon input" style="background-color: #6c7989;" onclick="remove_search();"><i class="fa fa-times fa-2x"></i></span>
	            	</form>
	            </div>
        	</div>
		</div>
	</div>   	  	

  	<div class="row content">
  		<div class="col-lg-12">
  			<h1>Lamp Stand Studio</h1>
  			<p>Original Christian Music <br>Free. Always.</p>

  			<!--<a href=""><button type="button" class="btn">LISTEN MORE</button></a>-->
  		</div>
  	</div>
</div>

<!--<div class="container search-form">
	<div class="row">
		<div class="col-lg-8 input"><input type="text" name="search" placeholder="Artist, Music, Genre"></div>
		<div class="col-lg-4 input"><a href="#"><button type="button" class="btn btn-dark">SEARCH</button></a></div>
	</div>
</div>-->

<div class="container categories">
	<div class="row">
		
		<div class="col-lg-6">
			<h1>About <br>Lampstand Music.</h1>
			<p id="large">A little about Lampstand Music goes here. A little about Lampstand Music goes here.</p>
			<p>A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here.  A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here. A little about Lampstand Music goes here.</p>
		</div>

		<div class="col-lg-6">
			<img class="img-fluid" src="img/guitar.jpg">
		</div>
		<!--<div class="col-lg-3 cats">
			<a href="#">
				<h1>SCENE</h1>
				<img src="img/scene-bg.jpg" class="img-fluid">
				<p>Subcontent of the area goes here.</p>
			</a>
		</div>
		
		<div class="col-lg-3 cats">
			<a href="#">
				<h1>GENRE</h1>
				<img src="img/genre-bg.jpg" class="img-fluid">
				<p>Subcontent of the area goes here.</p>
			</a>
		</div>

		<div class="col-lg-3 cats">
			<a href="#">
				<h1>ARTISTS</h1>
				<img src="img/artists-bg.jpg" class="img-fluid">
				<p>Subcontent of the area goes here.</p>
			</a>
		</div>

		<div class="col-lg-3 cats">
			<a href="#">
				<h1>MOOD</h1>
				<img src="img/mood-bg.jpg" class="img-fluid">
				<p>Subcontent of the area goes here.</p>
			</a>
		</div>-->
	</div>
</div>

<div class="container-fluid feat-music">
		<h1>F.A.Q.</h1>
		<p>Ask us to know more.</p>
	<div class="row">
		<div class="col-lg-12">
			<div id="accordion" role="tablist">
			  <div class="card">
			    <div class="card-header" role="tab" id="headingOne">
			      <h5 class="mb-0">
			        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          How do I add my own music?
			        </a>
			      </h5>
			    </div>

			    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
			      <div class="card-body">
			        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingTwo">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Are there any fees for song submission?
			        </a>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
			      <div class="card-body">
			        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingThree">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Are all the songs downloadable?
			        </a>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
			      <div class="card-body">
			        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
			      </div>
			    </div>
			  </div>
			</div>
			<a href="#"><button type="button" class="btn">READ MORE</button></a>
		</div>
	</div>
</div>

<div class="container-fluid cta">
	<div class="row">
		<div class="col-lg-6">
			
		</div>

		<div class="col-lg-6 content">
			<h1>PLAY MUSIC EVERYWHERE!</h1>
			<a href="#"><button type="button" class="btn btn-dark">SUBSCRIBE NOW!</button></a>
		</div>
	</div>
</div>

<div class="container-fluid top-tracks">
	<div class="row">
		<div class="col-lg-12"><h1>TOP TRACKS</h1></div>
	</div>
</div>

<div class="container-fluid tracks-table">
	<div class="row">
		<?php include('tracks.php'); ?>
	</div>
</div>

<footer class="container-fluid">
	<?php include('footer.php'); ?>
</footer>

</body>
</html>