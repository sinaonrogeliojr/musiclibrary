<div class="modal fade" id="login_form"  data-keyboard="true" data-backdrop="false" aria-hidden="true">
      <div class="modal-dialog modal-sm" role="dialog">
        <div class="modal-content w3-card-4 w3-round-xlarge" style="background-color:#f1f1f1;">
          <div class="modal-header" style=" border:none;">
            <button class="close text-dark" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body"> 
            <center><img class=" id="load_img" width="250" src="img/logo2.png"></center>
            <br>
              <div class="card-body">
                  <div class="form-group">
                       <input class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" autofocus="" style="" />
                  </div>
                  <div class="form-group">
                   
                    <input class="form-control" name="password" id="password" type="password" placeholder="Password" required="" autofocus=""  style=""/>
                  </div>
                
                  <button type="submit" name="submit" id="submit" onclick="login()" class="btn btn-dark btn-block  ml3"> LOGIN</button>
                  <!-- <a href="#" class="btn btn-info ml3 btn-block" data-dismiss="modal" data-toggle="modal" data-target="#register_form">Register Now !</a> -->
              </div>
          </div>
        </div>
      </div>
    </div>



     <div class="modal fade" style="margin-top:0px;" role="dialog" id="register_form" data-backdrop="false" data-keyboard="true" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content w3-card-4 w3-round-xlarge" style="background-color:#f1f1f1;">
          <div class="modal-header"  style="border: none;">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
               <center><img class=" id="load_img" width="290" src="img/logo2.png"></center>
            <hr>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text" class="form-control text-capitalize" name="" autocomplete="off" id="fn" placeholder="Firstname...">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                         <input type="text" class="form-control text-capitalize" name="" autocomplete="off" id="ln" placeholder="Lastname...">
                    </div>
                  </div>
              
                   <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" class="form-control" autocomplete="off" onkeypress="$(this).removeClass('w3-red');" name="email" id="email" placeholder="E-mail...">
                    </div>
                  </div>
  
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="password" oninput="verify_pwd();" onkeypress="lenght_input();" class="form-control" autocomplete="off" name="pwd" id="pwd" placeholder="Password...">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="password" oninput="verify_pwd();" class="form-control" name="c_pwd" id="c_pwd" placeholder="Confirm-Password...">
                    </div>
                  </div>
                </div>
              </div>
               <div class="col-sm-12 text-right" style="padding-top:0px;">
               <span id="loading_reg"></span>
                  <button id="submit" onclick="register();" class="btn btn-primary  ml3"> Register</button>
               </div>
          </div>
          
        </div>
      </div>
    </div>


    <div class="modal fade" role="dialog" id="verify">
      <div class="modal-dialog modal-sm">
        <div class="modal-content bg-dark">
          <div class="modal-body">
            <div class="text-center">
              <h3 class="text-white"><i class="fa fa-lock fa-lg"></i> Verify Account !</h3>
            </div>
            <hr>
            <input type="hidden" name="v_email" id="v_email">
            <div class="form-group">
              <input type="text" class="form-control" name="verify_code" id="verify_code" placeholder="Enter verification code...">
            </div>
            <div class="text-right">
              <button class="btn btn-primary" onclick="verify_check($('#v_email').val(),$('#verify_code').val());">Verify</button>
            </div>
          </div>
        </div>
      </div>
    </div>