<?php 
session_start();
date_default_timezone_set('Australia/Brisbane');
require("../api/fpdf.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

//subscribe through email:
function subscribe($con, $email,$name){
	$date_ = date('Y-m-d H:i:s');

	$check_email = mysqli_query($con, "SELECT * from tbl_subscribers where email = '$email'");
    if (mysqli_num_rows($check_email)>0) { 
        echo 2;
    }else{
	    $sql = mysqli_query($con, "INSERT INTO tbl_subscribers (email,name,date_trans) VALUES('$email','$name','$date_')");
		if($sql){
			echo 1;
		}	
    }
}

function forgot_password($con, $email){
	$check_email = mysqli_query($con, "SELECT * from tbl_user_profile where email_add = '$email'");
	if (mysqli_num_rows($check_email)>0) { 
		//Send Email 
		$row = mysqli_fetch_assoc($check_email);
		$name = $row['fn'];
		$pwd = $row['password'];
        echo 1;
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
		    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
		    $mail->Password = 'testing5!';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587;                                    // TCP port to connect to

		    $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		        )
		    );
		    //Recipients
		    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
		    $mail->addAddress($email);     // Add a recipient

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Forgot Password';
		    $mail->Body    = '<p>Hello '.ucfirst($name).'<br>
		    				  Email Address: '.$email.' <br>
		    				  Your Password is: <b>'.$pwd.'</b> <br>
		    				  </p>';
		    $mail->AltBody = '';

		    $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}

    }else{
		echo 2;
    }
}

function get_order_name($con){
$sql = mysqli_query($con,"SELECT * from tbl_shuffle_index");
$row = mysqli_fetch_assoc($sql);
return $row['order_row'];
}


function get_order_sort($con){
$sql = mysqli_query($con,"SELECT * from tbl_shuffle_index");
$row = mysqli_fetch_assoc($sql);
return $row['order_sort'];
}


//login
function login($con,$uname,$pwd){
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt from tbl_user_profile where email_add='$uname' and password ='$pwd' and is_approve = 1");
	if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$date_ = date('Y-m-d H:i:s');
			$id = $row['user_id'];
			$log = mysqli_query($con, "UPDATE tbl_user_profile set last_log = '$date_' WHERE user_id = '$id'");
			if ($row['is_approve'] == 1) {
				if ($row['user_type'] == 1) {
					$_SESSION['admin'] = $row['user_id'];
					$_SESSION['fn'] = $row['fn'];
					$_SESSION['mn'] = $row['mn'];
					$_SESSION['ln'] = $row['ln'];
					$_SESSION['gender'] = $row['gender'];
					$_SESSION['b_date'] = $row['dt'];
					$_SESSION['email_add'] = $row['email_add'];
					$_SESSION['username'] = $row['username'];
					$_SESSION['password'] = $row['password'];
					$_SESSION['user_type'] = $row['user_type'];
					if ($row['picture'] == "") {
						$_SESSION['user_pic'] = '../avatar/def.png';
						
					}
					else
					{
						$_SESSION['user_pic'] = $row['picture'];
					}
					echo 1;
				}
				else if ($row['user_type'] == 2) {
					$_SESSION['users'] = $row['user_id'];
					$_SESSION['fn'] = $row['fn'];
					$_SESSION['mn'] = $row['mn'];
					$_SESSION['ln'] = $row['ln'];
					$_SESSION['gender'] = $row['gender'];
					$_SESSION['b_date'] = $row['dt'];
					$_SESSION['email_add'] = $row['email_add'];
					$_SESSION['username'] = $row['username'];
					$_SESSION['password'] = $row['password'];
					$_SESSION['user_type'] = $row['user_type'];
					if ($row['picture'] == "") {
						$_SESSION['user_pic'] = '../avatar/def.png';
						
					}
					else
					{
						$_SESSION['user_pic'] = $row['picture'];
					}

					echo 2;
				}
				else if($row['user_type'] == 3)
				{
					$_SESSION['guest'] = $row['user_id'];
					$_SESSION['fn'] = $row['fn'];
					$_SESSION['mn'] = $row['mn'];
					$_SESSION['ln'] = $row['ln'];
					$_SESSION['gender'] = $row['gender'];
					$_SESSION['b_date'] = $row['dt'];
					$_SESSION['email_add'] = $row['email_add'];
					$_SESSION['username'] = $row['username'];
					$_SESSION['password'] = $row['password'];
					$_SESSION['user_type'] = $row['user_type'];
					if ($row['picture'] == "") {
						$_SESSION['user_pic'] = '../avatar/def.png';
						
					}
					else
					{
						$_SESSION['user_pic'] = $row['picture'];
					}

					echo 3;
				}
				else
				{
					echo 00;
				}
			}
			else if ($row['is_approve'] == 0) {
			echo "verify";
			}
		}
	}
	else
	{
		echo 0;
	}
}

//check User if exist
function exist_user($con,$email){

	$sql = mysqli_query($con,"SELECT * from tbl_user_profile where email_add='$email'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else
	{
		return 0;
	}
}

//Generate unique id
function generate_unique_id($con){
	$sql = mysqli_query($con,"SELECT max(id) from tbl_user_profile");
	$row = mysqli_fetch_assoc($sql);
	$rand_gen = rand(1,9).rand(1,9).($row['max(id)'] + 1).rand(1,9).rand(1,9);
	return 'user-'.$rand_gen;
}

//Registration function
function register_user($con,$fn,$ln,$email,$pwd,$rfi){

	$email_admin = 'music@lampstandstudio.com';
	if (exist_user($con,$email)>0) {
		echo 1;
	}
	else
	{
		$new_id = generate_unique_id($con);
		$pwd_code = rand(0,9).'M'.rand(0,9).rand(0,9).'S'.rand(0,9).'L'.rand(0,9);
		$sql = mysqli_query($con,"INSERT INTO tbl_user_profile values(id,'$new_id','$fn','N/A','$ln','N/A','N/A','$email','N/A','$pwd',3,0,null,'$pwd_code',0,0,null)");
		if ($sql) {
		echo 2;
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

		try {
		    //Server settings
		    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
		    $mail->Password = 'testing5!';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587;                                    // TCP port to connect to

		    $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		        )
		    );
		    //Recipients
		    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
		    $mail->addAddress($email_admin);     // Add a recipient

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'New Lampstand Registration Request.';
		    $mail->Body    = '<p>Name: '.ucfirst($fn).' '.ucfirst($ln).'<br>
		    				  Email Address: '.$email.' <br>
		    				  Password: '.$pwd.' <br>
		    				  Member of the RFI?: '.$rfi.'</p>';
		    $mail->AltBody = '';

		    $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
		}
	}	
}

function verify_user($con,$email,$code){
	$sql = mysqli_query($con,"SELECT * from tbl_user_profile where email_add='$email' and pwd_code='$code'");
	$row = mysqli_fetch_assoc($sql);
	$id = $row['user_id'];
	//verify
	if ($sql) {
	$sql1 = mysqli_query($con,"SELECT * from tbl_user_profile where email_add='$email' and pwd_code='$code' and user_id='$id'");
	$row2 = mysqli_fetch_assoc($sql1);
	$id2 = $row2['user_id'];
	//verify account
	if ($sql1) {
		$sqls = mysqli_query($con,"UPDATE tbl_user_profile set is_active=1,pwd_code=null where user_id='$id'");
		if (mysqli_num_rows($sql1)>0) {
			echo 1;
		}
	}
	}
}



function show_music_index($con,$start,$limit,$order_by,$sort){
	$num = 1;
	$imgs= '';
	$sql = mysqli_query($con,"SELECT a.*,b.*,DATE_FORMAT(b.`approved_date`,'%Y-%m-%d') as app_date from tbl_album a right join tbl_audios b on a.album_id=b.album_id where active = 1 order by ".$order_by." ".$sort." LIMIT ".$start.",".$limit." "); 
	?>
	<!-- WEBSITE -->
	<table class="table table-hover" id="default_table">
	<thead style="background-color: #616e7e; color: #fff;">

	<tr id="labels">
		<th></th>
	<th class="text-left">PLAY</th>
	<th class="text-left">TRACK/ALBUM</th>
	<th class="text-left">GENRE</th>
	<th class="text-left">Description</th>
	<th></th>
	</tr>
	</thead>
	<!-- /WEBSITE -->
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
	$imgs = 'img/slogo3.png';
	}
	else
	{
	$imgs = $row['album_artwork'];
	}
	$date_ = date('Y-m-d');
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	?>
	<tbody>
		<tr class="tracks">
			<td></td>
		<td class="left" >
			<span style="cursor:pointer;" class="fa fa-play-circle fa-3x " id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_play('<?php echo $row['audio_id'] ?>');" title="Play">
			</span>
		</td>
		<td class="left">
			<h6 style="margin-left: 5%;">
				<?php echo $row['song_name']; ?><?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?>
				
			</h6>
			<p style="margin-left: 5%;"><?php echo $row['album_name']; ?></p>
		</td>
		<td class="left" style="width: 30%;">
			<h6 style="margin-left: 3%;"><?php echo $row['genre']; ?></h6>
		</td>
		<td class="text-left" style="width: 30%;">
			<h6 style="margin-left: 3%;"><?php echo $row['description']; ?></h6>
		</td>
		<!--<td class="text-left"><h6 style="margin-left: 8%;"><?php //echo date('H:i', strtotime($row['duration'])); ?></h6></td>-->
		<td></td>
		</tr>
	</tbody>
	<?php
	} 
	?>
	</table>
<?php }

function show_music_index2($con,$start,$limit,$order_by,$sort){
	$num = 1;
	$imgs= '';
	$sql = mysqli_query($con,"SELECT a.*,b.*,DATE_FORMAT(b.`approved_date`,'%Y-%m-%d') as app_date from tbl_album a right join tbl_audios b on a.album_id=b.album_id where active = 1 order by ".$order_by." ".$sort." LIMIT ".$start.",".$limit." "); 
	?>
	<!-- MOBILE -->
	<div id="accordion">
	<!-- /MOBILE -->
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
	$imgs = 'img/slogo3.png';
	}
	else
	{
	$imgs = $row['album_artwork'];
	}
	$date_ = date('Y-m-d');
	$app_date = $row['app_date'];
	?>
	<div class="card">
		<div class="card-header">
		<span class="fa fa-play-circle fa-2x btn" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play()"></span>
		<a class="card-link" data-toggle="collapse" href="#<?php echo $row["audio_id"]; ?>">
		<strong><?php echo $row['song_name']; ?></strong>
		</a>
		</div>
		<div id="<?php echo $row["audio_id"]; ?>" class="collapse" data-parent="#accordion">
		<div class="card-body">
		<div class="list-group">
		  <a href="#" class="list-group-item list-group-item-action">
		    <small><?php echo $row['album_name']; ?></small>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <span class="fa fa-plus fa-2x"></span>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <b><?php echo $row['genre']; ?></b>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <b><?php echo $row['description']; ?></b>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <b><?php echo $row['duration']; ?></b>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <span class="fa fa-cut fa-2x"></span>
		  </a>
		  <a href="#" class="list-group-item list-group-item-action">
		    <span class="fa fa-cart-plus fa-2x"></span>
		  </a>
		</div>
		</div>
		</div>
		</div>

	<?php
	} 
	?>
	</div>
<?php } 

//NEW UPDATES

function load_genres($con){

	$query = mysqli_query($con, "SELECT * FROM tbl_genre");

	while ($row = mysqli_fetch_assoc($query)) { 

	?>
		<ul class="list-group list-group-flush w3-hover-shadow">
		  <li class="list-group-item" onclick="get_genres('<?php echo $row['genre_name']; ?>')">
		  	<?php echo $row['genre_name']; ?>
		  </li>
		</ul>

	<?php }
}

function load_genre_musics($con,$search){
	if ($search == "" || $search == null) {
		$sql =  mysqli_query($con, "SELECT t1.*,t2.*,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`");
	}else{

		if($search == 'All'){
		$sql =  mysqli_query($con, "SELECT t1.*,t2.*,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`");
		}else{
		$sql =  mysqli_query($con, "SELECT t1.*,t2.*,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id` Where t1.`genre` like '%$search%' or concat(t1.`song_name`,t1.`description`,t1.`composer`,t1.`keyword`,t1.`genre`, t2.`album_name`) like '%$search%'");
		}
	}

?>
	<table class="table table-responsive-sm table-hover" id="default_table">
		<thead style="background-color: #616e7e; color: #fff;">
		<tr id="labels">
			<th></th>
			<th scope="col">PLAY</th>
			<th scope="col">TRACK/ALBUM</th>
			<th scope="col">GENRE/MOOD</th>
			<th scope="col">Description</th>
			<th scope="col">LENGTH</th>
			<th></th>
		</tr>
		</thead>
<?php
if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) { 
		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
		$imgs = 'img/slogo3.png';
		}
		else
		{
		$imgs = $row['album_artwork'];
		}
		$date_ = date('Y-m-d');
		$app_date = $row['app_date'];
		?>

		<tbody>
			<tr class="tracks">
				<td></td>
			<td>
				<span class="fa fa-play-circle fa-3x" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play()"></span>
			</td>
			<td class="left">
				<a href="#"><h6><?php echo $row['song_name']; ?></h6></a>
				<a href="#"><p><?php echo $row['album_name']; ?></p></a>
			</td>
			<td class="left" style="width: 30%;">
				<a href="#"><h6><?php echo $row['genre']; ?></h6></a>
			</td>
			<td style="width: 30%;">
				<a href="#"><h6><?php echo $row['description']; ?></h6></a>
			</td>
			<td><a href="#"><h6><?php echo date('H:i', strtotime($row['duration'])); ?></h6></td>
			<td></td>
			</tr>
		</tbody>

	<?php
	} ?>

	
<?php }
else{ ?>

	<div class="col-sm-12">No data found.</div>

<?php }
?>

<?php 
}

//NEW UPDATES
function send_message($con,$name,$email,$message){

	//$email_admin = 'music@lampstandstudio.com';
	$email_admin = 'sinaonrogeliojr@gmail.com';
	echo 2;
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($email_admin);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'New Lampstand Contact Request.';
	    $mail->Body    = '<p>Name: '.ucfirst($name).'<br>
	    				  Email Address: '.$email.' <br>
	    				  Message: '.$message.' <br>';
	    $mail->AltBody = '';

	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}
	}

//About Content
function about_content($con){

	$sql = mysqli_query($con, "SELECT * FROM tbl_about_content where active = 1 limit 1");
	$row = mysqli_fetch_assoc($sql);
	$header = $row['header'];
	$body = $row['body']; 
	$image = $row['image'];
	?>

	<div class="col-lg-6" >
	<h1 style="margin-left: 3%;">About <br>Lampstand Studio</h1>
	
	<p id="large" style="margin-left: 3%;"><?php echo $header; ?></p>
	<p style="margin-left: 3%;"><?php echo $body; ?></p>

	</div>
	<div class="col-lg-6 text-center">
		<img class="img-fluid" src="<?php echo $image; ?>">
	</div>

<?php
}

function faqs_content($con){
	$sql = mysqli_query($con, "SELECT * FROM tbl_faq_content where status = 1 order by id asc limit 5 "); 
	?>
	<div id="accordion" role="tablist">
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	?>

		<div class="card">
		    <div class="card-header" role="tab" id="headingOne">
		      <h5>
		        <a data-toggle="collapse" href="#<?php echo $row["id"]; ?>" aria-expanded="true" aria-controls="collapseOne">
		          <?php echo $row['title']; ?>
		        </a>
		      </h5>
		    </div>

		    <div id="<?php echo $row["id"]; ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
		      <div class="card-body">
		        <?php echo $row['content']; ?>
		      </div>
		    </div>
		  </div>
	<?php
	}
	?>
	</div>
<?php
}

function save_play($con, $audio_id){
	$user = 'index';
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "INSERT INTO tbl_played_songs(audio_id,user_id,date_play,location) VALUES('$audio_id','$user','$date_',null)");
	if($sql){
		echo 1;
	}
}

?>


