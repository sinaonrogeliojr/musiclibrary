<?php 
include_once("../dbconnection.php");
include_once("classes.php");
$action = mysqli_real_escape_string($con,$_POST['action']);

switch ($action) {

	case 'save_play':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		save_play($con,$id);
	break;

	case 'subscribe':
	$email_address = mysqli_real_escape_string($con,$_POST['email_address']);
	$name = mysqli_real_escape_string($con,$_POST['name']);
	if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
	  echo 404;
	}
	else
	{
		subscribe($con,$email_address,$name);
	}
	break;

	case 'login':
	$uname = mysqli_real_escape_string($con,$_POST['username']);
	$pwd = mysqli_real_escape_string($con,$_POST['password']);
	if (!filter_var($uname, FILTER_VALIDATE_EMAIL)) {
	  echo 303;
	}
	else
	{
		login($con,$uname,$pwd);
	}
	break;

	case 'register':
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
    $ln = mysqli_real_escape_string($con,$_POST['ln']);
    $email = mysqli_real_escape_string($con,$_POST['email']);
	$pwd = mysqli_real_escape_string($con,$_POST['password']);
	$rfi = mysqli_real_escape_string($con,$_POST['rfi']);
	//
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  echo 3;
	}
	else{
		 register_user($con,$fn,$ln,$email,$pwd,$rfi);
	}
	break;

	case 'verify_user':
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$code = mysqli_real_escape_string($con,$_POST['code']);
	verify_user($con,$email,$code);
	break;


	case 'sound_audio':
		$id = $_POST['id'];
		$num = 1;
		$order_by = get_order_name($con);
		$sort = get_order_sort($con);
		$start = mysqli_real_escape_string($con,$_POST['start']);
		$limit = mysqli_real_escape_string($con,$_POST['limit']);
		/*$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_album a right join tbl_audios b on a.album_id=b.album_id  order by ".$order_by." ".$sort."");*/
		$sql = mysqli_query($con,"SELECT a.*,b.*,DATE_FORMAT(b.`approved_date`,'%Y-%m-%d') as app_date from tbl_album a right join tbl_audios b on a.album_id=b.album_id where active = 1 order by ".$order_by." ".$sort." LIMIT ".$start.",".$limit." "); 
		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo $row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
	break;

	case 'get_max_au':
		$sql = mysqli_query($con,"SELECT count(id) from tbl_audios");
		if (mysqli_num_rows($sql)>0) {
		$row = mysqli_fetch_assoc($sql);
		echo $row['count(id)'];
		}
	break;

	case 'music_list':
	$start = mysqli_real_escape_string($con,$_POST['start']);
	$limit = mysqli_real_escape_string($con,$_POST['limit']);
	$order_by = get_order_name($con);
	$sort = get_order_sort($con);
	show_music_index($con,$start,$limit,$order_by,$sort);
	break;

	case 'music_list2':
	$start = mysqli_real_escape_string($con,$_POST['start']);
	$limit = mysqli_real_escape_string($con,$_POST['limit']);
	$order_by = get_order_name($con);
	$sort = get_order_sort($con);
	show_music_index2($con,$start,$limit,$order_by,$sort);
	break;

	//NEW UPDATES 9/20/2018

	case 'load_genres':
	load_genres($con);
	break;

	case 'load_genre_musics':
	//$genre_name = mysqli_real_escape_string($con,$_POST['genre_name']);
	$search = mysqli_real_escape_string($con,$_POST['search_music2']);
	load_genre_musics($con,$search);
	break;

	//NEW UPDATES

	case 'send_message':
	$name = mysqli_real_escape_string($con,$_POST['name']);
    $email = mysqli_real_escape_string($con,$_POST['email']);
    $message = mysqli_real_escape_string($con,$_POST['message']);
	//
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  echo 3;
	}
	else{
		 send_message($con,$name,$email,$message);
	}
	break;

	case 'about_content':
	about_content($con);
	break;

	case 'faqs_content':
		faqs_content($con);
	break;

	case 'forgot_password':
	$email = mysqli_real_escape_string($con,$_POST['email_verify']);
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  echo 404;
	}
	else
	{
		forgot_password($con,$email);
	}
	break;
}
 ?>