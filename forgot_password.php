<?php 
session_start();
if (isset($_SESSION['admin'])) {
 @header('location:admin_panel');
}
else if (isset($_SESSION['users'])) {
  @header('location:user_profile/');
}
else if (isset($_SESSION['email_add'])) {
  @header('location:guest_profile/');
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/sweetalert.css">

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/function.js"></script>
  <script src="js/sweetalert.min.js"></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="css/index.css">

  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
</head>
<body>


<div class="container-fluid hero hero-contact">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="index.php"><img class="img-fluid" src="img/logo.png"/></a>
		</div>
		<div class="col-lg-9 col-6 head w3-animate-right" id="default2"><p><img class="img-fluid fence" src="img/profile.png"><span><a href="login.php">LOGIN</a> <span class="fence">|</span> <a href="register.php">REGISTER</a></span></p></div>
	</div> 	
	<div class="row">
		<div class="col-lg-12">
			<h1>Forgot Password</h1>
		</div>	
	</div>
</div>

<div class="container-fluid login">
	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-8">
			<div class="row">
				<div class="contact-form">
					<h2>Forgot Password?</h2>
				  <div class="form-group">
				    <input type="email" class="form-control" id="email_verify" placeholder="Enter Valid Email">
				  </div>
				  <div class="row margin-0 text-right">
					  <button type="submit" class="btn col-auto btn-contact" onclick="send_email()">Send</button>
					</div>
					<hr />
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="container-fluid">
	<?php include('footer.php'); ?>
</footer>

</body>
</html>
<script>
  var input_uname = document.getElementById("username");
  input_uname.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
          document.getElementById("submit").click();
      }
  });

  var input = document.getElementById("password");
  input.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
          document.getElementById("submit").click();
      }
  });
</script>