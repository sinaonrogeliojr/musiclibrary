<style type="text/css">
.b_genre {
    /* Height & width depends on how you want to reveal the overlay (see JS below) */    
    height: 100%;
    width: 0;
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    background-color: #f1f1f1; /* Black w/opacity */
    overflow-x: hidden; /* Disable horizontal scroll */
    transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}
/* Position the content inside the overlay */
.overlay-content-genre {
    position: relative;
    top: 5%; /* 25% from the top */
    width: 100%; /* 100% width */
    text-align: center; /* Centered text/links */
    margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
}
/* The navigation links inside the overlay */
/* When you mouse over the navigation links, change their color */
/* Position the close button (top right corner) */
.b_genre .closebtn {
    position: absolute;
    top: 10px;
    right: 45px;
    font-size: 60px;
    text-decoration: none;
    color: red;
}


/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
@media screen and (max-height: 450px) {
    .b_genre a {font-size: 20px}
    .b_genre .closebtn {
        font-size: 40px;
        top: 15px;
        right: 35px;
    }
}

</style>

<script>

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("genre_search").style.width = "0%";
}
</script>
<style type="text/css">
	.none{
		display: none;
	}
</style>
<br>
<div id="genre_search" class="b_genre">
    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <!-- Overlay content -->
    <div class="overlay-content-genre">
        <div class="container-fluid categories">
        

        <p id="header">Find out what you prefer about music.</p>
                <div class="row">
                    <div class="col-lg-3 cats">
                        <div class="dropdown">
                            <a href="#">
                                <h1>SCENE</h1>
                                <img src="img/scene-bg.jpg" class="img-fluid">
                            </a>
                          <div class="dropdown-content col-lg-12 scroll_">
                            <ul class="list-group list-group-flush">
                              <li class="list-group-item">Action Scene</li>
                              <li class="list-group-item">Activities</li> 
                              <li class="list-group-item">Competition</li>
                              <li class="list-group-item">Love Scenes</li>
                            </ul>
                          </div>
                        </div>
                        <!--<a href="#">
                            <h1>SCENE</h1>
                            <img src="img/scene-bg.jpg" class="img-fluid">
                            <p>Subcontent of the area goes here.</p>
                        </a>-->
                    </div>
                    
                    <div class="col-lg-3 cats">
                        <div class="dropdown">
                            <a href="#">
                                <h1>GENRE</h1>
                                <img src="img/genre-bg.jpg" class="img-fluid">
                            </a>
                          <div class="dropdown-content col-lg-12 scroll_" id="load_genres2">
                            
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-3 cats">
                        <a href="#">
                            <h1>ARTISTS</h1>
                            <img src="img/artists-bg.jpg" class="img-fluid">
                            <p>Subcontent of the area goes here.</p>
                        </a>
                    </div>

                    <div class="col-lg-3 cats">
                        <a href="#">
                            <h1>MOOD</h1>
                            <img src="img/mood-bg.jpg" class="img-fluid">
                            <p>Subcontent of the area goes here.</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="container search-form" style="margin-top: -20px; padding: 5px;">
                <div class="row">
                    <div class="col-lg-8 input"><input type="text" name="search_music2" id="search_music2" placeholder="Artist, Music, Genre" onchange="load_genre_music();" oninput="load_genre_music();"></div>
                    <div class="col-lg-4 input"><a href="#"><button type="button" class="btn btn-dark" onclick="load_genre_music();">SEARCH</button></a></div>
                </div>
            </div>

            <!--<div class="form-group w3-card-2">
                <div class="input-group">
                    <input type="text" id="genre_name" disabled="" class="form-control">
                    <span class="input-group-addon input" onclick="remove_genre();"><i class="fa fa-times fa-2x"></i></span>
                </div>
            </div>-->

            <!-- Display Search -->
            <div class="container-fluid tracks-table">
                <div class="row">
                    <div class="col-lg-12" id="tbl_searched">
                    </div>
                </div>
            </div>
    </div>
</div>
