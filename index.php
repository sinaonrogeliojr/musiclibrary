<?php //include('multiple_email_sender'); 
date_default_timezone_set('Australia/Brisbane');
$lastday = date('t',strtotime('today'));
$date_today = date("d",strtotime('today'));
$result = strcmp($lastday, $date_today);
if($result == 0){
	//echo 'true';
	//include('multiple_email_sender.php');
}else{
	//echo 'false';
  //include('email_sender/multiple_email_sender.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link href="css/w3.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="css/index2.css">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/jl-audio.css">
<link rel="stylesheet" type="text/css" href="css/index-oji.css">

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/function.js"></script>
  <script src="js/audio-index.js"></script>
  <script src="js/sweetalert.min.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <style type="text/css">
    .img_playlist {
	width: 101px; /* You can set the dimensions to whatever you want */
	height: 101px;
	object-fit: cover;
	border-radius: 10px;
	}
	.img_logo {
	width: 101px; /* You can set the dimensions to whatever you want */
	height: 101px;
	object-fit: cover;
	border-radius: 10px;
	}
  </style>
</head>
<body onload="show_music(); load_about_content(); load_faqs_content();">
<div class="" id="home" style="">
	<div class="container-fluid hero">
		<div class="row header">
			<div class="col-lg-3 col-2 head">
				<a href="index.php"><img class="img-fluid" src="img/logo.png"/></a>
			</div>
			<div class="col-lg-9 col-6 head w3-animate-right" id="default2"><p><img class="img-fluid fence" src="img/profile.png"><span><a href="login.php">LOGIN</a> <span class="fence">|</span> <a href="register.php">REGISTER</a></span></p></div>
		</div>   	  	

	  	<div class="row content">
	  		<div class="col-lg-12">
	  			<h1>Lamp Stand Studio</h1>
	  			<p>Original Christian Music <br>Free. Always.</p>

	  			<!--<a href=""><button type="button" class="btn">LISTEN MORE</button></a>-->
	  		</div>
	  	</div>
	</div>
</div>
<!--<div class="container search-form">
	<div class="row">
		<div class="col-lg-8 input"><input type="text" name="search" placeholder="Artist, Music, Genre"></div>
		<div class="col-lg-4 input"><a href="#"><button type="button" class="btn btn-dark">SEARCH</button></a></div>
	</div>
</div>-->
<div class="" id="about" style="">
	<div class="container-fluid categories">
		<div class="row" id="about_content">
						
		</div>
	</div>
</div>

<div class="" id="faq" style="">
	<div class="container-fluid feat-music">
			<h1>F.A.Q.s</h1>
		<div class="row" >
			<div class="col-lg-12" id="faqs_content">
			</div>
		</div>
	</div>
</div>

<div class="container-fluid cta">
	<div class="row">
		<div class="col-lg-6">
			
		</div>

		<div class="col-lg-6 content">
			<h1>PLAY MUSIC EVERYWHERE!</h1>
		</div>
	</div>
</div>

<div class="container-fluid top-tracks">
	<div class="row">
		<div class="col-lg-12"><h1>TOP TRACKS</h1></div>
		<div class="col-lg-12">
                  <div class="col-sm-12">
                    <div class="col-sm-12">
                      <h4 style="margin-bottom: 0px;">
                      <marquee id="music_title" class="col-sm-12">LAMPSTAND STUDIO</marquee>
                      </h4>
                    </div>
                    <input type="range" min="0"  max="100" class="player_slider" onmousemove=" move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'));" onmouseup=" move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">

                    <div class="col-sm-12 row">
                      <div class="col-sm-2">
                          <div class="text-dark"><span id="counter_strike">00:00</span>/<span id="timer_audio">00:00</span></div>
                      </div>
                      <div class="col-sm-8 text-center">
                          <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg text-dark"></i></a>
                          <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="fa fa-play-circle fa-lg text-dark"></i></a>
                          <a href="#prev" onclick="next_aud();" class="btn-musics "><i class="fa fa-forward fa-lg text-dark"></i></a>
                      </div>
                      <div class="col-sm-2 text-dark">
                            <a href="#vol"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg text-dark"></i></a>
                            <input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);">
                      </div>
                    </div>
                  </div>

                <div id="music_audio" style="width: 100%;">
                <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud2();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
                Sorry, your browser does not support audio
                </audio>
                <div id="show_progress"></div>
                </div>
		</div>
	</div>
</div>

<div class="container-fluid tracks-table">
	<div class="row">
		<?php include('tracks.php'); ?>
	</div>
</div>
<div class="" id="contact" style="">
<div class="container-fluid" style="background: #f1f1f1; padding: 0px;">
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="row">
        <div class="contact-form">
          <h2><span class="fa fa-phone"></span> Contact Us</h2>
          <div class="form-group">
            <input type="text" class="form-control" id="name" placeholder="Name" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="email" placeholder="Email" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="message" placeholder="How can we help you?" autocomplete="off">
          </div>
          <hr>
            <div class="row">
            <span id="loading_reg"></span>
               <div class="col-lg-6 text-center">
                 <div class="g-recaptcha" data-sitekey="6Lc-L3IUAAAAALYHDZRRFXVzf7mPwzzatwayc7IV" data-callback="callback"></div>
               </div>
               <div class="col-lg-6 text-center">
                <div class="form-group">
                    <button type="submit" disabled="" id="btn_send" class="btn col-auto btn-contact" onclick="send_message();">Send Message</button>
                </div>
                <span id="loading"></span>
               </div>
           </div>
          <!--<div class="row margin-0 align-items-center">
            <div class="form-group form-check col-auto mr-auto">
              <span id="loading_reg"></span>
            </div>
            <button type="submit" class="btn col-auto btn-contact" onclick="register();">REGISTER</button>
          </div>-->
          <hr />
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<footer class="container-fluid">
	<?php include('footer.php'); ?>
</footer>
<script type="text/javascript">
	function callback(){
    console.log("The user has already solved the captcha, now you can submit your form.");
    document.getElementById('btn_send').disabled= false;
  }
</script>
</body>
</html>