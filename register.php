<?php 
session_start();
if (isset($_SESSION['admin'])) {
 @header('location:admin_panel');
}
else if (isset($_SESSION['users'])) {
  @header('location:user_profile/');
}
else if (isset($_SESSION['email_add'])) {
  @header('location:guest_profile/');
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/sweetalert.css">

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/function.js"></script>
  <script src="js/sweetalert.min.js"></script>
  <script type="text/javascript" src="fbapp/fb.js"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" type="text/css" href="css/index.css">

  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <script type="text/javascript">
  function show_captcha(){
    var cap_num = Math.floor(Math.random() * 9);
    get_code_caps(cap_num);
    $("#show_quest").html(' <div class="text-center"><img src="img/capcha/c'+cap_num+'.png" class="img-fluid w3-round w3-animate-opacity" style="width: 100%; height: 80px;"></div>');
  }
  </script>
</head>
<body onload="show_captcha();">
<div class="container-fluid hero hero-contact">
  <div class="row header">
    <div class="col-lg-3 col-2 head">
      <a href="index.php"><img class="img-fluid" src="img/logo.png"/></a>
    </div>
    <div class="col-lg-9 col-6 head w3-animate-right" id="default2"><p><img class="img-fluid fence" src="img/profile.png"><span><a href="login.php">LOGIN</a> <span class="fence">|</span> <a href="register.php">REGISTER</a></span></p></div>
  </div>  
  <div class="row">
    <div class="col-lg-12">
      <h1>REGISTER</h1>
    </div>  
  </div>
</div>

<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="row">
        <div class="contact-form">
          <h2>SIGN UP</h2>
          <div class="form-group">
            <input type="text" class="form-control" id="fn" placeholder="First Name" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="ln" placeholder="Last Name" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" onkeypress="$(this).removeClass('w3-red');" name="email" id="email" placeholder="Email" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" oninput="verify_pwd();" onkeypress="lenght_input();" id="pwd" name="pwd" placeholder="Password" autocomplete="off">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" oninput="verify_pwd();" id="c_pwd" name="c_pwd" placeholder="Retype Password" autocomplete="off">
          </div>
          <div class="row">
            <input type="hidden" id="rfi"">
            <div class="col-lg-12">
              My email is listed on the RFI Lampstand site. &nbsp; 
              <div class="form-check-inline">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" checked="" id="yes" name="optradio">Yes
                </label>
              </div>
              <div class="form-check-inline">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" id="no" name="optradio">No
                </label>
              </div>
            </div>
          </div>
          <hr>
            <div class="row">
               <div class="col-lg-6 text-center">
                 <div class="g-recaptcha" data-sitekey="6Lc-L3IUAAAAALYHDZRRFXVzf7mPwzzatwayc7IV" data-callback="callback"></div>
               </div>
               <div class="col-lg-6 text-center">
                <div class="form-group">
                    <button type="submit" style="margin-top: 3.7%;" class="btn col-auto btn-contact" id="btn-register" onclick="register();" disabled="">REGISTER</button>
                </div>
               </div>
               <span id="loading_reg"></span>
           </div>
          <hr />
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid">
  <?php include('footer.php'); ?>
</footer>
<script type="text/javascript">
  $("#rfi").val('YES');
  $("#yes, #no").change(function () {
      if ($("#yes").is(":checked")) {
         $("#rfi").val('YES');
      }
      else if ($("#no").is(":checked")) {
         $("#rfi").val('NO');
      }
  });

  function callback(){
    console.log("The user has already solved the captcha, now you can submit your form.");
    document.getElementById('btn-register').disabled= false;
  }


</script>
</body>
</html>