<?php
session_start();
if (!isset($_SESSION['guest'])) {
	@header('location:../');
}

include('../dbconnection.php');
function get_application($con){
    $sql = mysqli_query($con,"SELECT * from tbl_application");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['application_name'].'">'.$row['application_name'].'</option>';
        }
    }
}

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="guest.css">
	<link href="../css/w3.css" rel="stylesheet">
	<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="music_player/audio.css">

	<script src="../js/jquery.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/guest.js"></script>
	<script src="geo_loc.js"></script>
	<script src="https://www.w3schools.com/lib/w3.js"></script>
	<script src="music_player/audio-index.js"></script>
	<style type="text/css">
		.imgs {
			width: 50px; /* You can set the dimensions to whatever you want */
			height: 50px;
			object-fit: cover;
			}
		
		
		.img_music2 {
	    width: 200px; /* You can set the dimensions to whatever you want */
	    height:200px;    
	    object-fit: cover;
		}

		.img_played{
			width: 101px; /* You can set the dimensions to whatever you want */
	    height:101px;    
	    object-fit: cover;
		}

		.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #304756;
   text-align: center;
   color: #f2f2f2;
}
	</style>
</head>
<body onload="recent_playlists(); refresh_account(); load_pictures();">

<div class="sidenav">
	<ul>
		<li class="w3-hover-shadow">
			<a href="index.php"><span class="fa fa-music fa-3x" style="color: #8eadab;"></span></a>
	    </li>
		<li class="w3-hover-shadow">
			<a href="show_playlist.php"><span class="fa fa-list-ul fa-3x" style="color: #3d4c59;"></span></a>
	    </li>
	    <li class="w3-hover-shadow">
			<a href="download_list.php"><span class="fa fa-download fa-3x" style="color: #3d4c59;"></span></a>
	    </li>
	    <li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>
	</ul>
</div>

<div class="main">
	<div class="container-fluid hero hero-db hero-admin">
		<div class="row header">
			<div class="col-lg-3 col-2 head">
				 <a href="#" onclick="recent_playlists(); refresh_account(); load_pictures();"><img class="img-fluid" src="../img/logo.png"/></a>
			</div>
			<div class="col-lg-9 head">
				<div class="dropdown pull-right">
				  <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
				  <div class="dropdown-content">
				    <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
				    <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
				  </div>
				</div>
			</div>
		</div>  	  	
	</div>

	<div class="container-fluid user-mng">
		<div class="row">
			
			
		</div>

		<div class="row">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-5">
						<p class="title">Recently Played</p>
					</div>
					<div class="col-lg-7">
						<div class="input-group">
					      <input type="show" oninput="recent_playlists();" name="search_rp" id="search_rp" placeholder="Search Title, Album, Application, Composer, Keyword...">
					      <span class="input-group-addon"><i class="fa fa-search"></i></span>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div></div>
	<div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
		<div class="row">
			<div class="col-lg-12 table-responsive">
				<table class="table table-striped table-hover" id="musics">
					<tr><td colspan="10" class="text-left"><span>Click on the column title to sort</span></td></tr>
					<tr id="labels">
						<td></td>
						<td></td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(3)')" style="cursor:pointer">Title/Album</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(4)')" style="cursor:pointer">Description</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(5)')" style="cursor:pointer">Genre</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(6)')" style="cursor:pointer">Composer</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(7)')" style="cursor:pointer">Application</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(8)')" style="cursor:pointer; width: 13%;">Supporting Files</td>
						<td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(9)')" style="cursor:pointer">Contributor</td>
						<td class="text-center" style="">Action</td>
					</tr>
					<tbody id="recent_playlists" style="">
						
					</tbody>
					</table>
			</div>
		</div>
	</div>
	<hr>

<footer class="container-fluid">
	<?php include('../footer.php'); ?>
</footer>
</div>

<div class="footer none">
	<div class="row" style="padding-left: 110px; padding-right: 20px; padding-bottom: 10px;">
	      <div class="col-sm-12">
	        <div class="col-sm-12">
	          <h4 style="margin-bottom: 0px;">
	          <marquee id="music_title" class="col-sm-12 default-fs">LAMPSTAND STUDIO</marquee>
	          </h4>
	        </div>
	        <input type="range" min="0"  max="100" class="player_slider" onmousemove="move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'));" onmouseup="move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">

	        <div class="col-sm-12 row">
	          <div class="col-sm-2">
	              <div class="text-white"><span id="counter_strike">00:00</span>/<span id="timer_audio">00:00</span></div>
	          </div>
	          <div class="col-sm-8 text-center">
	              <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg text-white"></i></a>
	              <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="fa fa-play-circle fa-lg text-white"></i></a>
	              <a href="#prev" onclick="next_aud();" class="btn-musics "><i class="fa fa-forward fa-lg text-white"></i></a>
	          </div>
	          <div class="col-sm-2 text-white">
	                <a href="#vol"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg text-white"></i></a>
	                <input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);">
	          </div>
	        </div>
	      </div>

	    <div id="music_audio" style="width: 100%;">
	    <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud2();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
	    Sorry, your browser does not support audio
	    </audio>
	    <div id="show_progress"></div>
	    </div>
	</div>
</div>
</body>
</html>