<?php
session_start();
if (!isset($_SESSION['guest'])) {
	@header('location:../');
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="guest.css">
	<script src="../js/jquery.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/guest.js"></script>
	<script src="geo_loc.js"></script>
	<script src="https://www.w3schools.com/lib/w3.js"></script>
	<link href="../css/w3.css" rel="stylesheet">
	<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
	<link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
  <style type="text/css">
  	.imgs {
    width: 60px; /* You can set the dimensions to whatever you want */
    height: 60px;
    object-fit: cover;
}
.profile_pic {
    width: 250px; /* You can set the dimensions to whatever you want */
    height: 250px;
    object-fit: cover;
	}

	.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
  </style>
</head>
<body onload="refresh_account(); load_pictures(); uploading();">
<script type="text/javascript">
$(document).ready(function(){  

	$('#change_profile_pic').on('hidden.bs.modal', function () {
	load_pictures();
	});

});
</script>
<div class="sidenav">
	<ul>
		 <li class="w3-hover-shadow">
			<a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
	    </li>
		 <li class="w3-hover-shadow">
			<a href="show_playlist.php"><span class="fa fa-list-ul fa-3x" style="color: #3d4c59;"></span></a>
	    </li>
	     <li class="w3-hover-shadow">
			<a href="download_list.php"><span class="fa fa-download fa-3x" style="color: #3d4c59;"></span></a>
	    </li>
	    <li class="w3-hover-shadow">
			<!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
			<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
			
		</li>
	</ul>
</div>

<div class="main">

<div class="container-fluid hero hero-db hero-admin">
	<div class="row header">
		<div class="col-lg-3 col-2 head">
			<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
		</div>
		<div class="col-lg-9 head">
			<div class="dropdown pull-right">
			  <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
			  <div class="dropdown-content">
			    <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
			    <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
			  </div>
			</div>
		</div>
	</div>  	  	
</div>

<div class="container-fluid user-mng">
	<div class="row">
		<div class="col-lg-12">
			<p class="title"><i class="fa fa-lg fa-user"></i> Account Settings</p>
		</div>
	</div>
	<hr>
	<div class="row" style=" margin-top: -20px;">
		<div class="col-sm-3 mb-2">
			<div class="glass w3-padding w3-round">
				<div class="w3-display-container text-center">
					<div id="profile_pic">
					</div>
					<div class="w3-display-middle w3-display-hover">
						<button class="btn btn-dark animated fadeIn btn-small" data-toggle="modal" data-target="#change_profile_pic">Change Profile Picture</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-5 mb-2">
			<div id="my_data"></div>
		</div>
		<div class="col-sm-4">
			<div id="choose">
			</div>
			<input type="hidden" name="old" id="old">
			<input type="hidden" name="id" id="id">
			<?php include_once("forms.php"); ?>
		</div>
	</div>
</div>

<footer class="container-fluid">
	<div class="row copyright">
		<div class="col-lg-12">
			<p class="nav-item"><span><a href="#">HOME</a></span><span class="fence">|</span><span><a href="#">ABOUT US</a></span><span class="fence">|</span><span><a href="#">CONTACT</a></span></p>
			<p ><span><a href="#">FAQ</a></span><span class="fence">|</span><span><a href="#">TERMS OF USE</a></span><span class="fence">|</span><span><a href="#">PRIVACY POLICY</a></span></p>
			<p ><span>© 2018, <a href="#">LAMPSTAND STUDIO</a></span><span class="fence">|</span><span>ALL RIGHTS RESERVED.</span></p>
		</div>
	</div>
</footer>
</div>

<div class="modal fade" role="dialog" id="change_profile_pic">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">Change Profile Picture<button class="close" data-dismiss="modal"><i class="fa fa-remove fa-sm"></i></button></div>
			<div class="modal-body">
				<div></div>
				<input type="file" name="user_image" id="user_image" accept="image/*" style="display: none;">
				<div class="text-center">
					<div id="image_preview">
						<img src="<?php echo $_SESSION['user_pic'] ?>" class="profile_pic img-album w3-round" style="margin-bottom:5px;">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-lg-12 text-center">
				<button class="btn btn-small btn-dark" onclick="$('#user_image').click();"><i class="fa fa-image"></i> Upload Picture</button>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>