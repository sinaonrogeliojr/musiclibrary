<?php
session_start();
if (!isset($_SESSION['guest'])) {
  @header('location:../');
}
include('../dbconnection.php');
function get_application($con){
    $sql = mysqli_query($con,"SELECT * from tbl_application");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['application_name'].'">'.$row['application_name'].'</option>';
        }
    }
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>

  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="guest.css">
  <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="../js/guest.js"></script>
  <script src="https://www.w3schools.com/lib/w3.js"></script>
  <link href="../css/w3.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  
  <style type="text/css">
    .imgs {
      width: 50px; /* You can set the dimensions to whatever you want */
      height: 50px;
      object-fit: cover;
      }
    .imgs_album {
      width: 101px; /* You can set the dimensions to whatever you want */
      height: 101px;
      object-fit: cover;
      }
          .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
  </style>

</head>
<body onload="music_list(); refresh_account(); load_pictures();">
<div class="sidenav">
  <ul>
    <li class="w3-hover-shadow">
      <a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
      </li>
    <li class="w3-hover-shadow">
      <a href="show_playlist.php"><span class="fa fa-list-ul fa-3x" style="color: #8eadab;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <a href="download_list.php"><span class="fa fa-download fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
    </li>
  </ul>
</div>

<div class="main">
  <div class="container-fluid hero hero-db hero-admin">
    <div class="row header">
      <div class="col-lg-3 col-2 head">
        <a href="#" onclick="music_list(); refresh_account(); load_pictures();"><img class="img-fluid" src="../img/logo.png"/></a>
        
      </div>
      <div class="col-lg-9 head">
        <div class="dropdown pull-right">
          <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
          <div class="dropdown-content">
            <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
            <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
          </div>
        </div>
      </div>
    </div>        
  </div>

  <div class="container-fluid user-mng">
    <div class="row">
    </div>
    <div class="row">
      <div class="col-lg-5">
        <div class="row">
          <div class="col-lg-5">
            <p class="title">My Playlists</p>
          </div>
          <div class="col-lg-7">
            <div class="input-group">
                <input type="show" oninput="music_list();" name="search1" id="search1" placeholder="Search Playlist...">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <button type="button" class="btn btn-dark btn-small" data-toggle="modal" data-target="#new_playlist">Add Playlist</button>
      </div>
    </div>
  </div>

  <div class="container-fluid tracks-table " style="margin-top: -30px; margin-bottom: 1px;">
    <div class="row">
      <div class="col-lg-12 table-responsive table-striped">
        <table class="table table-hover" id="playlists">
          <tr><td colspan="5" class="text-left"><span>Click on the column title to sort</span></td></tr>
          <tr id="labels" style="">
            <td></td>
            <td class="text-left" onclick="w3.sortHTML('#playlists', '.item', 'td:nth-child(2)')" style="cursor:pointer">Playlist</td>
            <td class="text-left" onclick="w3.sortHTML('#playlists', '.item', 'td:nth-child(3)')" style="cursor:pointer">Song</td>
            <td class="text-left" onclick="w3.sortHTML('#playlists', '.item', 'td:nth-child(4)')" style="cursor:pointer">Date Created</td>
            <td class="text-left">Action</td>
          </tr>
          <tbody id="display_playlist" style="">
          </tbody>
          </table>
      </div>
    </div>
  </div>

  <hr>

  <footer class="container-fluid">
    <?php include('../footer.php'); ?>
  </footer>
</div>

<!-- MODALS -->
<div class="modal fade" role="dialog" id="update_playlist">
  <div class="modal-dialog">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header text-center">
            <h2 class="calbum modal-title" style="text-shadow:2px 2px 4px #555;">Update Playlist</h2>  
            <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
        </div> 
      <div class="modal-body" style="height: auto;">
        <input type="hidden" name="u_id" id="u_id">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                  <input type="show" maxlength="25" class="form-control" autocomplete="off" name="u_playlist" id="u_playlist" placeholder="Enter playlist name...">
                </div>
              </div>
            </div>
        </div>  
      </div>
      <div class="modal-footer">
        <div class="container text-center">
          <button type="button" class="btn btn-dark btn-small" name="submit" id="submit" onclick="update_playlist();" >Save</button>
            <button class="btn btn-dark btn-small" data-dismiss="modal"><strong><span class="fa fa-times"></span> Cancel</strong>
            </button>
        </div>   
      </div>
    </div>
  </div>
</div> 

<!-- Add Playlist -->

<div class="modal fade" id="new_playlist">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
            <h2 class="modal-title" id="p_title"><span class="fa fa-plus"></span> New Playlist</h2>  
            <button type="button" class="close" data-dismiss="modal">&times;</button>        
      </div>
      <div class="modal-body">
        <input type="hidden" id="play_id">
        <div class="col-lg-12" style="margin-top: 2%;">
          <div class="form-group">
          <input type="show" class="form-control" name="p_name" id="p_name" placeholder="Enter playlist name...">
        </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-small btn-dark" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
        <button class="btn btn-small btn-dark" onclick="save_plist();" id="btn-playlist"><span class="fa fa-save"></span> Save</button>
      </div>
    </div>
  </div>
</div>

<!-- View Music Information -->
<div id="music_info_modal" class="modal fade">  
  <div class="modal-dialog modal-small">  
    <input type="hidden" id="music_id">
    <div class="modal-content" id="music_info" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */"> 

    </div>  
  </div>  
</div>

</body>
</html>