<?php 
session_start();
include_once("../dbconnection.php");
include_once("classes.php");

$action = mysqli_real_escape_string($con,$_POST['action']);
switch ($action) {

	//save new playlist
	case 'save_plist':
		$play_id = mysqli_real_escape_string($con, $_POST['play_id']);
		$playlist_name = mysqli_real_escape_string($con, $_POST['p_name']);
		save_plist($con,$play_id,$playlist_name);
	break;

	case 'sound_audio':
		$id = $_POST['id'];
		$num = 1;
		$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
	break;

	//Sound Audio Album
	case 'sound_audio_album':
		$id = $_POST['id'];
		$album_id = $_POST['album_id'];
		$num = 1;
		/*$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");*/

		$sql = mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
			LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
			LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
			LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
			LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
			WHERE t1.`album_id` = '$album_id' and t1.`active` = 1
			ORDER BY song_name ASC");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
	break;

	//Sound Audio Album
	case 'sound_audio_playlist':
		$id = $_POST['id'];
		$v_pid = $_POST['v_pid'];
		$num = 1;
		/*$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");*/

		$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture`,t5.`id` as t_id from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		right join tbl_playlist_info t5 on t2.`id` = t5.`audio_id`
		where t5.`playlist_id`='$v_pid' and t2.active = 1  order by song_name");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn" style="display: none;" onclick="choose_play('<?php echo $row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
	break;

	
	case 'count_downloads':
	count_downloads($con);
	break;

	case 'not_notify_me':
	not_notify_me($con);
	break;

	case 'notify_me':
	notify_me($con);
	break;
	
	case 'show_plist':
	$search1 = mysqli_real_escape_string($con,$_POST['search1']);
	show_plist($con,$search1);
	break;

	case 'showalbums':
	$search = mysqli_real_escape_string($con,$_POST['search']);
	show_albums($con,$search);
	break;

	case 'del_plist':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	delete_plist($con,$id);
	break;

	case 'save_song':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	save_song($con,$id);
	break;

	case 'save_play':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	save_play($con,$id);
	break;

	case 'save_song1':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	save_song1($con,$id);
	break;

	case 'save_all_music':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	save_all_music($con,$id);
	break;

	case 'save_song2':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	save_song2($con,$id);
	break;

	case 'clear_songs':
	$id = mysqli_real_escape_string($con,$_POST['v_pid']);
	clear_songs($con,$id);
	break;

	case 'show_music_playlist':
	$search_song = mysqli_real_escape_string($con,$_POST['search_song']);
	$id = mysqli_real_escape_string($con,$_POST['v_pid']);
	show_music_playlist($con,$id,$search_song);
	break;

	case 'show_songs2':
	$id = mysqli_real_escape_string($con,$_POST['v_pid']);
	show_music_playlist($con,$id);
	break;

	//website
	case 'load_audio2':
	$genres2 = mysqli_real_escape_string($con,$_POST['genres2']);
	$search_audio = mysqli_real_escape_string($con,$_POST['search_audio']);
	$playlist = mysqli_real_escape_string($con,$_POST['v_pid']);
	load_audio2($con,$genres2,$search_audio,$playlist);
	break;

	case 'delete_all_plist':
	$id = mysqli_real_escape_string($con,$_POST['user_id']);
	delete_all_plist($con, $id);
	break;

	case 'delete_all_download':
	$id = mysqli_real_escape_string($con,$_POST['user_id']);
	delete_all_download($con, $id);
	break;

	case 'delete_download':
	$id = mysqli_real_escape_string($con,$_POST['audio_id']);
	delete_download($con, $id);
	break;

	case 'delete1_music':
	$id = mysqli_real_escape_string($con,$_POST['t_id']);
	delete1_music($con, $id);
	break;

	case 'update_plist':
	$id = mysqli_real_escape_string($con,$_POST['u_id']);
	$name = mysqli_real_escape_string($con,$_POST['u_playlist']);
	update_plist($con, $id, $name);
	break;

	case 'del_music':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$music = mysqli_real_escape_string($con,$_POST['music']);
	delete_music($con,$id,$music);
	break;


	case 'del_album':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$album_artwork = mysqli_real_escape_string($con,$_POST['album_artwork']);
	delete_album($con,$id,$album_artwork);
	break;

	case 'insert_album':
	$album_name = mysqli_real_escape_string($con,$_POST['album_name']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	insert_album($con,$album_name,$id);
	break;

	case 'insert_music':
	$song_name = mysqli_real_escape_string($con,$_POST['song_name']);
	$composer = mysqli_real_escape_string($con,$_POST['composer']);
	$artist = mysqli_real_escape_string($con,$_POST['artist']);
	$genre = mysqli_real_escape_string($con,$_POST['genre']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	insert_music($con,$song_name,$composer,$artist,$genre,$id);
	break;

	case 'showdate_asc':
	$asc = mysqli_real_escape_string($con,$_POST['asc']);
	showdate_asc($con,$asc);
	break;

	case 'refresh_account':
	$myid = $_SESSION['guest'];
	refresh_account($con,$myid);
	break;

	case 'update_picture':
	pict_session($con);
	break;

	case 'update_account':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	$fn = mysqli_real_escape_string($con,$_POST['fn']);
	$ln = mysqli_real_escape_string($con,$_POST['ln']);
	update_name($con,$id,$fn,$ln);
	break;

	case 'update_email':
	$email = mysqli_real_escape_string($con,$_POST['email']);
	update_email($con,$email);
	break;

	case 'update_username':
	$username = mysqli_real_escape_string($con,$_POST['username']);
	update_username($con,$username,$myid);
	break;
	
	case 'update_pwd':
	$pwd = mysqli_real_escape_string($con,$_POST['pwd']);
	update_pwd($con,$pwd);
	break;

	case 'showsongs':
	$search3 = mysqli_real_escape_string($con,$_POST['search3']);
	show_musics($con,$search3);
	break;

	case 'download_music':
	$current_loc = mysqli_real_escape_string($con,$_POST['current_loc2']);
	$id = mysqli_real_escape_string($con,$_POST['id']);
	download_music($con,$current_loc,$id);
	break;

	case 'add_download':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	add_download($con, $id);
	break;

	case 'add_download_album':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	add_download_album($con, $id);
	break;

	//website
	case 'show_download_list':
	$search_dl = mysqli_real_escape_string($con,$_POST['search_dl']);
	show_download_list($con,$search_dl);
	break;
	//website

	case 'show_contributor':
	$search_c = mysqli_real_escape_string($con,$_POST['search_c']);
	show_contributor($con,$search_c);
	break;

	case 'subscribe':
	$user_id = mysqli_real_escape_string($con,$_POST['user_id']);
	subscribe($con, $user_id);
	break;
	
	case 'contri_details':
	$c_id = mysqli_real_escape_string($con,$_POST['c_id']);
	contri_details($con, $c_id);
	break;

	case 'subscription_details':
	$subs_id = mysqli_real_escape_string($con,$_POST['subs_id']);
	subscription_details($con, $subs_id);
	break;

	case 'show_subscriptions':
	$search_s = mysqli_real_escape_string($con,$_POST['search_s']);
	show_subscriptions($con,$search_s);
	break;

	case 'get_notify':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	get_notify($con,$id);
	break;


	case 'not_notify':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	not_notify($con,$id);
	break;

	case 'new_notif':
	new_notif($con);
	break;

	case 'show_notif':
	show_notif($con);
	break;

	case 'show_music_contributor':
	$search5 = mysqli_real_escape_string($con,$_POST['search5']);
	show_music_contributor($con,$search5);
	break;

	case 'showtopics':
	$search4 = mysqli_real_escape_string($con,$_POST['search4']);
	show_topics($con,$search4);
	break;

	case 'show_composers':
	$search = mysqli_real_escape_string($con,$_POST['search_composer']);
	show_composers($con,$search);
	break;

	case 'show_descriptions':
	$search = mysqli_real_escape_string($con,$_POST['search_desc']);
	show_descriptions($con,$search);
	break;

	case 'music_details':
	$music_id = mysqli_real_escape_string($con,$_POST['music_id']);
	music_details($con, $music_id);
	break;

	case 'subscriptions':
	$search = mysqli_real_escape_string($con,$_POST['search_subs']);
	subscriptions($con,$search);
	break;

	case 'unsubscribe':
	$id = mysqli_real_escape_string($con,$_POST['id']);
	unsubscribe($con,$id);
	break;

	case 'add_downloadlist':
	$search = mysqli_real_escape_string($con,$_POST['search_download']);
	add_downloadlist($con,$search);
	break;

	case 'total_duration':
	$playlist_id = mysqli_real_escape_string($con,$_POST['v_pid']);
	total_duration($con,$playlist_id);
	break;

	case 'total_songs':
	$playlist_id = mysqli_real_escape_string($con,$_POST['v_pid']);
	total_songs($con,$playlist_id);
	break;

	case 'total_album_songs':
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	total_album_songs($con,$album_id);
	break;

	case 'save_image':
	unlink($_SESSION['user_pic']);
	$path = mysqli_real_escape_string($con,$_POST['path']);
	update_pf($con,$path);
	break;

	case 'get_max_au':
		$sql = mysqli_query($con,"SELECT count(id) from tbl_audios");
		if (mysqli_num_rows($sql)>0) {
		$row = mysqli_fetch_assoc($sql);
		echo $row['count(id)'];
		}
	break;

	case 'save_recent_play':
		$id = mysqli_real_escape_string($con,$_POST['id']);
		save_recent_play($con,$id);
	break;

	case 'show_recently_played':
		show_recently_played($con);
	break;

	case 'recent_playlists':
		$search = mysqli_real_escape_string($con,$_POST['search_rp']);
		recent_playlists($con,$search);
	break;

	case 'album_musics':
	$search = mysqli_real_escape_string($con,$_POST['album_search']);
	$album_id = mysqli_real_escape_string($con,$_POST['album_id']);
	album_musics($con, $search, $album_id);
	break;

	case 'del_image':
	$path = mysqli_real_escape_string($con,$_POST['path']);
	unlink($path);
	echo 1;
	break;
}
