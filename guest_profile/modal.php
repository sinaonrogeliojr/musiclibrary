 <?php  
include_once("../dbconnection.php");

//get genre
function get_genre($con){
    $sql = mysqli_query($con,"SELECT * from tbl_genre WHERE genre_name != 'All' order by genre_name ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['genre_name'].'">'.$row['genre_name'].'</option>';
        }
    }
}

//get genre
function get_playlist($con){
    $myid= $_SESSION[''];
    $sql = mysqli_query($con,"SELECT * from tbl_playlist order by name ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
    }
}
 ?>
  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
        <div class="modal-header ">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
           <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Are you sure you want to Logout?..</div>
        <div class="modal-footer">
          <button class="btn btn-danger btn-oji rounded" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-info btn-oji rounded" href="../logout.php" >Logout</a>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" role="dialog" id="play_musics">
  <div class="modal-dialog " >
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header bg-dark"> <div class="text-center"><h4 class="text-white text-capitalize"> Now Playing </h4></div>
        <button type="button" class="close" data-dismiss="modal">&times;</button></div>
          <div class="modal-body rounded-bottom">
            <div id="music_player"></div><br><br><br><br><br><br>
              <div  style="background-color: #704b7775; width: 500px; margin-left: -15px; margin-bottom: -15px;">
                  <div id="music_name" class="mn w3-margin-left"></div>
                  <div id="music_composer" class="mc text-center text-white"></div>
                  <div id="music_artist" class="ma text-center text-white"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

<div class="modal fade" role="dialog" id="editmusic">
  <div class="modal-dialog">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header">Update Music
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="height: auto;">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="songid" id="songid">
            
            <div class="input-group">
              <span >Genre:</span>
              <select class="btn btn-info option col-sm-12 btn-oji" id="sgen"  style="margin-top: 4px; margin-bottom: 20px;">
                <option selected disabled>Genres</option>
                  <?php 
                  include_once("../dbconnection.php");
                  $sql = mysqli_query($con,"SELECT * from tbl_genre");
                  while ($row = mysqli_fetch_assoc($sql)) {
                  ?>
                  <option><?php echo $row['genre_name']; ?></option>
                  <?php
                  }
                  ?>
              </select>
            </div>
            
              <div class="input-group" style="margin-bottom: 5px;">
                <span class="btn btn-primary btn-oji input-group-addon bg-primary text-white"><i class="fa fa-music"></i> &nbsp; Song Name:</span>
                <input type="text" name="sname" id="sname" class="form-control" placeholder="Enter Song Name...">
              </div>
                
              <div class="input-group" style="margin-bottom: 5px;">
                <span class="btn btn-primary input-group-addon bg-primary text-white btn-oji"><i class="fa fa-microphone"></i> &nbsp; Composer:&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <input type="text" name="scomp" id="scomp" class="form-control" placeholder="Enter Composer">
              </div>
                
              <div class="input-group">
                <span class="btn btn-primary input-group-addon bg-primary text-white"><i class="fa fa-user"></i> &nbsp; Artist: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <input type="text" name="sart" id="sart" class="form-control" placeholder="Enter Artist">
              </div>
                
              </div>
            </div>
              
      </div>

            <div class="modal-footer" style="height: auto; overflow: hidden;">
              <div class="col-sm-12" style="padding-left: 0px;">
                <button class="btn btn-primary " onclick="save_music();"><i class="fa fa-save"></i> Save</button>
                <span id="loader"></span>
                <button  class="btn btn-danger" data-dismiss="modal" >Cancel</button>
              </div>
            </div>
    </div>
  </div>
</div>


<!-- Create New Album --> 
  <div id="newAlbumModal" class="modal fade">  
    <div class="modal-dialog">  
      <div class="N_content modal-content N_radius">  
        <div class="modal-header">
            <h5 class="calbum modal-title N_album">New Album</h5>  
            <a href="#"  class="pull-right N_album_close" data-dismiss="modal" onclick="N_cancel();">
            <i id="closehover" class="fa fa-times"></i></a>
          </div>  
          <div class="modal-body">         
            <div id="image_preview" class="text-center"> </div> <br/>
            <input type="file" name="album_image" id="album_image" style="display: none;"/> 
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                    <input type="text" name="album_name" id="album_name" class="form-control text-center" placeholder="Album Name" required="true" maxlength="30">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="">
                    <button class="btn N_button btn-dark" id="btn-chooser" onclick="$('#album_image').click();"> 
                      <i class="fa fa-file fa-lg"></i> Choose File</button>
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
          </div>  
          <div class="modal-footer">
            <div class="col-sm-5"></div>
            <div class="col-sm-3" style="padding-right: 0px;">
              <button class="btn btn-block N_cancel rounded" style="color: #fff;" data-dismiss="modal" onclick="N_cancel();">Cancel</button>
            </div>
            <div class="col-sm-4 pull-right" >
              <button class="btn btn-block rounded N_button" style="color: #fff;" name="submit" id="submit" onclick="submit_album();" >Create Album</button>
            </div>
        </div>
      </div>  
    </div>  
  </div>


<div class="modal fade" role="dialog" id="update_playlist">
  <div class="modal-dialog">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header text-center">
            <h2 class="calbum modal-title" style="text-shadow:2px 2px 4px #555;">Update Playlist</h2>  
            <button type="button" class="close" data-dismiss="modal" style="margin-top: -20px;">&times;</button>  
        </div> 
      <div class="modal-body" style="height: auto;">
        <input type="hidden" name="u_id" id="u_id">
              <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <input type="text" maxlength="25" class="form-control" autocomplete="off" name="u_playlist" id="u_playlist" placeholder="Enter playlist name...">
                </div>
                 <button class="btn btn-dark pull-right rounded btn-oji" name="submit" id="submit" onclick="update_playlist();" ><strong><span class="fa fa-save"></span> Save</strong>
               </button>
               <button class="btn btn-danger pull-right rounded btn-oji" data-dismiss="modal"><strong><span class="fa fa-times"></span> Cancel</strong>
               </button>
              </div>
             
            </div>
              
      </div>
    </div>
  </div>
</div>  

<style type="text/css">
  .scroll_full
    {
      height: 400px;
      /*overflow: scroll; */
    }

  .scroll2
    {
      height: 330px;
      overflow: scroll;
    }

/*modal fullscreen */
</style>

<div class="modal fade" role="dialog" id="change_profile_pic" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header bg-dark text-white" style="margin-right: -1px;">Change Profile Picture
        <button class="cp_close close" data-dismiss="modal">&times;</button></div>
      <div class="bg_img modal-body bg-dark" style="margin-top: -3px; margin-right: -1px;">
       
        <input type="file" name="user_img" id="user_img" accept="image/*" style="display: none;">
        <div class="text-center">
        <div id="user_img_preview">
          <img src="<?php echo '../'.$_SESSION['user_pic'] ?>" class="img-responsive img-fluid" width="150px;">
        </div>
        <div id="btn-up">
        <br>
        <button class="btn btn-primary btn-oji" onclick="$('#user_img').click();"><i class="fa fa-image"></i> Upload Picture</button>
        </div>
        
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="edit_name_modal" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header" style="margin-right: -1px;">Update Name
        <button class="cp_close close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body bg-dark" style=" margin-right: -1px;">
       
        <div class="text-center" style="">
          <div class="glass w3-padding w3-round w3-margin-bottom" id="form-names" style="padding-top: 20px; padding-left: 20px; padding-right: 20px; padding-bottom: 20px;">
            <div class="form-group">
              <input type="text" name="fn" id="fn" class="form-control text-capitalize" autocomplete="off" placeholder="Firstname..." style="text-transform: capitalize;">
            </div>
            <div class="form-group">
              <input type="text" name="ln" id="ln" class="form-control text-capitalize" autocomplete="off" placeholder="Lastname..." style="text-transform: capitalize;">
            </div>
            <div class="text-right w3-margin-bottom">
              <span id="load"></span>
              <button class="btn btn-danger btn-oji" data-dismiss="modal">Cancel</button>
              <button class="btn btn-primary btn-oji" onclick="update_user('name');"><i class="fa fa-save fa-lg"></i> Save Changes</button>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="edit_email_modal" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header text-white text-center" style="margin-right: -1px;">Update Email
        <button class="cp_close close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body bg-dark" style=" margin-right: -1px;">
       
        <div class="text-center" style="padding-bottom: 5px;">
          <div class="glass w3-padding w3-round w3-margin-bottom" id="form-names" style="padding-top: 20px; padding-left: 20px; padding-right: 20px; padding-bottom: 20px;">
            <div class="form-group">
              <input type="text" name="email" id="email" class="form-control" autocomplete="off" placeholder="E-mail...">
            </div>
            <div class="text-right w3-margin-bottom">
              <span id="load"></span>
              <button class="btn btn-danger btn-oji" data-dismiss="modal">Cancel</button>
              <button class="btn btn-primary btn-oji" onclick="update_user('email');"><i class="fa fa-save fa-lg"></i> Save Changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" role="dialog" id="edit_pass_modal" data-backdrop="static">
  <div class="modal-dialog modal-sm">
    <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
      <div class="modal-header text-center" style="margin-right: -1px;">Update Password
        <button class="cp_close close" data-dismiss="modal">&times;</button></div>
      <div class="modal-body bg-dark" style=" margin-right: -1px;">
       
        <div class="text-center" style="">
          <div class="glass w3-padding w3-round w3-margin-bottom" id="form-names" style="padding-top: 20px; padding-left: 20px; padding-right: 20px; padding-bottom: 20px;">
            <div class="form-group">
              <input type="text" name="o_pwd" id="o_pwd" class="form-control" autocomplete="off" placeholder="Old Password...">
            </div>
            <div class="form-group">
              <input type="Password" name="n_pwd" id="n_pwd" class="form-control" autocomplete="off" placeholder="New Password...">
            </div>
            <div class="form-group">
              <input type="Password" name="c_pwd" id="c_pwd" class="form-control" autocomplete="off" placeholder="Confirm-Password...">
            </div>
            <div class="text-right w3-margin-bottom">
              <span id="load"></span>
              <button class="btn btn-danger btn-oji" onclick="cancel_updates();">Cancel</button>
              <button class="btn btn-primary btn-oji" onclick="update_user('pwd');"><i class="fa fa-save fa-lg"></i> Save Changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- View Contributor Information 
<div id="contri_info_modal" class="modal fade ">  
  <div class="modal-dialog">  <input type="hidden" id="c_id">
    <div class="modal-content text-white" id="contri_info" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">  
    </div>  
  </div>  
</div>--> 

<!-- View Contributor Information --> 
<div id="subs_info_modal" class="modal fade ">  
  <div class="modal-dialog">  <input type="hidden" id="subs_id">
    <div class="modal-content text-white" id="subs_info" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">  
    </div>  
  </div>  
</div>

<!-- View Music Information -->
<div id="music_info_modal" class="modal fade ">  
  <div class="modal-dialog modal-lg">  <input type="hidden" id="music_id">
    <div class="modal-content text-white" id="music_info" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */"> 

    </div>  
  </div>  
</div>

<!-- Logout Modal-->
  <div class="modal fade" id="download_all_music" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content text-white" style=" background-color: #6C7A89; /*background:rgba(255,255,255,0.2); background-image: url('../img/main-bg.png'); background-repeat:no-repeat; background-size:cover; */">
        <div class="modal-header ">
          <h5 class="modal-title" id="exampleModalLabel">Download all music.</h5>
           <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Are you sure you want to Dowload ALl?..</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <span id="button_here"></span>
          <input type="text" id="data_here">
        </div>
      </div>
    </div>
  </div>