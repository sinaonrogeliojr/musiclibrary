<?php 
date_default_timezone_set('Australia/Brisbane');

require("../api/fpdf.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';

//Save new playlist

function save_plist($con,$play_id,$playlist_name){
	$date_ = date('Y-m-d H:i:s');
	$my_id = $_SESSION['guest'];
	if($play_id == ''){

		$sql = mysqli_query($con, "INSERT INTO tbl_playlist (name,date_created,user_id) values('$playlist_name','$date_','$my_id')");

		if($sql){
			echo 1;
		}

	}else{
		$sql = mysqli_query($con, "UPDATE tbl_playlist set name = '$playlist_name' where id = '$play_id'");

		if($sql){
			echo 2;
		}
	}
}

function count_downloads($con){
	$my_id = $_SESSION['guest'];
	$sql=mysqli_query($con,"SELECT * FROM tbl_download_list where user_id = '$my_id'");
	$downloads = mysqli_num_rows($sql);
	echo $downloads;
}

/*function get_order_name($con){
$sql = mysqli_query($con,"SELECT * from tbl_shuffle_index");
$row = mysqli_fetch_assoc($sql);
return $row['order_row'];
}load_audio2

function get_order_sort($con){
$sql = mysqli_query($con,"SELECT * from tbl_shuffle_index");
$row = mysqli_fetch_assoc($sql);
return $row['order_sort'];
} */

function sound_audio2($con,$audio_id,$num){
	$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1");

		if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) {
			$number = $num++;
			if ($audio_id == $number) {
				?>
				<button class="btn btn-primary" id="player_btn2" style="display: none;" onclick="choose_play2('<?php echo '../'.$row['album_artwork'] ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>','<?php echo $number ?>');">click</button>
				<?php
			}
		}
		}
}

function count_music($con){
	$sql=mysqli_query($con,"SELECT * FROM tbl_audios where active = 1");
	$count_music = mysqli_num_rows($sql);

	if($count_music == 1){
		echo $count_music .' '.'Song';
	}else{
		echo $count_music .' '.'Songs';
	}

}

function count_music_playlist($con){
	$sql=mysqli_query($con,"SELECT * FROM tbl_audios");
	echo $count_music = mysqli_num_rows($sql);	
}

//insert song to playlist
function download_music($con,$current_loc, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');

	//location:
	/*$user_ip = getenv('REMOTE_ADDR');
	$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
	$city = $geo["geoplugin_city"];
	$region = $geo["geoplugin_regionName"];
	$country = $geo["geoplugin_countryName"];
	$location = $city.','.' '.$region.','.' '.$country;*/

	$sql = "INSERT INTO  tbl_downloads(audio_id,user_id,date_,geo_location)
	  VALUES ('$id','$my_id','$date_','$current_loc');";

	$sql2 = "DELETE FROM tbl_download_list Where audio_id = '$id' and user_id = '$my_id'";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }

	 if (mysqli_query($con, $sql2)) {
	      echo '';
	 }
}

//detect audio
function detect_download($con,$id){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con, "SELECT * FROM tbl_download_list WHERE audio_id='$id' and user_id = '$myid' ");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}

}

//insert song to download list
function add_download($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$detect = detect_download($con,$id);
	if ($detect == 1) {
		echo 404;
	}else{
		$sql = "INSERT INTO  tbl_download_list(audio_id,user_id,datetrans)
	  VALUES ('$id','$my_id','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
	
}

//Add Download on album list
function add_download_album($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$detect = detect_download($con,$id);
	if ($detect == 1) {
		echo 404;
	}else{
		$sql = "INSERT INTO  tbl_download_list(audio_id,user_id,datetrans)
	  VALUES ('$id','$my_id','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
	
}


//delete playlist
function delete_plist($con, $id){
	$sql = mysqli_query($con,"DELETE from tbl_playlist where id='$id'");
	$sql2 = mysqli_query($con,"DELETE from tbl_playlist_info where playlist_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//insert song to playlist
function save_song($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$playlist = mysqli_real_escape_string($con,$_POST['playlist2']);
	$detect = detect_name($con,$id);
	if ($detect == 1) {
		echo 404;
	}
	else{

		$sql = "INSERT INTO  tbl_playlist_info(audio_id,user_id,user_type,playlist_id,date_)
	    VALUES ('$id','$my_id',3,'$playlist','$date_');";

		if (mysqli_query($con, $sql)) {
	      echo 1;
	 }

	}
 
}

//detect audio
function detect_name($con,$id,$playlist){
	$sql = mysqli_query($con, "SELECT * FROM tbl_playlist_info WHERE audio_id='$id' and playlist_id = '$playlist'");
	if (mysqli_num_rows($sql)>0) {
		return 1;
	}
	else{
		return 0;
	}

}

//insert song to playlist
function save_song1($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$playlist = mysqli_real_escape_string($con,$_POST['v_pid']);
	$detect = detect_name($con,$id,$playlist);
	if ($detect == 1) {
		echo 404;
	}
	else{

		$sql = "INSERT INTO  tbl_playlist_info(audio_id,user_id,user_type,playlist_id,date_)
	  VALUES ('$id','$my_id',3,'$playlist','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}

//insert song to playlist
function save_all_music($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$playlist = mysqli_real_escape_string($con,$_POST['play_id2']);
	$detect = detect_name($con,$id,$playlist);
	if ($detect == 1) {
		echo 404;
	}
	else{

		$sql = "INSERT INTO  tbl_playlist_info(audio_id,user_id,user_type,playlist_id,date_)
	  VALUES ('$id','$my_id',3,'$playlist','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}


//insert song to playlist
function pick_all($con, $genre){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$playlist = mysqli_real_escape_string($con,$_POST['play_id2']);
	
	$sql = "INSERT INTO  tbl_playlist_info(audio_id,user_id,user_type,playlist_id,date_)
	  VALUES ('$id','$my_id',3,'$playlist','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	
}

//insert song to playlist
function save_song2($con, $id){
	$my_id = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$playlist = mysqli_real_escape_string($con,$_POST['playlist2']);
	$detect = detect_name($con,$id,$playlist);
	if ($detect == 1) {
		echo 404;
	}
	else{

		$sql = "INSERT INTO  tbl_playlist_info(audio_id,user_id,user_type,playlist_id,date_)
	  VALUES ('$id','$my_id',3,'$playlist','$date_');";

	if (mysqli_query($con, $sql)) {
	      echo 1;
	 }
	}
}

//clear_songs to playlist
function clear_songs($con, $id){
	$sql = mysqli_query($con,"DELETE from tbl_playlist_info where playlist_id='$id'");
	if ($sql) {
		echo 1;
	}
}

//delete all playlist
function delete_all_plist($con, $id){
	$sql = mysqli_query($con,"DELETE FROM tbl_playlist where user_id = '$id'");
	if ($sql) {
		echo 1;
	}
}

//delete all downloadlist
function delete_all_download($con, $id){
	$sql = mysqli_query($con,"DELETE FROM tbl_download_list where user_id = '$id'");
	if ($sql) {
		echo 1;
	}
}

//delete all downloadlist
function delete_download($con, $id){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"DELETE FROM tbl_download_list where user_id = '$myid' and audio_id = '$id'");
	if ($sql) {
		echo 1;
	}
}

//delete music per playlist
function delete1_music($con, $id){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"DELETE FROM tbl_playlist_info where id = '$id' and user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

//update all playlist
function update_plist($con, $id, $name){
	$sql = mysqli_query($con,"UPDATE tbl_playlist set name = '$name' where id = '$id'");
	if ($sql) {
		echo 1;
	}
}

//GET NOTIFY
function get_notify($con, $id){
	$sql = mysqli_query($con,"UPDATE tbl_subscribers set get_notif = 1 where id='$id'");
	if ($sql) {
		echo 1;
	}
}

//GET NOTIFY
function not_notify($con, $id){
	$sql = mysqli_query($con,"UPDATE tbl_subscribers set get_notif = 0 where id='$id'");
	if ($sql) {
		echo 1;
	}
}

//count new notification
function new_notif($con){
	$date_ = date('Y-m-d');
	$myid = $_SESSION['guest'];	

	$query = mysqli_query($con, "SELECT is_notify FROM tbl_user_profile where user_id = '$myid'");
	$rows = mysqli_fetch_assoc($query); 

	$sql =mysqli_query($con,"SELECT count(id) as new FROM tbl_audios WHERE active = 1 and DATE_FORMAT(approved_date,'%Y-%m-%d') = '$date_' ");

	$row = mysqli_fetch_assoc($sql); 
	
	if($rows['is_notify'] == 1){

		if($row['new'] > 0){ ?>
			<span class="badge badge-sm up bg-danger "><?php echo $row['new']; ?></span>
		<?php
		}else{ ?>
			<span class="badge badge-sm up bg-danger "></span>
		<?php
		}

	}else{ ?>
			<span class="badge badge-sm up bg-danger "></span>
	<?php }		
	}

//delete playlist
function unsubscribe($con, $id){
	$sql = mysqli_query($con,"DELETE from tbl_subscribers where id='$id'");
	if ($sql) {
		echo 1;
	}
}


function total_songs($con, $playlist_id){
	$sql = mysqli_query($con,"SELECT COUNT(audio_id) FROM tbl_playlist_info WHERE playlist_id = '$playlist_id'");
	$row = mysqli_fetch_assoc($sql);

	if($row['COUNT(audio_id)'] > 1){
		echo $row['COUNT(audio_id)'].' '. 'Songs';
	}else{
		echo $row['COUNT(audio_id)'].' '. 'Song';
	}	
    
}

function total_album_songs($con, $album_id){
	$sql = mysqli_query($con,"SELECT COUNT(audio_id) FROM tbl_audios WHERE album_id = '$album_id'");
	$row = mysqli_fetch_assoc($sql);

	if($row['COUNT(audio_id)'] > 1){
		echo $row['COUNT(audio_id)'].' '. 'Songs';
	}else{
		echo $row['COUNT(audio_id)'].' '. 'Song';
	}	
}

function get_max_limit($con,$tbl){
$sql = mysqli_query($con,"SELECT count(id) from ".$tbl."");
$row = mysqli_fetch_assoc($sql);
return $row['count(id)'];
}

function show_plist($con,$search){
if ($search == "" || $search == null) {
	$myid = $_SESSION['guest'];
	$sql =mysqli_query($con,"SELECT * from tbl_playlist where user_id='$myid' ORDER BY name");

	//$sql = mysqli_query($con, "SELECT t1.*,t2.* FROM tbl_playlist t1 LEFT JOIN tbl_album t2 ON")

	if (mysqli_num_rows($sql)) {
		?>
		
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
			$p_id = $row['id'];
			$sql2=mysqli_query($con,"SELECT * FROM tbl_playlist_info where playlist_id = '$p_id'");
			$count_song = mysqli_num_rows($sql2);
		?>
			<tr class="tracks item" ondblclick="get_p_id('<?php echo $row['id'] ?>','<?php echo $row['name'] ?>');">
				<td></td>
				<td class="text-left"><p class="db-content default-fs" style="margin-left: 4%; margin-top: 4%;"><?php echo strtoupper($row['name']); ?></p></td>
				<td class="text-left">
					<p class="db-content default-fs" style="margin-left: 10%; font-size: 14px; margin-top: 6%;">
					<?php 
					if($count_song == 1 or $count_song ==0){ ?>
						<?php echo $count_song; ?> Song
					<?php }else{ ?>
						<?php echo $count_song; ?> Songs
					<?php	
					}
					?></p>
				</td>
				<td class="text-left">
					<p class="db-content default-fs"  style="margin-left: 4%; margin-top: 3%;"><?php echo date('M d, Y', strtotime($row['date_created'])); ?></p>
				</td>
				<td class="text-left">
					<div class="btn-group" style="margin-left: 4%; margin-top: 1%;"> 
						<form action="playlist.php" method="post">
							<input type="text" style="display: none" id="playlist_id" name="playlist_id" value="<?php echo $row['id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="playlist_name" name="playlist_name" value="<?php echo $row['name']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="play_nows" name="play_nows" value="1" autocomplete="off"/>
							<button class="btn btn-dark btn-small" type="submit" id="btn_submit"><span class="fa fa-play fa-lg"></span>
	                   	 	</button>  
			                
		            	</form>
						<form action="playlist.php" method="post">
							<input type="text" style="display: none" id="playlist_id" name="playlist_id" value="<?php echo $row['id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="playlist_name" name="playlist_name" value="<?php echo $row['name']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="play_nows" name="play_nows" value="0" autocomplete="off"/>
							<button class="btn btn-dark btn-small" type="submit" id="btn_submit"><span class="fa fa-eye fa-lg"></span>
	                   	 	</button>  
			                
		            	</form>
	                   	 <button class="btn btn-dark btn-small" onclick="edit_playlist('<?php echo $row['id'] ?>','<?php echo $row['name']?>');"><span class="fa fa-edit fa-lg"></span>
	                   	 </button>               	

	                   	 <button class="btn btn-dark btn-small " onclick="delete_plist('<?php echo $row['id'] ?>');">
							<span class="fa fa-trash fa-lg"></span>
	                   	 </button>     

	                  </div>
				</td>
			</tr>
		<?php
		}
	}
	else
	{
	?>
		<tr>
			<td colspan="5">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Playlist Found!</strong>
				</div>
			</td>
		</tr>
	<?php 
	}
}
else
{
	$myid = $_SESSION['guest'];
	$sql =mysqli_query($con,"SELECT * FROM tbl_playlist where name  like '%$search%' and user_id='$myid' ORDER BY name");
	if (mysqli_num_rows($sql)) {
	?>
		<?php
		while ($row = mysqli_fetch_assoc($sql)) {
			$p_id = $row['id'];
		$sql2=mysqli_query($con,"SELECT * FROM tbl_playlist_info where playlist_id = '$p_id'");
		$count_song = mysqli_num_rows($sql2);
    	$imgs = '../img/plist.jpg';
		?>	
		<tr class="tracks item" ondblclick="get_p_id('<?php echo $row['id'] ?>','<?php echo $row['name'] ?>');">
				<td></td>
				<td class="text-left"><p class="db-content default-fs" style="margin-left: 4%; margin-top: 4%;"><?php echo strtoupper($row['name']); ?></p></td>
				<td class="text-left">
					<p class="db-content default-fs" style="margin-left: 10%; font-size: 14px; margin-top: 6%;">
					<?php 
					if($count_song == 1 or $count_song ==0){ ?>
						<?php echo $count_song; ?> Song
					<?php }else{ ?>
						<?php echo $count_song; ?> Songs
					<?php	
					}
					?></p>
				</td>
				<td class="text-left">
					<p class="db-content default-fs"  style="margin-left: 4%; margin-top: 3%;"><?php echo date('M d, Y', strtotime($row['date_created'])); ?></p>
				</td>
				<td class="text-left">
					<div class="btn-group" style="margin-left: 4%; margin-top: 1%;"> 
						<form action="playlist.php" method="post">
							<input type="text" style="display: none" id="playlist_id" name="playlist_id" value="<?php echo $row['id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="playlist_name" name="playlist_name" value="<?php echo $row['name']; ?>" autocomplete="off"/>
							<button class="btn btn-dark btn-small" type="submit" id="btn_submit"><span class="fa fa-eye fa-lg"></span>
	                   	 	</button>  
			                
		            	</form>
	                   	 <button class="btn btn-dark btn-smallest" onclick="edit_playlist('<?php echo $row['id'] ?>','<?php echo $row['name']?>');"><span class="fa fa-edit fa-lg"></span>
	                   	 </button>               	

	                   	 <button class="btn btn-dark btn-smallest " onclick="delete_plist('<?php echo $row['id'] ?>');">
							<span class="fa fa-trash fa-lg"></span>
	                   	 </button>     

	                  </div>
				</td>
			</tr>

			<?php
			}
			?>
	<?php
	}
	else
	{
	?>
		<tr>
			<td colspan="5">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Playlist Found!</strong>
				</div>
			</td>
		</tr>
	<?php  
	}
}

}
//PLAYLIST
function show_music_playlist($con,$id,$search_song){
	if ($search_song == "" || $search_song == null) {

	$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture`,t5.`id` as t_id from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		right join tbl_playlist_info t5 on t2.`id` = t5.`audio_id`
		where t5.`playlist_id`='$id' and t2.active = 1  order by song_name");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$id = $row['audio_id'];
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$detect = detect_download($con,$id);
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

	if ($row['picture'] == "") {
		$img = '../img/contributor.png';
	}
	else
	{
		$img = '../'.$row['picture'];
	}

	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	$toll_title="<center>".'Contributor'."</center>";
	?>

		<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');">
			<td style="width: 0px; "></td>
			<td class="play"  style="width: 10%; "><a href="#"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></a></td>
			<td class="left" style=" width: 15%;">
					<h6 class="db-content default-fs">
					<?php echo ucfirst($row['song_name']); ?>&nbsp; 
						<?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?>
					</h6>
				<form action="album_musics.php" method="post">
					<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
	                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
					<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
            	</form>  
			</td>
			<td class="text-left" style=" width: 17%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['description']); ?></p>
			</td>
			<td class="text-left" style=" width: 17%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['genre']); ?></p>
			</td>
			<td class="text-left" style=" width: 12%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['composer']); ?></p>
			</td>
			<td class="text-left" style=" width: 15%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['application_id']); ?></p>
			</td>
			<td class="text-left" style=" width: 2%;">
				
					<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</p>
			</td>
			<td class="text-left">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></p>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified"> 
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="Play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>'); save_play('<?php echo $row['audio_id'] ?>');"><span class="fa fa-play-circle fa-lg"></span>
					 </button>
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button>  
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="Remove Song from playlist."  onclick="del_music('<?php echo $row['t_id'];?>');" title="Delete From Playlist"><span class="fa fa-times fa-lg"></span></button>   
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>
               	</div>
			</td>
			<td style=""></td>
		</tr>
	<?php
	}
	?>
	<script type="text/javascript">
		enable_tooltips();
	</script>
	<?php
	}
		else
		{ ?>
			<td colspan="10" class="text-center">
			  <strong class="text-default">No Music Found!</strong>
			</td>
	<?php	
		}
	}
	else
	{	
		$sql =mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture`,t5.`id` as t_id from tbl_album t1 
			right join tbl_audios t2 on t1.album_id=t2.album_id 
			right join tbl_album t3 on t2.album_id=t3.album_id 
			right join tbl_user_profile t4 on t3.user_id=t4.user_id 
			right join tbl_playlist_info t5 on t2.`id` = t5.`audio_id`
			where t5.`playlist_id`='$id' and t2.active = 1 and concat(song_name,genre,fn,ln,composer,description,application_id,keyword) like '%$search_song%'  order by song_name ");

		if (mysqli_num_rows($sql)>0) {
			?>
				
			<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$id = $row['audio_id'];
					$app_date = $row['app_date'];
					$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
					$group_id = $row['file_id'];
					$detect = detect_download($con,$id);
					$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
					$rows = mysqli_fetch_assoc($query);
					$total_files = $rows['files'];
					if ($row['picture'] == "") {
						$img = '../img/contributor.png';
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
				    	$imgs = '../img/slogo3.png';
					}
					else
					{
						$imgs = '../'.$row['album_artwork'];
					}

					$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
					$toll_title="<center>".'Contributor'."</center>";

					?>
						<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');">
			<td style="width: 0px; "></td>
			<td class="play"  style="width: 10%; "><a href="#"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></a></td>
			<td class="left" style=" width: 15%;">
					<h6 class="db-content default-fs">
					<?php echo ucfirst($row['song_name']); ?>
					<?php if($detect_month != 1){ ?>
						<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
					<?php }else{ ?>

					<?php } ?>
					</h6>
				<form action="album_musics.php" method="post">
					<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
	                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
					<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
            	</form> 
			</td>
			<td class="text-left" style=" width: 17%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['description']); ?></p>
			</td>
			<td class="text-left" style=" width: 17%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['genre']); ?></p>
			</td>
			<td class="text-left" style=" width: 12%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['composer']); ?></p>
			</td>
			<td class="text-left" style=" width: 15%;">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['application_id']); ?></p>
			</td>
			<td class="text-left" style=" width: 2%;">
				
					<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</p>
			</td>
			<td class="text-left">
				<p class="db-content default-fs" style="margin-left: 9%; margin-top: 5%;"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></p>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified"> 
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="Play" onclick="choose_play2('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>','<?php echo $row['audio_id']?>'); on_firs_play2(); only_play2();"><span class="fa fa-play-circle fa-lg"></span>
					 </button>
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button>  
					 <button class="btn btn-dark btn-small" data-toggle="tooltip" title="Remove Song from playlist."  onclick="del_music('<?php echo $row['t_id'];?>');" title="Delete From Playlist"><span class="fa fa-times fa-lg"></span></button>   
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>
               	</div>
			</td>
			<td style=""></td>
		</tr>
					<?php
					}
				?>
				<script type="text/javascript">
					enable_tooltips();
				</script>
			<?php
		}
		else
		{
			?>
			<td colspan="10" class="text-center">
			  <strong class="text-default"> No Music Found!</strong>
			</td>
	<?php
		}
	}
}

//WEBSITE
function load_audio2($con,$genres2, $search_audio,$playlist){

	if ($search_audio == "" || $search_audio == null) {

		if($genres2 === 'null'){
			$query = "SELECT *,DATE_FORMAT(approved_date,'%Y-%m-%d') as app_date FROM tbl_audios Where active = 1 order by song_name ASC";
		}else{
			$query = "SELECT *,DATE_FORMAT(approved_date,'%Y-%m-%d') as app_date FROM tbl_audios Where active = 1 and concat(genre) LIKE '%$genres2%' order by song_name ASC";
		}
		
		$result = mysqli_query($con, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 while($row = mysqli_fetch_array($result))
		 {
		 	  //$playlist = mysqli_real_escape_string($con,$_POST['play_id2']);
		  $id = $row["audio_id"];
		  $id2 = $row['id'];
		  $detect = detect_name($con,$id2,$playlist);
		  $date_ = date('Y-m-d');
		  $app_date = $row['app_date'];

		?>
		  <tr  class="tracks">
		    <td style="width: 15%;">      
		      <input type="hidden" id="p_id" value="<?php echo $id; ?>">
		        <?php echo $row["song_name"]; ?>&nbsp; 
		        <?php if($date_ == $app_date){ ?> 
		        	<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently added">New!</span>
		        <?php }else{ ?>

		        <?php } ?>  
		    </td>
		    <td style="width: 20%;">
		        <?php echo $row["description"]; ?> 
		    </td>
		    <td style="width: 20%;">
		        <?php echo $row["genre"]; ?> 
		    </td>
		    <td style="width: 15%;">
		        <?php echo $row["composer"]; ?> 
		    </td>
		    
		    <td style="width: 10%;">
		        <?php echo date('m-d-y', strtotime($row['upload_date'])); ?> 
		    </td>
		    <td>
		    <?php 
		    	if($detect == 1){ ?>
				<button class="btn btn-dark btn-small">
		        	<span class="fa fa-check-circle" data-toggle="tooltip" title="Already added to playlist."></span> Added
		        </button>
		    <?php }else{ ?>
				<button class="btn btn-dark btn-small" id="add_song" name="add_song" onclick="save_song1('<?php echo $row['id'] ?>');">
		        	<span class="fa fa-plus" ></span> Add Song
		        </button>
		    <?php } ?>
		        
		    </td>
		    <td style="display: none;">
		        <?php echo $row["id"]; ?> 
		    </td>
		  </tr>
		  <script type="text/javascript">
			enable_tooltips();
		  </script>

		<?php  
		 }?>		 
		<?php 
		}
		else
		{
		?>
		<div class="alert alert-default alert-dismissible text-center" >
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
		</div>
			
		<?php  
		}

	}else{

		if($genres2 === 'null'){
			$query = "SELECT *,DATE_FORMAT(approved_date,'%Y-%m-%d') as app_date FROM tbl_audios Where active = 1 and concat(song_name,composer,description,keyword,genre) like '%$search_audio%' order by song_name ASC";
		}else{
			$query = "SELECT *,DATE_FORMAT(approved_date,'%Y-%m-%d') as app_date FROM tbl_audios Where active = 1 and concat(genre) LIKE '%$genres2%' and concat(song_name,composer,description,keyword,genre) like '%$search_audio%'and concat(song_name,composer,description,keyword,genre) like '%$search_audio%' order by song_name ASC";
		}

		$result = mysqli_query($con, $query);
		if(mysqli_num_rows($result) > 0)
		{
		 while($row = mysqli_fetch_array($result))
		 {
		 	  //$playlist = mysqli_real_escape_string($con,$_POST['play_id2']);
		  $id = $row["audio_id"];
		  $id2 = $row['id'];
		  $detect = detect_name($con,$id2,$playlist);
		  $date_ = date('Y-m-d');
		  $app_date = $row['app_date'];
		?>
		  <tr  class="tracks">
		    <td style="width: 15%;">      
		      <input type="hidden" id="p_id" value="<?php echo $id; ?>">
		        <?php echo $row["song_name"]; ?>&nbsp; 
		        <?php if($date_ == $app_date){ ?> 
		        	<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently added">New!</span>
		        <?php }else{ ?>

		        <?php } ?>  
		    </td>
		    <td style="width: 20%;">
		        <?php echo $row["description"]; ?> 
		    </td>
		    <td style="width: 20%;">
		        <?php echo $row["genre"]; ?> 
		    </td>
		    <td style="width: 15%;">
		        <?php echo $row["composer"]; ?> 
		    </td>
		    
		    <td style="width: 10%;">
		        <?php echo date('m-d-y', strtotime($row['upload_date'])); ?> 
		    </td>
		    <td>
		    <?php 
		    	if($detect == 1){ ?>
				<button class="btn btn-dark btn-small">
		        	<span class="fa fa-check-circle" data-toggle="tooltip" title="Already added to playlist."></span> Added
		        </button>
		    <?php }else{ ?>
				<button class="btn btn-dark btn-small" id="add_song" name="add_song" onclick="save_song1('<?php echo $row['id'] ?>');">
		        	<span class="fa fa-plus" ></span> Add Song
		        </button>
		    <?php } ?>
		        
		    </td>
		    <td style="display: none;">
		        <?php echo $row["id"]; ?> 
		    </td>
		  </tr>

		<?php  
		 }
		}
		else
		{
		?>
			<div class="alert alert-default alert-dismissible text-center" >
			  <strong class="text-default "><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
			</div>
		<?php  
		}

	}

}

function not_notify_me($con){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set is_notify = 0 where user_id = '$myid'");
	
	if ($sql) {
		echo 1;
	$query3=mysqli_query($con, "SELECT fn,email_add FROM tbl_user_profile where user_id='$myid'");
	$row3 = mysqli_fetch_assoc($query3);
	//send email to disapproved
	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

	try {
	    //Server settings
	    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
	    $mail->Password = 'testing5!';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 587;                                    // TCP port to connect to

	    $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true
	        )
	    );
	    //Recipients
	    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
	    $mail->addAddress($row3['email_add']);     // Add a recipient

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Subscriber welcome '.$row3['email_add'].':';
	    $mail->Body    = '<p>Hi '.$row3['fn'].',</p>
						<p>You have unsubscribed from the Lampstand Monthly Updates. You will no longer receive the monthly update email. </p>

						<p>You can re-subscribe at any time via your account management area.</p>

						<p>Kind Regards,</p>
						<p>The Lampstand Studio Team</p>';
	    $mail->AltBody = '';
	    $mail->send();
	    // echo 'Message has been sent';
	} catch (Exception $e) {
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}

	}

}

function notify_me($con){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set is_notify = 1 where user_id = '$myid'");
	
	if ($sql) {
		echo 1;
		$query2=mysqli_query($con, "SELECT fn,email_add FROM tbl_user_profile where user_id='$myid'");
		$row2 = mysqli_fetch_assoc($query2);
		//send email to approved
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions

		try {
		    //Server settings
		    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'teamtester5gh@gmail.com';                 // SMTP username
		    $mail->Password = 'testing5!';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 587;                                    // TCP port to connect to

		    $mail->SMTPOptions = array(
		    'ssl' => array(
		        'verify_peer' => false,
		        'verify_peer_name' => false,
		        'allow_self_signed' => true
		        )
		    );
		    //Recipients
		    $mail->setFrom('teamtester5gh@gmail.com', 'Music Library');
		    $mail->addAddress($row2['email_add']);     // Add a recipient

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Subscriber welcome '.$row2['email_add'].':';
		    $mail->Body    = ' <p>Hi '.$row2['fn'].',</p>
								<p>Thanks for subscribing to the Lampstand Studio Monthly update. Once a month we will send you a list of any new songs that have been added to the site since the last update. </p>

								<p>If no new music is added, you will not receive an email. </p>

								<p>These are the only emails that you will receive from Lampstand Studio apart from any technical notifications if we are going to be doing upgrades or maintenance.</p>
								<p>You can manage your subscription from within the account section of your login.</p>

								<p>Enjoy your free music!</p>

								<p>Kind Regards,</p>
								<p>The Lampstand Studio Team</p>
		    				';
		    $mail->AltBody = '';
		    $mail->send();
		    // echo 'Message has been sent';
		} catch (Exception $e) {
		    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		}
	}

}

//Session update
function refresh_account($con,$myid){
	$id = $_SESSION['guest'];
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$myid' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['guest'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
	$_SESSION['user_pic'] = '../avatar/def.png';
					
	}
	else
	{
		$_SESSION['user_pic'] = '../'.$row['picture'];
	}

	$dt1 = date_create($row['age']);
	$dt2 = date('Y-m-d');
	$dt3 = date_create($dt2);	
	$age = date_diff($dt1,$dt3);
	$is_notify = $row['is_notify'];

	?>
	<div class="list-group glass" style="padding-top: 10px; padding-right: 10px; padding-left: 10px; padding-bottom: 10px;">
			
			<a href="#" data-target="#edit_name_modal" style="color: #6C7A89" onclick="update_name('<?php echo $_SESSION['fn'] ?>','<?php echo $_SESSION['ln'] ?>','<?php echo $_SESSION['guest'] ?>');" class="list-group-item text-left w3-hover-shadow" data-toggle="modal" data-placement="left" title="Name"><i class=" fa fa-user fa-lg"></i> &nbsp; <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a><br>
			
			<a href="#" data-target="#edit_email_modal"style="color: #6C7A89"  onclick="update_email('<?php echo $_SESSION['email_add'] ?>');" class="list-group-item w3-hover-shadow text-left" data-toggle="modal" data-placement="left" title="E-mail"><i class=" fa fa-envelope fa-lg"></i> &nbsp; <?php echo $_SESSION['email_add'] ?></a><br>
			
			<a href="#" data-target="#edit_pass_modal" style="color: #6C7A89" onclick="update_pwd('<?php echo $_SESSION['password'] ?>');"  data-toggle="modal" data-placement="left" title="Change your password" class="list-group-item w3-hover-shadow text-left"><i class=" fa fa-lock fa-lg"></i> &nbsp; Change Password</a><br>

			<style type="text/css">
				.switch {
				  position: relative;
				  display: inline-block;
				  width: 60px;
				  height: 34px;
				}

				.switch input {display:none;}

				.slider {
				  position: absolute;
				  cursor: pointer;
				  top: 0;
				  left: 0;
				  right: 0;
				  bottom: 0;
				  background-color: #ccc;
				  -webkit-transition: .4s;
				  transition: .4s;
				}

				.slider:before {
				  position: absolute;
				  content: "";
				  height: 26px;
				  width: 26px;
				  left: 4px;
				  bottom: 4px;
				  background-color: white;
				  -webkit-transition: .4s;
				  transition: .4s;
				}

				input:checked + .slider {
				  background-color: #2196F3;
				}

				input:focus + .slider {
				  box-shadow: 0 0 1px #2196F3;
				}

				input:checked + .slider:before {
				  -webkit-transform: translateX(26px);
				  -ms-transform: translateX(26px);
				  transform: translateX(26px);
				}

				/* Rounded sliders */
				.slider.round {
				  border-radius: 34px;
				}

				.slider.round:before {
				  border-radius: 50%;
				}
			</style>
			<div class="form-group">
	            <div class="col-lg-12">
	            <?php 
	            	if($is_notify == 1){ ?>
	            		<div class="row">
	            			<div class="col-lg-10">
								<label style="margin-top: 2%;" class="custom-control-label" for="customCheck1">Notify me when new music is added?</label>
							</div>
	            			<div class="col-lg-2"> 
	            				<label class="switch">
								<input type="checkbox" checked="true" id="customCheck1" onclick="not_notify_me();">
								<span class="slider round"></span>
								</label>
	            			</div>
	            		</div>
					
	            <?php }else{ ?>
	            		<div class="row">
	            			<div class="col-lg-10">
	            				<label style="margin-top: 2%;" class="custom-control-label" for="customCheck2">Notify me when new music is added?</label>
	            			</div>
	            			<div class="col-lg-2">
	            				<label class="switch">
								  <input type="checkbox" onclick="notify_me();" id="customCheck2">
								  <span class="slider round"></span>
								</label>
	            				
	            			</div>

	            		</div>
					    	 
	            <?php } ?> 
	            	
	            </div>
	         </div><br>
		</div>
		<script type="text/javascript">

		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		});
		</script>
	<?php
}

function pict_session($con){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(b_date,'%M %d%, %Y') as dt,DATE(b_date) as age from tbl_user_profile where user_id='$myid' and is_active=1");
	$row = mysqli_fetch_assoc($sql);
	$_SESSION['guest'] = $row['user_id'];
	$_SESSION['fn'] = $row['fn'];
	$_SESSION['mn'] = $row['mn'];
	$_SESSION['ln'] = $row['ln'];
	$_SESSION['gender'] = $row['gender'];
	$_SESSION['b_date'] = $row['dt'];
	$_SESSION['email_add'] = $row['email_add'];
	$_SESSION['username'] = $row['username'];
	$_SESSION['password'] = $row['password'];
	$_SESSION['user_type'] = $row['user_type'];
	if ($row['picture'] == "") {
			$_SESSION['user_pic'] = '../avatar/dev.png';
		
		}
		else
		{
			$_SESSION['user_pic'] = '..'.$row['picture'];
		}

		echo $_SESSION['user_pic'];
}
//WEBSITE
function show_download_list($con,$search_dl)
{	
	$my_id = $_SESSION['guest'];
	if ($search_dl == "" || $search_dl == null) {

		$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,COUNT(t3.`audio_id`) AS downloads,t4.`album_artwork`,t4.`album_name`, t5.`fn`, t5.`ln` from tbl_download_list t1 
				left join tbl_audios t2 on t1.`audio_id`=t2.`audio_id`
				left join tbl_downloads t3 on t1.`audio_id`=t3.`audio_id` 
				left join tbl_album t4 on t2.`album_id`=t4.`album_id`
				left join tbl_user_profile t5 on t4.`user_id`=t5.`user_id`  
				where t1.`user_id` = '$my_id' and t1.`status`= 0 GROUP BY t2.`audio_id` order by song_name");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$group_id = $row['file_id'];
	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];


	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
	$imgs = '../img/slogo3.png';
	}
	else
	{
	$imgs = '../'.$row['album_artwork'];
	}
	?>
	<tr class="tracks item">
	<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
					    <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
						<form action="album_musics.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
			        	</form> 
					</h6>
				
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td class="text-left style="">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></h6>
			</td>
	<td class="text-left">
		<div class="btn-group btn-justified"> 
			<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
			<i class="fa fa-download fa-lg text-white"></i>
			</a> 
			<a id="download_" class="btn btn-dark btn-small" data-toggle="tooltip" title="Remove song from Download List" onclick="delete_download('<?php echo $row['audio_id'] ?>');">
			<i class="fa fa-times fa-lg text-white"></i>
			</a>
			<a class="btn btn-dark btn-small" data-toggle="tooltip" title="Play" onclick="choose_play2('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>','<?php echo $row['audio_id']?>'); on_firs_play2(); only_play2();"><i class="fa fa-play fa-lg text-white"></i>
			</a>                 	
		</div>
	</td>
	</tr>
	<?php 
	}
	?>
	<script type="text/javascript">
		enable_tooltips();	
	</script>
	<?php
	}
		else
		{
			?>
	
			<tr>
				<td colspan="10">
					<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-music fa-3x"></span> <br> No Music Found!</strong>
					</div>
				</td>
			</tr>
			
			
			
	<?php
		}
	}
	else
	{

		$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,COUNT(t3.`audio_id`) AS downloads,t4.`album_artwork`,t4.`album_name`, t5.`fn`, t5.`ln` from tbl_download_list t1 
				left join tbl_audios t2 on t1.`audio_id`=t2.`audio_id`
				left join tbl_downloads t3 on t1.`audio_id`=t3.`audio_id` 
				left join tbl_album t4 on t2.`album_id`=t4.`album_id`
				left join tbl_user_profile t5 on t4.`user_id`=t5.`user_id`   
				where t1.`user_id` = '$my_id' and t1.`status`= 0 and concat(t2.song_name,t2.composer,t2.genre,t2.description,t2.keyword,t4.`album_name`) like '%$search_dl%' GROUP BY t2.`audio_id` order by song_name");
		
		if (mysqli_num_rows($sql)>0) {
			?>

	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
		$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$group_id = $row['file_id'];
	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];


	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
	$imgs = '../img/slogo3.png';
	}
	else
	{
	$imgs = '../'.$row['album_artwork'];
	}
	?>
	<tr class="tracks item">
	<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
					    <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
						<form action="album_musics.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
			        	</form>
					</h6>

			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td class="text-left style="">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></h6>
			</td>
	<td class="text-left">
		<div class="btn-group btn-justified"> 
			<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
			<i class="fa fa-download fa-lg text-white"></i>
			</a> 
			<a id="download_" class="btn btn-dark btn-small" data-toggle="tooltip" title="Remove song from Download List" onclick="delete_download('<?php echo $row['audio_id'] ?>');">
			<i class="fa fa-times fa-lg text-white"></i>
			</a>
			<a class="btn btn-dark btn-small" data-toggle="tooltip" title="Play" onclick="choose_play2('<?php echo $imgs ?>','../<?php echo $row['music'] ?>','<?php echo strtoupper($row['song_name']) ?>','<?php echo $row['audio_id']?>'); on_firs_play2(); only_play2();"><i class="fa fa-play fa-lg text-white"></i>
			</a>                 	
		</div>
	</td>
	</tr>
	<?php
	}
			?>
			<?php
		}
		else
		{
			?>
			<tr>
				<td colspan="10">
					<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-music fa-3x"></span> <br> No Music Found!</strong>
					</div>
				</td>
			</tr>
	<?php
		}
		
	}
	
}
//WEBSITE
//show notifications
function show_notif($con){
	$date_ = date('Y-m-d');
	$myid = $_SESSION['guest'];

	$query = mysqli_query($con, "SELECT is_notify FROM tbl_user_profile where user_id = '$myid'");
	$rows = mysqli_fetch_assoc($query); 
	$is_notify = $rows['is_notify'];

	$sql = mysqli_query($con, "SELECT t1.*,t2.`album_name`,t3.`fn`,t3.`ln`,t3.`picture` FROM tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
		LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
		WHERE t1.`active` = 1 and DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') = '$date_' order by t1.`approved_date` DESC");

	if (mysqli_num_rows($sql)>0) {

		while ($row = mysqli_fetch_assoc($sql)) {
			$date_app = $row['approved_date'];

			if ($row['picture'] == "") {
				$img = '../avatar/def.png';		
				}
			else
			{
				$img = '../'.$row['picture'];
			}
		?>	

			<?php 
				if($is_notify == 1){ ?>

					<a href="#" class="media list-group-item" onclick="hide_notif(); pager('show_musics'); "> 
						<span class="pull-left thumb-sm"> 
				          <img src="../img/img.png" style="background-image: url(<?php echo $img; ?>); border-radius: 5px;" width="40"class="img-fluid img-album w3-round"> 
				      </span> 
				      <span class="media-body block m-b-none"> 
				      	Name: <b><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></b><br> 
				      	Song: <b><?php echo $row['song_name']; ?></b><br> 
				      	Album: <b><?php echo $row['album_name']; ?></b><br> 
				      	Date: <b><small class="text-muted"><?php echo date('M d, Y', strtotime($row['approved_date'])); ?></small></b> <br>
				      	<small class="text-muted"> <?php echo time_ago($date_app); ?></small> 
				      </span>
				      
				    </a>

			<?php	}  
		} ?>
		<br>
		<div class="form-group">
				<?php 
	            	if($is_notify == 1){ ?>

						<label class="col-sm-9 control-label text-black"><b><span class="fa fa-cogs"></span> Unnotify me when new music is added.</b></label> 
						<div class="col-sm-3">

						<label class="switch" > <input type="checkbox" checked="true" onclick="not_notify_me();"> <span></span> </label> 
						</div>

	            <?php }else{ ?>

	            		<label class="col-sm-9 control-label text-black"><b><span class="fa fa-cogs"></span> Notify me when new music is added.</b></label> 
						<div class="col-sm-3">
						<label class="switch" > <input type="checkbox" onclick="notify_me();"> <span></span> </label> 

						</div>

	            <?php } ?> 
	         </div>
    <?php
		

	}else{ ?>
		<br>
		<div class="form-group">
				<?php 
	            	if($is_notify == 1){ ?>

						<label class="col-sm-9 control-label text-black"><b><span class="fa fa-cogs"></span> Unnotify me when new music is added.</b></label> 
						<div class="col-sm-3">

						<label class="switch" > <input type="checkbox" checked="true" onclick="not_notify_me();"> <span></span> </label> 
						</div>

	            <?php }else{ ?>

	            		<label class="col-sm-9 control-label text-black"><b><span class="fa fa-cogs"></span> Notify me when new music is added.</b></label> 
						<div class="col-sm-3">
						<label class="switch" > <input type="checkbox" onclick="notify_me();"> <span></span> </label> 

						</div>

	            <?php } ?> 
	         </div>
	<?php
	}
}

function show_musics($con,$search3)
{
	if ($search3 == "" || $search3 == null) {

	$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture`, t3.`album_id`, t3.`album_name`,t3.`album_description` from tbl_album t1 
		right join tbl_audios t2 on t1.album_id=t2.album_id 
		right join tbl_album t3 on t2.album_id=t3.album_id 
		right join tbl_user_profile t4 on t3.user_id=t4.user_id 
		where t2.active = 1  order by song_name");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$id = $row['audio_id'];
	$app_date = $row['app_date'];

	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$detect = detect_download($con,$id);
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

	if ($row['picture'] == "") {
		$img = '../img/contributor.png';
	}
	else
	{
		$img = '../'.$row['picture'];
	}

	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	$toll_title="<center>".'Contributor'."</center>";
	?>

		<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');" >
			<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
					    <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
		            	<form action="album_musics.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_description" name="album_description" value="<?php echo $row['album_description']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>   
					</h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>'); save_play('<?php echo $row['audio_id'] ?>');" title="Play"><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
		</tr>
	<?php
	}
	?>
	<script type="text/javascript">
		enable_tooltips();
	</script>
	<?php
	}
		else
		{ ?>

			<td colspan="10" class="text-center">
			 	<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
			</td>
			
	<?php	
		}
	}
	else
	{

		$sql =mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
			right join tbl_audios t2 on t1.album_id=t2.album_id 
			right join tbl_album t3 on t2.album_id=t3.album_id 
			right join tbl_user_profile t4 on t3.user_id=t4.user_id 
			where t2.active = 1 and concat(song_name,genre,fn,ln,composer,description,application_id,keyword) like '%$search3%'  order by song_name");

		if (mysqli_num_rows($sql)>0) {
			?>
				
			<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$id = $row['audio_id'];
				    $date_ = date('Y-m-d');
					$app_date = $row['app_date'];
					$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
					$group_id = $row['file_id'];
					$detect = detect_download($con,$id);
					$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
					$rows = mysqli_fetch_assoc($query);
					$total_files = $rows['files'];
					if ($row['picture'] == "") {
						$img = '../img/contributor.png';
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

					$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
					$toll_title="<center>".'Contributor'."</center>";

					?>
			<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');" >
			<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
						<?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
						<form action="album_musics.php" method="post">
							<input type="text" style="display: none" id="album_id" name="album_id" value="<?php echo $row['album_id']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_name" name="album_name" value="<?php echo $row['album_name']; ?>" autocomplete="off"/>
			                <input type="text" style="display: none" id="album_description" name="album_description" value="<?php echo $row['album_description']; ?>" autocomplete="off"/>
							<button class="lbl_btn" title="View Album." type="submit" id="btn_submit" style="margin-left: -6px;"><?php echo $row['album_name']; ?></button>  
		            	</form>   
					</h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>'); save_play('<?php echo $row['audio_id'] ?>'); " title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
		</tr>
					<?php
					}
				?>
				<script type="text/javascript">
					enable_tooltips();
				</script>
			<?php
		}
		else
		{
			?>
			<td colspan="10" class="text-center">
			   <div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
			</td>
	<?php
		}
	}
}


function add_downloadlist($con,$search)
{
	if ($search == "" || $search == null) {
	$sql = mysqli_query($con,"SELECT a.*,b.*,DATE_FORMAT(b.`approved_date`,'%Y-%m-%d') as app_date,COUNT(c.`audio_id`) AS downloads from tbl_album a 
		left join tbl_audios b on a.album_id=b.album_id 
		left join tbl_downloads c on b.audio_id=c.audio_id 
		where b.active = 1 GROUP BY b.`audio_id`  order by downloads desc");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
		$id= $row['audio_id'];
		$detect = detect_download($con,$id);
		$date_ = date('Y-m-d');
		$app_date = $row['app_date'];
	?>

		<ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto" >
            <li class="list-group-item text-white">
               <div class="pull-right "> 
               		<?php 
               			if($detect == 1){ ?>

               			<a class="btn btn-dark rounded btn-oji" data-toggle="tooltip" title="Already Added to Download list"><i class="icon-check icon-lg"></i> Added
                        </a>

               		<?php }else{ ?>

               			<a class="btn btn-info rounded btn-oji" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><i class="icon-plus icon-lg"></i> Add
                        </a>

               		<?php } ?>                       
					<br>          	
                    <div class="clear text-ellipsis text-muted" style="font-size: 13px;">
                     <?php //echo  date('M d, Y H:i A', strtotime($row['approved_date'])); ?>
                    </div>
				</div>        
                  	<a onclick="music_info('<?php echo $row['audio_id']; ?>');" class=" m-r-sm pull-left"> 
                    	<img src="../img/musicicon2.png" width="40" height="40">
                   	</a> 
					<div class="clear text-ellipsis"> 
						<span><?php echo ucfirst($row['song_name']); ?>
						&nbsp; <?php if($date_ == $app_date){ ?>
							<span class="badge badge-sm text-success">New!</span>
						<?php }else{ ?>

						<?php } ?>
						</span>
						<div class="clear text-ellipsis text-muted" style="font-size: 13px;">
							<span class="icon-cloud-download"></span>&nbsp;<?php echo ucfirst($row['downloads']); ?>
						</div>
					</div>
			</li>
		</ul>
	<?php
	}
	?>
	<?php
	}
		else
		{ ?>
			<div class="alert alert-default alert-dismissible text-center" >
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
			</div>
	<?php	
		}
	}
	else
	{
		$sql =mysqli_query($con,"SELECT a.*,b.*,DATE_FORMAT(b.`approved_date`,'%Y-%m-%d') as app_date,COUNT(c.`audio_id`) AS downloads from tbl_album a 
			left join tbl_audios b on a.album_id=b.album_id
			left join tbl_downloads c on b.audio_id=c.audio_id  
			where b.active = 1 and song_name like '%$search%' GROUP BY b.`audio_id`  order by downloads desc");
		if (mysqli_num_rows($sql)>0) {
			?>
				
			<?php
	while ($row = mysqli_fetch_assoc($sql)) {
		$id= $row['audio_id'];
		$detect = detect_download($con,$id);
		$date_ = date('Y-m-d');
		$app_date = $row['app_date'];
	?>

		<ul class="list-group list-group-lg no-radius no-border no-bg m-t-n-xxs m-b-none auto" >
            <li class="list-group-item text-white">
               <div class="pull-right "> 
               		<?php 
               			if($detect == 1){ ?>

               			<a class="btn btn-dark rounded btn-oji" data-toggle="tooltip" title="Already Added to Download list"><i class="icon-check icon-lg"></i> Added
                        </a>

               		<?php }else{ ?>

               			<a class="btn btn-info rounded btn-oji" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><i class="icon-plus icon-lg"></i> Add
                        </a>

               		<?php } ?>                       
					<br>          	
                    <div class="clear text-ellipsis text-muted" style="font-size: 13px;">
                     <?php //echo  date('M d, Y H:i A', strtotime($row['approved_date'])); ?>
                    </div>
				</div>        
                  	<a onclick="music_info('<?php echo $row['audio_id']; ?>');" class=" m-r-sm pull-left"> 
                    	<img src="../img/musicicon2.png" width="40" height="40">
                   	</a> 
					<div class="clear text-ellipsis"> 
						<span><?php echo ucfirst($row['song_name']); ?>
						&nbsp; <?php if($date_ == $app_date){ ?>
							<span class="badge badge-sm text-success">New!</span>
						<?php }else{ ?>

						<?php } ?>
						</span>
						<div class="clear text-ellipsis text-muted" style="font-size: 13px;">
							<span class="icon-cloud-download"></span>&nbsp;<?php echo ucfirst($row['downloads']); ?>
						</div>
					</div>
			</li>
		</ul>
	<?php
	}
			?>
			<?php
		}
		else
		{
			?>
			<div class="alert alert-default alert-dismissible text-center" >
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Music Found!</strong>
			</div>
	<?php
		}	
	}
}

//show upload details
function music_details($con,$id){
	$sql = mysqli_query($con,"SELECT t1.*,t3.*, t4.`genre_name`,t2.`album_name`, t2.`album_artwork`,count(t5.`file_name`) as files,DATE_FORMAT(t1.`approved_date`,'%Y-%m-%d') as app_date FROM tbl_audios t1 
		LEFT JOIN tbl_album t2 on t1.album_id = t2.album_id 
		LEFT JOIN tbl_user_profile t3 on t2.user_id = t3.user_id
		LEFT JOIN tbl_genre t4 on t1.genre = t4.gen_id 
		LEFT JOIN tbl_supporting_file t5 on t1.file_id = t5.group_id 
		WHERE t1.`audio_id` = '$id'");
	$row = mysqli_fetch_assoc($sql);
	$description = $row['description'];
	$song_name = $row['song_name'];
	$composer = $row['composer'];
	$artist = $row['artist'];
	$genre = $row['genre'];
	$application_id = $row['application_id'];
	$files = $row['files'];
	$group_id = $row['file_id'];
	$album_name = $row['album_name'];
	$app_date = $row['app_date'];
	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));

	if($row['picture'] == ''){
			$img = '../img/contributor.png';
			}else{
				$img = '../'.$row['picture'];
			}

			$tooltip ="<p><img src='".$img."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
			$toll_title="<center>".'Contributor'."</center>";

			if($row['album_artwork'] == ''){
			$album_image = '../img/logo2.png';
			}else{
				$album_image = '../'.$row['album_artwork'];
			}

			$tooltip_album ="<p><img src='".$album_image."' class='img_music2 img-thumbnail'></p><p class='text-capitalize text-center'>".$row['album_name']."</p>";
			$album_title="<center>".'ALBUM'."</center>";
	?>	

		<div class="modal-header music-dark mtxt-light">
		Music Information: <?php echo $song_name; ?> 
		<?php if($detect_month != 1){ ?>
			<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
		<?php }else{ ?>

		<?php } ?>
		<button class="close" data-dismiss="modal">&times;</button></div>
		<style type="text/css">
			.padding-0{
				padding: 0px;
			}
		</style>
		<div class="modal-body"> 
				<div class="row">
					<div class="col-lg-12">
						<div class="coupon">
						  <div class="container" style="line-height: 25px;"><br>
						  	<div class="col-lg-12 table-responsive">
       	 						<table class="table table-sm table-hover">
       	 							<tr>
       	 								<td><span class="text-default">Album: </span></td>
       	 								<td>
       	 									<span class="text-default" data-toggle="popover" data-html="true" data-title="<?php echo $album_title ?>" data-trigger="hover" data-content="<?php echo $tooltip_album ?>">
						    					<?php echo $row['album_name']; ?>
						    				</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Description:</span> </td>
       	 								<td><span class="text-default"><b><?php echo $description; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Contributor:</span> </td>
       	 								<td><span class="text-default" ><b data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>"><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Composer:</span></td>
       	 								<td><span class="text-default" ><b><?php echo $composer; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Genres:</span></td>
       	 								<td><span><strong style="font-weight: bolder;"><?php echo $genre; ?></strong></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Application:</span> </td>
       	 								<td><span class="text-default" ><b><?php echo $application_id; ?></b></span></td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Date Uploaded:</span></td>
       	 								<td><span class="text-default" ><b><?php echo date('M d, Y', strtotime($row['upload_date'])); ?></b>
						    			</span>
						    			</td>
       	 							</tr>
       	 							<tr>
       	 								<td><span class="text-default">Supporting Files:</span> </td>
       	 								<td>
       	 									<span class="text-default" >
										    	<b>
											    	<?php
											    		if($files < 1){ ?>
											    			None
											    	<?php } elseif($files = 1){ ?>
											    		<?php echo $files ?> file
											    	<?php } else{ ?>
											    		<?php echo $files ?> files
											    	<?php } ?>
										    	</b>
										    </span>		
										</td>
       	 							</tr>
       	 							<tr>
       	 								<td colspan="2">
       	 									<div class="list-group">
												<?php 
													$query = mysqli_query($con, "SELECT file_name,file_path FROM tbl_supporting_file where group_id = '$group_id'");
													while ($rows = mysqli_fetch_assoc($query)) { ?>

														<li class="list-group-item d-flex justify-content-between align-items-center">
													    <span class="text-primary"><b><span class="fa fa-file-text-o"></span>&nbsp;<?php echo $rows['file_name'];  ?></b></span> 
													    <span class="text-primary">
													    	<a href="<?php echo $rows['file_path'] ?>" title="Download <?php echo $rows['file_name'] ?>" download >
															<span class="fa fa-download"></span> 
															</a>
													    </span>
													  </li>										
													<?php }	?>
											</div>
       	 								</td>

       	 							</tr>
       	 						</table>
       	 					</div>
						  </div>
						</div>
					</div>
				</div>
		</div>   
		<script type="text/javascript">
				enable_tooltips();	
				</script>
<?php
}

function subscriptions($con,$search)
{
	$myid = $_SESSION['guest'];	
	if ($search == "" || $search == null) {
	//$sql = mysqli_query($con,"SELECT * FROM tbl_user_profile where user_type = 2 order by ln ASC");
		$sql = mysqli_query($con,"SELECT t2.*, t1.`id` as subs_id, t1.`contri_id`,t1.`get_notif` FROM tbl_subscribers t1 
			LEFT JOIN tbl_user_profile t2 on t1.`contri_id` = t2.`user_id`
			where t1.`user_id` = '$myid' GROUP by t1.`contri_id`");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	$counter = 0;
	while ($row = mysqli_fetch_assoc($sql)) {
    $get_notif = $row['get_notif'];
    $contri_id = $row['contri_id'];

	if($row['picture'] == ''){
	$img = '../img/contributor.png';
	}else{
		$img = '../'.$row['picture'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	$toll_title="<center>".'Contributor'."</center>";

	?>	

		<tr class="text-black bg-white">
			<td><a href="#" class=" m-r-sm pull-left">
                	<img src="../img/img.png" style="background-image: url(<?php echo $img; ?>); border-radius: 5px;" onclick="subs_info('<?php echo $row['contri_id']; ?>');" width="40" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>" class="img-fluid img-album w3-round">
               	</a> </td>
			<td><span><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></span></td>
			<td>
				<div class="text-center"> 
	                   	 <?php 
	                   	 	if($get_notif == 0){ ?>
							<button class="btn btn-warning rounded btn-oji" onclick="get_notified('<?php echo $row['subs_id'] ?>')"><span class="fa fa-exclamation"></span> Not Notify</button>
	                   	 <?php
	                   	 	}else{ ?>
	                   	 	<button class="btn btn-info rounded btn-oji" onclick="not_notify('<?php echo $row['subs_id'] ?>')"><span class="fa fa-bell"></span> Notified</button>
	                   	 <?php
	                   	 	}
	                   	 ?>
	                   	 <button class="btn btn-danger rounded btn-oji" onclick="unsubscribe('<?php echo $row['subs_id'] ?>');"><span class="icon-user-unfollow"></span> Unsubscribe
	                   	 </button>               	
				</div></td>
			</tr>
	<?php
	}
	?>

	<script type="text/javascript">
				enable_tooltips();	
				</script>

	<?php
	}
		else
		{
			?>
			<tr colspan="3">
				<td >
					<div class="alert alert-default alert-dismissible text-center" >
			  			<strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Contributor Found!</strong>
					</div>
				</td>
			</tr>
			
	<?php
		}
	}
	else
	{	

		$sql = mysqli_query($con,"SELECT t2.*, t1.`id` as subs_id, t1.`contri_id`,t1.`get_notif` FROM tbl_subscribers t1 
			LEFT JOIN tbl_user_profile t2 on t1.`contri_id` = t2.`user_id`
		    WHERE t1.`user_id` = '$myid' and concat(ln,fn) like '%$search%' GROUP BY contri_id order by t2.`ln` ASC");
		if (mysqli_num_rows($sql)>0) {
			?>
			<?php
			while ($row = mysqli_fetch_assoc($sql)) {
				$get_notif = $row['get_notif'];
				if($row['picture'] == ''){
					$img = '../img/contributor.png';
					}else{
						$img = '../'.$row['picture'];
					}

				$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
				$toll_title="<center>".'Contributor'."</center>";
				?>
				<tr class="text-black bg-white">
			<td><a href="#" class=" m-r-sm pull-left">
                	<img src="../img/img.png" style="background-image: url(<?php echo $img; ?>); border-radius: 5px;" onclick="subs_info('<?php echo $row['contri_id']; ?>');" width="40" data-toggle="popover" data-html="true" data-title="<?php echo $toll_title ?>" data-trigger="hover" data-content="<?php echo $tooltip ?>" class="img-fluid img-album w3-round">
               	</a> </td>
			<td><span><?php echo strtoupper($row['fn'].' '.$row['ln']); ?></span></td>
			<td>
				<div class="text-center"> 
	                   	 <?php 
	                   	 	if($get_notif == 0){ ?>
							<button class="btn btn-warning rounded btn-oji" onclick="get_notified('<?php echo $row['subs_id'] ?>')"><span class="fa fa-exclamation"></span> Not Notify</button>
	                   	 <?php
	                   	 	}else{ ?>
	                   	 	<button class="btn btn-info rounded btn-oji" onclick="not_notify('<?php echo $row['subs_id'] ?>')"><span class="fa fa-bell"></span> Notified</button>
	                   	 <?php
	                   	 	}
	                   	 ?>
	                   	 <button class="btn btn-danger rounded btn-oji" onclick="unsubscribe('<?php echo $row['subs_id'] ?>');"><span class="icon-user-unfollow"></span> Unsubscribe
	                   	 </button>               	
				</div></td>
			</tr>
				<?php
			}
			?>
			<script type="text/javascript">
				enable_tooltips();	
				</script>
			<?php
		}
		else
		{
			?>
			<div class="alert alert-default alert-dismissible text-center" >
			  <strong class="text-default"><span class="fa fa-music fa-5x"></span> <br> No Subscriptions Found!</strong>
			</div>
	<?php
		}
	}
	
}

//update  account name
function update_name($con,$fn,$ln){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set fn='$fn',ln='$ln' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_email($con,$email){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set email_add='$email' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_pwd($con,$pwd){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set password='$pwd' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function update_pf($con,$path){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con,"UPDATE tbl_user_profile set picture='$path' where user_id='$myid'");
	if ($sql) {
		echo 1;
	}
}

function time_ago($timestamp)  
 {  
      $time_ago = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $current_time - $time_ago;  
      $seconds = $time_difference;  
      $minutes = round($seconds / 60 );           // value 60 is seconds  
      $hours   = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days    = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks   = round($seconds / 604800);          // 7*24*60*60;  
      $months  = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years   = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
      if($seconds <= 60)  
      {  
     return "Just Now";  
      }  
      else if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return "1 minute ago";  
     }  
     else  
           {  
       return "$minutes minutes ago";  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return "1 hour ago";  
     }  
           else  
           {  
       return "$hours hrs ago";  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return "yesterday";  
     }  
           else  
           {  
       return "$days days ago";  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return "a week ago";  
     }  
           else  
           {  
       return "$weeks weeks ago";  
     }  
   }  
       else if($months <=12)  
      {  
     if($months==1)  
           {  
       return "a month ago";  
     }  
           else  
           {  
       return "$months months ago";  
     }  
   }  
      else  
      {  
     if($years==1)  
           {  
       return "1 year ago";  
     }  
           else  
           {  
       return "$years years ago";  
     }  
   }  
 }


function save_recent_play($con, $id){
	$myid = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "INSERT INTO tbl_recently_played (user_id,audio_id,date_play)values('$myid','$id', '$date_')");

	if($sql){
		echo 1;
	}
}

function show_recently_played($con){
	$myid = $_SESSION['guest'];
	$sql = mysqli_query($con, "SELECT t3.`album_artwork`,t2.`song_name`,t3.`album_name`,t2.`duration` FROM tbl_recently_played t1 
		RIGHT JOIN tbl_audios t2 ON t1.`audio_id` = t2.`audio_id` 
		RIGHT JOIN tbl_album t3 ON t2.`album_id` = t3.`album_id`
		WHERE t1.`user_id` = '$myid' GROUP BY t2.`audio_id` ORDER BY t1.`date_play` DESC LIMIT 3 ");

		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) { 

				if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
				    	$imgs = '../img/slogo3.png';
					}
					else
					{
						$imgs = '../'.$row['album_artwork'];
					}

				?>
				<tr class="tracks">
					<td>
						<img class="img_played img-fluid" src="<?php echo $imgs; ?>" alt="">
					</td>
					<td><a href="#"><img src="../img/icons/play-big.png"></a></td>
					<td>
						 <a href="#"><h6><?php echo $row['song_name'] ?></h6></a>
						 <a href="#"><p><?php echo $row['album_name'] ?></p></a>
					</td>
					<td><a href="#"><img src="../img/icons/add-big.png"></a></td>
					<td><?php echo date('H:i', strtotime($row['duration'])); ?></td>
				</tr>
			<?php
			}
		}else{ ?>
			<td colspan="4">Recently Play Music Here...</td>
		<?php
		}

}

function recent_playlists($con,$search)
{
	$myid = $_SESSION['guest'];
	if ($search == "" || $search == null) {

	$sql = mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') AS app_date,t4.`fn`,t4.`ln`,t4.`picture` FROM tbl_album t1 
		RIGHT JOIN tbl_audios t2 ON t1.album_id=t2.album_id 
		RIGHT JOIN tbl_album t3 ON t2.album_id=t3.album_id 
		RIGHT JOIN tbl_user_profile t4 ON t3.user_id=t4.user_id 
		RIGHT JOIN tbl_recently_played t5 ON t2.`audio_id` = t5.`audio_id`
		WHERE t5.`user_id` = '$myid' GROUP BY t2.`audio_id` ORDER BY t5.`date_play` DESC");

	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$id = $row['audio_id'];
	echo $app_date = $row['app_date'];

	$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
	$detect = detect_download($con,$id);
	$group_id = $row['file_id'];

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

	if ($row['picture'] == "") {
		$img = '../img/contributor.png';
	}
	else
	{
		$img = '../'.$row['picture'];
	}

	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

	$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
	$toll_title="<center>".'Contributor'."</center>";
	?>

		<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');" >
			<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
					    <?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
						<?php echo ucfirst($row['album_name']); ?>
					</h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td class="text-left style="">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></h6>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
		</tr>
	<?php
	}
	?>
	<script type="text/javascript">
		enable_tooltips();
	</script>
	<?php
	}
		else
		{ ?>

			<td colspan="10" class="text-center">
			 	<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
			</td>
			
	<?php	
		}
	}
	else
	{

		$sql =mysqli_query($con,"SELECT t1.*,t2.*, DATE_FORMAT(t2.`approved_date`,'%Y-%m-%d') as app_date,t4.`fn`,t4.`ln`,t4.`picture` from tbl_album t1 
			right join tbl_audios t2 on t1.album_id=t2.album_id 
			right join tbl_album t3 on t2.album_id=t3.album_id 
			right join tbl_user_profile t4 on t3.user_id=t4.user_id
			RIGHT JOIN tbl_recently_played t5 ON t2.`audio_id` = t5.`audio_id` 
			where t5.`user_id` = '$myid' and concat(song_name,genre,fn,ln,composer,description,application_id,keyword) like '%$search%' GROUP BY t2.`audio_id` ORDER BY t5.`date_play` DESC");

		if (mysqli_num_rows($sql)>0) {
			?>
				
			<?php
				while ($row = mysqli_fetch_assoc($sql)) {
					$id = $row['audio_id'];
				    $date_ = date('Y-m-d');
					$app_date = $row['app_date'];
					$detect_month = (strtotime($app_date) < strtotime('1 month ago'));
					$group_id = $row['file_id'];
					$detect = detect_download($con,$id);
					$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
					$rows = mysqli_fetch_assoc($query);
					$total_files = $rows['files'];
					if ($row['picture'] == "") {
						$img = '../img/contributor.png';
					}
					else
					{
						$img = '../'.$row['picture'];
					}

					if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
    	$imgs = '../img/slogo3.png';
	}
	else
	{
		$imgs = '../'.$row['album_artwork'];
	}

					$tooltip ="<p><img src='../img/img.png' class='img-album w3-round img-thumbnail'  width='150' style='background-image: url(".$img.");'></p><p class='text-capitalize text-center'>".$row['fn'].' '.$row['ln']."</p>";
					$toll_title="<center>".'Contributor'."</center>";

					?>
			<tr class="tracks item" ondblclick="music_info('<?php echo $row['audio_id']; ?>');" >
			<td></td>
			<td class="play" style="width:10%;"><img class="imgs_album img-fluid" src="<?php echo $imgs; ?>" alt=""></td>
			<td class="text-left" style=" width: 15%;">
					<h6 class="default-fs default_margin" style="vertical-align:center; line-height: 20px;">
					<b><?php echo ucfirst($row['song_name']); ?>
						<?php if($detect_month != 1){ ?>
							<span class="badge badge-sm text-success" data-toggle="tooltip" title="Recently Added.">New!</span>
						<?php }else{ ?>

						<?php } ?></b><br>
						<?php echo ucfirst($row['album_name']); ?>
					</h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description']); ?></h6>
			</td>
			<td class="text-left" style=" width: 17%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre']); ?></h6>
			</td>
			<td class="text-left" style=" width: 12%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['composer']); ?></h6>
			</td>
			<td class="text-left" style=" width: 15%; vertical-align:center;">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['application_id']); ?></h6>
			</td>
			<td class="text-left" style=" width: 2%; vertical-align:center;">
					<h6 class="db-content default-fs default_margin">
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							None
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>
					</h6>
			</td>
			<td class="text-left style="">
				<h6 class="db-content default-fs default_margin"><?php echo ucfirst($row['fn'].' ' .$row['ln']); ?></h6>
			</td>
			<td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play()" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
		</tr>
					<?php
					}
				?>
				<script type="text/javascript">
					enable_tooltips();
				</script>
			<?php
		}
		else
		{
			?>
			<td colspan="10" class="text-center">
			   <div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
			</td>
	<?php
		}
	}
}

//music player
function album_musics($con,$search,$album_id)
{
	if ($search == "" || $search == null) {
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t5.`application_name`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	LEFT JOIN tbl_application t5 on t1.`application_id` = t5.`application_id` 
	WHERE t1.`album_id` = '$album_id' and t1.`active` = 1
	ORDER BY song_name ASC");
	if (mysqli_num_rows($sql)>0) {
	?>
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
		//$id = $row["t_id"];
		$a_id = $row["audio_id"];
		$group_id = $row['file_id'];
		$detect = detect_download($con,$a_id);

		$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
		$rows = mysqli_fetch_assoc($query);
		$total_files = $rows['files'];

	  	if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}
    	 $date_upload =  $row['upload_date'];
    	 //am - Album Musics
	?>	
			<tr class="track item">                
				<td class="text-left">
                   	<h6 class="db-content default-fs" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs"><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
            <td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download_album('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>'); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center" >
			<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
	}
	else
	{
	$sql =mysqli_query($con,"SELECT t1.*,t2.`user_id`,t2.`album_artwork`,t2.`album_name`,t3.fn,t3.ln  from tbl_audios t1 
	LEFT JOIN tbl_album t2 on t1.`album_id` = t2.`album_id`
	LEFT JOIN tbl_user_profile t3 on t2.`user_id` = t3.`user_id`
	LEFT JOIN tbl_audience t4 on t1.`audience_id` = t4.`audience_id`
	WHERE t1.`album_id` = '$album_id' and t1.`active` = 1 
	AND concat(song_name,composer,upload_date,genre,application_id,description,keyword) 
	LIKE '%$search%'
	ORDER BY song_name ASC ");
	if (mysqli_num_rows($sql)>0) {
	?>	
	<?php
	while ($row = mysqli_fetch_assoc($sql)) {
	$active = $row['active'];
	$a_id = $row["audio_id"];
	$group_id = $row['file_id'];
	$detect = detect_download($con,$a_id);

	$query = mysqli_query($con, "SELECT COUNT(file_id) as files FROM tbl_supporting_file where group_id = '$group_id' Group by group_id");
	$rows = mysqli_fetch_assoc($query);
	$total_files = $rows['files'];

		if ($row['album_artwork'] == "" || $row['album_artwork'] == null ) {
        	$imgs = '../img/slogo3.png';
    	}
    	else
    	{
    		$imgs = '../'.$row['album_artwork'];
    	}

	?>
			<tr class="track item">
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" style="line-height: 20px;"><b><?php echo ucfirst($row['song_name'])?></b>
                   		<br>
						<?php echo ucfirst($row['album_name']); ?>
                   	</h6>
                </td>
                <td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['description'])?></h6>
                </td>
                
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['genre'])?></h6>
           		</td>
           		<td class="text-left">
                   	<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['composer'])?></h6>
               	</td>
               	<td class="text-left">
               		<h6 class="db-content default-fs default_margin" ><?php echo ucfirst($row['application_id'])?></h6>
           		</td>
           		<td class="text-left">
           			<h6 class="db-content default-fs default_margin" >
						<?php 
						if($total_files == 1){ ?>
							<?php echo $total_files; ?>&nbsp; File

						<?php }
						else if($total_files == 0){ ?>
							NONE
						<?php }
						else{ ?>
							<?php echo $total_files; ?>&nbsp; Files
						<?php
						}
						?>	
					</h6>			
			</td>
            <td style=" width: 10%;">
				<div class="btn-group btn-justified text-center" id="btns"> 
					<a href="../<?php echo $row['music'] ?>"  class="btn btn-dark btn-small" name="download_" id="download_" data-toggle="tooltip" title="Download <?php echo $row['song_name'] ?>" download onclick="download_music('<?php echo $row['audio_id'] ?>');">
					<i class="fa fa-download fa-lg text-white"></i>
					</a> 
                   	 <?php 
               			if($detect == 1){ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Already Added to Download list"><span class="fa fa-check fa-lg"></span></button>

               		<?php }else{ ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="Add <?php echo $row['song_name'] ?> to Download List " onclick="add_download_album('<?php echo $row['audio_id'] ?>');"><span class="fa fa-plus fa-lg"></span></button>
               		<?php } ?>

               			<button class="btn btn-dark btn-small" data-toggle="tooltip" title="View Details" onclick="music_info('<?php echo $row['audio_id']; ?>');"><span class="fa fa-eye fa-lg"></span></button> 
               			<button class="btn btn-dark btn-small" data-toggle="tooltip" id="btn-play" onclick="choose_play('<?php echo $imgs ?>','<?php echo '../'.$row['music'] ?>','<?php echo ucfirst($row['song_name']) ?>'); on_firs_play(); only_play(); save_recent_play('<?php echo $row['audio_id'] ?>'); save_play('<?php echo $row['audio_id'] ?>');" title="Play""><span class="fa fa-play-circle fa-lg"></span></button>            	
				</div>
			</td>
            </tr>
	<?php
	}
	?>
	<?php
	}
	else
	{
	?>
		<td colspan="7" class="text-center">
				<div class="alert alert-default alert-dismissible text-center" >
					  <strong class="text-default"><span class="fa fa-play fa-3x"></span> <br> No Music Found!</strong>
				</div>
		</td>
	<?php
	}
  }
}

function save_play($con, $audio_id){
	$user = $_SESSION['guest'];
	$date_ = date('Y-m-d H:i:s');
	$sql = mysqli_query($con, "INSERT INTO tbl_played_songs(audio_id,user_id,date_play,location) VALUES('$audio_id','$user','$date_',null)");
	if($sql){
		echo 1;
	}
}
?>