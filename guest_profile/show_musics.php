<?php 
include('../dbconnection.php');
include('classes.php'); ?>
<script type="text/javascript">
songs_list();
</script>
      <div id="myplaylist" class="animated fadeInLeft">
        <div class="col-sm-12" style="background-color: rgb(91,66,99,0.2);  margin-bottom: 10px; margin-top: 10px; border-radius: 10px;">
          <h2 class="ml3 font-thin m-b" style="font-family: MavenPro-Medium;">
            <div class="col-sm-5" style=" ">
              <i class="icon-music-tone-alt"></i> Search for Music
            <span class="musicbar animate inline m-l-sm" style="width:20px;height:20px"> 
              <span class="bar1 a1 bg-success lter"></span> 
              <span class="bar2 a2 bg-success lt"></span> 
              <span class="bar3 a3 bg-success"></span> 
              <span class="bar4 a4 bg-success dk"></span> 
              <span class="bar5 a5 bg-success dker"></span> 
            </span>
            </div>
            
          <div class="col-sm-4 pull-right" style="">
            <div class="form-group">
              <div class="input-group">
                <a class="btn btn-sm btn-default input-group-addon bg-default text-white btn-oji rounded" onclick="songs_list();" ><i class="icon-magnifier"></i>
                </a>
                <input type="text" oninput="songs_list();" name="search3" id="search3" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: MavenPro-Medium;">
              </div>
            </div>
          </div>
          <div class="col-sm-3 pull-right" style=" ">
               <div class="btn-group pull-right">
                 <select id="title_name" onchange="songs_list();" oninput="songs_list();" class="btn btn-default rounded">
                  <option value="song_name" selected="">Title</option>
                  <option value="description">Description</option>
                  <option value="genre">Genre</option>
                  <option value="composer">Composer</option>
                  <option value="application_id">Application</option>
                  <option value="fn">Contributor</option>
                </select>   
                <select id="order" onchange="songs_list();" oninput="songs_list();" class="btn btn-default rounded">
                  <option value="ASC" selected="">ASC</option>
                  <option value="DESC">DESC</option>
                </select>
               </div>
          </div>               
          </h2>
        </div> 
      </div>
    <footer  class="footer bg-black animated fadeInRight"  id="foot" style="height: 70px; padding-top: 9px; display: none;"> 
      <div id="myAudio2" class="col-sm-12">
           
      </div>
    </footer><br>
  <div id="content">
      <div class="vbox">
        <div class="w-f-md">
          <div class="hbox stretch dker">
            <aside class="col-sm-5 no-padder" id="sidebar">
              <div class="vbox animated fadeInUp">
              
                <div class="table-responsive scroll"  style="height: 400px; background-color: #f1f1f1;">
                  <table class="table table-responsive-sm table-hover table-striped">
                     <thead class="text-white thbgcolor">
                        <tr>
                            <th class="thheader">Title</th>
                            <th class="thheader">Description</th>
                            <th class="thheader">Genre</th>
                            <th class="thheader">Composer</th>
                            <th class="thheader">Application</th>
                            <th class="thheader">Supporting Files</th>
                            <th class="thheader">Contributor</th>
                            <th class="thheader text-center">Action</th>
                        </tr>
                     </thead>
                     <tbody id="tbl_songs"></tbody>
                  </table>
                </div>
           
              </div>
            </aside>
          </div>
        </div>
      </div>
    </div>
 
