<?php
session_start();
if (!isset($_SESSION['guest'])) {
  @header('location:../');
}
include_once("../dbconnection.php");
//get genre
function get_genre($con){
    $sql = mysqli_query($con,"SELECT * from tbl_genre WHERE genre_name != 'All' order by genre_name ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['genre_name'].'">'.$row['genre_name'].'</option>';
        }
    }
}

function get_application($con){
    $sql = mysqli_query($con,"SELECT * from tbl_application");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['application_name'].'">'.$row['application_name'].'</option>';
        }
    }
}

 //echo $_POST['playlist_id']; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="guest.css">
<link rel="stylesheet" type="text/css" href="music_player_playlist/audio.css">
  <script src="music_player/audio-index.js"></script>

  <link href="https://www.w3schools.com/w3css/4/w3.css" rel="stylesheet" />
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="../js/guest.js"></script>
  <script src="https://www.w3schools.com/lib/w3.js"></script>
  <script src="music_player_playlist/audio-index.js"></script>
  <link href="../css/w3.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">

  <style type="text/css">
    .imgs {
      width: 50px; /* You can set the dimensions to whatever you want */
      height: 50px;
      object-fit: cover;
      }
      .user-mng h3{
  		font-weight: 500;
  		padding-left: 100px;
  		font-size: 16px;
  	  }

      .modal-add-music{
        max-width: 1000px;
        width: 100%;
      }
          .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}

.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #304756;
   text-align: center;
   color: #f2f2f2;
}
</style>
<script type="text/javascript">
    function select_all(){
    var buttons = document.getElementsByName('add_song');
    for(var i = 0; i <= buttons.length; i++)  
       buttons[i].click();
    }
</script>
</head>
<body onload="play_nows();  p_music_list(); total_songs();">
<input type="hidden" name="v_pid" id="v_pid" value="<?php echo $_POST['playlist_id']; ?>">
<input type="hidden" name="play_nows" id="play_nows" value="<?php echo $_POST['play_nows']; ?>">
<div class="sidenav">
	<ul>
     <li class="w3-hover-shadow">
      <a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
      </li>
    <li class="w3-hover-shadow">
      <a href="show_playlist.php"><span class="fa fa-list-ul fa-3x" style="color: #8eadab;"></span></a>
      </li>
       <li class="w3-hover-shadow">
      <a href="download_list.php"><span class="fa fa-download fa-3x" style="color: #3d4c59;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
    </li>
  </ul>
</div>

<div class="main">

	<div class="container-fluid hero hero-db hero-admin">
		<div class="row header">
			<div class="col-lg-3 col-2 head">
				<a href="#"><img class="img-fluid" src="../img/logo.png"/></a>
			</div>
			<div class="col-lg-9 head">
        <div class="dropdown pull-right">
          <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
          <div class="dropdown-content">
            <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
            <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
          </div>
        </div>
      </div>
		</div>  	  	
	</div>

	<div class="container-fluid user-mng">
		<div class="row">
      <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-5">
              <p class="title" style="text-transform: uppercase;"><?php echo $_POST['playlist_name']; ?> 
              <span class="badge default-fs" id="total_songs"></span> 
            </p>
            </div>
          <div class="col-lg-3">
            <div class="input-group">
                <input type="show" name="search_song" id="search_song" oninput="p_music_list();" placeholder="Search Artist, Music, Genre...">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
              </div>
          </div>
          <div class="col-lg-2"> 
            <button type="button" class="btn btn-dark btn-small" data-toggle="modal" data-target="#p_addmusic" onclick="load_audio2();"><span class="fa fa-plus fa-lg"></span> Add Song
            </button>
          </div>
          <div class="col-lg-2">
            <button class="btn btn-dark btn-small" onclick="clear_songs();"><span class="fa fa-trash fa-lg"></span> Empty Playlist</button> 
          </div>
        </div>
      </div>
		</div>
	</div>

	<div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
		<div class="row">
			<div class="col-lg-12 table-responsive">
				<table class="table table-striped table-hover" id="music-playlist">
          <tr><td colspan="10" class="text-left"><span>Click on the column title to sort</span></td></tr>
					<tr id="labels" style="">
						<td style=""></td>
						<td style=""></td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(3)')" style="cursor:pointer">Title/Album</td>
            <td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(4)')" style="cursor:pointer">Description</td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(5)')" style="cursor:pointer">Genre</td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(6)')" style="cursor:pointer">Composer</td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(7)')" style="cursor:pointer">Application</td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(8)')" style="cursor:pointer; width: 15%;">Supporting Files</td>
						<td class="text-left" onclick="w3.sortHTML('#music-playlist', '.item', 'td:nth-child(9)')" style="cursor:pointer; width: 15%;">Contributor</td>
						<td class="text-left" style="">Action</td>
						<td style=""></td>
					</tr>
					<tbody id="display_musics" style="">
						
					</tbody>
					</table>
			</div>
		</div>
	</div>
  <hr>
	<footer class="container-fluid">
		<div class="row copyright">
			<div class="col-lg-12">
				<p class="nav-item"><span><a href="#">HOME</a></span><span class="fence">|</span><span><a href="#">ABOUT US</a></span><span class="fence">|</span><span><a href="#">CONTACT</a></span></p>
				<p ><span><a href="#">FAQ</a></span><span class="fence">|</span><span><a href="#">TERMS OF USE</a></span><span class="fence">|</span><span><a href="#">PRIVACY POLICY</a></span></p>
				<p ><span>© 2018, <a href="#">LAMPSTAND STUDIO</a></span><span class="fence">|</span><span>ALL RIGHTS RESERVED.</span></p>
			</div>
		</div>
	</footer>
</div>
<!-- Add Musics to the playlists--> 
<div class="modal fade" role="dialog" id="p_addmusic" data-keyboard="false" >
  <div class="modal-dialog modal-add-music">
    <div class="modal-content">
      <div class="modal-header music-dark mtxt-light">Add Music <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <select class="btn btn-dark form-control btn-small" id="genres2" onchange="load_audio2();" oninput="load_audio2();">
                  <option selected="" value="null">All</option>
                  <?php 
                    echo get_genre($con);
                   ?>
                </select>
              </div>
            </div>
            <div class="col-lg-6">
              <input type="show" oninput="load_audio2();" name="search_audio" id="search_audio" class="form-control" placeholder="Search..." autocomplete="off">
            </div>
            </div>
          </div>
            <div class="row">
              <div class="col-lg-12">
                  <table class="table table-responsive table-hover" id="sampleTbl">
                      <thead> 
                        <tr class="label text-left">
                          <th>Title</th>
                          <th>Composer</th>
                          <th>Genre</th>
                          <th>Description</th>
                          <th>Date</th>
                          <th>Action</th>
                          <th style="display: none;">Audio ID</th>
                        </tr>
                      </thead>
                      <tbody id="display_audio2">
                        
                      </tbody>
                    </table>
              </div>
            </div>

        </div>
        <div class="modal-footer text-center">
          <div class="container">
            <button class="btn btn-dark btn-small" data-dismiss="modal">Done</button>
            <button class="btn btn-dark btn-small" name="submitsong" id="submitsong" onclick="select_all();"><i class="fa-check-square-o"></i> Pick All</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Add Musics to the playlists--> 

<div class="footer none">
  <div class="row" style="padding-left: 110px; padding-right: 20px; padding-bottom: 10px;">
        <div class="col-sm-12">
          <div class="col-sm-12">
            <h4 style="margin-bottom: 0px;">
            <marquee id="music_title" class="col-sm-12 default-fs">LAMPSTAND STUDIO</marquee>
            </h4>
          </div>
          <input type="range" min="0"  max="100" class="player_slider" onmousemove="move_slider_progress($(this).val(),$(this).attr('min'),$(this).attr('max'));" onmouseup="move_slider($(this).val(),$(this).attr('min'),$(this).attr('max'));" id="pl" name="" value="0">

          <div class="col-sm-12 row">
            <div class="col-sm-2">
                <div class="text-white"><span id="counter_strike">00:00</span>/<span id="timer_audio">00:00</span></div>
            </div>
            <div class="col-sm-8 text-center">
                <a href="#prev" onclick="prev_aud();" class="btn-musics "><i class="fa fa-backward fa-lg text-white"></i></a>
                <a href="#play" class="btn-musics"><i onclick="play_music($(this).attr('id'));" id="plays" class="fa fa-play-circle fa-lg text-white"></i></a>
                <a href="#prev" onclick="next_aud();" class="btn-musics "><i class="fa fa-forward fa-lg text-white"></i></a>
            </div>
            <div class="col-sm-2 text-white">
                  <a href="#vol"><i onclick="toggle_mute();" id="volume_control" class="fa fa-volume-up fa-lg text-white"></i></a>
                  <input type="range" id="vol_controler" step="10" name="volume" style="width:80%;" value="100" class="player_slider" oninput="volup(this.value);">
            </div>
          </div>
        </div>

      <div id="music_audio" style="width: 100%;">
      <audio hidden="" onplay="start_player(); setInterval(function(){interval_playing(this.duration)},500)" oncanplay="get_duration(this.duration); get_minutes(this.duration);" controls id="my_player" style="width: 100%;" onended="next_aud2();" onplay="on_firs_play();" onpause="pause_media();" controlsList="nodownload" >
      Sorry, your browser does not support audio
      </audio>
      <div id="show_progress"></div>
      </div>
  </div>
</div>


<?php include('mymodals.php') ?>

</body>
</html>