<?php  
include_once("session.php");
include('modal.php');
$user_id = $_SESSION['guest'];
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" /> 
 <title><?php echo ucwords($_SESSION['fn'].' '.$_SESSION['ln']) ?></title> 
 <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="icon/png" href="../img/musicicon.png">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<!-- <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../css/w3.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../css/animate.css">
<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="../css/jl.css">
<link rel="stylesheet" type="text/css" href="../css/hover-min.css">
<link rel="stylesheet" type="text/css" href="../css/hover.css">


<script src="../vendor/jquery/jquery.min.js"></script>

<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/guest.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script type="text/javascript" src="audio-index.js"></script>
<script type="text/javascript" src="geo_loc.js"></script>

<style type="text/css">
 @font-face{
  font-family: MavenPro-Medium;
  src: url('../fonts/MavenPro-Medium.ttf');
  }

 @font-face{
        font-family: Stardust;
        src: url('../fonts/Stardust.ttf');
      }


  @font-face{
        font-family: ananda;
        src: url('../fonts/ananda.ttf');
      }
      .back{
    background-image: url('../img/main-bg.png');
    background-repeat: no-repeat;
    -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
      -o-background-size: cover;
    -webkit-font-smoothing:antialiased;line-height:1.40;
}

.modal-content{
  font-family: MavenPro-Medium;
}


.img_hover {
position: relative;

}

.text-block {
    position: absolute;
    bottom: 20px;
    right: 20px;
    background-color: black;
    color: white;
    padding-left: 20px;
    padding-right: 20px;
}

/* Make the image to responsive */
.image {
  width: 100%;
  height: auto;
}

/* The overlay effect (full height and width) - lays on top of the container and over the image */
.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .3s ease;
  background-color: transparent;
}

/* When you mouse over the container, fade in the overlay icon*/
.img_hover:hover .overlay {
  opacity: 1;
}

/* The icon inside the overlay is positioned in the middle vertically and horizontally */
.icon {
  color: white;
  font-size: 50px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

/* When you move the mouse over the icon, change color */
.fa-music:hover {
  color: #2c3e50;
}
 </style>
</head>
<body class="back" onload=" play_list(play_now); music_list(); play_list2(play_now2); uploading(); upload_picture(); load_pictures(); reload(); getgeoloc(); "> 

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Music Library</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Playlist <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Music</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Download List</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<br>
<div class="col-sm-12">

  <div class="col-sm-12">
    <div class="row">
      <div class="col-sm-3" ><h3>My Playlist</h3></div>

        <div class="col-sm-4 float-right" >
          <div class="form-group">
            <div class="input-group">
              <a class="btn btn-sm btn-default input-group-addon bg-default text-white btn-oji rounded" onclick="music_list();" ><i class="icon-magnifier"></i>
              </a>
              <input type="text" oninput="music_list();" name="search1" id="search1" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: MavenPro-Medium;">
            </div>
          </div> 
        </div>

        <div class="col-sm-2 float-right" >
              <a href="#" class="btn btn-icon btn-info rounded btn-oji " style="width: 70px;" title="Add Playlist" data-toggle="modal" data-target="#add_playlist"><i class="fa fa-plus" ></i>
              </a>
              <a href="#" class="btn btn-icon btn-danger rounded btn-oji " style="width: 70px;" onclick="delete_all_plist('<?php echo $user_id; ?>');" title="Empty Playlist"><i class="fa fa-times"></i>
              </a>
          </div>

          <div class="col-sm-3 pull-right" style="margin-top: 4px;">
                <div class="btn-group pull-right">
                  <select id="title_name" onchange="music_list();" oninput="music_list();" class="btn btn-default rounded">
                  <option value="name" selected="">Playlist</option>
                  <option value="date_created">Date</option>
                </select>   
                <select id="order" onchange="music_list();" oninput="music_list();" class="btn btn-default rounded">
                  <option value="ASC" selected="">ASC</option>
                  <option value="DESC">DESC</option>
                </select>
                </div>
          </div>
    </div>
    
  </div>
  <hr>
  <div id="display_playlist" class="row">
      
  </div>
 
</div>


</body>
</html>