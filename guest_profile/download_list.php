<?php 
session_start();
if (!isset($_SESSION['guest'])) {
  @header('location:../');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lampstand Studios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="guest.css">

  <link href="../css/w3.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/sweetalert.min.js"></script>
  <script src="geo_loc.js"></script>
  <script src="../js/guest.js"></script>
  <script src="https://www.w3schools.com/lib/w3.js"></script>
<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <style type="text/css">
    .imgs {
      width: 50px; /* You can set the dimensions to whatever you want */
      height: 50px;
      object-fit: cover;
      }
    .imgs_album {
      width: 60px; /* You can set the dimensions to whatever you want */
      height: 60px;
      object-fit: cover;
      }
          .dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 12px 12px;
    z-index: 1;
}

.dropdown:hover .dropdown-content {
    display: block;
}
  </style>
  <script type="text/javascript">
    function click_all(){
    var buttons = document.getElementsByName('download_');
    for(var i = 0; i <= buttons.length; i++)  
       buttons[i].click();
    }
  </script>
</head>
<body onload="show_download_list(); refresh_account(); load_pictures(); getgeoloc();">
<input type="hidden" id="current_loc2">
<div class="sidenav">
  <ul>
     <li class="w3-hover-shadow">
      <a href="index.php"><span class="fa fa-music fa-3x" style="color: #3d4c59;"></span></a>
      </li>
     <li class="w3-hover-shadow">
      <a href="show_playlist.php"><span class="fa fa-list-ul fa-3x" style="color: #3d4c59;"></span></a>
      </li>
       <li class="w3-hover-shadow">
      <a href="download_list.php"><span class="fa fa-download fa-3x" style="color: #8eadab;"></span></a>
      </li>
      <li class="w3-hover-shadow">
      <!--<a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #8eadab;"></span></a>-->
      <a href="../logout.php"><span class="fa fa-sign-out fa-3x" style="color: #3a4b58;"></span></a>
    </li>

  </ul>
</div>

<div class="main">
  <div class="container-fluid hero hero-db hero-admin">
    <div class="row header">
      <div class="col-lg-3 col-2 head">
       <a href="#" onclick="show_download_list(); refresh_account(); load_pictures();"><img class="img-fluid" src="../img/logo.png"/></a>
      </div>
      <div class="col-lg-9 head">
        <div class="dropdown pull-right">
          <p><span>Hello! <a href="account_settings.php" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['fn'].' '.$_SESSION['ln'] ?></a></span></p>
          <div class="dropdown-content">
            <button class="btn btn-small btn-dark" onclick="window.location='account_settings.php'"><span class="fa fa-user fa-lg"></span> Profile</button>
            <button class="btn btn-small btn-dark" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-lg" ></span> Logout</button>
          </div>
        </div>
      </div>
    </div>        
  </div>

<div class="container-fluid user-mng">
    <div class="row">
    </div>
    <div class="row">
      <div class="col-lg-5">
        <div class="row">
          <div class="col-lg-5">
            <p class="title">Downloads</p>
          </div>
          <div class="col-lg-7">
            <div class="input-group">
                <input type="show" oninput="show_download_list();" name="search_dl" id="search_dl" placeholder="Search Music...">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
              </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 select">
            <button class="btn btn-small btn-dark" onclick="click_all();">Download All</button>
          </div>
    </div>
  </div>
  
  <div class="container-fluid tracks-table" style="margin-top: -30px; margin-bottom: 1px;">
    <div class="row">
      <div class="col-lg-12 table-responsive">
        <table class="table table-striped table-hover" id="downloadlist">
          <tr><td colspan="10" class="text-left"><span>Click on the column title to sort</span></td></tr>
          <tr id="labels" style="">
            <td></td>
            <td></td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(1)')" style="cursor:pointer">Title/Album</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(2)')" style="cursor:pointer">Description</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(3)')" style="cursor:pointer">Genre</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(4)')" style="cursor:pointer">Composer</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(5)')" style="cursor:pointer">Application</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(6)')" style="cursor:pointer; width: 13%;">Supporting Files</td>
            <td class="text-left" onclick="w3.sortHTML('#musics', '.item', 'td:nth-child(7)')" style="cursor:pointer">Contributor</td>
            <td class="text-center" style="">Action</td>
          </tr>
          <tbody id="tbl_download_list" style="">
            
          </tbody>
          </table>
      </div>
    </div>
  </div>


  <footer class="container-fluid">
    <?php include('../footer.php'); ?>
  </footer>
</div>
</body>
</html>