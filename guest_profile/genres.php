<script type="text/javascript">
topic_list();
</script>
      <div id="myplaylist" class="animated fadeInLeft">
        <div class="col-sm-12" style="background-color: rgb(91,66,99,0.2); margin-bottom: 10px; margin-top: 10px; border-radius: 10px;">
          <h2 class="ml3 font-thin m-b"><i class="icon-playlist"></i> Music Library 
            <span class="musicbar animate inline m-l-sm" style="width:20px;height:20px"> 
              <span class="bar1 a1 bg-success lter"></span> 
              <span class="bar2 a2 bg-success lt"></span> 
              <span class="bar3 a3 bg-success"></span> 
              <span class="bar4 a4 bg-success dk"></span> 
              <span class="bar5 a5 bg-success dker"></span> 
            </span>
          <div class="col-sm-5 pull-right" style="margin-top: 5px;">
            <div class="form-group">
              <div class="input-group">
                <a class="btn btn-sm btn-default input-group-addon bg-default text-white btn-oji rounded" onclick="topic_list();" ><i class="icon-magnifier"></i>
                </a>
                <input type="text" oninput="topic_list();" name="search4" id="search4" class="form-control rounded" placeholder="Search..." autocomplete="off" style="font-family: serif; height: 30px;">
              </div>
            </div>
          </div>

          </h2>
        </div>
         <div class="">
    
                <div class="btn-group btn-group-justified"> 
                  <a href="#" class="btn btn-primary rounded btn-oji" onclick="pager('show_musics');">Title</a> 
                  <a href="#" class="btn btn-primary rounded btn-oji" onclick="pager('show_contributors');">Contributor</a> 
                  <a href="#" class="btn btn-primary rounded btn-oji active" onclick="pager('genres');">Genre</a> 
                  <a href="#" class="btn btn-primary rounded btn-oji" onclick="pager('show_composer');">Composer</a>
                  <a href="#" class="btn btn-primary rounded btn-oji" onclick="pager('show_descriptions');">Description</a>
                  <a href="#" class="btn btn-primary rounded btn-oji" onclick="pager('add_downloads');">Add to Download</a>
                </div>
         
        </div>
        <br>

        
      </div>

    <?php 
    include_once("../dbconnection.php");
      $sql = mysqli_query($con,"SELECT * from tbl_genre ORDER BY genre_name ASC");
      while ($row = mysqli_fetch_assoc($sql)) {
        ?>

              <section id="content">
                  <section class="vbox">
                     <section class="w-f-md">
                        <section class="hbox stretch dker" style="background-color: rgba(119,117,117,0.23);">
                           <!-- side content --> 
                           <aside class="col-sm-5 no-padder" id="sidebar">
                              <section class="vbox animated fadeInUp">

                                <div class="center bg-dark dker" id="tbl_genres"></div>


                              </section>
                                 </aside>
                               </section>
                             </section>
                           </section>
                         </section>


        <?php
      }
     ?>