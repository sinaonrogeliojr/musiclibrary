<?php  
include_once("../dbconnection.php");

$genre = mysqli_real_escape_string($con, $_POST['genres']);
$playlist2 = mysqli_real_escape_string($con, $_POST['playlist2']);


if($playlist2 == 'Playlist'){

  if($genre === 'All'){
      $query = "SELECT t1.*,t2.`genre_name` FROM tbl_audios t1 LEFT JOIN tbl_genre t2 on t1.`genre` = t2.`gen_id` where t1.`active` = 1 order by song_name ASC";
    }else{
      $query = "SELECT t1.*,t2.`genre_name` FROM tbl_audios t1 LEFT JOIN tbl_genre t2 on t1.`genre` = t2.`gen_id` where t1.`active` = 1 and genre_name = '$genre' order by song_name ASC";
   }   

}else{

  if($genre === 'All'){
      $query = "SELECT t1.*,t2.`genre_name` FROM tbl_audios t1 LEFT JOIN tbl_genre t2 on t1.`genre` = t2.`gen_id` where t1.`active` = 1 order by song_name ASC";
    }else{
      $query = "SELECT t1.*,t2.`genre_name` FROM tbl_audios t1 LEFT JOIN tbl_genre t2 on t1.`genre` = t2.`gen_id` where t1.`active` = 1 and genre_name = '$genre' order by song_name ASC";
   }

  /*if($genre === 'All'){
    $query = "SELECT t1.*, t2.`playlist_id` FROM tbl_audios t1
    LEFT JOIN tbl_playlist_info t2 ON t1.`id` = t2.`audio_id`
    WHERE t2.`playlist_id` != '$playlist2' OR t2.`playlist_id` IS NULL GROUP BY t1.`id`";
    }else{
      $query = "SELECT t2.* FROM tbl_playlist_info t1 
      LEFT JOIN tbl_audios t2 ON t1.`audio_id` = t2.`id` 
      LEFT JOIN tbl_playlist t3 ON t1.`playlist_id` = t3.`id`
      WHERE t1.`playlist_id` != '$playlist2' GROUP BY t2.`id` ";
   }   */

}
  

$result = mysqli_query($con, $query);
if(mysqli_num_rows($result) > 0)
{
     while($row = mysqli_fetch_array($result))
     {

      $id = $row["audio_id"];
    ?>
      <tr  class="table-default">
        <td >      
          <input type="hidden" id="p_id" value="<?php echo $id; ?>">
            <?php echo $row["id"]; ?>     
        </td>
        <td>
            <?php echo $row["composer"]; ?> 
        </td>
        <td>
            <?php echo $row["artist"]; ?> 
        </td>
        <td>
            <?php echo $row["genre_name"]; ?> 
        </td>
        <td>
            <?php echo date('M d, Y', strtotime($row['upload_date'])); ?> 
        </td>
        <td>
            <button class="btn btn-info" name="pick" id="pick" onclick="save_song2('<?php echo $row['id'] ?>');"><span class="fa fa-plus"></span> Add</button>
        </td>
      </tr>

    <?php  
     }
}
else
{
?>
	<td colspan="6" class="text-center"><div class="alert alert-default alert-dismissible" >
	  <strong class="text-danger">No Data Found.</strong>
	</div>
	</td>
<?php  
}
?>